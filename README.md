# Knights of Rendlion board game ![](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2020-2021/18-knights-of-rendlion-board-game/-/raw/master/images/icons/knight64.png)

- Knights of Rendlion is a 2 player board game. The aim of the game is to destroy enemy pieces or castles.

## Programming Language 

- C++ using Qt Library

## Build :hammer:

### Requirements
- g++ (recommended version: 9.3.0, working with 8.1.0 and up) 
- [CMake](https://cmake.org/) (recommended version: 3.16.3)
- [Qt5](https://doc.qt.io/qt-5.12/index.html)
- Make tool

### Build with Qt Creator
- Download and install [Qt Creator](https://www.qt.io/download)
- Clone the repository from terminal using the command:  
 ```   git clone https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2020-2021/18-knights-of-rendlion-board-game ```
- Configure project: open Qt Creator and  CMakeLists.txt file located in Knights-of-Rendlion repository and build project by clicking the green play button.

### Build from terminal

Position at the directory where the project source code is and execute next commands in terminal :

```
mkdir ../build-18-knights-of-rendlion-board-game
cd ../build-18-knights-of-rendlion-board-game
cmake ../18-knights-of-rendlion-board-game/
make
./knights-of-rendlion-board-game
```






## Commands :keyboard:

- Use mouse click
- <kbd>Q</kbd> - move 
- <kbd>W</kbd> - attack
- <kbd>Space</kbd> - click when choosing a card


## Developers

- [Aleksandar Mladenović, 464/2018](https://gitlab.com/alexein777)
- [Vera Milosavljević, 471/2018](https://gitlab.com/vecavera)
- [Uroš Vučićević, 470/2018](https://gitlab.com/vucicevic)
- [Nela Todorović, 476/2017](https://gitlab.com/nelat)
- [Marina Caran, 495/2017](https://gitlab.com/marina495)


