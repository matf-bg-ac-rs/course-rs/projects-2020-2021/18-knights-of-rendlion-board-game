#ifndef GLOBALS_HPP
#define GLOBALS_HPP

#include "./src/engine/gameController.hpp"

namespace globals{

     extern GameController::Part GAME_PART;
     extern GameController::Actions ACTION;


};

#endif // GLOBALS_HPP
