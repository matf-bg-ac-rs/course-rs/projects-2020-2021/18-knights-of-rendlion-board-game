#ifndef TARGETDAMAGEBONUS_HPP
#define TARGETDAMAGEBONUS_HPP

#include "piecevisitor.hpp"
#include "../pieces/piece.hpp"


class TargetDamageBonus : public PieceVisitor
{
public:
    TargetDamageBonus();
    TargetDamageBonus(Piece* attacker, Piece* target_to_visit);

    int damage_bonus_percent() const;
    Piece* attacker() const;
    operator int() const;

    void visit_archer(Archer* archer) override;
    void visit_castle(Castle* castle) override;
    void visit_catapult(Catapult* catapult) override;
    void visit_horseman(Horseman* horseman) override;
    void visit_knight(Knight* knight) override;
    void visit_mage(Mage* mage) override;
    void visit_priest(Priest* priest) override;
    void visit_shieldman(Shieldman* shieldman) override;
    void visit_spearman(Spearman* spearman) override;

private:
    int m_damage_bonus_percent;
    Piece* m_attacker;
};

#endif // TARGETDAMAGEBONUS_HPP
