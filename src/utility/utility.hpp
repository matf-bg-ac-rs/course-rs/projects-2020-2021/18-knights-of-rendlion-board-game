#ifndef UTILITY_HPP
#define UTILITY_HPP

#include <iostream>
#include <string>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <random>

// #define __PIECE_TEST_MOVE_INF__ 1
// #define __PIECE_TEST_ATTACK_INF__ 1
//#define __PIECE_TEST_ATTACKS_REMAINING__ 1
//#define __BOARD_TEST_INDEX_RANGE__ 1
//#define __LOG__ 1
//#define __TEST_ATTACK_INF__ 1
//#define __TEST_MOVE_INF__ 1
//#define __TEST_PLAYER_TURN_INF__ 1

static std::random_device dev;
static std::mt19937 rng(dev());

template <typename T>
std::string bonus_info_check_sign(T bonus) {
    std::stringstream info_stream;
    info_stream << std::fixed << std::setprecision(2);

    if (bonus >= static_cast<T>(0)) {
        info_stream << "+" << bonus;
    } else {
        info_stream << "-" << (-bonus);
    }

    return info_stream.str();
}

// IMPORTANT: NOT SAFE. USE IT ONLY IF YOU ARE SURE THAT COLLECTION IS NOT EMPTY.
template <typename TK, typename T = typename TK::value_type>
T random_element(const TK& collection) {
    int _size = static_cast<int>(std::size(collection) - 1);
    int upper_bound = std::max(_size, 0);
    std::uniform_int_distribution<std::mt19937::result_type> unif(0, upper_bound);
    int rnd_dist = unif(rng);
    auto it = collection.begin();
    std::advance(it, rnd_dist);

    // NOTE. If collection is empty, it can point to invalid memory location.
    return *it;
}

template <typename TK>
auto random_element_it(const TK& collection) -> decltype(std::begin(collection)) {
    int _size = static_cast<int>(std::size(collection) - 1);
    int upper_bound = std::max(_size, 0);
    std::uniform_int_distribution<std::mt19937::result_type> unif(0, upper_bound);
    int rnd_dist = unif(rng);
    auto it = collection.begin();
    std::advance(it, rnd_dist);
    return it;
}

inline double random_v() {
    std::uniform_real_distribution<double> unif(0, 1);
    double rnd_double = unif(rng);
    return rnd_double;
}

constexpr unsigned int str2int(const char* str, int h = 0)
{
    return !str[h] ? 5381 : (str2int(str, h+1) * 33) ^ str[h];
}

#endif
