#include "targetdamagebonus.hpp"

TargetDamageBonus::TargetDamageBonus()
   : m_damage_bonus_percent(0), m_attacker(nullptr)
{}

TargetDamageBonus::TargetDamageBonus(Piece* attacker, Piece* target_to_visit)
   : m_damage_bonus_percent(0), m_attacker(attacker)
{
   target_to_visit->_accept_visit(*this);
}

int TargetDamageBonus::damage_bonus_percent() const
{
    return m_damage_bonus_percent;
}

Piece* TargetDamageBonus::attacker() const
{
    return m_attacker;
}

TargetDamageBonus::operator int() const
{
    return m_damage_bonus_percent;
}

void TargetDamageBonus::visit_archer(Archer* archer)
{
   m_damage_bonus_percent = m_attacker->get_target_damage_bonus(Piece::Type::ARCHER);
}

void TargetDamageBonus::visit_castle(Castle* castle)
{
   m_damage_bonus_percent = m_attacker->get_target_damage_bonus(Piece::Type::CASTLE);
}

void TargetDamageBonus::visit_catapult(Catapult* catapult)
{
   m_damage_bonus_percent = m_attacker->get_target_damage_bonus(Piece::Type::CATAPULT);
}
void TargetDamageBonus::visit_horseman(Horseman* horseman)
{
   m_damage_bonus_percent = m_attacker->get_target_damage_bonus(Piece::Type::HORSEMAN);
}

void TargetDamageBonus::visit_knight(Knight* knight)
{
   m_damage_bonus_percent = m_attacker->get_target_damage_bonus(Piece::Type::KNIGHT);
}

void TargetDamageBonus::visit_mage(Mage* mage)
{
   m_damage_bonus_percent = m_attacker->get_target_damage_bonus(Piece::Type::MAGE);
}

void TargetDamageBonus::visit_priest(Priest* priest)
{
   m_damage_bonus_percent = m_attacker->get_target_damage_bonus(Piece::Type::PRIEST);
}

void TargetDamageBonus::visit_shieldman(Shieldman* shieldman)
{
   m_damage_bonus_percent = m_attacker->get_target_damage_bonus(Piece::Type::SHIELDMAN);
}

void TargetDamageBonus::visit_spearman(Spearman* spearman)
{
   m_damage_bonus_percent = m_attacker->get_target_damage_bonus(Piece::Type::SPEARMAN);
}
