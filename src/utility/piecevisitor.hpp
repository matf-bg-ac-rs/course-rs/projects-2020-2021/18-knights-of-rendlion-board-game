#ifndef PIECEVISITOR_HPP
#define PIECEVISITOR_HPP

//#include "../pieces/archer.hpp"
//#include "../pieces/castle.hpp"
//#include "../pieces/catapult.hpp"
//#include "../pieces/horseman.hpp"
//#include "../pieces/knight.hpp"
//#include "../pieces/mage.hpp"
//#include "../pieces/priest.hpp"
//#include "../pieces/shieldman.hpp"
//#include "../pieces/spearman.hpp"

class Archer;
class Castle;
class Catapult;
class Horseman;
class Knight;
class Mage;
class Priest;
class Shieldman;
class Spearman;

class PieceVisitor
{
public:
    virtual void visit_archer(Archer* archer) = 0;
    virtual void visit_castle(Castle* castle) = 0;
    virtual void visit_catapult(Catapult* catapult) = 0;
    virtual void visit_horseman(Horseman* horseman) = 0;
    virtual void visit_knight(Knight* knight) = 0;
    virtual void visit_mage(Mage* mage) = 0;
    virtual void visit_priest(Priest* priest) = 0;
    virtual void visit_shieldman(Shieldman* shieldman) = 0;
    virtual void visit_spearman(Spearman* spearman) = 0;
};

#endif // PIECEVISITOR_HPP
