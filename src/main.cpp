#include <QApplication>
#include <QIcon>

#include "./engine/gameController.hpp"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    app.setWindowIcon(QIcon(":/images/icons/knight64.png"));

    GameController* game_contoller = GameController::create_gameController();
    game_contoller->show();

    return app.exec();
}
