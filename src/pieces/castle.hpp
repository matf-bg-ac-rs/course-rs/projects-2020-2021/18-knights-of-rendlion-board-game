#ifndef CASTLE_HPP
#define CASTLE_HPP

#include <map>
#include <string>
#include <QString>
#include "piece.hpp"
#include "../utility/piecevisitor.hpp"


class Castle: public Piece
{
public:
    enum class Default {
        HEALTH = 100,
        DAMAGE = 0,
        MAX_ORTHO_MOVES = 0,
        MAX_DIAG_MOVES = 0,
        MAX_ATTACKS = 0,
        MAX_ORTHO_TARGET_RANGE = 0,
        MAX_DIAG_TARGET_RANGE = 0
    };

    static inline const std::string IMAGE_RED_PATH = ":/images/pieces/castle_red.png";
    static inline const QString IMAGE_RED_QPATH = QString(":/images/pieces/castle_red.png");
    static inline const std::string IMAGE_BLUE_PATH = ":/images/pieces/castle_blue.png";
    static inline const QString IMAGE_BLUE_QPATH = QString(":/images/pieces/castle_blue.png");

    static std::string default_info();
    static QString default_Qinfo();
    static std::map<QString,QString> default_values();

    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
    const QRect get_rect() const override;

    Castle();
    Castle(Player::Index owner, std::pair<unsigned, unsigned> board_index);

    unsigned get_default_health() const override;
    unsigned get_default_damage() const override;
    unsigned get_max_attacks() const override;
    unsigned get_max_ortho_moves() const override;
    unsigned get_max_diag_moves() const override;
    unsigned get_max_ortho_target_range() const override;
    unsigned get_max_diag_target_range() const override;
    unsigned get_max_ortho_spells_range(int spell_index) const override;
    unsigned get_max_diag_spells_range(int spell_index) const override;
    bool get_use_g_fields(int spell_index) const override;
    int get_target_damage_bonus(Piece::Type piece_type) const override;
    
    std::pair<int, int> get_ground_bonuses() const override;
    std::pair<int, int> get_plain_bonuses() const override;
    std::pair<int, int> get_hill_bonuses() const override;
    std::pair<int, int> get_forest_bonuses() const override;
    std::pair<int, int> get_bridge_bonuses() const override;

    std::string cooldowns_info() const override;
    QString cooldowns_Qinfo() const override;
    std::vector<QString> spell_names() const override;
    std::vector<unsigned> spell_costs() const override;
    void _accept_visit(PieceVisitor& visitor) override;

private:
    void init_default_values() override;
};

#endif // CASTLE_HPP
