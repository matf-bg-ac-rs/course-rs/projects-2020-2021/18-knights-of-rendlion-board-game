#ifndef ARCHER_HPP
#define ARCHER_HPP

#include <map>
#include <string>
#include <QString>
#include "piece.hpp"
#include "../utility/piecevisitor.hpp"

//class Piece;

class Archer : public Piece
{
public:
    enum class Spell {
        STEADY_AIM
    };

    enum class SpellCost {
        STEADY_AIM = 3
    };

    enum class SpellCooldown {
        STEADY_AIM = 2
    };

    enum class SpellRange {
        STEADY_AIM_ORTHO = 2,
        STEADY_AIM_DIAG = 1,
        STEADY_AIM_USE_G_FIELDS = 1
    };

    enum class SpellData {
        STEADY_AIM_SELF_STUN_DURATION = 1,
        STEADY_AIM_DMG_BONUS = 100,
    };

    enum class Default {
        HEALTH = 70,
        DAMAGE = 45,
        MAX_ORTHO_MOVES = 2,
        MAX_DIAG_MOVES = 0,
        MAX_ATTACKS = 1,
        MAX_ORTHO_TARGET_RANGE = 2,
        MAX_DIAG_TARGET_RANGE = 1
    };

    enum class FieldBonus {
        GROUND_HEALTH = 0,
        GROUND_DAMAGE = 0,
        PLAIN_HEALTH = -20,
        PLAIN_DAMAGE = -15,
        HILL_HEALTH = 20,
        HILL_DAMAGE = 30,
        FOREST_HEALTH = 30,
        FOREST_DAMAGE = 40,
        BRIDGE_HEALTH = -30,
        BRIDGE_DAMAGE = -10,
    };

    enum class TargetBonus {
        KNIGHT_DAMAGE = 10,
        SPEARMAN_DAMAGE = 15,
        SHIELDMAN_DAMAGE = -30,
        HORSEMAN_DAMAGE = -15,
        ARCHER_DAMAGE = 0,
        CATAPULT_DAMAGE = 10,
        MAGE_DAMAGE = 10,
        PRIEST_DAMAGE = 0,
        CASTLE_DAMAGE = -50,
    };

    static inline const std::string IMAGE_RED_PATH =":/images/pieces/archer_red_new.png";
    static inline const QString IMAGE_RED_QPATH = QString(":/images/pieces/archer_red_new.png");
    static inline const std::string IMAGE_BLUE_PATH =":/images/pieces/archer_blue_new.png";
    static inline const QString IMAGE_BLUE_QPATH = QString(":/images/pieces/archer_blue_new.png");

    static std::string default_info();
    static QString default_Qinfo();
    static std::vector<std::map<QString, QString>> spells_info();
    static std::string field_bonus_info();
    static QString field_bonus_Qinfo();
    static std::string target_bonus_info();
    static QString target_bonus_Qinfo();
    static std::map<QString,QString> default_values();


    static std::pair<int, int> ground_bonuses();
    static std::pair<int, int> plain_bonuses();
    static std::pair<int, int> hill_bonuses();
    static std::pair<int, int> forest_bonuses();
    static std::pair<int, int> bridge_bonuses();

    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
    const QRect get_rect() const override;

    Archer();
    Archer(Player::Index owner, std::pair<unsigned, unsigned> board_index);

    const std::map<Spell, int>& cooldowns() const;

    unsigned get_default_health() const override;
    unsigned get_default_damage() const override;
    unsigned get_max_attacks() const override;
    unsigned get_max_ortho_moves() const override;
    unsigned get_max_diag_moves() const override;
    unsigned get_max_ortho_target_range() const override;
    unsigned get_max_diag_target_range() const override;
    unsigned get_max_ortho_spells_range(int spell_index) const override;
    unsigned get_max_diag_spells_range(int spell_index) const override;
    bool get_use_g_fields(int spell_index) const override;
    int get_target_damage_bonus(Piece::Type piece_type) const override;
    std::pair<int, int> get_ground_bonuses() const override;
    std::pair<int, int> get_plain_bonuses() const override;
    std::pair<int, int> get_hill_bonuses() const override;
    std::pair<int, int> get_forest_bonuses() const override;
    std::pair<int, int> get_bridge_bonuses() const override;

    std::string cooldowns_info() const override;
    QString cooldowns_Qinfo() const override;
    std::vector<QString> spell_names() const override;
    std::vector<unsigned> spell_costs() const override;
    void _accept_visit(PieceVisitor& visitor) override;

    std::string spell_to_string(Spell spell) const;
    QString spell_to_Qstring(Spell spell) const;
    unsigned get_spell_cooldown(Spell spell) const;
    unsigned get_spell_cooldown_cost(Spell spell) const;
    std::vector<Spell> get_spells() const;


private:
    void init_default_values() override;
};

#endif // ARCHER_HPP
