#include "mage.hpp"

std::string Mage::default_info()
{
    std::stringstream info_stream;

    info_stream << "Mage" << "\n----------------------------";
    info_stream << "\nHealth: " << static_cast<unsigned>(Mage::Default::HEALTH);
    info_stream << "\nDamage: " << static_cast<unsigned>(Mage::Default::DAMAGE);
    info_stream << "\nMax attacks: " << static_cast<unsigned>(Mage::Default::MAX_ATTACKS);
    info_stream << "\nMax ortho moves: " << static_cast<unsigned>(Mage::Default::MAX_ORTHO_MOVES);
    info_stream << "\nMax diag moves: " << static_cast<unsigned>(Mage::Default::MAX_DIAG_MOVES);
    info_stream << "\nMax ortho target range: " << static_cast<unsigned>(Mage::Default::MAX_ORTHO_TARGET_RANGE);
    info_stream << "\nMax diag target range: " << static_cast<unsigned>(Mage::Default::MAX_DIAG_TARGET_RANGE);
    info_stream << "\n";

    return info_stream.str();
}

QString Mage::default_Qinfo()
{
    return QString(default_info().data());
}

std::vector<std::map<QString, QString>> Mage::spells_info(){

    std::vector<std::map<QString, QString>> spell_map_vec;
    std::map<QString, QString> spell_map;
    QString spell_info_str;

    spell_info_str += QString("Casts firebolt at the enemy target, dealing ");
    spell_info_str += QString::number(static_cast<unsigned>(SpellData::FIREBOLT_DMG)) + QString(", and ");
    spell_info_str += (QString::number(static_cast<unsigned>(SpellData::FIREBOLT_DOT_DMG)) + QString(" additional damage over 3 turns.\n"));
    spell_info_str += (QString("* Cooldown: ") + QString::number(static_cast<int>(SpellCooldown::FIREBOLT)) + QString("\n"));
    spell_info_str += (QString("* Cost: ") +  QString::number(static_cast<int>(SpellCost::FIREBOLT)));
    spell_info_str += QString("\n");

    spell_map[QString("FIREBOLT")]=spell_info_str;
    spell_map_vec.push_back(spell_map);


    spell_info_str = QString("");
    spell_info_str += QString("Casts frostbolt at the enemy target, dealing ");
    spell_info_str += (QString::number(static_cast<unsigned>(SpellData::FROSTBOLT_DMG)) + QString(" damage "));
    spell_info_str += QString("and freezing enemy, making him immovable for ");
    spell_info_str += (QString::number(static_cast<unsigned>(SpellData::FROSTBOLT_FREEZE_DURATION)) + QString(" turns.\n"));
    spell_info_str += (QString("* Cooldown: ") + QString::number(static_cast<int>(SpellCooldown::FROSTBOLT)) + QString("\n"));
    spell_info_str += (QString("* Cost: ") +  QString::number(static_cast<int>(SpellCost::FROSTBOLT)));
    spell_info_str += QString("\n");

    spell_map.clear();
    spell_map[QString("FROSTBOLT")]=spell_info_str;
    spell_map_vec.push_back(spell_map);


    spell_info_str = QString("");
    spell_info_str += QString("Casts arcanebolt at the enemy target, dealing 40 damage ");
    spell_info_str += QString("and pushing enemy 2 tiles back. If one of the tiles is not standable, target dies.\n");
    spell_info_str += (QString("* Cooldown: ") + QString::number(static_cast<int>(SpellCooldown::ARCANEBOLT)) + QString("\n"));
    spell_info_str += (QString("* Cost: ") +  QString::number(static_cast<int>(SpellCost::ARCANEBOLT)));
    spell_info_str += QString("\n");

    spell_map.clear();
    spell_map[QString("ARCANEBOLT")]=spell_info_str;
    spell_map_vec.push_back(spell_map);

    spell_info_str = QString("");
    spell_info_str += QString("Releases deadly shockwave around mage, dealing ");
    spell_info_str += (QString::number(static_cast<unsigned>(SpellData::SHOCKWAVE_DMG)) + QString(" damage to all pieces. "));
    spell_info_str += QString("Deals damage to friendly pieces as well.\n");
    spell_info_str += (QString("* Cooldown: ") + QString::number(static_cast<int>(SpellCooldown::SHOCKWAVE)) + QString("\n"));
    spell_info_str += (QString("* Cost: ") +  QString::number(static_cast<int>(SpellCost::SHOCKWAVE)));
    spell_info_str += QString("\n");

    spell_map.clear();
    spell_map[QString("SHOCKWAVE")]=spell_info_str;
    spell_map_vec.push_back(spell_map);

    return spell_map_vec;
}


std::string Mage::field_bonus_info()
{
    std::stringstream info_stream;

    info_stream << "Field bonus" << "\n----------------------------------\n";
    info_stream << "* Ground: ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::GROUND_HEALTH)) << "% HP, ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::GROUND_DAMAGE)) << "% DMG\n";
    info_stream << "* Plain: ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::PLAIN_HEALTH)) << "% HP, ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::PLAIN_DAMAGE)) << "% DMG\n";
    info_stream << "* Hill: ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::HILL_HEALTH)) << "% HP, ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::HILL_DAMAGE)) << "% DMG\n";
    info_stream << "* Forest: ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::FOREST_HEALTH)) << "% HP, ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::FOREST_DAMAGE)) << "% DMG\n";
    info_stream << "* Bridge: ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::BRIDGE_HEALTH)) << "% HP, ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::BRIDGE_DAMAGE)) << "% DMG";
    info_stream << "\n";

    return info_stream.str();
};

QString Mage::field_bonus_Qinfo()
{
    return QString(field_bonus_info().data());
}

std::string Mage::target_bonus_info()
{
    std::stringstream info_stream;

    info_stream << "Target bonus" << "\n----------------------------------\n";
    info_stream << "* Archer: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::ARCHER_DAMAGE)) << "% DMG\n";
    info_stream << "* Castle: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::CASTLE_DAMAGE)) << "% DMG\n";
    info_stream << "* Catapult: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::CATAPULT_DAMAGE)) << "% DMG\n";
    info_stream << "* Horseman: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::HORSEMAN_DAMAGE)) << "% DMG\n";
    info_stream << "* Knight: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::KNIGHT_DAMAGE)) << "% DMG\n";
    info_stream << "* Mage: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::MAGE_DAMAGE)) << "% DMG\n";
    info_stream << "* Priest: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::PRIEST_DAMAGE)) << "% DMG\n";
    info_stream << "* Shieldman: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::SHIELDMAN_DAMAGE)) << "% DMG\n";
    info_stream << "* Spearman: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::SPEARMAN_DAMAGE)) << "% DMG";
    info_stream << "\n";

    return info_stream.str();
}

QString Mage::target_bonus_Qinfo()
{
    return QString(target_bonus_info().data());
}

std::pair<int, int> Mage::ground_bonuses()
{
    return std::make_pair(
        static_cast<int>(Mage::FieldBonus::GROUND_HEALTH),
        static_cast<int>(Mage::FieldBonus::GROUND_DAMAGE)
    );
}

std::pair<int, int> Mage::plain_bonuses()
{
    return std::make_pair(
        static_cast<int>(Mage::FieldBonus::PLAIN_HEALTH),
        static_cast<int>(Mage::FieldBonus::PLAIN_DAMAGE)
    );
}

std::pair<int, int> Mage::hill_bonuses()
{
    return std::make_pair(
        static_cast<int>(Mage::FieldBonus::HILL_HEALTH),
        static_cast<int>(Mage::FieldBonus::HILL_DAMAGE)
    );
}

std::pair<int, int> Mage::forest_bonuses()
{
    return std::make_pair(
        static_cast<int>(Mage::FieldBonus::FOREST_HEALTH),
        static_cast<int>(Mage::FieldBonus::FOREST_DAMAGE)
    );
}

std::pair<int, int> Mage::bridge_bonuses()
{
    return std::make_pair(
        static_cast<int>(Mage::FieldBonus::BRIDGE_HEALTH),
        static_cast<int>(Mage::FieldBonus::BRIDGE_DAMAGE)
    );
}

QRectF Mage::boundingRect() const
{
    return QRectF(0,0,40,40);
}

const QRect Mage::get_rect() const{
    return QRect(0,0,40,40);
}

void Mage::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);
    if(Player::Index::PLAYER_1 == m_owner )
        painter->drawPixmap(get_rect(),QPixmap(Mage::IMAGE_RED_QPATH));
    else
        painter->drawPixmap(get_rect(),QPixmap(Mage::IMAGE_BLUE_QPATH));
}

Mage::Mage(Player::Index owner, std::pair<unsigned, unsigned> board_index)
    : Piece(owner, board_index)
{
    init_default_values();
}

unsigned Mage::get_default_health() const 
{
    return static_cast<unsigned>(Mage::Default::HEALTH);
}

unsigned Mage::get_default_damage() const 
{
    return static_cast<unsigned>(Mage::Default::DAMAGE);
}

unsigned Mage::get_max_attacks() const
{
    return static_cast<unsigned>(Mage::Default::MAX_ATTACKS);
}

unsigned Mage::get_max_ortho_moves() const
{
    return static_cast<unsigned>(Mage::Default::MAX_ORTHO_MOVES);
}

unsigned Mage::get_max_diag_moves() const
{
    return static_cast<unsigned>(Mage::Default::MAX_DIAG_MOVES);
}

unsigned Mage::get_max_ortho_target_range() const
{
    return static_cast<unsigned>(Mage::Default::MAX_ORTHO_TARGET_RANGE);
}

unsigned Mage::get_max_diag_target_range() const
{
    return static_cast<unsigned>(Mage::Default::MAX_DIAG_TARGET_RANGE);
}

unsigned Mage::get_max_ortho_spells_range(int spell_index) const
{
    switch(spell_index){
    case 1:
        return static_cast<unsigned>(Mage::SpellRange::FIREBOLT_ORTHO);
        break;
    case 2:
        return static_cast<unsigned>(Mage::SpellRange::FROSTBOLT_ORTHO);
        break;
    case 3:
        return static_cast<unsigned>(Mage::SpellRange::ARCANEBOLT_ORTHO);
        break;
    case 4:
        return static_cast<unsigned>(Mage::SpellRange::SHOCKWAVE_ORTHO);
        break;
    default:
        throw "UNKNOWN_SPELL";
    }

}

unsigned Mage::get_max_diag_spells_range(int spell_index) const
{
    switch(spell_index){
    case 1:
        return static_cast<unsigned>(Mage::SpellRange::FIREBOLT_DIAG);
        break;
    case 2:
        return static_cast<unsigned>(Mage::SpellRange::FROSTBOLT_DIAG);
        break;
    case 3:
        return static_cast<unsigned>(Mage::SpellRange::ARCANEBOLT_DIAG);
        break;
    case 4:
        return static_cast<unsigned>(Mage::SpellRange::SHOCKWAVE_DIAG);
        break;
    default:
        throw "UNKNOWN_SPELL";
    }
}

bool Mage::get_use_g_fields(int spell_index) const
{
    switch(spell_index){
    case 1:
        return static_cast<bool>(Mage::SpellRange::FIREBOLT_USE_G_FIELDS);
        break;
    case 2:
        return static_cast<bool>(Mage::SpellRange::FROSTBOLT_USE_G_FIELDS);
        break;
    case 3:
        return static_cast<bool>(Mage::SpellRange::ARCANEBOLT_USE_G_FIELDS);
        break;
    case 4:
        return static_cast<bool>(Mage::SpellRange::SHOCKWAVE_USE_G_FIELDS);
        break;
    default:
        throw "UNKNOWN_SPELL";
    }
}

int Mage::get_target_damage_bonus(Piece::Type piece_type) const
{
   switch (piece_type) {
       case Piece::Type::ARCHER:
           return static_cast<int>(Mage::TargetBonus::ARCHER_DAMAGE);
       case Piece::Type::CASTLE:
           return static_cast<int>(Mage::TargetBonus::CASTLE_DAMAGE);
       case Piece::Type::CATAPULT:
           return static_cast<int>(Mage::TargetBonus::CATAPULT_DAMAGE);
       case Piece::Type::HORSEMAN:
           return static_cast<int>(Mage::TargetBonus::HORSEMAN_DAMAGE);
       case Piece::Type::KNIGHT:
           return static_cast<int>(Mage::TargetBonus::KNIGHT_DAMAGE);
       case Piece::Type::MAGE:
           return static_cast<int>(Mage::TargetBonus::MAGE_DAMAGE);
       case Piece::Type::PRIEST:
           return static_cast<int>(Mage::TargetBonus::PRIEST_DAMAGE);
       case Piece::Type::SHIELDMAN:
           return static_cast<int>(Mage::TargetBonus::SHIELDMAN_DAMAGE);
       case Piece::Type::SPEARMAN:
           return static_cast<int>(Mage::TargetBonus::SPEARMAN_DAMAGE);
       default:
           throw UNKNOWN_CLASS_ERROR;
   }
}

std::pair<int, int> Mage::get_ground_bonuses() const
{
    return std::make_pair(
        static_cast<int>(Mage::FieldBonus::GROUND_HEALTH),
        static_cast<int>(Mage::FieldBonus::GROUND_DAMAGE)
    );
}

std::pair<int, int> Mage::get_plain_bonuses() const
{
    return std::make_pair(
        static_cast<int>(Mage::FieldBonus::PLAIN_HEALTH),
        static_cast<int>(Mage::FieldBonus::PLAIN_DAMAGE)
    );
}

std::pair<int, int> Mage::get_hill_bonuses() const
{
    return std::make_pair(
        static_cast<int>(Mage::FieldBonus::HILL_HEALTH),
        static_cast<int>(Mage::FieldBonus::HILL_DAMAGE)
    );
}

std::pair<int, int> Mage::get_forest_bonuses() const
{
    return std::make_pair(
        static_cast<int>(Mage::FieldBonus::FOREST_HEALTH),
        static_cast<int>(Mage::FieldBonus::FOREST_DAMAGE)
    );
}

std::pair<int, int> Mage::get_bridge_bonuses() const
{
    return std::make_pair(
        static_cast<int>(Mage::FieldBonus::BRIDGE_HEALTH),
        static_cast<int>(Mage::FieldBonus::BRIDGE_DAMAGE)
    );
}

std::string Mage::cooldowns_info() const
{
    std::stringstream info_stream;
    info_stream << "Cooldowns" << "\n-----------------------------\n";

    if (has_cooldowns()) {
        for (auto& [spell, cooldown] : m_cooldowns) {
            info_stream << "* " << spell << ": ";
            info_stream << std::to_string(cooldown) << "\n";
        }
    } else {
        info_stream << "No cooldowns.";
    }

    return info_stream.str();
}

QString Mage::cooldowns_Qinfo() const
{
    return QString(cooldowns_info().data());
}

std::vector<QString> Mage::spell_names() const
{
    return {
        spell_to_Qstring(Mage::Spell::FIREBOLT),
        spell_to_Qstring(Mage::Spell::FROSTBOLT),
        spell_to_Qstring(Mage::Spell::ARCANEBOLT),
        spell_to_Qstring(Mage::Spell::SHOCKWAVE)
    };
}

std::vector<unsigned> Mage::spell_costs() const
{
    std::vector<unsigned> costs;
    costs.push_back(static_cast<unsigned>(SpellCost::FIREBOLT));
    costs.push_back(static_cast<unsigned>(SpellCost::FROSTBOLT));
    costs.push_back(static_cast<unsigned>(SpellCost::ARCANEBOLT));
    costs.push_back(static_cast<unsigned>(SpellCost::SHOCKWAVE));
    return costs;
}

void Mage::_accept_visit(PieceVisitor& visitor)
{
    visitor.visit_mage(this);
}

std::map<QString, QString> Mage::default_values()
{
    std::map<QString,QString> default_values;
    default_values[QString("HEALTH")]=QString::number(static_cast<int>(Default::HEALTH));
    default_values[QString("DAMAGE")] = QString::number(static_cast<int>(Default::DAMAGE));
    default_values[QString("MAX_ORTHO_MOVES")] = QString::number(static_cast<int>(Default::MAX_ORTHO_MOVES));
    default_values[QString("MAX_DIAG_MOVES")] = QString::number(static_cast<int>(Default::MAX_DIAG_MOVES));
    default_values[QString("MAX_ATTACKS")] = QString::number(static_cast<int>(Default::MAX_ATTACKS));
    default_values[QString("MAX_ORTHO_TARGET_RANGE")] = QString::number(static_cast<int>(Default::MAX_ORTHO_TARGET_RANGE));
    default_values[QString("MAX_DIAG_TARGET_RANGE")] = QString::number(static_cast<int>(Default::MAX_DIAG_TARGET_RANGE));

    default_values[QString("GROUND_HEALTH")] = QString::number(static_cast<int>(FieldBonus::GROUND_HEALTH));
    default_values[QString("GROUND_DAMAGE")] = QString::number(static_cast<int>(FieldBonus::GROUND_DAMAGE));
    default_values[QString("PLAIN_HEALTH")] = QString::number(static_cast<int>(FieldBonus::PLAIN_HEALTH));
    default_values[QString("PLAIN_DAMAGE")] = QString::number(static_cast<int>(FieldBonus::PLAIN_DAMAGE));
    default_values[QString("FOREST_HEALTH")] = QString::number(static_cast<int>(FieldBonus::FOREST_HEALTH));
    default_values[QString("FOREST_DAMAGE")] = QString::number(static_cast<int>(FieldBonus::FOREST_DAMAGE));
    default_values[QString("BRIDGE_HEALTH")] = QString::number(static_cast<int>(FieldBonus::BRIDGE_HEALTH));
    default_values[QString("BRIDGE_DAMAGE")] = QString::number(static_cast<int>(FieldBonus::BRIDGE_DAMAGE));
    default_values[QString("HILL_HEALTH")] = QString::number(static_cast<int>(FieldBonus::HILL_HEALTH));
    default_values[QString("HILL_DAMAGE")] = QString::number(static_cast<int>(FieldBonus::HILL_DAMAGE));

    default_values[QString("KNIGHT_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::KNIGHT_DAMAGE));
    default_values[QString("SPEARMAN_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::SPEARMAN_DAMAGE));
    default_values[QString("SHIELDMAN_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::SHIELDMAN_DAMAGE));
    default_values[QString("HORSEMAN_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::HORSEMAN_DAMAGE));
    default_values[QString("ARCHER_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::ARCHER_DAMAGE));
    default_values[QString("CATAPULT_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::CATAPULT_DAMAGE));
    default_values[QString("MAGE_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::MAGE_DAMAGE));
    default_values[QString("PRIEST_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::PRIEST_DAMAGE));
    default_values[QString("CASTLE_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::CASTLE_DAMAGE));

    return default_values;

}


std::string Mage::spell_to_string(Mage::Spell spell) const
{
    switch (spell) {
        case Spell::FIREBOLT:
            return "Firebolt";
            break;
        case Spell::FROSTBOLT:
            return "Frostbolt";
            break;
        case Spell::ARCANEBOLT:
            return "Arcanebolt";
            break;
        case Spell::SHOCKWAVE:
            return "Shockwave";
            break;
        default:
            throw SPELL_ERROR; 
    }
}

QString Mage::spell_to_Qstring(Mage::Spell spell) const
{
    return QString(spell_to_string(spell).data());
}

unsigned Mage::get_spell_cooldown(Mage::Spell spell) const
{
    switch (spell) {
        case Mage::Spell::FIREBOLT:
            return static_cast<unsigned>(Mage::SpellCooldown::FIREBOLT);
        case Mage::Spell::FROSTBOLT:
            return static_cast<unsigned>(Mage::SpellCooldown::FROSTBOLT);
        case Mage::Spell::ARCANEBOLT:
            return static_cast<unsigned>(Mage::SpellCooldown::ARCANEBOLT);
        case Mage::Spell::SHOCKWAVE:
            return static_cast<unsigned>(Mage::SpellCooldown::SHOCKWAVE);
        default:
            throw SPELL_ERROR;
    }
}

unsigned Mage::get_spell_cooldown_cost(Mage::Spell spell) const
{
    switch (spell) {
        case Mage::Spell::FIREBOLT:
            return static_cast<unsigned>(Mage::SpellCost::FIREBOLT);
        case Mage::Spell::FROSTBOLT:
            return static_cast<unsigned>(Mage::SpellCost::FROSTBOLT);
        case Mage::Spell::ARCANEBOLT:
            return static_cast<unsigned>(Mage::SpellCost::ARCANEBOLT);
        case Mage::Spell::SHOCKWAVE:
            return static_cast<unsigned>(Mage::SpellCost::SHOCKWAVE);
        default:
            throw SPELL_ERROR;
    }
}

std::vector<Mage::Spell> Mage::get_spells() const
{
    return {
        Mage::Spell::FIREBOLT,
        Mage::Spell::FROSTBOLT,
        Mage::Spell::ARCANEBOLT,
        Mage::Spell::SHOCKWAVE
    };
}

void Mage::init_default_values()
{
    m_type = Piece::Type::MAGE;
    m_health = static_cast<double>(Mage::Default::HEALTH);
    m_damage = static_cast<double>(Mage::Default::DAMAGE);
    m_ortho_moves_remaining = static_cast<int>(Mage::Default::MAX_ORTHO_MOVES);
    m_diag_moves_remaining = static_cast<int>(Mage::Default::MAX_DIAG_MOVES);
    m_attacks_remaining = static_cast<int>(Mage::Default::MAX_ATTACKS);
}

