#include "castle.hpp"


std::string Castle::default_info()
{
    std::stringstream info_stream;

    info_stream << "Castle" << "\n----------------------------";
    info_stream << "\nHealth: " << static_cast<unsigned>(Castle::Default::HEALTH);
    info_stream << "\nDamage: " << static_cast<unsigned>(Castle::Default::DAMAGE);
    info_stream << "\nMax attacks: " << static_cast<unsigned>(Castle::Default::MAX_ATTACKS);
    info_stream << "\nMax ortho moves: " << static_cast<unsigned>(Castle::Default::MAX_ORTHO_MOVES);
    info_stream << "\nMax diag moves: " << static_cast<unsigned>(Castle::Default::MAX_DIAG_MOVES);
    info_stream << "\nMax ortho target range: " << static_cast<unsigned>(Castle::Default::MAX_ORTHO_TARGET_RANGE);
    info_stream << "\nMax diag target range: " << static_cast<unsigned>(Castle::Default::MAX_DIAG_TARGET_RANGE);
    info_stream << "\n";

    return info_stream.str();
}

QString Castle::default_Qinfo()
{
    return QString(default_info().data());
}

QRectF Castle::boundingRect() const
{
    return QRectF(0,0,40,40);
}

const QRect Castle::get_rect() const{
    return QRect(0,0,40,40);
}

void Castle::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);
    if(Player::Index::PLAYER_1 == m_owner)
        painter->drawPixmap(get_rect(),QPixmap(Castle::IMAGE_RED_QPATH));
    else
        painter->drawPixmap(get_rect(),QPixmap(Castle::IMAGE_BLUE_QPATH));
}

Castle::Castle() = default;

Castle::Castle(Player::Index owner, std::pair<unsigned, unsigned> board_index)
    : Piece(owner, board_index)
{
    init_default_values();
}

unsigned Castle::get_default_health() const { return static_cast<unsigned>(Castle::Default::HEALTH); }

// These functions don't need to be implemented since castle has no functions
unsigned Castle::get_default_damage() const { return 0; }
unsigned Castle::get_max_attacks() const { return 0; }
unsigned Castle::get_max_ortho_moves() const { return 0; }
unsigned Castle::get_max_diag_moves() const { return 0; }
unsigned Castle::get_max_ortho_target_range() const { return 0; }
unsigned Castle::get_max_diag_target_range() const { return 0; }
unsigned Castle::get_max_ortho_spells_range(int spell_index) const { return 0; }
unsigned Castle::get_max_diag_spells_range(int spell_index) const { return 0; }
bool Castle::get_use_g_fields(int spell_index) const { return false; }
int Castle::get_target_damage_bonus(Piece::Type piece_type) const { return 0; }
std::pair<int, int> Castle::get_ground_bonuses() const { return std::make_pair(0, 0); }
std::pair<int, int> Castle::get_plain_bonuses() const { return std::make_pair(0, 0); }
std::pair<int, int> Castle::get_hill_bonuses() const { return std::make_pair(0, 0); }
std::pair<int, int> Castle::get_forest_bonuses() const { return std::make_pair(0, 0); }
std::pair<int, int> Castle::get_bridge_bonuses() const { return std::make_pair(0, 0); }

std::string Castle::cooldowns_info() const { return std::string(); }
QString Castle::cooldowns_Qinfo() const { return QString(); }

std::vector<QString> Castle::spell_names() const
{
    return {};
}

std::vector<unsigned> Castle::spell_costs() const
{
    return {};
}

void Castle::_accept_visit(PieceVisitor& visitor)
{
    visitor.visit_castle(this);
}

std::map<QString, QString> Castle::default_values()
{
    std::map<QString,QString> default_values;
    default_values[QString("HEALTH")]=QString::number(static_cast<int>(Default::HEALTH));
    default_values[QString("DAMAGE")] = QString::number(static_cast<int>(Default::DAMAGE));
    default_values[QString("MAX_ORTHO_MOVES")] = QString::number(static_cast<int>(Default::MAX_ORTHO_MOVES));
    default_values[QString("MAX_DIAG_MOVES")] = QString::number(static_cast<int>(Default::MAX_DIAG_MOVES));
    default_values[QString("MAX_ATTACKS")] = QString::number(static_cast<int>(Default::MAX_ATTACKS));
    default_values[QString("MAX_ORTHO_TARGET_RANGE")] = QString::number(static_cast<int>(Default::MAX_ORTHO_TARGET_RANGE));
    default_values[QString("MAX_DIAG_TARGET_RANGE")] = QString::number(static_cast<int>(Default::MAX_DIAG_TARGET_RANGE));

    return default_values;
}


void Castle::init_default_values()
{
    m_type = Piece::Type::CASTLE;
    m_health = static_cast<double>(Castle::Default::HEALTH);
    m_damage = static_cast<double>(Castle::Default::DAMAGE);
    m_ortho_moves_remaining = static_cast<int>(Castle::Default::MAX_ORTHO_MOVES);
    m_diag_moves_remaining = static_cast<int>(Castle::Default::MAX_DIAG_MOVES);
    m_attacks_remaining = static_cast<int>(Castle::Default::MAX_ATTACKS);
}
