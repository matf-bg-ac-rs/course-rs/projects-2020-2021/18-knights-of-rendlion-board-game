#include "horseman.hpp"

std::string Horseman::default_info()
{
    std::stringstream info_stream;

    info_stream << "Horseman" << "\n----------------------------";
    info_stream << "\nHealth: " << static_cast<unsigned>(Horseman::Default::HEALTH);
    info_stream << "\nDamage: " << static_cast<unsigned>(Horseman::Default::DAMAGE);
    info_stream << "\nMax attacks: " << static_cast<unsigned>(Horseman::Default::MAX_ATTACKS);
    info_stream << "\nMax ortho moves: " << static_cast<unsigned>(Horseman::Default::MAX_ORTHO_MOVES);
    info_stream << "\nMax diag moves: " << static_cast<unsigned>(Horseman::Default::MAX_DIAG_MOVES);
    info_stream << "\nMax ortho target range: " << static_cast<unsigned>(Horseman::Default::MAX_ORTHO_TARGET_RANGE);
    info_stream << "\nMax diag target range: " << static_cast<unsigned>(Horseman::Default::MAX_DIAG_TARGET_RANGE);
    info_stream << "\n";

    return info_stream.str();
}

QString Horseman::default_Qinfo()
{
    return QString(default_info().data());
}

std::vector<std::map<QString, QString>> Horseman::spells_info()
{
    std::vector<std::map<QString, QString>> spell_map_vec;
    std::map<QString, QString> spell_map;
    QString spell_info_str;

    spell_info_str += QString("Horseman rushes into the battle, moving up to 5 tiles, ");
    spell_info_str += QString("but is immovable for the next ");
    spell_info_str += (QString::number(static_cast<unsigned>(Horseman::SpellData::GALLOP_SELF_IMMOVABLE_DURATION)) + QString(" turns.\n"));
    spell_info_str += (QString("* Cooldown: ") + QString::number(static_cast<int>(Horseman::SpellCooldown::GALLOP)) + QString("\n"));
    spell_info_str += (QString("* Cost: ") +  QString::number(static_cast<int>(Horseman::SpellCost::GALLOP)));
    spell_info_str += QString("\n");

    spell_map[QString("GALLOP")]=spell_info_str;
    spell_map_vec.push_back(spell_map);

    return spell_map_vec;
}

std::string Horseman::field_bonus_info()
{
    std::stringstream info_stream;

    info_stream << "Field bonus" << "\n----------------------------------\n";
    info_stream << "* Ground: ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::GROUND_HEALTH)) << "% HP, ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::GROUND_DAMAGE)) << "% DMG\n";
    info_stream << "* Plain: ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::PLAIN_HEALTH)) << "% HP, ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::PLAIN_DAMAGE)) << "% DMG\n";
    info_stream << "* Hill: ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::HILL_HEALTH)) << "% HP, ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::HILL_DAMAGE)) << "% DMG\n";
    info_stream << "* Forest: ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::FOREST_HEALTH)) << "% HP, ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::FOREST_DAMAGE)) << "% DMG\n";
    info_stream << "* Bridge: ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::BRIDGE_HEALTH)) << "% HP, ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::BRIDGE_DAMAGE)) << "% DMG";
    info_stream << "\n";

    return info_stream.str();
};

QString Horseman::field_bonus_Qinfo()
{
    return QString(field_bonus_info().data());
}

std::string Horseman::target_bonus_info()
{
    std::stringstream info_stream;

    info_stream << "Target bonus" << "\n----------------------------------\n";
    info_stream << "* Archer: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::ARCHER_DAMAGE)) << "% DMG\n";
    info_stream << "* Castle: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::CASTLE_DAMAGE)) << "% DMG\n";
    info_stream << "* Catapult: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::CATAPULT_DAMAGE)) << "% DMG\n";
    info_stream << "* Horseman: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::HORSEMAN_DAMAGE)) << "% DMG\n";
    info_stream << "* Knight: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::KNIGHT_DAMAGE)) << "% DMG\n";
    info_stream << "* Mage: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::MAGE_DAMAGE)) << "% DMG\n";
    info_stream << "* Priest: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::PRIEST_DAMAGE)) << "% DMG\n";
    info_stream << "* Shieldman: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::SHIELDMAN_DAMAGE)) << "% DMG\n";
    info_stream << "* Spearman: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::SPEARMAN_DAMAGE)) << "% DMG";
    info_stream << "\n";

    return info_stream.str();
}

QString Horseman::target_bonus_Qinfo()
{
    return QString(target_bonus_info().data());
}

std::pair<int, int> Horseman::ground_bonuses()
{
    return std::make_pair(
        static_cast<int>(Horseman::FieldBonus::GROUND_HEALTH),
        static_cast<int>(Horseman::FieldBonus::GROUND_DAMAGE)
    );
}

std::pair<int, int> Horseman::plain_bonuses()
{
    return std::make_pair(
        static_cast<int>(Horseman::FieldBonus::PLAIN_HEALTH),
        static_cast<int>(Horseman::FieldBonus::PLAIN_DAMAGE)
    );
}

std::pair<int, int> Horseman::hill_bonuses()
{
    return std::make_pair(
        static_cast<int>(Horseman::FieldBonus::HILL_HEALTH),
        static_cast<int>(Horseman::FieldBonus::HILL_DAMAGE)
    );
}

std::pair<int, int> Horseman::forest_bonuses()
{
    return std::make_pair(
        static_cast<int>(Horseman::FieldBonus::FOREST_HEALTH),
        static_cast<int>(Horseman::FieldBonus::FOREST_DAMAGE)
    );
}

std::pair<int, int> Horseman::bridge_bonuses()
{
    return std::make_pair(
        static_cast<int>(Horseman::FieldBonus::BRIDGE_HEALTH),
        static_cast<int>(Horseman::FieldBonus::BRIDGE_DAMAGE)
    );
}

QRectF Horseman::boundingRect() const
{
    return QRectF(0,0,40,40);
}

const QRect Horseman::get_rect() const{
    return QRect(0,0,40,40);
}

void Horseman::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);
    if(Player::Index::PLAYER_1 == m_owner)
        painter->drawPixmap(get_rect(),QPixmap(Horseman::IMAGE_RED_QPATH));
    else
        painter->drawPixmap(get_rect(),QPixmap(Horseman::IMAGE_BLUE_QPATH));
}

Horseman::Horseman() = default;

Horseman::Horseman(Player::Index owner, std::pair<unsigned, unsigned> board_index)
    : Piece(owner, board_index)
{
    init_default_values();
}

unsigned Horseman::get_default_health() const 
{
    return static_cast<unsigned>(Horseman::Default::HEALTH);
}

unsigned Horseman::get_default_damage() const 
{
    return static_cast<unsigned>(Horseman::Default::DAMAGE);
}

unsigned Horseman::get_max_attacks() const
{
    return static_cast<unsigned>(Horseman::Default::MAX_ATTACKS);
}

unsigned Horseman::get_max_ortho_moves() const
{
    return static_cast<unsigned>(Horseman::Default::MAX_ORTHO_MOVES);
}

unsigned Horseman::get_max_diag_moves() const
{
    return static_cast<unsigned>(Horseman::Default::MAX_DIAG_MOVES);
}

unsigned Horseman::get_max_ortho_target_range() const
{
    return static_cast<unsigned>(Horseman::Default::MAX_ORTHO_TARGET_RANGE);
}

unsigned Horseman::get_max_diag_target_range() const
{
    return static_cast<unsigned>(Horseman::Default::MAX_DIAG_TARGET_RANGE);
}

unsigned Horseman::get_max_ortho_spells_range(int spell_index) const
{
    return static_cast<unsigned>(Horseman::SpellRange::GALLOP_ORTHO);
}

unsigned Horseman::get_max_diag_spells_range(int spell_index) const
{
    return static_cast<unsigned>(Horseman::SpellRange::GALLOP_DIAG);
}

bool Horseman::get_use_g_fields(int spell_index) const
{
    return static_cast<bool>(Horseman::SpellRange::GALLOP_USE_G_FIELDS);
}

int Horseman::get_target_damage_bonus(Piece::Type piece_type) const
{
   switch (piece_type) {
       case Piece::Type::ARCHER:
           return static_cast<int>(Horseman::TargetBonus::ARCHER_DAMAGE);
       case Piece::Type::CASTLE:
           return static_cast<int>(Horseman::TargetBonus::CASTLE_DAMAGE);
       case Piece::Type::CATAPULT:
           return static_cast<int>(Horseman::TargetBonus::CATAPULT_DAMAGE);
       case Piece::Type::HORSEMAN:
           return static_cast<int>(Horseman::TargetBonus::HORSEMAN_DAMAGE);
       case Piece::Type::KNIGHT:
           return static_cast<int>(Horseman::TargetBonus::KNIGHT_DAMAGE);
       case Piece::Type::MAGE:
           return static_cast<int>(Horseman::TargetBonus::MAGE_DAMAGE);
       case Piece::Type::PRIEST:
           return static_cast<int>(Horseman::TargetBonus::PRIEST_DAMAGE);
       case Piece::Type::SHIELDMAN:
           return static_cast<int>(Horseman::TargetBonus::SHIELDMAN_DAMAGE);
       case Piece::Type::SPEARMAN:
           return static_cast<int>(Horseman::TargetBonus::SPEARMAN_DAMAGE);
       default:
           throw UNKNOWN_CLASS_ERROR;
   }
}

std::pair<int, int> Horseman::get_ground_bonuses() const
{
    return std::make_pair(
        static_cast<int>(Horseman::FieldBonus::GROUND_HEALTH),
        static_cast<int>(Horseman::FieldBonus::GROUND_DAMAGE)
    );
}

std::pair<int, int> Horseman::get_plain_bonuses() const
{
    return std::make_pair(
        static_cast<int>(Horseman::FieldBonus::PLAIN_HEALTH),
        static_cast<int>(Horseman::FieldBonus::PLAIN_DAMAGE)
    );
}

std::pair<int, int> Horseman::get_hill_bonuses() const
{
    return std::make_pair(
        static_cast<int>(Horseman::FieldBonus::HILL_HEALTH),
        static_cast<int>(Horseman::FieldBonus::HILL_DAMAGE)
    );
}

std::pair<int, int> Horseman::get_forest_bonuses() const
{
    return std::make_pair(
        static_cast<int>(Horseman::FieldBonus::FOREST_HEALTH),
        static_cast<int>(Horseman::FieldBonus::FOREST_DAMAGE)
    );
}

std::pair<int, int> Horseman::get_bridge_bonuses() const
{
    return std::make_pair(
        static_cast<int>(Horseman::FieldBonus::BRIDGE_HEALTH),
        static_cast<int>(Horseman::FieldBonus::BRIDGE_DAMAGE)
    );
}

std::string Horseman::cooldowns_info() const
{
    std::stringstream info_stream;
    info_stream << "Cooldowns" << "\n-----------------------------\n";

    if (has_cooldowns()) {
        for (auto& [spell, cooldown] : m_cooldowns) {
            info_stream << "* " << spell << ": ";
            info_stream << std::to_string(cooldown) << "\n";
        }
    } else {
        info_stream << "No cooldowns.";
    }

    return info_stream.str();
}

QString Horseman::cooldowns_Qinfo() const
{
    return QString(cooldowns_info().data());
}

std::vector<QString> Horseman::spell_names() const
{
    return { spell_to_Qstring(Horseman::Spell::GALLOP) };
}

std::vector<unsigned> Horseman::spell_costs() const
{
    std::vector<unsigned> costs;
    costs.push_back(static_cast<unsigned>(SpellCost::GALLOP));
    return costs;
}

void Horseman::_accept_visit(PieceVisitor& visitor)
{
    visitor.visit_horseman(this);
}

std::map<QString, QString> Horseman::default_values()
{
    std::map<QString,QString> default_values;
    default_values[QString("HEALTH")]=QString::number(static_cast<int>(Default::HEALTH));
    default_values[QString("DAMAGE")] = QString::number(static_cast<int>(Default::DAMAGE));
    default_values[QString("MAX_ORTHO_MOVES")] = QString::number(static_cast<int>(Default::MAX_ORTHO_MOVES));
    default_values[QString("MAX_DIAG_MOVES")] = QString::number(static_cast<int>(Default::MAX_DIAG_MOVES));
    default_values[QString("MAX_ATTACKS")] = QString::number(static_cast<int>(Default::MAX_ATTACKS));
    default_values[QString("MAX_ORTHO_TARGET_RANGE")] = QString::number(static_cast<int>(Default::MAX_ORTHO_TARGET_RANGE));
    default_values[QString("MAX_DIAG_TARGET_RANGE")] = QString::number(static_cast<int>(Default::MAX_DIAG_TARGET_RANGE));

    default_values[QString("GROUND_HEALTH")] = QString::number(static_cast<int>(FieldBonus::GROUND_HEALTH));
    default_values[QString("GROUND_DAMAGE")] = QString::number(static_cast<int>(FieldBonus::GROUND_DAMAGE));
    default_values[QString("PLAIN_HEALTH")] = QString::number(static_cast<int>(FieldBonus::PLAIN_HEALTH));
    default_values[QString("PLAIN_DAMAGE")] = QString::number(static_cast<int>(FieldBonus::PLAIN_DAMAGE));
    default_values[QString("FOREST_HEALTH")] = QString::number(static_cast<int>(FieldBonus::FOREST_HEALTH));
    default_values[QString("FOREST_DAMAGE")] = QString::number(static_cast<int>(FieldBonus::FOREST_DAMAGE));
    default_values[QString("BRIDGE_HEALTH")] = QString::number(static_cast<int>(FieldBonus::BRIDGE_HEALTH));
    default_values[QString("BRIDGE_DAMAGE")] = QString::number(static_cast<int>(FieldBonus::BRIDGE_DAMAGE));
    default_values[QString("HILL_HEALTH")] = QString::number(static_cast<int>(FieldBonus::HILL_HEALTH));
    default_values[QString("HILL_DAMAGE")] = QString::number(static_cast<int>(FieldBonus::HILL_DAMAGE));

    default_values[QString("KNIGHT_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::KNIGHT_DAMAGE));
    default_values[QString("SPEARMAN_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::SPEARMAN_DAMAGE));
    default_values[QString("SHIELDMAN_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::SHIELDMAN_DAMAGE));
    default_values[QString("HORSEMAN_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::HORSEMAN_DAMAGE));
    default_values[QString("ARCHER_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::ARCHER_DAMAGE));
    default_values[QString("CATAPULT_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::CATAPULT_DAMAGE));
    default_values[QString("MAGE_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::MAGE_DAMAGE));
    default_values[QString("PRIEST_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::PRIEST_DAMAGE));
    default_values[QString("CASTLE_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::CASTLE_DAMAGE));

    return default_values;
}

std::string Horseman::spell_to_string(Horseman::Spell spell) const
{
    return "Gallop";
}

QString Horseman::spell_to_Qstring(Horseman::Spell spell) const
{
    return QString("Gallop");
}

unsigned Horseman::get_spell_cooldown(Horseman::Spell spell) const
{
    return static_cast<unsigned>(Horseman::SpellCost::GALLOP);
}

unsigned Horseman::get_spell_cooldown_cost(Horseman::Spell spell) const
{
    return static_cast<unsigned>(Horseman::SpellCost::GALLOP);
}

std::vector<Horseman::Spell> Horseman::get_spells() const
{
    return { Horseman::Spell::GALLOP };
}

void Horseman::init_default_values()
{
    m_type = Piece::Type::HORSEMAN;
    m_health = static_cast<double>(Horseman::Default::HEALTH);
    m_damage = static_cast<double>(Horseman::Default::DAMAGE);
    m_ortho_moves_remaining = static_cast<int>(Horseman::Default::MAX_ORTHO_MOVES);
    m_diag_moves_remaining = static_cast<int>(Horseman::Default::MAX_DIAG_MOVES);
    m_attacks_remaining = static_cast<int>(Horseman::Default::MAX_ATTACKS);
}
