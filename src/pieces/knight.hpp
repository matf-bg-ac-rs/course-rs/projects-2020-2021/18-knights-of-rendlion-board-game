#ifndef KNIGHT_HPP
#define KNIGHT_HPP

#include <map>
#include <string>
#include <QString>
#include "piece.hpp"
#include "../utility/piecevisitor.hpp"
#include "../gui/board.hpp"

class Piece;
class PieceVisitor;
class Board;

class Knight : public Piece
{
public:
    enum class Spell {
        MORTAL_STRIKE
    };

    enum class SpellCost {
        MORTAL_STRIKE = 2
    };

    enum class SpellCooldown {
        MORTAL_STRIKE = 2
    };

    enum class SpellRange {
        MORTAL_STRIKE_ORTHO = 1,
        MORTAL_STRIKE_DIAG = 0,
        MORTAL_STRIKE_USE_G_FIELDS = 0
    };

    enum class SpellData {
        MORTAL_STRIKE_KILL_TRESHOLD = 50,
        MORTAL_STRIKE_SELF_DAMAGE = 15
    };

    enum class Default {
        HEALTH = 110,
        DAMAGE = 25,
        MAX_ORTHO_MOVES = 2,
        MAX_DIAG_MOVES = 1,
        MAX_ATTACKS = 3,
        MAX_ORTHO_TARGET_RANGE = 1,
        MAX_DIAG_TARGET_RANGE = 0
    };

    enum class FieldBonus {
        GROUND_HEALTH = 0,
        GROUND_DAMAGE = 0,
        PLAIN_HEALTH = 15,
        PLAIN_DAMAGE = 15,
        HILL_HEALTH = -5,
        HILL_DAMAGE = -5,
        FOREST_HEALTH = 30,
        FOREST_DAMAGE = 25,
        BRIDGE_HEALTH = 10,
        BRIDGE_DAMAGE = 0,
        RBRIDGE_HEALTH = 10,
        RBRIDGE_DAMAGE = 0,
    };

    enum class TargetBonus {
        KNIGHT_DAMAGE = 0,
        SPEARMAN_DAMAGE = 30,
        SHIELDMAN_DAMAGE = -30,
        HORSEMAN_DAMAGE = 0,
        ARCHER_DAMAGE = 10,
        CATAPULT_DAMAGE = 30,
        MAGE_DAMAGE = 20,
        PRIEST_DAMAGE = 20,
        CASTLE_DAMAGE = -10,
    };

    static inline const std::string IMAGE_RED_PATH =":/images/pieces/knight_red_new.png";
    static inline const QString IMAGE_RED_QPATH = QString(":/images/pieces/knight_red_new.png");
    static inline const std::string IMAGE_BLUE_PATH =":/images/pieces/knight_blue_new.png";
    static inline const QString IMAGE_BLUE_QPATH = QString(":/images/pieces/knight_blue_new.png");

    static std::string default_info();
    static QString default_Qinfo();
    static std::vector<std::map<QString, QString>> spells_info();
    static std::string field_bonus_info();
    static QString field_bonus_Qinfo();
    static std::string target_bonus_info();
    static QString target_bonus_Qinfo();
    static std::map<QString,QString> default_values();


    static std::pair<int, int> ground_bonuses();
    static std::pair<int, int> plain_bonuses();
    static std::pair<int, int> hill_bonuses();
    static std::pair<int, int> forest_bonuses();
    static std::pair<int, int> bridge_bonuses();

    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
    const QRect get_rect() const override;

    Knight();
    Knight(Player::Index owner, std::pair<unsigned, unsigned> board_index);

    unsigned get_default_health() const override;
    unsigned get_default_damage() const override;
    unsigned get_max_attacks() const override;
    unsigned get_max_ortho_moves() const override;
    unsigned get_max_diag_moves() const override;
    unsigned get_max_ortho_target_range() const override;
    unsigned get_max_diag_target_range() const override;
    unsigned get_max_ortho_spells_range(int spell_index) const override;
    unsigned get_max_diag_spells_range(int spell_index) const override;
    bool get_use_g_fields(int spell_index) const override;
    int get_target_damage_bonus(Piece::Type piece_type) const override;

    std::pair<int, int> get_ground_bonuses() const override;
    std::pair<int, int> get_plain_bonuses() const override;
    std::pair<int, int> get_hill_bonuses() const override;
    std::pair<int, int> get_forest_bonuses() const override;
    std::pair<int, int> get_bridge_bonuses() const override;

    std::string cooldowns_info() const override;
    QString cooldowns_Qinfo() const override;
    std::vector<QString> spell_names() const override;
    std::vector<unsigned> spell_costs() const override;
    void _accept_visit(PieceVisitor& visitor) override;

    std::string spell_to_string(Spell spell) const;
    QString spell_to_Qstring(Spell spell) const;
    unsigned get_spell_cooldown(Spell spell) const;
    unsigned get_spell_cooldown_cost(Spell spell) const;
    std::vector<Spell> get_spells() const;


private:
    void init_default_values() override;
};

#endif // KNIGHT_HPP
