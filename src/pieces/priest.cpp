#include "priest.hpp"


std::string Priest::default_info()

{
    std::stringstream info_stream;

    info_stream << "Priest" << "\n----------------------------";
    info_stream << "\nHealth: " << static_cast<unsigned>(Priest::Default::HEALTH);
    info_stream << "\nDamage: " << static_cast<unsigned>(Priest::Default::DAMAGE);
    info_stream << "\nMax attacks: " << static_cast<unsigned>(Priest::Default::MAX_ATTACKS);
    info_stream << "\nMax ortho moves: " << static_cast<unsigned>(Priest::Default::MAX_ORTHO_MOVES);
    info_stream << "\nMax diag moves: " << static_cast<unsigned>(Priest::Default::MAX_DIAG_MOVES);
    info_stream << "\nMax ortho target range: " << static_cast<unsigned>(Priest::Default::MAX_ORTHO_TARGET_RANGE);
    info_stream << "\nMax diag target range: " << static_cast<unsigned>(Priest::Default::MAX_DIAG_TARGET_RANGE);
    info_stream << "\n";

    return info_stream.str();
}

QString Priest::default_Qinfo()
{
    return QString(default_info().data());
}

std::vector<std::map<QString, QString>> Priest::spells_info(){

    std::vector<std::map<QString, QString>> spell_map_vec;
    std::map<QString, QString> spell_map;
    QString spell_info_str;

    spell_info_str += QString("Heals friendly target for ");
    spell_info_str += QString::number(static_cast<unsigned>(Priest::SpellData::HEAL_HP)) + QString(" HP.\n");
    spell_info_str += (QString("* Cooldown: ") + QString::number(static_cast<int>(Priest::SpellCooldown::HEAL)) + QString("\n"));
    spell_info_str += (QString("* Cost: ") +  QString::number(static_cast<int>(Priest::SpellCost::HEAL)));
    spell_info_str += QString("\n");

    spell_map[QString("HEAL")]=spell_info_str;
    spell_map_vec.push_back(spell_map);


    spell_info_str = QString("");
    spell_info_str += QString("Empowers friendly target, giving him ");
    spell_info_str += (QString::number(static_cast<unsigned>(Priest::SpellData::EMPOWER_BONUS_PERCENT)) + QString("% boost to health and damage for "));
    spell_info_str += (QString::number(static_cast<unsigned>(Priest::SpellData::EMPOWER_DURATION)) + QString(" turns.\n"));
    spell_info_str += (QString("* Cooldown: ") + QString::number(static_cast<int>(Priest::SpellCooldown::EMPOWER)) + QString("\n"));
    spell_info_str += (QString("* Cost: ") +  QString::number(static_cast<int>(Priest::SpellCost::EMPOWER)));
    spell_info_str += QString("\n");

    spell_map.clear();
    spell_map[QString("EMPOWER")]=spell_info_str;
    spell_map_vec.push_back(spell_map);



    spell_info_str = QString("");
    spell_info_str += QString("Stuns enemy target, making him completely unplayable for ");
    spell_info_str += (QString::number(static_cast<unsigned>(Priest::SpellData::STUN_DURATION)) + QString(" turns.\n"));
    spell_info_str += (QString("* Cooldown: ") + QString::number(static_cast<int>(Priest::SpellCooldown::STUN)) + QString("\n"));
    spell_info_str += (QString("* Cost: ") +  QString::number(static_cast<int>(Priest::SpellCost::STUN)));
    spell_info_str += QString("\n");

    spell_map.clear();
    spell_map[QString("STUN")]=spell_info_str;
    spell_map_vec.push_back(spell_map);


    spell_info_str = QString("");
    spell_info_str += QString("Shields friendly target by a holy shield, making him invulnerable for ");
    spell_info_str += (QString::number(static_cast<unsigned>(Priest::SpellData::HOLY_SHIELD_DURATION)) + QString(" turns.\n"));
    spell_info_str += (QString("* Cooldown: ") + QString::number(static_cast<int>(Priest::SpellCooldown::HOLY_SHIELD)) + QString("\n"));
    spell_info_str += (QString("* Cost: ") +  QString::number(static_cast<int>(Priest::SpellCost::HOLY_SHIELD)));
    spell_info_str += QString("\n");

    spell_map.clear();
    spell_map[QString("HOLY SHIELD")]=spell_info_str;
    spell_map_vec.push_back(spell_map);

    return spell_map_vec;
}

std::string Priest::field_bonus_info()
{
    std::stringstream info_stream;

    info_stream << "Field bonus" << "\n----------------------------------\n";
    info_stream << "* Ground: ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::GROUND_HEALTH)) << "% HP, ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::GROUND_DAMAGE)) << "% DMG\n";
    info_stream << "* Plain: ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::PLAIN_HEALTH)) << "% HP, ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::PLAIN_DAMAGE)) << "% DMG\n";
    info_stream << "* Hill: ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::HILL_HEALTH)) << "% HP, ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::HILL_DAMAGE)) << "% DMG\n";
    info_stream << "* Forest: ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::FOREST_HEALTH)) << "% HP, ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::FOREST_DAMAGE)) << "% DMG\n";
    info_stream << "* Bridge: ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::BRIDGE_HEALTH)) << "% HP, ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::BRIDGE_DAMAGE)) << "% DMG";
    info_stream << "\n";

    return info_stream.str();
};

QString Priest::field_bonus_Qinfo()
{
    return QString(field_bonus_info().data());
}

std::string Priest::target_bonus_info()
{
    std::stringstream info_stream;

    info_stream << "Target bonus" << "\n----------------------------------\n";
    info_stream << "* Archer: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::ARCHER_DAMAGE)) << "% DMG\n";
    info_stream << "* Castle: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::CASTLE_DAMAGE)) << "% DMG\n";
    info_stream << "* Catapult: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::CATAPULT_DAMAGE)) << "% DMG\n";
    info_stream << "* Horseman: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::HORSEMAN_DAMAGE)) << "% DMG\n";
    info_stream << "* Knight: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::KNIGHT_DAMAGE)) << "% DMG\n";
    info_stream << "* Mage: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::MAGE_DAMAGE)) << "% DMG\n";
    info_stream << "* Priest: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::PRIEST_DAMAGE)) << "% DMG\n";
    info_stream << "* Shieldman: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::SHIELDMAN_DAMAGE)) << "% DMG\n";
    info_stream << "* Spearman: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::SPEARMAN_DAMAGE)) << "% DMG";
    info_stream << "\n";

    return info_stream.str();
}

QString Priest::target_bonus_Qinfo()
{
    return QString(target_bonus_info().data());
}

std::pair<int, int> Priest::ground_bonuses()
{
    return std::make_pair(
        static_cast<int>(Priest::FieldBonus::GROUND_HEALTH),
        static_cast<int>(Priest::FieldBonus::GROUND_DAMAGE)
    );
}

std::pair<int, int> Priest::plain_bonuses()
{
    return std::make_pair(
        static_cast<int>(Priest::FieldBonus::PLAIN_HEALTH),
        static_cast<int>(Priest::FieldBonus::PLAIN_DAMAGE)
    );
}

std::pair<int, int> Priest::hill_bonuses()
{
    return std::make_pair(
        static_cast<int>(Priest::FieldBonus::HILL_HEALTH),
        static_cast<int>(Priest::FieldBonus::HILL_DAMAGE)
    );
}

std::pair<int, int> Priest::forest_bonuses()
{
    return std::make_pair(
        static_cast<int>(Priest::FieldBonus::FOREST_HEALTH),
        static_cast<int>(Priest::FieldBonus::FOREST_DAMAGE)
    );
}

std::pair<int, int> Priest::bridge_bonuses()
{
    return std::make_pair(
        static_cast<int>(Priest::FieldBonus::BRIDGE_HEALTH),
        static_cast<int>(Priest::FieldBonus::BRIDGE_DAMAGE)
    );
}

QRectF Priest::boundingRect() const
{
    return QRectF(0,0,40,40);
}

const QRect Priest::get_rect() const{
    return QRect(0,0,40,40);
}

void Priest::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);
    if(Player::Index::PLAYER_1 == m_owner)
        painter->drawPixmap(get_rect(),QPixmap(Priest::IMAGE_RED_QPATH));
    else
        painter->drawPixmap(get_rect(),QPixmap(Priest::IMAGE_BLUE_QPATH));
}

Priest::Priest() = default;

Priest::Priest(Player::Index owner, std::pair<unsigned, unsigned> board_index)
    : Piece(owner, board_index)
{
    init_default_values();
}

unsigned Priest::get_default_health() const 
{
    return static_cast<unsigned>(Priest::Default::HEALTH);
}

unsigned Priest::get_default_damage() const 
{
    return static_cast<unsigned>(Priest::Default::DAMAGE);
}

unsigned Priest::get_max_attacks() const
{
    return static_cast<unsigned>(Priest::Default::MAX_ATTACKS);
}

unsigned Priest::get_max_ortho_moves() const
{
    return static_cast<unsigned>(Priest::Default::MAX_ORTHO_MOVES);
}

unsigned Priest::get_max_diag_moves() const
{
    return static_cast<unsigned>(Priest::Default::MAX_DIAG_MOVES);
}

unsigned Priest::get_max_ortho_target_range() const
{
    return static_cast<unsigned>(Priest::Default::MAX_ORTHO_TARGET_RANGE);
}

unsigned Priest::get_max_diag_target_range() const
{
    return static_cast<unsigned>(Priest::Default::MAX_DIAG_TARGET_RANGE);
}

unsigned Priest::get_max_ortho_spells_range(int spell_index) const
{
    switch(spell_index){
    case 1:
        return static_cast<unsigned>(Priest::SpellRange::HEAL_ORTHO);
        break;
    case 2:
        return static_cast<unsigned>(Priest::SpellRange::EMPOWER_ORTHO);
        break;
    case 3:
        return static_cast<unsigned>(Priest::SpellRange::STUN_ORTHO);
        break;
    case 4:
        return static_cast<unsigned>(Priest::SpellRange::HOLY_SHIELD_ORTHO);
        break;
    default:
        throw "UNKNOWN_SPELL";

    }
}

unsigned Priest::get_max_diag_spells_range(int spell_index) const
{
    switch(spell_index){
    case 1:
        return static_cast<unsigned>(Priest::SpellRange::HEAL_DIAG);
        break;
    case 2:
        return static_cast<unsigned>(Priest::SpellRange::EMPOWER_DIAG);
        break;
    case 3:
        return static_cast<unsigned>(Priest::SpellRange::STUN_DIAG);
        break;
    case 4:
        return static_cast<unsigned>(Priest::SpellRange::HOLY_SHIELD_DIAG);
        break;
    default:
        throw "UNKNOWN_SPELL";

    }
}

bool Priest::get_use_g_fields(int spell_index) const
{
    switch(spell_index){
    case 1:
        return static_cast<bool>(Priest::SpellRange::HEAL_USE_G_FIELDS);
        break;
    case 2:
        return static_cast<bool>(Priest::SpellRange::EMPOWER_USE_G_FIELDS);
        break;
    case 3:
        return static_cast<bool>(Priest::SpellRange::STUN_USE_G_FIELDS);
        break;
    case 4:
        return static_cast<bool>(Priest::SpellRange::HOLY_SHIELD_USE_G_FIELDS);
        break;
    default:
        throw "UNKNOWN_SPELL";

    }
}

int Priest::get_target_damage_bonus(Piece::Type piece_type) const
{
   switch (piece_type) {
       case Piece::Type::ARCHER:
           return static_cast<int>(Priest::TargetBonus::ARCHER_DAMAGE);
       case Piece::Type::CASTLE:
           return static_cast<int>(Priest::TargetBonus::CASTLE_DAMAGE);
       case Piece::Type::CATAPULT:
           return static_cast<int>(Priest::TargetBonus::CATAPULT_DAMAGE);
       case Piece::Type::HORSEMAN:
           return static_cast<int>(Priest::TargetBonus::HORSEMAN_DAMAGE);
       case Piece::Type::KNIGHT:
           return static_cast<int>(Priest::TargetBonus::KNIGHT_DAMAGE);
       case Piece::Type::MAGE:
           return static_cast<int>(Priest::TargetBonus::MAGE_DAMAGE);
       case Piece::Type::PRIEST:
           return static_cast<int>(Priest::TargetBonus::PRIEST_DAMAGE);
       case Piece::Type::SHIELDMAN:
           return static_cast<int>(Priest::TargetBonus::SHIELDMAN_DAMAGE);
       case Piece::Type::SPEARMAN:
           return static_cast<int>(Priest::TargetBonus::SPEARMAN_DAMAGE);
       default:
           throw UNKNOWN_CLASS_ERROR;
   }
}

std::pair<int, int> Priest::get_ground_bonuses() const
{
    return std::make_pair(
        static_cast<int>(Priest::FieldBonus::GROUND_HEALTH),
        static_cast<int>(Priest::FieldBonus::GROUND_DAMAGE)
    );
}

std::pair<int, int> Priest::get_plain_bonuses() const
{
    return std::make_pair(
        static_cast<int>(Priest::FieldBonus::PLAIN_HEALTH),
        static_cast<int>(Priest::FieldBonus::PLAIN_DAMAGE)
    );
}

std::pair<int, int> Priest::get_hill_bonuses() const
{
    return std::make_pair(
        static_cast<int>(Priest::FieldBonus::HILL_HEALTH),
        static_cast<int>(Priest::FieldBonus::HILL_DAMAGE)
    );
}

std::pair<int, int> Priest::get_forest_bonuses() const
{
    return std::make_pair(
        static_cast<int>(Priest::FieldBonus::FOREST_HEALTH),
        static_cast<int>(Priest::FieldBonus::FOREST_DAMAGE)
    );
}

std::pair<int, int> Priest::get_bridge_bonuses() const
{
    return std::make_pair(
        static_cast<int>(Priest::FieldBonus::BRIDGE_HEALTH),
        static_cast<int>(Priest::FieldBonus::BRIDGE_DAMAGE)
    );
}

std::string Priest::cooldowns_info() const
{
    std::stringstream info_stream;
    info_stream << "Cooldowns" << "\n-----------------------------\n";

    if (has_cooldowns()) {
        for (auto& [spell, cooldown] : m_cooldowns) {
            info_stream << "* " << spell << ": ";
            info_stream << std::to_string(cooldown) << "\n";
        }
    } else {
        info_stream << "No cooldowns.";
    }

    return info_stream.str();
}

QString Priest::cooldowns_Qinfo() const
{
    return QString(cooldowns_info().data());
}

std::vector<QString> Priest::spell_names() const
{
    return {
        spell_to_Qstring(Priest::Spell::HEAL),
        spell_to_Qstring(Priest::Spell::EMPOWER),
        spell_to_Qstring(Priest::Spell::STUN),
        spell_to_Qstring(Priest::Spell::HOLY_SHIELD)
    };
}

std::vector<unsigned> Priest::spell_costs() const
{
    std::vector<unsigned> costs;
    costs.push_back(static_cast<unsigned>(SpellCost::HEAL));
    costs.push_back(static_cast<unsigned>(SpellCost::EMPOWER));
    costs.push_back(static_cast<unsigned>(SpellCost::STUN));
    costs.push_back(static_cast<unsigned>(SpellCost::HOLY_SHIELD));
    return costs;
}

void Priest::_accept_visit(PieceVisitor& visitor)
{
    visitor.visit_priest(this);
}

std::map<QString, QString> Priest::default_values()
{
    std::map<QString,QString> default_values;
    default_values[QString("HEALTH")]=QString::number(static_cast<int>(Default::HEALTH));
    default_values[QString("DAMAGE")] = QString::number(static_cast<int>(Default::DAMAGE));
    default_values[QString("MAX_ORTHO_MOVES")] = QString::number(static_cast<int>(Default::MAX_ORTHO_MOVES));
    default_values[QString("MAX_DIAG_MOVES")] = QString::number(static_cast<int>(Default::MAX_DIAG_MOVES));
    default_values[QString("MAX_ATTACKS")] = QString::number(static_cast<int>(Default::MAX_ATTACKS));
    default_values[QString("MAX_ORTHO_TARGET_RANGE")] = QString::number(static_cast<int>(Default::MAX_ORTHO_TARGET_RANGE));
    default_values[QString("MAX_DIAG_TARGET_RANGE")] = QString::number(static_cast<int>(Default::MAX_DIAG_TARGET_RANGE));

    default_values[QString("GROUND_HEALTH")] = QString::number(static_cast<int>(FieldBonus::GROUND_HEALTH));
    default_values[QString("GROUND_DAMAGE")] = QString::number(static_cast<int>(FieldBonus::GROUND_DAMAGE));
    default_values[QString("PLAIN_HEALTH")] = QString::number(static_cast<int>(FieldBonus::PLAIN_HEALTH));
    default_values[QString("PLAIN_DAMAGE")] = QString::number(static_cast<int>(FieldBonus::PLAIN_DAMAGE));
    default_values[QString("FOREST_HEALTH")] = QString::number(static_cast<int>(FieldBonus::FOREST_HEALTH));
    default_values[QString("FOREST_DAMAGE")] = QString::number(static_cast<int>(FieldBonus::FOREST_DAMAGE));
    default_values[QString("BRIDGE_HEALTH")] = QString::number(static_cast<int>(FieldBonus::BRIDGE_HEALTH));
    default_values[QString("BRIDGE_DAMAGE")] = QString::number(static_cast<int>(FieldBonus::BRIDGE_DAMAGE));
    default_values[QString("HILL_HEALTH")] = QString::number(static_cast<int>(FieldBonus::HILL_HEALTH));
    default_values[QString("HILL_DAMAGE")] = QString::number(static_cast<int>(FieldBonus::HILL_DAMAGE));

    default_values[QString("KNIGHT_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::KNIGHT_DAMAGE));
    default_values[QString("SPEARMAN_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::SPEARMAN_DAMAGE));
    default_values[QString("SHIELDMAN_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::SHIELDMAN_DAMAGE));
    default_values[QString("HORSEMAN_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::HORSEMAN_DAMAGE));
    default_values[QString("ARCHER_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::ARCHER_DAMAGE));
    default_values[QString("CATAPULT_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::CATAPULT_DAMAGE));
    default_values[QString("MAGE_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::MAGE_DAMAGE));
    default_values[QString("PRIEST_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::PRIEST_DAMAGE));
    default_values[QString("CASTLE_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::CASTLE_DAMAGE));

    return default_values;
}


std::string Priest::spell_to_string(Priest::Spell spell) const
{
    switch (spell) {
          case Spell::HEAL:
              return "Heal";
              break;
          case Spell::EMPOWER:
              return "Empower";
              break;
          case Spell::STUN:
              return "Stun";
              break;
          case Spell::HOLY_SHIELD:
              return "Holy shield";
              break;
          default:
              throw SPELL_ERROR;
      }

}

QString Priest::spell_to_Qstring(Priest::Spell spell) const
{
    switch (spell) {
          case Spell::HEAL:
              return QString("Heal");
              break;
          case Spell::EMPOWER:
              return QString("Empower");
              break;
          case Spell::STUN:
              return QString("Stun");
              break;
          case Spell::HOLY_SHIELD:
              return QString("Holy Shield");
              break;
          default:
              throw SPELL_ERROR;
    }
}

unsigned Priest::get_spell_cooldown(Priest::Spell spell) const
{
     switch(spell){
     case (Priest::Spell::HEAL):
         return static_cast<unsigned>(Priest::SpellCost::HEAL);
     case (Priest::Spell::EMPOWER):
         return static_cast<unsigned>(Priest::SpellCost::EMPOWER);
     case (Priest::Spell::STUN):
         return static_cast<unsigned>(Priest::SpellCost::STUN);
     case (Priest::Spell::HOLY_SHIELD):
         return static_cast<unsigned>(Priest::SpellCost::HOLY_SHIELD);
     default:
         throw SPELL_ERROR;
     }

}

unsigned Priest::get_spell_cooldown_cost(Priest::Spell spell) const
{
    switch(spell){
    case (Priest::Spell::HEAL):
        return static_cast<unsigned>(Priest::SpellCost::HEAL);
    case (Priest::Spell::EMPOWER):
        return static_cast<unsigned>(Priest::SpellCost::EMPOWER);
    case (Priest::Spell::STUN):
        return static_cast<unsigned>(Priest::SpellCost::STUN);
    case (Priest::Spell::HOLY_SHIELD):
        return static_cast<unsigned>(Priest::SpellCost::HOLY_SHIELD);
    default:
        throw SPELL_ERROR;
    }
}

std::vector<Priest::Spell> Priest::get_spells() const
{
    return {
        Priest::Spell::HEAL,
        Priest::Spell::EMPOWER,
        Priest::Spell::STUN,
        Priest::Spell::HOLY_SHIELD
    };
}

void Priest::init_default_values()
{
    m_type = Piece::Type::PRIEST;
    m_health = static_cast<double>(Priest::Default::HEALTH);
    m_damage = static_cast<double>(Priest::Default::DAMAGE);
    m_ortho_moves_remaining = static_cast<int>(Priest::Default::MAX_ORTHO_MOVES);
    m_diag_moves_remaining = static_cast<int>(Priest::Default::MAX_DIAG_MOVES);
    m_attacks_remaining = static_cast<int>(Priest::Default::MAX_ATTACKS);
}

