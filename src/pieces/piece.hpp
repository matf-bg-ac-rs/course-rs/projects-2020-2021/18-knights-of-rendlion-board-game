#ifndef PIECE_HPP
#define PIECE_HPP

#include <iostream>
#include <sstream>
#include <fstream>

#include "../engine/player.hpp"
#include "../utility/piecevisitor.hpp"
#include "../utility/targetdamagebonus.hpp"
#include "../utility/utility.hpp"
#include "../gui/card.hpp"

#include <QGraphicsItem>
#include <QPainter>
#include <QWidget>
#include <QRectF>


class Piece : public QGraphicsItem
{
public:
    // Saves piece type according to their class
    enum class Type {
        _NO_TYPE,
        ARCHER,
        CASTLE,
        CATAPULT,
        HORSEMAN,
        KNIGHT,
        MAGE,
        PRIEST,
        SHIELDMAN,
        SPEARMAN
    };

    enum class SpellEffect {
        FIREBOLT_DOT,
        HOLY_SHIELD_IMMUNITY,
        EMPOWER_BONUS,
        TURTLE_BONUS,
        DEADLY_POISON
    };

    enum class SpellEffectCache {
        FIREBOLT_DOT_DMG = 10,
        EMPOWER_BONUS = 30
    };

    enum class DataIndicator {
        STATS,
        MOBILITY,
        STUN_STATUS
    };


    static inline unsigned num_of_pieces = 0;
    static inline const std::string SPELL_ERROR = "Unkown spell type";
    static inline const char* UNKNOWN_CLASS_ERROR = "Error: unknown class type";
    static inline const char* ATTACK_LOG_FNAME = "log_attack.txt";

    static std::string spell_effect_to_str(Piece::SpellEffect spell_effect);

    // QGraphicsItem interface
    virtual QRectF boundingRect() const = 0;
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) = 0;
    const virtual QRect get_rect() const = 0;


    /**
     * Pure virtual methods specific for different classes
     **/

    // Helper method for extracting default health
    virtual unsigned get_default_health() const = 0;

    // Helper method for extracting default damage
    virtual unsigned get_default_damage() const = 0;

    // Helper method for extracting max attacks per turn
    virtual unsigned get_max_attacks() const = 0;

    // Helper method for extracting max orthogonal moves per turn
    virtual unsigned get_max_ortho_moves() const = 0;

    // Helper method for extracting max diagonal moves per turn
    virtual unsigned get_max_diag_moves() const = 0;

    // Helper method for extracting max orthogonal range when targeting piece
    virtual unsigned get_max_ortho_target_range() const = 0;

    // Helper method for extracting max diagonal range when targeting piece
    virtual unsigned get_max_diag_target_range() const = 0;

    // Helper method for extracting x% target damage bonus from enum
    virtual int get_target_damage_bonus(Type piece_type) const = 0;

    // Helper method for extracting max orthogonal spell range
    virtual unsigned get_max_ortho_spells_range(int spell_index) const = 0;

    // Helper method for extracting max diagonal spell range
    virtual unsigned get_max_diag_spells_range(int spell_index) const = 0;

    // Helper method for checking if spell use G fields
    virtual bool get_use_g_fields(int spell_index) const = 0;

    // helper method for extracting x% field-ground bonuses for this piece
    virtual std::pair<int, int> get_ground_bonuses() const = 0;

    // helper method for extracting x% field-plain bonuses for this piece
    virtual std::pair<int, int> get_plain_bonuses() const = 0;

    // helper method for extracting x% field-hill bonuses for this piece
    virtual std::pair<int, int> get_hill_bonuses() const = 0;

    // helper method for extracting x% field-forest bonuses for this piece
    virtual std::pair<int, int> get_forest_bonuses() const = 0;

    // helper method for extracting x% field-bridge bonuses for this piece
    virtual std::pair<int, int> get_bridge_bonuses() const = 0;

    // Visitor design functionality, neccessary for extracting target damage bonus
    virtual void _accept_visit(PieceVisitor& visitor) = 0;

     // Get piece's spell cooldowns, if any
     virtual std::string cooldowns_info() const = 0;

     // Get piece's spell cooldowns as QString, if any
     virtual QString cooldowns_Qinfo() const = 0;

    // Get spell names
    virtual std::vector<QString> spell_names() const=0;

    virtual std::vector<unsigned> spell_costs() const = 0;

    //virtual std::map<QString,QString> default_values() const = 0;

    const std::map<QString,QString> values() const;

    virtual ~Piece();

    /**
     * Geters
     */
    Type piece_type() const;
    Player::Index owner() const;
    const std::pair<unsigned, unsigned>& board_index() const;
    unsigned at_row() const;
    unsigned at_col() const;
    double health() const;
    double damage() const;
    int last_field_health_bonus_percent() const;
    double last_field_health_bonus() const;
    int last_field_damage_bonus_percent() const;
    double last_field_damage_bonus() const;
    double last_target_damage_bonus() const;
    int last_spell_health_bonus_percent() const;
    double last_spell_health_bonus() const;
    int last_spell_damage_bonus_percent() const;
    double last_spell_damage_bonus() const;
    int ortho_moves_remaining() const;
    int diag_moves_remaining() const;
    int attacks_remaining() const;
    const std::pair<bool, int>& stunned() const;
    bool is_stunned() const;
    bool has_new_stun() const;
    const std::pair<bool, int>& immovable() const;
    bool is_immovable() const;
    bool has_new_immovable() const;
    bool is_immune() const;
    std::map<std::string, int>& spell_effects();
    bool has_spell_effects() const;
    bool has_spell_effect(const std::string& spell_effect_str) const;
    void add_spell_effect(const std::string& spell_effect_str, int duration);
    void remove_spell_effect(const std::string& spell_effect_str);
    virtual void update_spell_effect(const std::string& spell_effect_str);  // virtual due to shieldman's turtle bonus
    std::map<std::string, int>& cooldowns();
    bool has_cooldowns() const;
    bool has_cooldown(const std::string& spell_str) const;
    void add_cooldown(const std::string& spell_str, int duration);
    void remove_cooldown(const std::string& spell_str);
    void update_cooldown(const std::string& spell_str);

    /**
     * Seters
     **/
    void set_board_index(const std::pair<unsigned, unsigned>& board_index);
    void set_health(const double health);
    void set_damage(const double damage);
    void set_last_field_health_bonus_percent(const int bonus_percent);
    void set_last_field_health_bonus(const double bonus);
    void set_last_field_damage_bonus_percent(const int bonus_percent);
    void set_last_field_damage_bonus(const double bonus);
    void set_last_target_damage_bonus(const double bonus);
    void set_last_spell_health_bonus_percent(const int bonus_percent);
    void set_last_spell_health_bonus(const double bonus);
    void set_last_spell_damage_bonus_percent(const int bonus_percent);
    void set_last_spell_damage_bonus(const double bonus);
    void ortho_moves_remaining(const int n_moves);
    void diag_moves_remaining(const int n_moves);
    void attacks_remaining(const int n_attacks);
    void set_max_diag_target_range(const unsigned max_diag_target_range);
    void set_stunned(const std::pair<bool, int>& stunned);
    void set_stunned(const bool is_stunned, const int n_turns);
    void set_has_new_stun(bool has_new_stun);
    void set_immovable(const std::pair<bool, int>& immovable);
    void set_immovable(const bool is_immovable, const int n_turns);
    void set_has_new_immovable(bool has_new_immovable);

    /**
     * Piece action methods
     **/

    void activate_target_damage_bonus(Piece* target);

    void reset_target_damage_bonus();

    // Moves piece to given board index
    void move(const std::pair<unsigned, unsigned>& new_board_index);

    // Update moves remaining with respect to board index changes
    void update_moves_remaining(
        const std::pair<unsigned, unsigned>& old_board_index,
        const std::pair<unsigned, unsigned>& new_board_index
    );

    // Piece deals damage to a target piece
    void attack(Piece* target);

    // Check if piece has died
    bool is_dead() const;

    // Updates immovable status
    void update_mobility();

    // Updates stun status
    void update_stun_status();

    // Updates attacks and moves remaining
    void update_actions();

    // Helper method for converting piece type into string
    std::string type_str(Type piece_Type) const;

    // Helper method for converting piece type into Qstring
    QString type_Qstr(Type piece_Type) const;

    // Get piece's current info
    std::string info() const;

    // Get piece's current info as a QString
    QString Qinfo() const;

    // Get piece's default info
    std::string default_info() const;

    // Get piece's default info as a QString
    QString default_Qinfo() const;


protected:
    Type m_type;
    Player::Index m_owner;
    std::pair<unsigned, unsigned> m_board_index;
    double m_health;
    double m_damage;
    int m_last_field_health_bonus_percent;
    double m_last_field_health_bonus;
    int m_last_field_damage_bonus_percent;
    double m_last_field_damage_bonus;
    double m_last_target_damage_bonus;
    int m_last_spell_health_bonus_percent;
    double m_last_spell_health_bonus;
    int m_last_spell_damage_bonus_percent;
    double m_last_spell_damage_bonus;
    int m_ortho_moves_remaining;
    int m_diag_moves_remaining;
    int m_attacks_remaining;
    std::pair<bool, int> m_stunned;
    bool m_has_new_stun;
    std::pair<bool, int> m_immovable;
    bool m_has_new_immovable;
    std::map<std::string, int> m_spell_effects;
    std::map<std::string, int> m_cooldowns;

    /**
     * Constructor parameters are:
     * 1) Reference to game board
     * 2) player index (1 or 2), i.e owner of the piece
     * 3) board index where the piece is (either pair or pointer to Field)
     **/
    Piece();
    Piece(Player::Index owner, const std::pair<unsigned, unsigned>& board_index);

    virtual void init_default_values() = 0;

    std::string stunned_info() const;
    std::string immovable_info() const;

private:
//
};


#endif // PIECE_HPP
