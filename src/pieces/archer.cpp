#include "archer.hpp"


std::string Archer::default_info()
{
    std::stringstream info_stream;

    info_stream << "Archer" << "\n----------------------------";
    info_stream << "\nHealth: " << static_cast<unsigned>(Archer::Default::HEALTH);
    info_stream << "\nDamage: " << static_cast<unsigned>(Archer::Default::DAMAGE);
    info_stream << "\nMax attacks: " << static_cast<unsigned>(Archer::Default::MAX_ATTACKS);
    info_stream << "\nMax ortho moves: " << static_cast<unsigned>(Archer::Default::MAX_ORTHO_MOVES);
    info_stream << "\nMax diag moves: " << static_cast<unsigned>(Archer::Default::MAX_DIAG_MOVES);
    info_stream << "\nMax ortho target range: " << static_cast<unsigned>(Archer::Default::MAX_ORTHO_TARGET_RANGE);
    info_stream << "\nMax diag target range: " << static_cast<unsigned>(Archer::Default::MAX_DIAG_TARGET_RANGE);
    info_stream << "\n";

    return info_stream.str();
}

QString Archer::default_Qinfo()
{
    return QString(default_info().data());
}

std::vector<std::map<QString, QString>> Archer::spells_info()
{
    std::vector<std::map<QString, QString>> spell_map_vec;
    std::map<QString, QString> spell_map;
    QString spell_info_str;

    spell_info_str += QString("Next attack deals double damage, but archer is stunned for ");
    spell_info_str += (QString::number(static_cast<unsigned>(Archer::SpellData::STEADY_AIM_SELF_STUN_DURATION)) + QString(" turn.\n"));
    spell_info_str += (QString("* Cooldown: ") + QString::number(static_cast<int>(Archer::SpellCooldown::STEADY_AIM)) + QString("\n"));
    spell_info_str += (QString("* Cost: ") +  QString::number(static_cast<int>(Archer::SpellCost::STEADY_AIM)));
    spell_info_str += QString("\n");

    spell_map[QString("STEADY AIM")]=spell_info_str;
    spell_map_vec.push_back(spell_map);

    return spell_map_vec;
}


std::string Archer::field_bonus_info()
{
    std::stringstream info_stream;

    info_stream << "Field bonus" << "\n----------------------------------\n";
    info_stream << "* Ground: ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::GROUND_HEALTH)) << "% HP, ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::GROUND_DAMAGE)) << "% DMG\n";
    info_stream << "* Plain: ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::PLAIN_HEALTH)) << "% HP, ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::PLAIN_DAMAGE)) << "% DMG\n";
    info_stream << "* Hill: ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::HILL_HEALTH)) << "% HP, ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::HILL_DAMAGE)) << "% DMG\n";
    info_stream << "* Forest: ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::FOREST_HEALTH)) << "% HP, ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::FOREST_DAMAGE)) << "% DMG\n";
    info_stream << "* Bridge: ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::BRIDGE_HEALTH)) << "% HP, ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::BRIDGE_DAMAGE)) << "% DMG";
    info_stream << "\n";

    return info_stream.str();
}

QString Archer::field_bonus_Qinfo()
{
    return QString(field_bonus_info().data());
}

std::string Archer::target_bonus_info()
{
    std::stringstream info_stream;

    info_stream << "Target bonus" << "\n----------------------------------\n";
    info_stream << "* Archer: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::ARCHER_DAMAGE)) << "% DMG\n";
    info_stream << "* Castle: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::CASTLE_DAMAGE)) << "% DMG\n";
    info_stream << "* Catapult: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::CATAPULT_DAMAGE)) << "% DMG\n";
    info_stream << "* Horseman: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::HORSEMAN_DAMAGE)) << "% DMG\n";
    info_stream << "* Knight: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::KNIGHT_DAMAGE)) << "% DMG\n";
    info_stream << "* Mage: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::MAGE_DAMAGE)) << "% DMG\n";
    info_stream << "* Priest: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::PRIEST_DAMAGE)) << "% DMG\n";
    info_stream << "* Shieldman: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::SHIELDMAN_DAMAGE)) << "% DMG\n";
    info_stream << "* Spearman: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::SPEARMAN_DAMAGE)) << "% DMG";
    info_stream << "\n";

    return info_stream.str();
}

QString Archer::target_bonus_Qinfo()
{
    return QString(target_bonus_info().data());
}

std::pair<int, int> Archer::ground_bonuses()
{
    return std::make_pair(
        static_cast<int>(Archer::FieldBonus::GROUND_HEALTH),
        static_cast<int>(Archer::FieldBonus::GROUND_DAMAGE)
    );
}

std::pair<int, int> Archer::plain_bonuses()
{
    return std::make_pair(
        static_cast<int>(Archer::FieldBonus::PLAIN_HEALTH),
        static_cast<int>(Archer::FieldBonus::PLAIN_DAMAGE)
    );
}

std::pair<int, int> Archer::hill_bonuses()
{
    return std::make_pair(
        static_cast<int>(Archer::FieldBonus::HILL_HEALTH),
        static_cast<int>(Archer::FieldBonus::HILL_DAMAGE)
    );
}

std::pair<int, int> Archer::forest_bonuses()
{
    return std::make_pair(
        static_cast<int>(Archer::FieldBonus::FOREST_HEALTH),
        static_cast<int>(Archer::FieldBonus::FOREST_DAMAGE)
    );
}

std::pair<int, int> Archer::bridge_bonuses()
{
    return std::make_pair(
        static_cast<int>(Archer::FieldBonus::BRIDGE_HEALTH),
        static_cast<int>(Archer::FieldBonus::BRIDGE_DAMAGE)
    );
}

QRectF Archer::boundingRect() const
{
    return QRectF(0,0,40,40);
}

const QRect Archer::get_rect() const
{
    return QRect(0,0,40,40);
}

void Archer::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);
    if(Player::Index::PLAYER_1 == m_owner)
        painter->drawPixmap(get_rect(),QPixmap(Archer::IMAGE_RED_QPATH));
    else
        painter->drawPixmap(get_rect(),QPixmap(Archer::IMAGE_BLUE_QPATH));
}

Archer::Archer() = default;

Archer::Archer(Player::Index owner, std::pair<unsigned, unsigned> board_index)
    : Piece(owner, board_index)
{
    init_default_values();
}

unsigned Archer::get_default_health() const 
{
    return static_cast<unsigned>(Archer::Default::HEALTH);
}

unsigned Archer::get_default_damage() const 
{
    return static_cast<unsigned>(Archer::Default::DAMAGE);
}

unsigned Archer::get_max_attacks() const
{
    return static_cast<unsigned>(Archer::Default::MAX_ATTACKS);
}

unsigned Archer::get_max_ortho_moves() const
{
    return static_cast<unsigned>(Archer::Default::MAX_ORTHO_MOVES);
}

unsigned Archer::get_max_diag_moves() const
{
    return static_cast<unsigned>(Archer::Default::MAX_DIAG_MOVES);
}

unsigned Archer::get_max_ortho_target_range() const
{
    return static_cast<unsigned>(Archer::Default::MAX_ORTHO_TARGET_RANGE);
}

unsigned Archer::get_max_diag_target_range() const
{
    return static_cast<unsigned>(Archer::Default::MAX_DIAG_TARGET_RANGE);
}

unsigned Archer::get_max_ortho_spells_range(int spell_index) const
{
    return static_cast<unsigned>(Archer::SpellRange::STEADY_AIM_ORTHO);
}

unsigned Archer::get_max_diag_spells_range(int spell_index) const
{
    return static_cast<unsigned>(Archer::SpellRange::STEADY_AIM_DIAG);
}

bool Archer::get_use_g_fields(int spell_index) const
{
   return static_cast<bool>(Archer::SpellRange::STEADY_AIM_USE_G_FIELDS);
}

int Archer::get_target_damage_bonus(Piece::Type piece_type) const
{
   switch (piece_type) {
       case Piece::Type::ARCHER:
           return static_cast<int>(Archer::TargetBonus::ARCHER_DAMAGE);
       case Piece::Type::CASTLE:
           return static_cast<int>(Archer::TargetBonus::CASTLE_DAMAGE);
       case Piece::Type::CATAPULT:
           return static_cast<int>(Archer::TargetBonus::CATAPULT_DAMAGE);
       case Piece::Type::HORSEMAN:
           return static_cast<int>(Archer::TargetBonus::HORSEMAN_DAMAGE);
       case Piece::Type::KNIGHT:
           return static_cast<int>(Archer::TargetBonus::KNIGHT_DAMAGE);
       case Piece::Type::MAGE:
           return static_cast<int>(Archer::TargetBonus::MAGE_DAMAGE);
       case Piece::Type::PRIEST:
           return static_cast<int>(Archer::TargetBonus::PRIEST_DAMAGE);
       case Piece::Type::SHIELDMAN:
           return static_cast<int>(Archer::TargetBonus::SHIELDMAN_DAMAGE);
       case Piece::Type::SPEARMAN:
           return static_cast<int>(Archer::TargetBonus::SPEARMAN_DAMAGE);
       default:
           throw UNKNOWN_CLASS_ERROR;
   }
}

std::pair<int, int> Archer::get_ground_bonuses() const
{
    return std::make_pair(
        static_cast<int>(Archer::FieldBonus::GROUND_HEALTH),
        static_cast<int>(Archer::FieldBonus::GROUND_DAMAGE)
    );
}

std::pair<int, int> Archer::get_plain_bonuses() const
{
    return std::make_pair(
        static_cast<int>(Archer::FieldBonus::PLAIN_HEALTH),
        static_cast<int>(Archer::FieldBonus::PLAIN_DAMAGE)
    );
}

std::pair<int, int> Archer::get_hill_bonuses() const
{
    return std::make_pair(
        static_cast<int>(Archer::FieldBonus::HILL_HEALTH),
        static_cast<int>(Archer::FieldBonus::HILL_DAMAGE)
    );
}

std::pair<int, int> Archer::get_forest_bonuses() const
{
    return std::make_pair(
        static_cast<int>(Archer::FieldBonus::FOREST_HEALTH),
        static_cast<int>(Archer::FieldBonus::FOREST_DAMAGE)
    );
}

std::pair<int, int> Archer::get_bridge_bonuses() const
{
    return std::make_pair(
        static_cast<int>(Archer::FieldBonus::BRIDGE_HEALTH),
        static_cast<int>(Archer::FieldBonus::BRIDGE_DAMAGE)
    );
}

std::string Archer::cooldowns_info() const
{
    std::stringstream info_stream;
    info_stream << "Cooldowns" << "\n-----------------------------\n";

    if (has_cooldowns()) {
        for (auto& [spell, cooldown] : m_cooldowns) {
            info_stream << "* " << spell << ": ";
            info_stream << std::to_string(cooldown) << "\n";
        }
    } else {
        info_stream << "No cooldowns.";
    }

    return info_stream.str();
}

QString Archer::cooldowns_Qinfo() const
{
    return QString(cooldowns_info().data());
}

std::vector<QString> Archer::spell_names() const
{
  return { spell_to_Qstring(Archer::Spell::STEADY_AIM) };
}

std::vector<unsigned> Archer::spell_costs() const
{
    std::vector<unsigned> costs;
    costs.push_back(static_cast<unsigned>(SpellCost::STEADY_AIM));
    return costs;
}

void Archer::_accept_visit(PieceVisitor& visitor)
{
    visitor.visit_archer(this);
}

std::map<QString, QString> Archer::default_values()
{
    std::map<QString,QString> default_values;
    default_values[QString("HEALTH")]=QString::number(static_cast<int>(Default::HEALTH));
    default_values[QString("DAMAGE")] = QString::number(static_cast<int>(Default::DAMAGE));
    default_values[QString("MAX_ORTHO_MOVES")] = QString::number(static_cast<int>(Default::MAX_ORTHO_MOVES));
    default_values[QString("MAX_DIAG_MOVES")] = QString::number(static_cast<int>(Default::MAX_DIAG_MOVES));
    default_values[QString("MAX_ATTACKS")] = QString::number(static_cast<int>(Default::MAX_ATTACKS));
    default_values[QString("MAX_ORTHO_TARGET_RANGE")] = QString::number(static_cast<int>(Default::MAX_ORTHO_TARGET_RANGE));
    default_values[QString("MAX_DIAG_TARGET_RANGE")] = QString::number(static_cast<int>(Default::MAX_DIAG_TARGET_RANGE));

    default_values[QString("GROUND_HEALTH")] = QString::number(static_cast<int>(FieldBonus::GROUND_HEALTH));
    default_values[QString("GROUND_DAMAGE")] = QString::number(static_cast<int>(FieldBonus::GROUND_DAMAGE));
    default_values[QString("PLAIN_HEALTH")] = QString::number(static_cast<int>(FieldBonus::PLAIN_HEALTH));
    default_values[QString("PLAIN_DAMAGE")] = QString::number(static_cast<int>(FieldBonus::PLAIN_DAMAGE));
    default_values[QString("FOREST_HEALTH")] = QString::number(static_cast<int>(FieldBonus::FOREST_HEALTH));
    default_values[QString("FOREST_DAMAGE")] = QString::number(static_cast<int>(FieldBonus::FOREST_DAMAGE));
    default_values[QString("BRIDGE_HEALTH")] = QString::number(static_cast<int>(FieldBonus::BRIDGE_HEALTH));
    default_values[QString("BRIDGE_DAMAGE")] = QString::number(static_cast<int>(FieldBonus::BRIDGE_DAMAGE));
    default_values[QString("HILL_HEALTH")] = QString::number(static_cast<int>(FieldBonus::HILL_HEALTH));
    default_values[QString("HILL_DAMAGE")] = QString::number(static_cast<int>(FieldBonus::HILL_DAMAGE));

    default_values[QString("KNIGHT_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::KNIGHT_DAMAGE));
    default_values[QString("SPEARMAN_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::SPEARMAN_DAMAGE));
    default_values[QString("SHIELDMAN_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::SHIELDMAN_DAMAGE));
    default_values[QString("HORSEMAN_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::HORSEMAN_DAMAGE));
    default_values[QString("ARCHER_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::ARCHER_DAMAGE));
    default_values[QString("CATAPULT_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::CATAPULT_DAMAGE));
    default_values[QString("MAGE_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::MAGE_DAMAGE));
    default_values[QString("PRIEST_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::PRIEST_DAMAGE));
    default_values[QString("CASTLE_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::CASTLE_DAMAGE));

    return default_values;
}

std::string Archer::spell_to_string(Archer::Spell spell) const
{
    return "Steady Aim";
}

QString Archer::spell_to_Qstring(Archer::Spell spell) const
{
    return QString("Steady Aim");
}

unsigned Archer::get_spell_cooldown(Archer::Spell spell) const
{
    return static_cast<unsigned>(Archer::SpellCost::STEADY_AIM);
}

unsigned Archer::get_spell_cooldown_cost(Archer::Spell spell) const
{
    return static_cast<unsigned>(Archer::SpellCost::STEADY_AIM);
}

std::vector<Archer::Spell> Archer::get_spells() const
{
    return { Archer::Spell::STEADY_AIM };
}

void Archer::init_default_values()
{
    m_type = Piece::Type::ARCHER;
    m_health = static_cast<double>(Archer::Default::HEALTH);
    m_damage = static_cast<double>(Archer::Default::DAMAGE);
    m_ortho_moves_remaining = static_cast<int>(Archer::Default::MAX_ORTHO_MOVES);
    m_diag_moves_remaining = static_cast<int>(Archer::Default::MAX_DIAG_MOVES);
    m_attacks_remaining = static_cast<int>(Archer::Default::MAX_ATTACKS);
}
