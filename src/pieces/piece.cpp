#include "piece.hpp"

// Note. This method is identical to SpellHandler::spell_effect_to_str and MUST REMAIN THE SAME
// due to maps that save spell effects as string, and it is to avoid collision
std::string Piece::spell_effect_to_str(Piece::SpellEffect spell_effect)
{
    switch (spell_effect) {
        case Piece::SpellEffect::FIREBOLT_DOT:
            return "FIREBOLT_DOT";
        case Piece::SpellEffect::EMPOWER_BONUS:
            return "EMPOWER_BONUS";
        case Piece::SpellEffect::HOLY_SHIELD_IMMUNITY:
            return "HOLLY_SHIELD_IMMUNITY";
        case Piece::SpellEffect::TURTLE_BONUS:
            return "TURTLE_BONUS";
        case Piece::SpellEffect::DEADLY_POISON:
            return "DEADLY_POISON";
        default:
            throw "Unknown spell effect";
    }
}

Piece::Piece() = default;

Piece::Piece(
    Player::Index owner, 
    const std::pair<unsigned, unsigned>& board_index
) : m_type(Piece::Type::_NO_TYPE)
  , m_owner(owner)
  , m_board_index(board_index)
  , m_health(0.0)
  , m_damage(0.0)
  , m_last_field_health_bonus_percent(0)
  , m_last_field_health_bonus(0.0)
  , m_last_field_damage_bonus_percent(0)
  , m_last_field_damage_bonus(0.0)
  , m_last_target_damage_bonus(0.0)
  , m_last_spell_health_bonus(0.0)
  , m_last_spell_health_bonus_percent(0)
  , m_last_spell_damage_bonus(0.0)
  , m_last_spell_damage_bonus_percent(0)
  , m_ortho_moves_remaining(0)
  , m_diag_moves_remaining(0)
  , m_attacks_remaining(0)
  , m_stunned({false, 0})
  , m_has_new_stun(false)
  , m_immovable({false, 0})
  , m_has_new_immovable(false)
{     
    Piece::num_of_pieces++;   
}

const std::map<QString, QString> Piece::values() const
{
    std::map<QString,QString> values;
    values[QString("Type")] = QString(type_str(m_type).c_str());
    values[QString("Health")] = QString::number(static_cast<double>(m_health),'f',2);
    values[QString("Damage")] = QString::number(static_cast<double>(m_damage),'f',2);
    values[QString("Last field health bonus")] = QString::number(static_cast<double>(m_last_field_health_bonus),'f',2);
    values[QString("Last field damage bonus")] = QString::number(static_cast<double>(m_last_field_damage_bonus),'f',2);
    values[QString("Attacks remaining")] = QString::number(static_cast<unsigned>(m_attacks_remaining));
    values[QString("Ortho moves remaining")] = QString::number(static_cast<unsigned>(m_ortho_moves_remaining));
    values[QString("Diag moves remaining")] = QString::number(static_cast<unsigned>(m_diag_moves_remaining));
    values[QString("Stunned")] = QString(stunned_info().c_str());
    values[QString("Immovable")] = QString(immovable_info().c_str());

    return values;
}

Piece::~Piece()
{
    Piece::num_of_pieces--;
}

/**
 * Geters
 */
Piece::Type Piece::piece_type() const
{
    return m_type;
}

Player::Index Piece::owner() const
{
    return m_owner;
}

const std::pair<unsigned, unsigned>& Piece::board_index() const
{
    return m_board_index;
}

unsigned Piece::at_row() const
{
    return m_board_index.first;
}

unsigned Piece::at_col() const
{
    return m_board_index.second;
}

double Piece::health() const
{
    return m_health;
}

double Piece::damage() const
{
    return m_damage;
}

int Piece::last_field_health_bonus_percent() const
{
    return m_last_field_health_bonus_percent;
}

double Piece::last_field_health_bonus() const
{
    return m_last_field_health_bonus;
}

int Piece::last_field_damage_bonus_percent() const
{
    return m_last_field_damage_bonus_percent;
}

double Piece::last_field_damage_bonus() const
{
    return m_last_field_damage_bonus;
}

double Piece::last_target_damage_bonus() const
{
    return m_last_target_damage_bonus;
}

double Piece::last_spell_health_bonus() const
{
    return m_last_spell_health_bonus;
}

double Piece::last_spell_damage_bonus() const
{
    return m_last_spell_damage_bonus;
}

int Piece::ortho_moves_remaining() const
{
    return m_ortho_moves_remaining;
}

int Piece::diag_moves_remaining() const
{
    return m_diag_moves_remaining;
}

int Piece::attacks_remaining() const
{
    return m_attacks_remaining;
}

const std::pair<bool, int>& Piece::stunned() const
{
    return m_stunned;
}

bool Piece::is_stunned() const
{
    return m_stunned.first;
}

bool Piece::has_new_stun() const
{
    return m_has_new_stun;
}

const std::pair<bool, int>& Piece::immovable() const
{
    return m_immovable;
}

bool Piece::is_immovable() const
{
    return m_immovable.first;
}

bool Piece::has_new_immovable() const
{
    return m_has_new_immovable;
}

std::map<std::string, int> &Piece::spell_effects()
{
    return m_spell_effects;
}

bool Piece::has_spell_effects() const
{
    return !m_spell_effects.empty();
}

bool Piece::has_spell_effect(const std::string& spell_effect_str) const
{
    return m_spell_effects.find(spell_effect_str) != m_spell_effects.end();
}

void Piece::add_spell_effect(const std::string& spell_effect_str, int duration)
{
    m_spell_effects[spell_effect_str] = duration;
}

void Piece::remove_spell_effect(const std::string& spell_effect_str)
{
    m_spell_effects.erase(spell_effect_str);
}

void Piece::update_spell_effect(const std::string& spell_effect_str)
{
    auto it = m_spell_effects.find(spell_effect_str);
    if (it != m_spell_effects.end()) {
        it->second = it->second - 1;

        // If Spell effect is firebolt, reduce piece hp
        if (spell_effect_str == Piece::spell_effect_to_str(Piece::SpellEffect::FIREBOLT_DOT)) {
            if (!is_immune()) {
                double fb_dot_dmg = static_cast<double>(Piece::SpellEffectCache::FIREBOLT_DOT_DMG);
                m_health -= fb_dot_dmg;
            }
        } else if (spell_effect_str == Piece::spell_effect_to_str(Piece::SpellEffect::DEADLY_POISON)) {
            if (!is_immune()) {
                double poison_dot_dmg = static_cast<double>(Card::DeadlyPoison::DAMAGE_PER_TURN);
                m_health -= poison_dot_dmg;
            }
        }

        if (it->second <= 0) {
            remove_spell_effect(spell_effect_str);

            if (spell_effect_str == Piece::spell_effect_to_str(Piece::SpellEffect::EMPOWER_BONUS)) {
                if (m_last_spell_health_bonus >= m_health) {
                    double hp_bonus_percent = static_cast<double>(Piece::SpellEffectCache::EMPOWER_BONUS);
                    double hp_bonus = hp_bonus_percent / 100 * m_health;
                    m_health -= hp_bonus;
                } else {
                    m_health -= m_last_spell_health_bonus;
                }

                m_damage -= m_last_spell_damage_bonus;
            }
        }
    }
}

std::map<std::string, int>& Piece::cooldowns()
{
    return m_cooldowns;
}

bool Piece::has_cooldowns() const
{
    return !m_cooldowns.empty();
}

bool Piece::has_cooldown(const std::string& spell_str) const
{
    return m_cooldowns.find(spell_str) != m_cooldowns.end();
}

void Piece::add_cooldown(const std::string& spell_str, int duration)
{
    if (duration > 0) {
        m_cooldowns[spell_str] = duration;
    }
}

void Piece::remove_cooldown(const std::string& spell_str)
{
    m_cooldowns.erase(spell_str);
}

void Piece::update_cooldown(const std::string& spell_str)
{
    auto it = m_cooldowns.find(spell_str);
    if (it != m_cooldowns.end()) {
        it->second = it->second - 1;

        if (it->second <= 0) {
            remove_cooldown(spell_str);
        }
    }
}

bool Piece::is_immune() const
{
    std::string hs_immunity = Piece::spell_effect_to_str(Piece::SpellEffect::HOLY_SHIELD_IMMUNITY);
    return m_spell_effects.find(hs_immunity) != m_spell_effects.end();
}

/**
 * Seters
 **/
void Piece::set_board_index(const std::pair<unsigned, unsigned>& board_index)
{
    m_board_index = board_index;
}

void Piece::set_health(const double health)
{
    m_health = health;
}

void Piece::set_damage(const double damage)
{
    m_damage = damage;
}

void Piece::set_last_field_health_bonus_percent(const int bonus_percent)
{
    m_last_field_health_bonus_percent = bonus_percent;
}

void Piece::set_last_field_health_bonus(const double bonus)
{
    m_last_field_health_bonus = bonus;
}

void Piece::set_last_field_damage_bonus_percent(const int bonus_percent)
{
    m_last_field_damage_bonus_percent = bonus_percent;
}

void Piece::set_last_field_damage_bonus(const double bonus)
{
    m_last_field_damage_bonus = bonus;
}

void Piece::set_last_target_damage_bonus(const double bonus)
{
    m_last_target_damage_bonus = bonus;
}

void Piece::set_last_spell_health_bonus_percent(const int bonus_percent)
{
    m_last_spell_health_bonus_percent = bonus_percent;
}

void Piece::set_last_spell_health_bonus(const double bonus)
{
    m_last_spell_health_bonus = bonus;
}

void Piece::set_last_spell_damage_bonus_percent(const int bonus_percent)
{
    m_last_spell_damage_bonus_percent = bonus_percent;
}

void Piece::set_last_spell_damage_bonus(const double bonus)
{
    m_last_spell_damage_bonus = bonus;
}

void Piece::ortho_moves_remaining(const int n_moves)
{
    m_ortho_moves_remaining = n_moves;
}

void Piece::diag_moves_remaining(const int n_moves)
{
    m_diag_moves_remaining = n_moves;
}

void Piece::attacks_remaining(const int n_attacks)
{
    m_attacks_remaining = n_attacks;
}

void Piece::set_stunned(const std::pair<bool, int>& stunned)
{

    // If piece is not stunned (or is stunned and being freed by external source), assign value
    if (!m_stunned.first || stunned.first == false) {
        m_stunned = stunned;
    } // If piece was already stunned and new duration is greater, stun it and memorize new stun
    else if (stunned.second >= m_stunned.second) {
        m_stunned = stunned;
        m_has_new_stun = true;
    }
}

void Piece::set_stunned(const bool is_stunned, const int n_turns)
{
    // If piece is not stunned (or is stunned and being freed by external source), assign value
    if (!m_stunned.first || is_stunned == false) {
        m_stunned = {is_stunned, n_turns};
    } // If piece was already stunned and new duration is greater, stun it and memorize new stun
    else if (n_turns >= m_stunned.second) {
        m_stunned = {is_stunned, n_turns};
        m_has_new_stun = true;
    }
}

void Piece::set_has_new_stun(bool has_new_stun)
{
    m_has_new_stun = has_new_stun;
}

void Piece::set_immovable(const std::pair<bool, int>& immovable)
{
    // If piece is not immovable (or is immovable and being freed by external source), assign value
    if (!m_immovable.first || immovable.first == false) {
        m_immovable = immovable;
    } // If piece was already immovable and new duration is greater, make it immovable and memorize new stun
    else if (immovable.second >= m_immovable.second) {
        m_immovable = immovable;
        m_has_new_immovable = true;
    }
}

void Piece::set_immovable(const bool is_immovable, const int n_turns)
{
    // If piece is not immovable (or is immovable and being freed by external source), assign value
    if (!m_immovable.first || is_immovable == false) {
        m_immovable = {is_immovable, n_turns};
    } // If piece was already immovable and new duration is greater, make it immovable and memorize new stun
    else if (n_turns >= m_immovable.second) {
        m_immovable = {is_immovable, n_turns};
        m_has_new_immovable = true;
    }
}

void Piece::set_has_new_immovable(bool has_new_immovable)
{
    m_has_new_immovable = has_new_immovable;
}

/**
 * Piece utility methods
 **/

void Piece::activate_target_damage_bonus(Piece* target)
{
    int damage_bonus_percent = TargetDamageBonus(this, target);
    double damage_bonus = static_cast<double>(damage_bonus_percent) / 100 * m_damage;
    m_last_target_damage_bonus = damage_bonus;
    m_damage += damage_bonus;
}

void Piece::reset_target_damage_bonus()
{
    m_damage -= m_last_target_damage_bonus;
}

void Piece::update_moves_remaining(
    const std::pair<unsigned, unsigned>& old_board_index,
    const std::pair<unsigned, unsigned>& new_board_index
)
{
    int row_diff = std::abs(static_cast<int>(old_board_index.first) - static_cast<int>(new_board_index.first));
    int col_diff = std::abs(static_cast<int>(old_board_index.second) - static_cast<int>(new_board_index.second));
    int diff = row_diff + col_diff;
    
    if (diff == 1) {
        m_ortho_moves_remaining--;
    } else if (diff == 2) {
        m_diag_moves_remaining--;
    } else {
        throw "Incorrect piece move: piece hasn't moved at all or has surpassed moving capabilities.";
    }
}

void Piece::attack(Piece* target)
{
    if (m_attacks_remaining > 0) {
        // Activate damage bonus before attacking
        activate_target_damage_bonus(target);

        // Deal actual damage to target
        double target_new_health = target->health() - m_damage;
        target->set_health(target_new_health);

        // Reset damage bonus after attacking
        reset_target_damage_bonus();

        // Update remaining attacks
#ifndef __PIECE_TEST_ATTACK_INF__
        m_attacks_remaining--;
#endif
    } else {
        std::cerr << "no attacks remaining!" << std::endl;
    }
}

bool Piece::is_dead() const
{
    return m_health <= 0.0;
}

// Updates immovable and stunned status
void Piece::update_mobility()
{
    // If piece is already immovable
    if (m_immovable.first) {
        // Update cooldown, e.g on his turn this value reduces
        m_immovable.second--;

        // If immovable value has reached 0, piece is no longer immovable
        if (m_immovable.second <= 0) {
            m_immovable.first = false;
            m_has_new_immovable = false;
        }
    }
}

void Piece::update_stun_status()
{
    // If piece is already stunned
    if (m_stunned.first) {
        // Update cooldown, e.g on his turn this value reduces
        m_stunned.second--;

        // If stunned value has reached 0, piece is no longer stunned
        if (m_stunned.second <= 0) {
            m_stunned.first = false;
            m_has_new_stun = false;
        }
    }
}

// Updates attacks and moves remaining
void Piece::update_actions()
{
    // Remaining moves and attacks are always reseted to default values on player's turn
    m_ortho_moves_remaining = get_max_ortho_moves();
    m_diag_moves_remaining = get_max_diag_moves();
    m_attacks_remaining = get_max_attacks();
}

std::string Piece::type_str(Type piece_type) const
{
    switch (piece_type) {
        case Type::ARCHER:
            return std::string("Archer");
        case Type::CASTLE:
            return std::string("Castle");
        case Type::CATAPULT:
            return std::string("Catapult");
        case Type::HORSEMAN:
            return std::string("Horseman");
        case Type::KNIGHT:
            return std::string("Knight");
        case Type::MAGE:
            return std::string("Mage");
        case Type::PRIEST:
            return std::string("Priest");
        case Type::SHIELDMAN:
            return std::string("Shieldman");
        case Type::SPEARMAN:
            return std::string("Spearman");
        case Type::_NO_TYPE:
            return std::string("[NO TYPE]");
        default:
            throw UNKNOWN_CLASS_ERROR;
    }
}

QString Piece::type_Qstr(Type piece_type) const
{
        switch (piece_type) {
        case Type::ARCHER:
            return QString("Archer");
        case Type::CASTLE:
            return QString("Castle");
        case Type::CATAPULT:
            return QString("Catapult");
        case Type::HORSEMAN:
            return QString("Horseman");
        case Type::KNIGHT:
            return QString("Knight");
        case Type::MAGE:
            return QString("Mage");
        case Type::PRIEST:
            return QString("Priest");
        case Type::SHIELDMAN:
            return QString("Shieldman");
        case Type::SPEARMAN:
            return QString("Spearman");
        case Type::_NO_TYPE:
            return QString("[NO TYPE]");
        default:
            throw UNKNOWN_CLASS_ERROR;
    }
}

std::string Piece::info() const
{
    std::stringstream info_stream;
    info_stream << std::fixed << std::setprecision(2);

    info_stream << type_str(m_type) << "\n---------------------------";
    info_stream << "\n* Health: " << m_health;
    info_stream << " (" << bonus_info_check_sign(m_last_field_health_bonus) << " FB)";
    info_stream << "\n* Damage: " << m_damage;
    info_stream << " (" << bonus_info_check_sign(m_last_field_damage_bonus) << " FB)";
    info_stream << "\n* Attacks remaining: " << m_attacks_remaining;
    info_stream << "\n* Ortho moves remaining: " << m_ortho_moves_remaining;
    info_stream << "\n* Diag moves remaining: " << m_diag_moves_remaining;
    info_stream << "\n* Stunned: " << stunned_info();
    info_stream << "\n* Immovable: " << immovable_info();
    info_stream << "\n";

    return info_stream.str();
}

QString Piece::Qinfo() const
{
    return QString(info().data());
}

std::string Piece::default_info() const 
{
    std::stringstream info_stream;
    info_stream << std::fixed << std::setprecision(2);

    info_stream << type_str(m_type) << "\n----------------------------";
    info_stream << "\n* Health: " << get_default_health();
    info_stream << "\n* Damage: " << get_default_damage();
    info_stream << "\n* Max attacks: " << get_max_attacks();
    info_stream << "\n* Max ortho moves: " << get_max_ortho_moves();
    info_stream << "\n* Max diag moves: " << get_max_ortho_moves();
    info_stream << "\n* Max ortho target range: " << get_max_ortho_target_range();
    info_stream << "\n* Max diag target range: " << get_max_ortho_target_range();
    info_stream << "\n";

    return info_stream.str();
}

QString Piece::default_Qinfo() const
{
    return QString(default_info().data());
}

std::string Piece::stunned_info() const 
{
    std::stringstream info_stream;
    if (m_stunned.first) {
        info_stream << "Yes (" << m_stunned.second << " turns)";
    } else {
        info_stream << "No";
    }

    return info_stream.str();
}

std::string Piece::immovable_info() const
{
    std::stringstream info_stream;
    if (m_immovable.first) {
        info_stream << "Yes (" << m_immovable.second << " turns)";
    } else {
        info_stream << "No";
    }

    return info_stream.str();
}
