#ifndef MAGE_HPP
#define MAGE_HPP

#include <map>
#include <string>
#include <QString>
#include "piece.hpp"
#include "../utility/piecevisitor.hpp"
#include "../gui/board.hpp"


class Mage : public Piece
{
public:
    // Mage special abilites
    enum class Spell {
        FIREBOLT,
        FROSTBOLT,
        ARCANEBOLT,
        SHOCKWAVE
    };

    // Mage cost to special abilites (measured in turn steps)
    enum class SpellCost {
        FIREBOLT = 1,
        FROSTBOLT = 1,
        ARCANEBOLT = 2,
        SHOCKWAVE = 3
    };

    // Mage cooldowns on special abilites measured in turns
    enum class SpellCooldown {
        FIREBOLT = 2,
        FROSTBOLT = 2,
        ARCANEBOLT = 2,
        SHOCKWAVE = 3
    };

    // Mage spells have their specific range
    enum class SpellRange {
        FIREBOLT_ORTHO = 2,
        FIREBOLT_DIAG = 1,
        FIREBOLT_USE_G_FIELDS = 1,
        FROSTBOLT_ORTHO = 2,
        FROSTBOLT_DIAG = 1,
        FROSTBOLT_USE_G_FIELDS = 1,
        ARCANEBOLT_ORTHO = 2,
        ARCANEBOLT_DIAG = 1,
        ARCANEBOLT_USE_G_FIELDS = 0,
        SHOCKWAVE_ORTHO = 2,
        SHOCKWAVE_DIAG = 1,
        SHOCKWAVE_USE_G_FIELDS = 1
    };

    // Stores special data values for different spells
    enum class SpellData {
        FIREBOLT_DMG = 40,
        FIREBOLT_DOT_DMG = 10,
        FIREBOLT_DURATION = 3,
        FROSTBOLT_DMG = 45,
        FROSTBOLT_FREEZE_DURATION = 3,
        ARCANEBOLT_DMG = 35,
        ARCANEBOLT_PUSH_ORTHO_RANGE = 2,
        ARCANEBOLT_PUSH_DIAG_RANGE = 1,
        SHOCKWAVE_DMG = 30,
    };

    // Mage default health and damage
    enum class Default {
        HEALTH = 70,
        DAMAGE = 15,
        MAX_ORTHO_MOVES = 2,
        MAX_DIAG_MOVES = 0,
        MAX_ATTACKS = 2,
        MAX_ORTHO_TARGET_RANGE = 2,
        MAX_DIAG_TARGET_RANGE = 1
    };

    // Mage has bonus to health and damage (in %) based on type of the field it stands on
    enum class FieldBonus {
        GROUND_HEALTH = 0,
        GROUND_DAMAGE = 0,
        PLAIN_HEALTH = -15,
        PLAIN_DAMAGE = -15,
        HILL_HEALTH = 10,
        HILL_DAMAGE = 20,
        FOREST_HEALTH = 30,
        FOREST_DAMAGE = 25,
        BRIDGE_HEALTH = -30,
        BRIDGE_DAMAGE = -20,
        RBRIDGE_HEALTH = -30,
        RBRIDGE_DAMAGE = -20,
    };

    // Mage has bonus damage (in %) against some enemies
    enum class TargetBonus {
        KNIGHT_DAMAGE = -20,
        SPEARMAN_DAMAGE = -10,
        SHIELDMAN_DAMAGE = 40,
        HORSEMAN_DAMAGE = 20,
        ARCHER_DAMAGE = -10,
        CATAPULT_DAMAGE = 0,
        MAGE_DAMAGE = 0,
        PRIEST_DAMAGE = 0,
        CASTLE_DAMAGE = -50,
    };

    // Mage images paths
    static inline const std::string IMAGE_RED_PATH =":/images/pieces/mage_red_new.png";
    static inline const QString IMAGE_RED_QPATH = QString(":/images/pieces/mage_red_new.png");
    static inline const std::string IMAGE_BLUE_PATH =":/images/pieces/mage_blue_new.png";
    static inline const QString IMAGE_BLUE_QPATH = QString(":/images/pieces/mage_blue_new.png");

    // Get info about piece stats
    static std::string default_info();
    static QString default_Qinfo();
    static std::vector<std::map<QString, QString>> spells_info();
    static std::string field_bonus_info();
    static QString field_bonus_Qinfo();
    static std::string target_bonus_info();
    static QString target_bonus_Qinfo();
    static std::map<QString,QString> default_values();

    static std::pair<int, int> ground_bonuses();
    static std::pair<int, int> plain_bonuses();
    static std::pair<int, int> hill_bonuses();
    static std::pair<int, int> forest_bonuses();
    static std::pair<int, int> bridge_bonuses();

    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
    const QRect get_rect() const override;

    // Constructor is identical to that of base class (for now)
    Mage();
    Mage(Player::Index owner, std::pair<unsigned, unsigned> board_index);

    // Overriding virtual methods
    unsigned get_default_health() const override;
    unsigned get_default_damage() const override;
    unsigned get_max_attacks() const override;
    unsigned get_max_ortho_moves() const override;
    unsigned get_max_diag_moves() const override;
    unsigned get_max_ortho_target_range() const override;
    unsigned get_max_diag_target_range() const override;
    unsigned get_max_ortho_spells_range(int spell_index) const override;
    unsigned get_max_diag_spells_range(int spell_index) const override;
    bool get_use_g_fields(int spell_index) const override;
    int get_target_damage_bonus(Piece::Type piece_type) const override;

    std::pair<int, int> get_ground_bonuses() const override;
    std::pair<int, int> get_plain_bonuses() const override;
    std::pair<int, int> get_hill_bonuses() const override;
    std::pair<int, int> get_forest_bonuses() const override;
    std::pair<int, int> get_bridge_bonuses() const override;

    // Spell-related methods
    std::string cooldowns_info() const override;
    QString cooldowns_Qinfo() const override;
    std::vector<QString> spell_names() const override;
    std::vector<unsigned> spell_costs() const override;
    void _accept_visit(PieceVisitor& visitor) override;

    // Converts spell enum to string
    std::string spell_to_string(Spell spell) const;

    // Converts spell enum to QString
    QString spell_to_Qstring(Spell spell) const;

    // Helper method for extracting spell cooldown from enum
    unsigned get_spell_cooldown(Spell spell) const;

    // Helper method for extracting spell cost from enum
    unsigned get_spell_cooldown_cost(Spell spell) const;

    // Get all mage spells
    std::vector<Spell> get_spells() const;


private:
    void init_default_values() override;
};

#endif // MAGE_HPP
