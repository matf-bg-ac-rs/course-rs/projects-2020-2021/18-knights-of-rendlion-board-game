#include "knight.hpp"


std::string Knight::default_info()
{
    std::stringstream info_stream;

    info_stream << "Knight" << "\n----------------------------";
    info_stream << "\n* Health: " << static_cast<unsigned>(Knight::Default::HEALTH);
    info_stream << "\n* Damage: " << static_cast<unsigned>(Knight::Default::DAMAGE);
    info_stream << "\n* Max attacks: " << static_cast<unsigned>(Knight::Default::MAX_ATTACKS);
    info_stream << "\n* Max ortho moves: " << static_cast<unsigned>(Knight::Default::MAX_ORTHO_MOVES);
    info_stream << "\n* Max diag moves: " << static_cast<unsigned>(Knight::Default::MAX_DIAG_MOVES);
    info_stream << "\n* Max ortho target range: " << static_cast<unsigned>(Knight::Default::MAX_ORTHO_TARGET_RANGE);
    info_stream << "\n* Max diag target range: " << static_cast<unsigned>(Knight::Default::MAX_DIAG_TARGET_RANGE);
    info_stream << "\n";

    return info_stream.str();
}

QString Knight::default_Qinfo()
{
    return QString(default_info().data());
}

std::vector<std::map<QString,QString>> Knight::spells_info()
{

    std::vector<std::map<QString, QString>> spell_map_vec;
    std::map<QString, QString> spell_map;
    QString spell_info_str;

    spell_info_str += QString("If enemy target has <= ");
    spell_info_str += (QString::number(static_cast<unsigned>(Knight::SpellData::MORTAL_STRIKE_KILL_TRESHOLD)) + QString(" HP, kill it instantly, but deal "));
    spell_info_str += (QString::number(static_cast<unsigned>(Knight::SpellData::MORTAL_STRIKE_SELF_DAMAGE)) + QString(" damage to self.\n"));
    spell_info_str += (QString("* Cooldown: ") + QString::number(static_cast<int>(Knight::SpellCooldown::MORTAL_STRIKE)) + QString("\n"));
    spell_info_str += (QString("* Cost: ") + QString::number(static_cast<int>(Knight::SpellCost::MORTAL_STRIKE)));
    spell_info_str += QString("\n");

    spell_map[QString("MORTAL STRIKE")]=spell_info_str;
    spell_map_vec.push_back(spell_map);

    return spell_map_vec;
}

std::string Knight::field_bonus_info()
{
    std::stringstream info_stream;

    info_stream << "Field bonus" << "\n----------------------------------\n";
    info_stream << "* Ground: ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::GROUND_HEALTH)) << "% HP, ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::GROUND_DAMAGE)) << "% DMG\n";
    info_stream << "* Plain: ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::PLAIN_HEALTH)) << "% HP, ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::PLAIN_DAMAGE)) << "% DMG\n";
    info_stream << "* Hill: ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::HILL_HEALTH)) << "% HP, ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::HILL_DAMAGE)) << "% DMG\n";
    info_stream << "* Forest: ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::FOREST_HEALTH)) << "% HP, ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::FOREST_DAMAGE)) << "% DMG\n";
    info_stream << "* Bridge: ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::BRIDGE_HEALTH)) << "% HP, ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::BRIDGE_DAMAGE)) << "% DMG";
    info_stream << "\n";

    return info_stream.str();
};

QString Knight::field_bonus_Qinfo()
{
    return QString(field_bonus_info().data());
}

std::string Knight::target_bonus_info()
{
    std::stringstream info_stream;

    info_stream << "Target bonus" << "\n----------------------------------\n";
    info_stream << "* Archer: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::ARCHER_DAMAGE)) << "% DMG\n";
    info_stream << "* Castle: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::CASTLE_DAMAGE)) << "% DMG\n";
    info_stream << "* Catapult: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::CATAPULT_DAMAGE)) << "% DMG\n";
    info_stream << "* Horseman: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::HORSEMAN_DAMAGE)) << "% DMG\n";
    info_stream << "* Knight: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::KNIGHT_DAMAGE)) << "% DMG\n";
    info_stream << "* Mage: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::MAGE_DAMAGE)) << "% DMG\n";
    info_stream << "* Priest: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::PRIEST_DAMAGE)) << "% DMG\n";
    info_stream << "* Shieldman: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::SHIELDMAN_DAMAGE)) << "% DMG\n";
    info_stream << "* Spearman: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::SPEARMAN_DAMAGE)) << "% DMG";
    info_stream << "\n";

    return info_stream.str();
}

QString Knight::target_bonus_Qinfo()
{
    return QString(target_bonus_info().data());
}

std::pair<int, int> Knight::ground_bonuses()
{
    return std::make_pair(
        static_cast<int>(Knight::FieldBonus::GROUND_HEALTH),
        static_cast<int>(Knight::FieldBonus::GROUND_DAMAGE)
    );
}

std::pair<int, int> Knight::plain_bonuses()
{
    return std::make_pair(
        static_cast<int>(Knight::FieldBonus::PLAIN_HEALTH),
        static_cast<int>(Knight::FieldBonus::PLAIN_DAMAGE)
    );
}

std::pair<int, int> Knight::hill_bonuses()
{
    return std::make_pair(
        static_cast<int>(Knight::FieldBonus::HILL_HEALTH),
        static_cast<int>(Knight::FieldBonus::HILL_DAMAGE)
    );
}

std::pair<int, int> Knight::forest_bonuses()
{
    return std::make_pair(
        static_cast<int>(Knight::FieldBonus::FOREST_HEALTH),
        static_cast<int>(Knight::FieldBonus::FOREST_DAMAGE)
    );
}

std::pair<int, int> Knight::bridge_bonuses()
{
    return std::make_pair(
        static_cast<int>(Knight::FieldBonus::BRIDGE_HEALTH),
        static_cast<int>(Knight::FieldBonus::BRIDGE_DAMAGE)
    );
}

QRectF Knight::boundingRect() const
{
    return QRectF(0,0,40,40);
}

const QRect Knight::get_rect() const
{
    return QRect(0,0,40,40);
}

void Knight::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);
    if(Player::Index::PLAYER_1 == m_owner)
        painter->drawPixmap(get_rect(),QPixmap(Knight::IMAGE_RED_QPATH));
    else
        painter->drawPixmap(get_rect(),QPixmap(Knight::IMAGE_BLUE_QPATH));
}

Knight::Knight() = default;

Knight::Knight(Player::Index owner, std::pair<unsigned, unsigned> board_index)
    : Piece(owner, board_index)
{
    init_default_values();
}

unsigned Knight::get_default_health() const 
{
    return static_cast<unsigned>(Knight::Default::HEALTH);
}

unsigned Knight::get_default_damage() const 
{
    return static_cast<unsigned>(Knight::Default::DAMAGE);
}

unsigned Knight::get_max_attacks() const
{
    return static_cast<unsigned>(Knight::Default::MAX_ATTACKS);
}

unsigned Knight::get_max_ortho_moves() const
{
    return static_cast<unsigned>(Knight::Default::MAX_ORTHO_MOVES);
}

unsigned Knight::get_max_diag_moves() const
{
    return static_cast<unsigned>(Knight::Default::MAX_DIAG_MOVES);
}

unsigned Knight::get_max_ortho_target_range() const
{
    return static_cast<unsigned>(Knight::Default::MAX_ORTHO_TARGET_RANGE);
}

unsigned Knight::get_max_diag_target_range() const
{
    return static_cast<unsigned>(Knight::Default::MAX_DIAG_TARGET_RANGE);
}

unsigned Knight::get_max_ortho_spells_range(int spell_index) const
{
    return static_cast<unsigned>(Knight::SpellRange::MORTAL_STRIKE_ORTHO);
}

unsigned Knight::get_max_diag_spells_range(int spell_index) const
{
    return static_cast<unsigned>(Knight::SpellRange::MORTAL_STRIKE_DIAG);
}

bool Knight::get_use_g_fields(int spell_index) const
{
    return static_cast<bool>(Knight::SpellRange::MORTAL_STRIKE_USE_G_FIELDS);

}

int Knight::get_target_damage_bonus(Piece::Type piece_type) const
{
   switch (piece_type) {
       case Piece::Type::ARCHER:
           return static_cast<int>(Knight::TargetBonus::ARCHER_DAMAGE);
       case Piece::Type::CASTLE:
           return static_cast<int>(Knight::TargetBonus::CASTLE_DAMAGE);
       case Piece::Type::CATAPULT:
           return static_cast<int>(Knight::TargetBonus::CATAPULT_DAMAGE);
       case Piece::Type::HORSEMAN:
           return static_cast<int>(Knight::TargetBonus::HORSEMAN_DAMAGE);
       case Piece::Type::KNIGHT:
           return static_cast<int>(Knight::TargetBonus::KNIGHT_DAMAGE);
       case Piece::Type::MAGE:
           return static_cast<int>(Knight::TargetBonus::MAGE_DAMAGE);
       case Piece::Type::PRIEST:
           return static_cast<int>(Knight::TargetBonus::PRIEST_DAMAGE);
       case Piece::Type::SHIELDMAN:
           return static_cast<int>(Knight::TargetBonus::SHIELDMAN_DAMAGE);
       case Piece::Type::SPEARMAN:
           return static_cast<int>(Knight::TargetBonus::SPEARMAN_DAMAGE);
       default:
           throw UNKNOWN_CLASS_ERROR;
   }
}

std::pair<int, int> Knight::get_ground_bonuses() const
{
    return std::make_pair(
        static_cast<int>(Knight::FieldBonus::GROUND_HEALTH),
        static_cast<int>(Knight::FieldBonus::GROUND_DAMAGE)
    );
}

std::pair<int, int> Knight::get_plain_bonuses() const
{
    return std::make_pair(
        static_cast<int>(Knight::FieldBonus::PLAIN_HEALTH),
        static_cast<int>(Knight::FieldBonus::PLAIN_DAMAGE)
    );
}

std::pair<int, int> Knight::get_hill_bonuses() const
{
    return std::make_pair(
        static_cast<int>(Knight::FieldBonus::HILL_HEALTH),
        static_cast<int>(Knight::FieldBonus::HILL_DAMAGE)
    );
}

std::pair<int, int> Knight::get_forest_bonuses() const
{
    return std::make_pair(
        static_cast<int>(Knight::FieldBonus::FOREST_HEALTH),
        static_cast<int>(Knight::FieldBonus::FOREST_DAMAGE)
    );
}

std::pair<int, int> Knight::get_bridge_bonuses() const
{
    return std::make_pair(
        static_cast<int>(Knight::FieldBonus::BRIDGE_HEALTH),
        static_cast<int>(Knight::FieldBonus::BRIDGE_DAMAGE)
    );
}

std::string Knight::cooldowns_info() const
{
    std::stringstream info_stream;
    info_stream << "Cooldowns" << "\n-----------------------------\n";

    if (has_cooldowns()) {
        for (auto& [spell, cooldown] : m_cooldowns) {
            info_stream << "* " << spell << ": ";
            info_stream << std::to_string(cooldown) << "\n";
        }
    } else {
        info_stream << "No cooldowns.";
    }

    return info_stream.str();
}

QString Knight::cooldowns_Qinfo() const
{
    return QString(cooldowns_info().data());
}

std::vector<QString> Knight::spell_names() const
{
    return { spell_to_Qstring(Knight::Spell::MORTAL_STRIKE) };
}

std::vector<unsigned> Knight::spell_costs() const
{
    std::vector<unsigned> costs;
    costs.push_back(static_cast<unsigned>(SpellCost::MORTAL_STRIKE));
    return costs;
}

void Knight::_accept_visit(PieceVisitor& visitor)
{
    visitor.visit_knight(this);
}

std::map<QString, QString> Knight::default_values()
{
    std::map<QString,QString> default_values;
    default_values[QString("HEALTH")]=QString::number(static_cast<int>(Default::HEALTH));
    default_values[QString("DAMAGE")] = QString::number(static_cast<int>(Default::DAMAGE));
    default_values[QString("MAX_ORTHO_MOVES")] = QString::number(static_cast<int>(Default::MAX_ORTHO_MOVES));
    default_values[QString("MAX_DIAG_MOVES")] = QString::number(static_cast<int>(Default::MAX_DIAG_MOVES));
    default_values[QString("MAX_ATTACKS")] = QString::number(static_cast<int>(Default::MAX_ATTACKS));
    default_values[QString("MAX_ORTHO_TARGET_RANGE")] = QString::number(static_cast<int>(Default::MAX_ORTHO_TARGET_RANGE));
    default_values[QString("MAX_DIAG_TARGET_RANGE")] = QString::number(static_cast<int>(Default::MAX_DIAG_TARGET_RANGE));

    default_values[QString("GROUND_HEALTH")] = QString::number(static_cast<int>(FieldBonus::GROUND_HEALTH));
    default_values[QString("GROUND_DAMAGE")] = QString::number(static_cast<int>(FieldBonus::GROUND_DAMAGE));
    default_values[QString("PLAIN_HEALTH")] = QString::number(static_cast<int>(FieldBonus::PLAIN_HEALTH));
    default_values[QString("PLAIN_DAMAGE")] = QString::number(static_cast<int>(FieldBonus::PLAIN_DAMAGE));
    default_values[QString("FOREST_HEALTH")] = QString::number(static_cast<int>(FieldBonus::FOREST_HEALTH));
    default_values[QString("FOREST_DAMAGE")] = QString::number(static_cast<int>(FieldBonus::FOREST_DAMAGE));
    default_values[QString("BRIDGE_HEALTH")] = QString::number(static_cast<int>(FieldBonus::BRIDGE_HEALTH));
    default_values[QString("BRIDGE_DAMAGE")] = QString::number(static_cast<int>(FieldBonus::BRIDGE_DAMAGE));
    default_values[QString("HILL_HEALTH")] = QString::number(static_cast<int>(FieldBonus::HILL_HEALTH));
    default_values[QString("HILL_DAMAGE")] = QString::number(static_cast<int>(FieldBonus::HILL_DAMAGE));

    default_values[QString("KNIGHT_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::KNIGHT_DAMAGE));
    default_values[QString("SPEARMAN_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::SPEARMAN_DAMAGE));
    default_values[QString("SHIELDMAN_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::SHIELDMAN_DAMAGE));
    default_values[QString("HORSEMAN_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::HORSEMAN_DAMAGE));
    default_values[QString("ARCHER_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::ARCHER_DAMAGE));
    default_values[QString("CATAPULT_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::CATAPULT_DAMAGE));
    default_values[QString("MAGE_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::MAGE_DAMAGE));
    default_values[QString("PRIEST_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::PRIEST_DAMAGE));
    default_values[QString("CASTLE_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::CASTLE_DAMAGE));

    return default_values;
}

std::string Knight::spell_to_string(Knight::Spell spell) const
{
    return "Mortal Strike";
}

QString Knight::spell_to_Qstring(Knight::Spell spell) const
{
    return QString("Mortal Strike");
}

unsigned Knight::get_spell_cooldown(Knight::Spell spell) const
{
    return static_cast<unsigned>(Knight::SpellCooldown::MORTAL_STRIKE);
}

unsigned Knight::get_spell_cooldown_cost(Knight::Spell spell) const
{
    return static_cast<unsigned>(Knight::SpellCost::MORTAL_STRIKE);
}

std::vector<Knight::Spell> Knight::get_spells() const
{
    return { Knight::Spell::MORTAL_STRIKE };
}

void Knight::init_default_values()
{
    m_type = Piece::Type::KNIGHT;
    m_health = static_cast<double>(Knight::Default::HEALTH);
    m_damage = static_cast<double>(Knight::Default::DAMAGE);
    m_ortho_moves_remaining = static_cast<int>(Knight::Default::MAX_ORTHO_MOVES);
    m_diag_moves_remaining = static_cast<int>(Knight::Default::MAX_DIAG_MOVES);
    m_attacks_remaining = static_cast<int>(Knight::Default::MAX_ATTACKS);
}
