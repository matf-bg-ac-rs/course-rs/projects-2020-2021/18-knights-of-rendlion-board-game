#include "shieldman.hpp"


std::string Shieldman::default_info()

{
    std::stringstream info_stream;

    info_stream << "Shieldman" << "\n----------------------------";
    info_stream << "\nHealth: " << static_cast<unsigned>(Shieldman::Default::HEALTH);
    info_stream << "\nDamage: " << static_cast<unsigned>(Shieldman::Default::DAMAGE);
    info_stream << "\nMax attacks: " << static_cast<unsigned>(Shieldman::Default::MAX_ATTACKS);
    info_stream << "\nMax ortho moves: " << static_cast<unsigned>(Shieldman::Default::MAX_ORTHO_MOVES);
    info_stream << "\nMax diag moves: " << static_cast<unsigned>(Shieldman::Default::MAX_DIAG_MOVES);
    info_stream << "\nMax ortho target range: " << static_cast<unsigned>(Shieldman::Default::MAX_ORTHO_TARGET_RANGE);
    info_stream << "\nMax diag target range: " << static_cast<unsigned>(Shieldman::Default::MAX_DIAG_TARGET_RANGE);
    info_stream << "\n";

    return info_stream.str();
}

QString Shieldman::default_Qinfo()
{
    return QString(default_info().data());
}

std::vector<std::map<QString, QString>> Shieldman::spells_info(){

    std::vector<std::map<QString, QString>> spell_map_vec;
    std::map<QString, QString> spell_map;
    QString spell_info_str;

    spell_info_str += QString("Increases shieldman's health by ");
    spell_info_str += (QString::number(static_cast<unsigned>(Shieldman::SpellData::TURTLE_BONUS_PERCENT)) + QString("% for "));
    spell_info_str += QString::number(static_cast<unsigned>(Shieldman::SpellData::TURTLE_DURATION)) + QString(" next turns, but ");
    spell_info_str += (QString("decreases damage by ") + QString::number(static_cast<unsigned>(Shieldman::SpellData::TURTLE_DMG_REDUCTION_PERCENT)));
    spell_info_str += QString("%.\n");
    spell_info_str += (QString("* Cooldown: ") + QString::number(static_cast<int>(Shieldman::SpellCooldown::TURTLE)) + QString("\n"));
    spell_info_str += (QString("* Cost: ") +  QString::number(static_cast<int>(Shieldman::SpellCost::TURTLE)));
    spell_info_str += QString("\n");

    spell_map[QString("TURTLE")]=spell_info_str;
    spell_map_vec.push_back(spell_map);

    return spell_map_vec;
}


std::string Shieldman::field_bonus_info()
{
    std::stringstream info_stream;

    info_stream << "Field bonus" << "\n----------------------------------\n";
    info_stream << "* Ground: ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::GROUND_HEALTH)) << "% HP, ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::GROUND_DAMAGE)) << "% DMG\n";
    info_stream << "* Plain: ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::PLAIN_HEALTH)) << "% HP, ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::PLAIN_DAMAGE)) << "% DMG\n";
    info_stream << "* Hill: ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::HILL_HEALTH)) << "% HP, ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::HILL_DAMAGE)) << "% DMG\n";
    info_stream << "* Forest: ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::FOREST_HEALTH)) << "% HP, ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::FOREST_DAMAGE)) << "% DMG\n";
    info_stream << "* Bridge: ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::BRIDGE_HEALTH)) << "% HP, ";
    info_stream << bonus_info_check_sign(static_cast<int>(FieldBonus::BRIDGE_DAMAGE)) << "% DMG";
    info_stream << "\n";

    return info_stream.str();
};

QString Shieldman::field_bonus_Qinfo()
{
    return QString(field_bonus_info().data());
}

std::string Shieldman::target_bonus_info()
{
    std::stringstream info_stream;

    info_stream << "Target bonus" << "\n----------------------------------\n";
    info_stream << "* Archer: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::ARCHER_DAMAGE)) << "% DMG\n";
    info_stream << "* Castle: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::CASTLE_DAMAGE)) << "% DMG\n";
    info_stream << "* Catapult: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::CATAPULT_DAMAGE)) << "% DMG\n";
    info_stream << "* Horseman: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::HORSEMAN_DAMAGE)) << "% DMG\n";
    info_stream << "* Knight: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::KNIGHT_DAMAGE)) << "% DMG\n";
    info_stream << "* Mage: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::MAGE_DAMAGE)) << "% DMG\n";
    info_stream << "* Priest: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::PRIEST_DAMAGE)) << "% DMG\n";
    info_stream << "* Shieldman: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::SHIELDMAN_DAMAGE)) << "% DMG\n";
    info_stream << "* Spearman: ";
    info_stream << bonus_info_check_sign(static_cast<int>(TargetBonus::SPEARMAN_DAMAGE)) << "% DMG";
    info_stream << "\n";

    return info_stream.str();
}

QString Shieldman::target_bonus_Qinfo()
{
    return QString(target_bonus_info().data());
}

std::pair<int, int> Shieldman::ground_bonuses()
{
    return std::make_pair(
        static_cast<int>(Shieldman::FieldBonus::GROUND_HEALTH),
        static_cast<int>(Shieldman::FieldBonus::GROUND_DAMAGE)
    );
}

std::pair<int, int> Shieldman::plain_bonuses()
{
    return std::make_pair(
        static_cast<int>(Shieldman::FieldBonus::PLAIN_HEALTH),
        static_cast<int>(Shieldman::FieldBonus::PLAIN_DAMAGE)
    );
}

std::pair<int, int> Shieldman::hill_bonuses()
{
    return std::make_pair(
        static_cast<int>(Shieldman::FieldBonus::HILL_HEALTH),
        static_cast<int>(Shieldman::FieldBonus::HILL_DAMAGE)
    );
}

std::pair<int, int> Shieldman::forest_bonuses()
{
    return std::make_pair(
        static_cast<int>(Shieldman::FieldBonus::FOREST_HEALTH),
        static_cast<int>(Shieldman::FieldBonus::FOREST_DAMAGE)
    );
}

std::pair<int, int> Shieldman::bridge_bonuses()
{
    return std::make_pair(
        static_cast<int>(Shieldman::FieldBonus::BRIDGE_HEALTH),
        static_cast<int>(Shieldman::FieldBonus::BRIDGE_DAMAGE)
    );
}

QRectF Shieldman::boundingRect() const
{
    return QRectF(0,0,40,40);
}

const QRect Shieldman::get_rect() const{
    return QRect(0,0,40,40);
}

void Shieldman::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);
    if(Player::Index::PLAYER_1 == m_owner)
        painter->drawPixmap(get_rect(),QPixmap(Shieldman::IMAGE_RED_QPATH));
    else
        painter->drawPixmap(get_rect(),QPixmap(Shieldman::IMAGE_BLUE_QPATH));
}

Shieldman::Shieldman() = default;

Shieldman::Shieldman(Player::Index owner, std::pair<unsigned, unsigned> board_index)
    : Piece(owner, board_index)
    , m_last_turtle_health_bonus(0.0)
    , m_last_turtle_damage_bonus(0.0)
{
    init_default_values();
}

unsigned Shieldman::get_default_health() const 
{
    return static_cast<unsigned>(Shieldman::Default::HEALTH);
}

unsigned Shieldman::get_default_damage() const 
{
    return static_cast<unsigned>(Shieldman::Default::DAMAGE);
}

unsigned Shieldman::get_max_attacks() const
{
    return static_cast<unsigned>(Shieldman::Default::MAX_ATTACKS);
}

unsigned Shieldman::get_max_ortho_moves() const
{
    return static_cast<unsigned>(Shieldman::Default::MAX_ORTHO_MOVES);
}

unsigned Shieldman::get_max_diag_moves() const
{
    return static_cast<unsigned>(Shieldman::Default::MAX_DIAG_MOVES);
}

unsigned Shieldman::get_max_ortho_target_range() const
{
    return static_cast<unsigned>(Shieldman::Default::MAX_ORTHO_TARGET_RANGE);
}

unsigned Shieldman::get_max_diag_target_range() const
{
    return static_cast<unsigned>(Shieldman::Default::MAX_DIAG_TARGET_RANGE);
}

unsigned Shieldman::get_max_ortho_spells_range(int spell_index) const
{
    return static_cast<unsigned>(Shieldman::SpellRange::TURTLE_ORTHO);
}

unsigned Shieldman::get_max_diag_spells_range(int spell_index) const
{
    return static_cast<unsigned>(Shieldman::SpellRange::TURTLE_DIAG);

}

bool Shieldman::get_use_g_fields(int spell_index) const
{
    return static_cast<bool>(Shieldman::SpellRange::TURTLE_USE_G_FIELDS);

}

int Shieldman::get_target_damage_bonus(Piece::Type piece_type) const
{
   switch (piece_type) {
       case Piece::Type::ARCHER:
           return static_cast<int>(Shieldman::TargetBonus::ARCHER_DAMAGE);
       case Piece::Type::CASTLE:
           return static_cast<int>(Shieldman::TargetBonus::CASTLE_DAMAGE);
       case Piece::Type::CATAPULT:
           return static_cast<int>(Shieldman::TargetBonus::CATAPULT_DAMAGE);
       case Piece::Type::HORSEMAN:
           return static_cast<int>(Shieldman::TargetBonus::HORSEMAN_DAMAGE);
       case Piece::Type::KNIGHT:
           return static_cast<int>(Shieldman::TargetBonus::KNIGHT_DAMAGE);
       case Piece::Type::MAGE:
           return static_cast<int>(Shieldman::TargetBonus::MAGE_DAMAGE);
       case Piece::Type::PRIEST:
           return static_cast<int>(Shieldman::TargetBonus::PRIEST_DAMAGE);
       case Piece::Type::SHIELDMAN:
           return static_cast<int>(Shieldman::TargetBonus::SHIELDMAN_DAMAGE);
       case Piece::Type::SPEARMAN:
           return static_cast<int>(Shieldman::TargetBonus::SPEARMAN_DAMAGE);
       default:
           throw UNKNOWN_CLASS_ERROR;
   }
}

std::pair<int, int> Shieldman::get_ground_bonuses() const
{
    return std::make_pair(
        static_cast<int>(Shieldman::FieldBonus::GROUND_HEALTH),
        static_cast<int>(Shieldman::FieldBonus::GROUND_DAMAGE)
    );
}

std::pair<int, int> Shieldman::get_plain_bonuses() const
{
    return std::make_pair(
        static_cast<int>(Shieldman::FieldBonus::PLAIN_HEALTH),
        static_cast<int>(Shieldman::FieldBonus::PLAIN_DAMAGE)
    );
}

std::pair<int, int> Shieldman::get_hill_bonuses() const
{
    return std::make_pair(
        static_cast<int>(Shieldman::FieldBonus::HILL_HEALTH),
        static_cast<int>(Shieldman::FieldBonus::HILL_DAMAGE)
    );
}

std::pair<int, int> Shieldman::get_forest_bonuses() const
{
    return std::make_pair(
        static_cast<int>(Shieldman::FieldBonus::FOREST_HEALTH),
        static_cast<int>(Shieldman::FieldBonus::FOREST_DAMAGE)
    );
}

std::pair<int, int> Shieldman::get_bridge_bonuses() const
{
    return std::make_pair(
        static_cast<int>(Shieldman::FieldBonus::BRIDGE_HEALTH),
        static_cast<int>(Shieldman::FieldBonus::BRIDGE_DAMAGE)
    );
}

std::string Shieldman::cooldowns_info() const
{
    std::stringstream info_stream;
    info_stream << "Cooldowns" << "\n-----------------------------\n";

    if (has_cooldowns()) {
        for (auto& [spell, cooldown] : m_cooldowns) {
            info_stream << "* " << spell << ": ";
            info_stream << std::to_string(cooldown) << "\n";
        }
    } else {
        info_stream << "No cooldowns.";
    }

    return info_stream.str();
}

QString Shieldman::cooldowns_Qinfo() const
{
    return QString(cooldowns_info().data());
}

std::vector<QString> Shieldman::spell_names() const
{
    return { spell_to_Qstring(Shieldman::Spell::TURTLE) };
}

std::vector<unsigned> Shieldman::spell_costs() const
{
    std::vector<unsigned> costs;
    costs.push_back(static_cast<unsigned>(SpellCost::TURTLE));
    return costs;
}

void Shieldman::_accept_visit(PieceVisitor& visitor)
{
    visitor.visit_shieldman(this);
}

std::map<QString, QString> Shieldman::default_values()
{
    std::map<QString,QString> default_values;
    default_values[QString("HEALTH")]=QString::number(static_cast<int>(Default::HEALTH));
    default_values[QString("DAMAGE")] = QString::number(static_cast<int>(Default::DAMAGE));
    default_values[QString("MAX_ORTHO_MOVES")] = QString::number(static_cast<int>(Default::MAX_ORTHO_MOVES));
    default_values[QString("MAX_DIAG_MOVES")] = QString::number(static_cast<int>(Default::MAX_DIAG_MOVES));
    default_values[QString("MAX_ATTACKS")] = QString::number(static_cast<int>(Default::MAX_ATTACKS));
    default_values[QString("MAX_ORTHO_TARGET_RANGE")] = QString::number(static_cast<int>(Default::MAX_ORTHO_TARGET_RANGE));
    default_values[QString("MAX_DIAG_TARGET_RANGE")] = QString::number(static_cast<int>(Default::MAX_DIAG_TARGET_RANGE));

    default_values[QString("GROUND_HEALTH")] = QString::number(static_cast<int>(FieldBonus::GROUND_HEALTH));
    default_values[QString("GROUND_DAMAGE")] = QString::number(static_cast<int>(FieldBonus::GROUND_DAMAGE));
    default_values[QString("PLAIN_HEALTH")] = QString::number(static_cast<int>(FieldBonus::PLAIN_HEALTH));
    default_values[QString("PLAIN_DAMAGE")] = QString::number(static_cast<int>(FieldBonus::PLAIN_DAMAGE));
    default_values[QString("FOREST_HEALTH")] = QString::number(static_cast<int>(FieldBonus::FOREST_HEALTH));
    default_values[QString("FOREST_DAMAGE")] = QString::number(static_cast<int>(FieldBonus::FOREST_DAMAGE));
    default_values[QString("BRIDGE_HEALTH")] = QString::number(static_cast<int>(FieldBonus::BRIDGE_HEALTH));
    default_values[QString("BRIDGE_DAMAGE")] = QString::number(static_cast<int>(FieldBonus::BRIDGE_DAMAGE));
    default_values[QString("HILL_HEALTH")] = QString::number(static_cast<int>(FieldBonus::HILL_HEALTH));
    default_values[QString("HILL_DAMAGE")] = QString::number(static_cast<int>(FieldBonus::HILL_DAMAGE));

    default_values[QString("KNIGHT_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::KNIGHT_DAMAGE));
    default_values[QString("SPEARMAN_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::SPEARMAN_DAMAGE));
    default_values[QString("SHIELDMAN_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::SHIELDMAN_DAMAGE));
    default_values[QString("HORSEMAN_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::HORSEMAN_DAMAGE));
    default_values[QString("ARCHER_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::ARCHER_DAMAGE));
    default_values[QString("CATAPULT_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::CATAPULT_DAMAGE));
    default_values[QString("MAGE_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::MAGE_DAMAGE));
    default_values[QString("PRIEST_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::PRIEST_DAMAGE));
    default_values[QString("CASTLE_DAMAGE")] = QString::number(static_cast<int>(TargetBonus::CASTLE_DAMAGE));

    return default_values;

}

void Shieldman::update_spell_effect(const std::string &spell_effect_str)
{
    auto it = m_spell_effects.find(spell_effect_str);
    if (it != m_spell_effects.end()) {
        it->second = it->second - 1;

        // If Spell effect is firebolt, reduce piece hp
        if (spell_effect_str == Piece::spell_effect_to_str(Piece::SpellEffect::FIREBOLT_DOT)) {
            int fb_dot_dmg = static_cast<int>(Piece::SpellEffectCache::FIREBOLT_DOT_DMG);
            m_health -= fb_dot_dmg;
        }

        if (it->second <= 0) {
            // Check if spell effect was turtle and remove bonus if yes
            if (spell_effect_str == Piece::spell_effect_to_str(Piece::SpellEffect::TURTLE_BONUS)) {
                if (m_last_turtle_health_bonus >= m_health) {
                    // Reduce health in percent in order not to kill shieldman after turtle bonus loss
                    double turtle_hp_bonus = static_cast<double>(Shieldman::SpellData::TURTLE_BONUS_PERCENT);
                    double health_loss = turtle_hp_bonus / 100 * m_health;
                    m_health -= health_loss;
                } else {
                    m_health -= m_last_turtle_health_bonus;
                }

                m_damage -= m_last_turtle_damage_bonus;
            } else if (spell_effect_str == Piece::spell_effect_to_str(Piece::SpellEffect::EMPOWER_BONUS)) {
                if (m_last_spell_health_bonus >= m_health) {
                    double empower_hp_bonus = static_cast<double>(Piece::SpellEffectCache::EMPOWER_BONUS);
                    double health_loss = empower_hp_bonus / 100 * m_health;
                    m_health -= health_loss;
                } else {
                    m_health -= m_last_spell_health_bonus;
                }

                m_damage -= m_last_spell_damage_bonus;
            }

            remove_spell_effect(spell_effect_str);
        }
    }
}

double Shieldman::last_turtle_health_bonus() const
{
    return m_last_turtle_health_bonus;
}

double Shieldman::last_turtle_damage_bonus() const
{
    return m_last_turtle_damage_bonus;
}

void Shieldman::set_last_turtle_health_bonus(const double bonus)
{
    m_last_turtle_health_bonus = bonus;
}

void Shieldman::set_last_turtle_damage_bonus(const double bonus)
{
    m_last_turtle_damage_bonus = bonus;
}

std::string Shieldman::spell_to_string(Shieldman::Spell spell) const
{
    return "Turtle";
}

QString Shieldman::spell_to_Qstring(Shieldman::Spell spell) const
{
    return QString("Turtle");
}

unsigned Shieldman::get_spell_cooldown(Shieldman::Spell spell) const
{
    return static_cast<unsigned>(Shieldman::SpellCost::TURTLE);
}

unsigned Shieldman::get_spell_cooldown_cost(Shieldman::Spell spell) const
{
    return static_cast<unsigned>(Shieldman::SpellCost::TURTLE);
}

std::vector<Shieldman::Spell> Shieldman::get_spells() const
{
    return { Shieldman::Spell::TURTLE };
}

void Shieldman::init_default_values()
{
    m_type = Piece::Type::SHIELDMAN;
    m_health = static_cast<double>(Shieldman::Default::HEALTH);
    m_damage = static_cast<double>(Shieldman::Default::DAMAGE);
    m_ortho_moves_remaining = static_cast<int>(Shieldman::Default::MAX_ORTHO_MOVES);
    m_diag_moves_remaining = static_cast<int>(Shieldman::Default::MAX_DIAG_MOVES);
    m_attacks_remaining = static_cast<int>(Shieldman::Default::MAX_ATTACKS);
}


