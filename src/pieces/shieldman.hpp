#ifndef SHIELDMAN_HPP
#define SHIELDMAN_HPP

#include <map>
#include <string>
#include <QString>
#include "piece.hpp"
#include "../utility/piecevisitor.hpp"
#include "../gui/board.hpp"


class Shieldman : public Piece
{
public:
    enum class Spell {
        TURTLE
    };

    enum class SpellCost {
        TURTLE = 2
    };

    enum class SpellCooldown {
        TURTLE = 2
    };

    enum class SpellRange {
        TURTLE_ORTHO = 0,
        TURTLE_DIAG = 0,
        TURTLE_USE_G_FIELDS = 0
    };

    enum class SpellData {
        TURTLE_BONUS_PERCENT = 40,
        TURTLE_DMG_REDUCTION_PERCENT = 30,
        TURTLE_DURATION = 2
    };

    enum class Default {
        HEALTH = 180,
        DAMAGE = 15,
        MAX_ORTHO_MOVES = 1,
        MAX_DIAG_MOVES = 0,
        MAX_ATTACKS = 3,
        MAX_ORTHO_TARGET_RANGE = 1,
        MAX_DIAG_TARGET_RANGE = 0
    };

    enum class FieldBonus {
        GROUND_HEALTH = 0,
        GROUND_DAMAGE = 0,
        PLAIN_HEALTH = 0,
        PLAIN_DAMAGE = 0,
        HILL_HEALTH = -20,
        HILL_DAMAGE = -20,
        FOREST_HEALTH = 20,
        FOREST_DAMAGE = 15,
        BRIDGE_HEALTH = 30,
        BRIDGE_DAMAGE = 20,
        RBRIDGE_HEALTH = 30,
        RBRIDGE_DAMAGE = 20,
    };

    enum class TargetBonus {
        KNIGHT_DAMAGE = 10,
        SPEARMAN_DAMAGE = 30,
        SHIELDMAN_DAMAGE = 0,
        HORSEMAN_DAMAGE = 10,
        ARCHER_DAMAGE = 20,
        CATAPULT_DAMAGE = -10,
        MAGE_DAMAGE = -10,
        PRIEST_DAMAGE = -10,
        CASTLE_DAMAGE = 0,
    };

    static inline const std::string IMAGE_RED_PATH =":/images/pieces/shieldman_red_new.png";
    static inline const QString IMAGE_RED_QPATH = QString(":/images/pieces/shieldman_red_new.png");
    static inline const std::string IMAGE_BLUE_PATH =":/images/pieces/shieldman_blue_new.png";
    static inline const QString IMAGE_BLUE_QPATH = QString(":/images/pieces/shieldman_blue_new.png");

    static std::string default_info();
    static QString default_Qinfo();
    static std::vector<std::map<QString, QString>> spells_info();
    static std::string field_bonus_info();
    static QString field_bonus_Qinfo();
    static std::string target_bonus_info();
    static QString target_bonus_Qinfo();
    static std::map<QString,QString> default_values();


    static std::pair<int, int> ground_bonuses();
    static std::pair<int, int> plain_bonuses();
    static std::pair<int, int> hill_bonuses();
    static std::pair<int, int> forest_bonuses();
    static std::pair<int, int> bridge_bonuses();

    virtual QRectF boundingRect() const override;
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
    const QRect get_rect() const override;

    Shieldman();
    Shieldman(Player::Index owner, std::pair<unsigned, unsigned> board_index);

    unsigned get_default_health() const override;
    unsigned get_default_damage() const override;
    unsigned get_max_attacks() const override;
    unsigned get_max_ortho_moves() const override;
    unsigned get_max_diag_moves() const override;
    unsigned get_max_ortho_target_range() const override;
    unsigned get_max_diag_target_range() const override;
    unsigned get_max_ortho_spells_range(int spell_index) const override;
    unsigned get_max_diag_spells_range(int spell_index) const override;
    bool get_use_g_fields(int spell_index) const override;
    int get_target_damage_bonus(Piece::Type piece_type) const override;

    std::pair<int, int> get_ground_bonuses() const override;
    std::pair<int, int> get_plain_bonuses() const override;
    std::pair<int, int> get_hill_bonuses() const override;
    std::pair<int, int> get_forest_bonuses() const override;
    std::pair<int, int> get_bridge_bonuses() const override;

    std::string cooldowns_info() const override;
    QString cooldowns_Qinfo() const override;
    std::vector<QString> spell_names() const override;
    std::vector<unsigned> spell_costs() const override;
    void _accept_visit(PieceVisitor& visitor) override;

    virtual void update_spell_effect(const std::string& spell_effect_str) override;

    double last_turtle_health_bonus() const;
    double last_turtle_damage_bonus() const;
    void set_last_turtle_health_bonus(const double bonus);
    void set_last_turtle_damage_bonus(const double bonus);

    std::string spell_to_string(Spell spell) const;
    QString spell_to_Qstring(Spell spell) const;
    unsigned get_spell_cooldown(Spell spell) const;
    unsigned get_spell_cooldown_cost(Spell spell) const;
    std::vector<Spell> get_spells() const;


private:
    double m_last_turtle_health_bonus;
    double m_last_turtle_damage_bonus;

    void init_default_values() override;
};

#endif // SHIELDMAN_HPP

