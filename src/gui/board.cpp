#include "board.hpp"

#include <utility>
#include <algorithm>

#include "../engine/gameController.hpp"

Board::Board()
    : m_rows(static_cast<unsigned>(Board::Size::ROWS))
    , m_cols(static_cast<unsigned>(Board::Size::COLS))
    , m_standable_fields(0)
{
    m_fields.resize(m_rows);

}

void Board::add_to_standable_list(Field* f)
{
    unsigned column = f->get_column_index();
    if(column >= 0 && column <= 7){
        m_left_standable.push_back(f);
    }
    else if (column >= 8 && column <= 12){
        m_center_standable.push_back(f);
    }
    else{
        m_right_standable.push_back(f);
    }

    m_standable_fields++;
}

unsigned Board::rows() const
{
    return m_rows;
}

unsigned Board::cols() const
{
    return m_cols;
}

std::vector<std::vector<Field *> > Board::fields() const
{
    return m_fields;
}

unsigned Board::num_of_standable_fields() const
{
    return m_standable_fields;
}

std::list<Field *> Board::get_left_standable_list() const
{
    return m_left_standable;
}

std::list<Field *> Board::get_center_standable_list() const
{
    return m_center_standable;
}

std::list<Field *> Board::get_right_standable_list() const
{
    return m_right_standable;
}

Board *Board::create_board()
{
    static Board* board = new Board();
    return board;
}

QRectF Board::boundingRect() const{

    return QRect(0,0,1050,750);
}
void Board::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget){
    Q_UNUSED(option);
    Q_UNUSED(widget);

    for(unsigned i = 0; i < m_rows; i++)
        for(unsigned j = 0; j < m_cols; j++)
            m_fields[i][j]->paint(painter, option, widget);

}

void Board::mousePressEvent(QGraphicsSceneMouseEvent *event){

    //get coords where mouse was clicked and emit that to GameController
    if(event->button() & Qt::LeftButton){
        unsigned row = event->pos().y()/50;
        unsigned column = event->pos().x()/50;
        emit call_handle_mouse_event(row,column);
    }
    else if (event->button() & Qt::RightButton){
        unsigned row = event->pos().y()/50;
        unsigned column = event->pos().x()/50;
        emit call_handle_right_click(row,column);
    }
    else{
        event->ignore();
    }

}

//this will initialize the board with fields according to map
void Board::initialize(){

    std::vector<std::string> maps;
    std::string path = "./maps";
    for (const auto & entry : std::filesystem::directory_iterator(path))
        maps.push_back(entry.path().string());

    srand(time(NULL));
    int index = rand() % maps.size();

    std::ifstream input;
    input.open(maps[index]);
    std::string line;
    unsigned row = 0;
    unsigned column = 0;
    while (std::getline(input, line)){
        for(auto l : line){
            if(isalpha(l)){
                Field* f;
                switch(l){
                case 'P':
                    f = new Field(Field::Type::PLAIN,true);
                    break;
                case 'H':
                    f = new Field(Field::Type::HILL,true);
                    break;
                case 'F':
                    f = new Field(Field::Type::FOREST,true);
                    break;
                case 'M':
                    f = new Field(Field::Type::MOUNTAIN,false);
                    break;
                case 'W':
                    f = new Field(Field::Type::WATER,false);
                    break;
                case 'B':
                    f = new Field(Field::Type::BRIDGE,true);
                    break;
                case 'b':
                    f = new Field(Field::Type::RBRIDGE,true);
                    break;
                case 'G':
                    f = new Field(Field::Type::GROUND,true);
                    break;
                case 'w':
                    f = new Field(Field::Type::RWATER,false);
                    break;
                case 'X':
                    f = new Field(Field::Type::CROSS_WATER,false);
                    break;
                case 'U':
                    f = new Field(Field::Type::CROSS_WATER1,false);
                    break;
                case 'u':
                    f = new Field(Field::Type::CROSS_WATER2,false);
                    break;
                case 'L':
                    f = new Field(Field::Type::CROSS_WATER3,false);
                    break;
                case 'l':
                    f = new Field(Field::Type::CROSS_WATER4,false);
                    break;
                default:
                    throw "Unknown type of field";
                }
                m_fields[row].push_back(f);
                f->set_row_index(row);
                f->set_column_index(column);
                f->setParentItem(this);
                f->set_marked(false);
                f->set_clicked(false);
                f->setPos(column*50,row*50);
                column++;

                if(f->is_standable())
                    add_to_standable_list(f);

            }
        }
        row++;
        column= 0;
    }


}

// Field getters
Field* Board::operator()(unsigned row,unsigned column){
    return m_fields[row][column];
}

const Field* Board::operator()(unsigned row, unsigned column) const{
    return m_fields[row][column];
}

Field* Board::at(unsigned row, unsigned col) const
{
    return m_fields[row][col];
}

const Field* Board::at_c(unsigned row, unsigned col) const
{
    return m_fields[row][col];
}

Field *Board::at(const std::pair<unsigned, unsigned> &board_index) const
{
    return m_fields[board_index.first][board_index.second];
}

const Field *Board::at_c(const std::pair<unsigned, unsigned> &board_index) const
{
    return m_fields[board_index.first][board_index.second];
}

void Board::move(Piece *p, Field *new_field, bool update_remaining_moves)
{
    Field* old_field = at(p->board_index());
    old_field->reset_field_bonuses();
    old_field->remove_piece();
    new_field->set_piece(p);
    new_field->activate_field_bonuses();
    p->set_board_index(new_field->get_board_index());

    if (update_remaining_moves) {
        p->update_moves_remaining(old_field->get_board_index(),new_field->get_board_index());
    }
}

std::vector<Field*> Board::fields_in_target_range(const Piece *piece) const
{
    std::vector<Field*> fields;

    if(0 == piece->attacks_remaining())
        return fields;

    unsigned max_ortho_target_range = piece->get_max_ortho_target_range();
    unsigned max_diag_target_range = piece->get_max_diag_target_range();

    for (unsigned i = 1; i <= max_ortho_target_range; i++) {
        int upper_row = piece->at_row() - i;
        int lower_row = piece->at_row() + i;
        int left_col = piece->at_col() - i;
        int right_col = piece->at_col() + i;

        if (upper_row >= 0) {
            fields.push_back(m_fields[upper_row][piece->at_col()]);
        }

        if (lower_row < m_rows) {
            fields.push_back(m_fields[lower_row][piece->at_col()]);
        }

        if (left_col >= 0) {
            fields.push_back(m_fields[piece->at_row()][left_col]);
        }

        if (right_col < m_cols) {
            fields.push_back(m_fields[piece->at_row()][right_col]);
        }

        // If piece is archer or catapult
        if (piece->piece_type() == Piece::Type::ARCHER || piece->piece_type() == Piece::Type::CATAPULT) {
            // Handle G-like fields (like chess knight)
            if (i > 1) {
                int j = i - 1;

                // Upper-left G-square
                if (upper_row >= 0 && left_col + j >= 0) {
                    fields.push_back(m_fields[upper_row][left_col + j]);
                }

                // Upper-right G-square
                if (upper_row >= 0 && right_col - j < m_cols) {
                    fields.push_back(m_fields[upper_row][right_col - j]);
                }

                // Lower-left G-square
                if (lower_row < m_rows && left_col + j >= 0) {
                    fields.push_back(m_fields[lower_row][left_col + j]);
                }

                // Lower-right G-square
                if (lower_row < m_rows && right_col - j < m_cols) {
                    fields.push_back(m_fields[lower_row][right_col - j]);
                }

                // Left-upper G-square
                if (upper_row + j >= 0 && left_col >= 0) {
                    fields.push_back(m_fields[upper_row + j][left_col]);
                }

                // Left-bottom G-square
                if (lower_row - j < m_rows && left_col >= 0) {
                    fields.push_back(m_fields[lower_row - j][left_col]);
                }

                // Right-upper G-square
                if (upper_row + j >= 0 && right_col < m_cols) {
                    fields.push_back(m_fields[upper_row + j][right_col]);
                }

                // Right-bottom G-square
                if (lower_row - j < m_rows && right_col < m_cols) {
                    fields.push_back(m_fields[lower_row - j][right_col]);
                }
            }
        }
    }

    for (unsigned i = 1; i <= max_diag_target_range; i++) {
        int upper_row = piece->at_row() - i;
        int lower_row = piece->at_row() + i;
        int left_col = piece->at_col() - i;
        int right_col = piece->at_col() + i;

        if (upper_row >= 0 && left_col >= 0) {
            fields.push_back(m_fields[upper_row][left_col]);
        }

        if (lower_row < m_rows && left_col >= 0) {
            fields.push_back(m_fields[lower_row][left_col]);
        }

        if (upper_row >= 0 && right_col < m_cols) {
            fields.push_back(m_fields[upper_row][right_col]);
        }

        if (lower_row < m_rows && right_col < m_cols) {
            fields.push_back(m_fields[lower_row][right_col]);
        }
    }

    return fields;
}

std::vector<Field*> Board::fields_in_move_range(const Piece *piece) const
{
    std::vector<Field*> fields;

    int upper_row = piece->at_row() - 1;
    int lower_row = piece->at_row() + 1;
    int left_col = piece->at_col() - 1;
    int right_col = piece->at_col() + 1;

    // Check moves remaining and
    if (piece->ortho_moves_remaining() > 0) {
        if (upper_row >= 0) {
            fields.push_back(m_fields[upper_row][piece->at_col()]);
        }

        if (lower_row < m_rows) {
            fields.push_back(m_fields[lower_row][piece->at_col()]);
        }

        if (left_col >= 0) {
            fields.push_back(m_fields[piece->at_row()][left_col]);
        }

        if (right_col < m_cols) {
            fields.push_back(m_fields[piece->at_row()][right_col]);
        }
    }

    if (piece->diag_moves_remaining() > 0) {
        if (upper_row >= 0 && left_col >= 0) {
            fields.push_back(m_fields[upper_row][left_col]);
        }

        if (lower_row < m_rows && left_col >= 0) {
            fields.push_back(m_fields[lower_row][left_col]);
        }

        if (upper_row >= 0 && right_col < m_cols) {
            fields.push_back(m_fields[upper_row][right_col]);
        }

        if (lower_row < m_rows && right_col < m_cols) {
            fields.push_back(m_fields[lower_row][right_col]);
        }
    }

    return fields;
}

std::list<Field *> Board::fields_in_range(
    Field* current_field,
    unsigned max_ortho_range,
    unsigned max_diag_range,
    bool use_g_fields,
    bool check_field_availability
) const
{
    std::list<Field*> fields;
    unsigned curr_row = current_field->get_row_index();
    unsigned curr_col = current_field->get_column_index();

    // If field availability has to be checked, push only available fields
    if (check_field_availability) {
        for (unsigned i = 1; i <= max_ortho_range; i++) {
            int upper_row = curr_row - i;
            int lower_row = curr_row + i;
            int left_col = curr_col - i;
            int right_col = curr_col + i;

            if (upper_row >= 0 && m_fields[upper_row][curr_col]->is_available()) {
                fields.push_back(m_fields[upper_row][curr_col]);
            }

            if (lower_row < m_rows && m_fields[lower_row][curr_col]->is_available()) {
                fields.push_back(m_fields[lower_row][curr_col]);
            }

            if (left_col >= 0 && m_fields[curr_row][left_col]->is_available()) {
                fields.push_back(m_fields[curr_row][left_col]);
            }

            if (right_col < m_cols && m_fields[curr_row][right_col]->is_available()) {
                fields.push_back(m_fields[curr_row][right_col]);
            }

            // If G-direction fields has to be calculated, include them
            if (use_g_fields) {
                if (i > 1) {
                    int j = i - 1;

                    // Upper-left G-square
                    if (upper_row >= 0 && left_col + j >= 0 && m_fields[upper_row][left_col + j]->is_available()) {
                        fields.push_back(m_fields[upper_row][left_col + j]);
                    }

                    // Upper-right G-square
                    if (upper_row >= 0 && right_col - j < m_cols && m_fields[upper_row][right_col - j]->is_available()) {
                        fields.push_back(m_fields[upper_row][right_col - j]);
                    }

                    // Lower-left G-square
                    if (lower_row < m_rows && left_col + j >= 0 && m_fields[lower_row][left_col + j]->is_available()) {
                        fields.push_back(m_fields[lower_row][left_col + j]);
                    }

                    // Lower-right G-square
                    if (lower_row < m_rows && right_col - j < m_cols && m_fields[lower_row][right_col - j]->is_available()) {
                        fields.push_back(m_fields[lower_row][right_col - j]);
                    }

                    // Left-upper G-square
                    if (upper_row + j >= 0 && left_col >= 0 && m_fields[upper_row + j][left_col]->is_available()) {
                        fields.push_back(m_fields[upper_row + j][left_col]);
                    }

                    // Left-bottom G-square
                    if (lower_row - j < m_rows && left_col >= 0 && m_fields[lower_row - j][left_col]->is_available()) {
                        fields.push_back(m_fields[lower_row - j][left_col]);
                    }

                    // Right-upper G-square
                    if (upper_row + j >= 0 && right_col < m_cols && m_fields[upper_row + j][right_col]->is_available()) {
                        fields.push_back(m_fields[upper_row + j][right_col]);
                    }

                    // Right-bottom G-square
                    if (lower_row - j < m_rows && right_col < m_cols && m_fields[lower_row - j][right_col]->is_available()) {
                        fields.push_back(m_fields[lower_row - j][right_col]);
                    }
                }
            }
        }

        for (unsigned i = 1; i <= max_diag_range; i++) {
            int upper_row = curr_row - i;
            int lower_row = curr_row + i;
            int left_col = curr_col - i;
            int right_col = curr_col + i;

            if (upper_row >= 0 && left_col >= 0 && m_fields[upper_row][left_col]->is_available()) {
                fields.push_back(m_fields[upper_row][left_col]);
            }

            if (lower_row < m_rows && left_col >= 0 && m_fields[lower_row][left_col]->is_available()) {
                fields.push_back(m_fields[lower_row][left_col]);
            }

            if (upper_row >= 0 && right_col < m_cols && m_fields[upper_row][right_col]->is_available()) {
                fields.push_back(m_fields[upper_row][right_col]);
            }

            if (lower_row < m_rows && right_col < m_cols && m_fields[lower_row][right_col]->is_available()) {
                fields.push_back(m_fields[lower_row][right_col]);
            }
        }
    } else {
        for (unsigned i = 1; i <= max_ortho_range; i++) {
            int upper_row = curr_row - i;
            int lower_row = curr_row + i;
            int left_col = curr_col - i;
            int right_col = curr_col + i;

            if (upper_row >= 0) {
                fields.push_back(m_fields[upper_row][curr_col]);
            }

            if (lower_row < m_rows) {
                fields.push_back(m_fields[lower_row][curr_col]);
            }

            if (left_col >= 0) {
                fields.push_back(m_fields[curr_row][left_col]);
            }

            if (right_col < m_cols) {
                fields.push_back(m_fields[curr_row][right_col]);
            }

            // If G-direction fields has to be calculated, include them
            if (use_g_fields) {
                if (i > 1) {
                    int j = i - 1;

                    // Upper-left G-square
                    if (upper_row >= 0 && left_col + j >= 0) {
                        fields.push_back(m_fields[upper_row][left_col + j]);
                    }

                    // Upper-right G-square
                    if (upper_row >= 0 && right_col - j < m_cols) {
                        fields.push_back(m_fields[upper_row][right_col - j]);
                    }

                    // Lower-left G-square
                    if (lower_row < m_rows && left_col + j >= 0) {
                        fields.push_back(m_fields[lower_row][left_col + j]);
                    }

                    // Lower-right G-square
                    if (lower_row < m_rows && right_col - j < m_cols) {
                        fields.push_back(m_fields[lower_row][right_col - j]);
                    }

                    // Left-upper G-square
                    if (upper_row + j >= 0 && left_col >= 0) {
                        fields.push_back(m_fields[upper_row + j][left_col]);
                    }

                    // Left-bottom G-square
                    if (lower_row - j < m_rows && left_col >= 0) {
                        fields.push_back(m_fields[lower_row - j][left_col]);
                    }

                    // Right-upper G-square
                    if (upper_row + j >= 0 && right_col < m_cols) {
                        fields.push_back(m_fields[upper_row + j][right_col]);
                    }

                    // Right-bottom G-square
                    if (lower_row - j < m_rows && right_col < m_cols) {
                        fields.push_back(m_fields[lower_row - j][right_col]);
                    }
                }
            }
        }

        for (unsigned i = 1; i <= max_diag_range; i++) {
            int upper_row = curr_row - i;
            int lower_row = curr_row + i;
            int left_col = curr_col - i;
            int right_col = curr_col + i;

            if (upper_row >= 0 && left_col >= 0) {
                fields.push_back(m_fields[upper_row][left_col]);
            }

            if (lower_row < m_rows && left_col >= 0) {
                fields.push_back(m_fields[lower_row][left_col]);
            }

            if (upper_row >= 0 && right_col < m_cols) {
                fields.push_back(m_fields[upper_row][right_col]);
            }

            if (lower_row < m_rows && right_col < m_cols) {
                fields.push_back(m_fields[lower_row][right_col]);
            }
        }
    }

    return fields;
}

std::list<Field*> Board::fields_in_range(
        const std::pair<unsigned, unsigned>& board_index,
        unsigned max_ortho_range,
        unsigned max_diag_range,
        bool use_g_fields,
        bool check_field_availability
    ) const
{
    Field* current_field = at(board_index);
    return fields_in_range(current_field, max_ortho_range, max_diag_range, use_g_fields, check_field_availability);
}

std::set<std::pair<unsigned, unsigned> > Board::trimmed_indices(
    std::set<std::pair<int, int> > &indices,
    bool check_availability = false
) const
{
    std::set<std::pair<unsigned, unsigned>> result_indices;

    if (check_availability) {
        for (auto& [row, col] : indices) {
            if (row >= 0 && row < m_rows && col >= 0 && col < m_cols &&
                    m_fields[row][col]->is_available()) {
                result_indices.insert(std::make_pair(static_cast<unsigned>(row), static_cast<unsigned>(col)));
            }
        }
    } else {
        for (auto& [row, col] : indices) {
            if (row >= 0 && row < m_rows && col >= 0 && col < m_cols) {
                result_indices.insert(std::make_pair(static_cast<unsigned>(row), static_cast<unsigned>(col)));
            }
        }
    }

    return result_indices;
}

std::set<Field *> Board::trimmed_fields(
    std::set<std::pair<int, int> > &indices,
    bool check_availability = false
) const
{
    std::set<Field*> fields;

    if (check_availability) {
        for (auto& [row, col] : indices) {
            if (row >= 0 && row < m_rows && col >= 0 && col < m_cols &&
                    m_fields[row][col]->is_available()) {
                fields.insert(m_fields[row][col]);
            }
        }
    } else {
        for (auto& [row, col] : indices) {
            if (row >= 0 && row < m_rows && col >= 0 && col < m_cols) {
                fields.insert(m_fields[row][col]);
            }
        }
    }

    return fields;
}

std::vector<Field*> Board::nearby_fields(const Field* position, unsigned distance) const
{
    if (distance == 0) {
        return std::vector<Field*>();
    }

    std::vector<Field*> fields;
    std::vector<int> direction_indices(2 * distance + 1);

    // For distance=1, this will fill vector to {-1, 0, 1}
    std::iota(direction_indices.begin(), direction_indices.end(), -static_cast<int>(distance));

    int curr_row = static_cast<int>(position->get_row_index());
    int curr_col = static_cast<int>(position->get_column_index());

    for (auto i : direction_indices) {
        for (auto j : direction_indices) {
            if (i == 0 && j == 0) {
                // This is current field (position), so ignore it
                continue;
            }

            int row = curr_row + i;
            int col = curr_col + j;

            if (row >= 0 && row < m_rows && col >= 0 && col < m_cols) {
                fields.push_back(m_fields[row][col]);
            }
        }
    }

    return fields;
}

bool Board::check_row(int row) const
{
    return row >= 0 && row < m_rows;
}

bool Board::check_col(int col) const
{
    return col >= 0 && col < m_cols;
}

bool Board::check_indices(int row, int col) const
{
    return row >= 0 && row < m_rows && col >= 0 && col < m_cols;
}

//marking empty player fields in INIT part
void Board::mark_player1_fields(){
    for(unsigned j = 0; j<Size::STAGING_AREA; j++)
        for(unsigned i = 0; i < Size::ROWS; i++)
            if(m_fields[i][j]->is_available()){
                m_fields[i][j]->set_marked(true);
                m_fields[i][j]->update();
            }
}

void Board::mark_player2_fields(){
    for(unsigned j = Size::COLS - Size::STAGING_AREA ; j < Size::COLS; j++)
        for(unsigned i = 0; i < Size::ROWS; i++)
            if(m_fields[i][j]->is_available()){
                m_fields[i][j]->set_marked(true);
                m_fields[i][j]->update();
            }
}

//unmarking empty player fields in INIT part
void Board::unmark_player1_fields(){
    for(unsigned j = 0; j<Size::STAGING_AREA; j++)
        for(unsigned i = 0; i < Size::ROWS; i++){
            m_fields[i][j]->set_marked(false);
            m_fields[i][j]->update();
        }

}

void Board::unmark_player2_fields(){
    for(unsigned j = Size::COLS - Size::STAGING_AREA ; j < Size::COLS; j++)
        for(unsigned i = 0; i < Size::ROWS; i++){
            m_fields[i][j]->set_marked(false);
            m_fields[i][j]->update();
        }
}

void Board::remove_from_standable_list(Field *f)
{
    unsigned column = f->get_column_index();
    if(column >= 0 && column <= 7){

        auto it = std::find(m_left_standable.begin(),m_left_standable.end(),f);
        if(it != m_left_standable.end())
            m_left_standable.erase(it);
        else
            throw "NO SUCH FIELD";
    }
    else if (column >= 8 && column <= 12){

        auto it = std::find(m_center_standable.begin(),m_center_standable.end(),f);
        if(it != m_center_standable.end())
            m_center_standable.erase(it);
        else
            throw "NO SUCH FIELD";
    }
    else{

        auto it = std::find(m_right_standable.begin(),m_right_standable.end(),f);
        if(it != m_right_standable.end())
            m_right_standable.erase(it);
        else
            throw "NO SUCH FIELD";
    }

    m_standable_fields--;
}


template<typename TK, typename Pred>
void Board::remove_fields_by(TK &fields, Pred remove_if_predicate)
{
    fields.erase(
        std::remove_if(
            std::begin(fields),
            std::end(fields),
            remove_if_predicate
        ),
        std::end(fields)
    );
}
