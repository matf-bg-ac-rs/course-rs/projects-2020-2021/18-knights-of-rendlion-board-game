#include "card.hpp"


#include "../engine/player.hpp"
#include "../utility/utility.hpp"
#include <iostream>
#include <vector>


// Initialize card non-integral data
const std::string Card::HolyLight::IMG_PATH = ":/images/cards/Holy Light.png";
const std::string Card::HolyLight::NAME = "Holy Light";
const Card::Type Card::HolyLight::TYPE = Card::Type::FRIENDLY_TARGET;
const Card::Rarity Card::HolyLight::RARITY = Card::Rarity::COMMON;

const std::string Card::WarCry::IMG_PATH = ":/images/cards/War Cry.png";
const std::string Card::WarCry::NAME = "War Cry";
const Card::Type Card::WarCry::TYPE = Card::Type::FRIENDLY_TARGET;
const Card::Rarity Card::WarCry::RARITY = Card::Rarity::UNCOMMON;

const std::string Card::DeadlyPoison::IMG_PATH = ":/images/cards/Deadly Poison.png" ;
const std::string Card::DeadlyPoison::NAME = "Deadly Poison";
const Card::Type Card::DeadlyPoison::TYPE = Card::Type::ENEMY_TARGET;
const Card::Rarity Card::DeadlyPoison::RARITY = Card::Rarity::MYTHICAL;

const std::string Card::CrushingBlow::IMG_PATH = ":/images/cards/Crushing Blow.png";
const std::string Card::CrushingBlow::NAME = "Crushing Blow";
const Card::Type Card::CrushingBlow::TYPE = Card::Type::ENEMY_TARGET;
const Card::Rarity Card::CrushingBlow::RARITY = Card::Rarity::COMMON;

const std::string Card::Thunderstorm::IMG_PATH = ":/images/cards/Thunderstorm.png";
const std::string Card::Thunderstorm::NAME = "Thunderstorm";
const Card::Type Card::Thunderstorm::TYPE = Card::Type::MIXED;
const Card::Rarity Card::Thunderstorm::RARITY = Card::Rarity::RARE;

const std::string Card::CallToArms::IMG_PATH = ":/images/cards/Call to Arms.png";
const std::string Card::CallToArms::NAME = "Call to Arms";
const Card::Type Card::CallToArms::TYPE = Card::Type::FRIENDLY_EFFECT;
const Card::Rarity Card::CallToArms::RARITY = Card::Rarity::COMMON;

const std::string Card::Teleport::IMG_PATH = ":/images/cards/Teleport.png";
const std::string Card::Teleport::NAME = "Teleport";
const Card::Type Card::Teleport::TYPE = Card::Type::FRIENDLY_TARGET;
const Card::Rarity Card::Teleport::RARITY = Card::Rarity::LEGENDARY;

const std::string Card::AdrenalineRush::IMG_PATH = ":/images/cards/Adrenaline Rush.png";
const std::string Card::AdrenalineRush::NAME = "Adrenaline Rush";
const Card::Type Card::AdrenalineRush::TYPE = Card::Type::FRIENDLY_EFFECT;
const Card::Rarity Card::AdrenalineRush::RARITY = Card::Rarity::RARE;

const std::string Card::Silence::IMG_PATH = ":/images/cards/Silence.png";
const std::string Card::Silence::NAME = "Silence";
const Card::Type Card::Silence::TYPE = Card::Type::ENEMY_EFFECT;
const Card::Rarity Card::Silence::RARITY = Card::Rarity::RARE;

const std::string Card::Affliction::IMG_PATH = ":/images/cards/Affliction.png";
const std::string Card::Affliction::NAME = "Affliction";
const Card::Type Card::Affliction::TYPE = Card::Type::ENEMY_TARGET;
const Card::Rarity Card::Affliction::RARITY = Card::Rarity::RARE;

const std::string Card::Rebuild::IMG_PATH = ":/images/cards/Rebuild.png";
const std::string Card::Rebuild::NAME = "Rebuild";
const Card::Type Card::Rebuild::TYPE = Card::Type::FRIENDLY_TARGET;
const Card::Rarity Card::Rebuild::RARITY = Card::Rarity::COMMON;

const std::string Card::DragonsBreath::IMG_PATH = ":/images/cards/Dragon's Breath.png";
const std::string Card::DragonsBreath::NAME = "Dragon's Breath";
const Card::Type Card::DragonsBreath::TYPE = Card::Type::MIXED;
const Card::Rarity Card::DragonsBreath::RARITY = Card::Rarity::LEGENDARY;

const std::string Card::DoubleEdge::IMG_PATH = ":/images/cards/Double Edge.png";
const std::string Card::DoubleEdge::NAME = "Double Edge";
const Card::Type Card::DoubleEdge::TYPE = Card::Type::BOTH_TARGETS;
const Card::Rarity Card::DoubleEdge::RARITY = Card::Rarity::COMMON;

const std::string Card::Meteor::IMG_PATH = ":/images/cards/Meteor.png";
const std::string Card::Meteor::NAME = "Meteor";
const Card::Type Card::Meteor::TYPE = Card::Type::ENEMY_TARGET;
const Card::Rarity Card::Meteor::RARITY = Card::Rarity::UNCOMMON;

const std::string Card::CunningWizardry::IMG_PATH = ":/images/cards/Cunning Wizardry.png";
const std::string Card::CunningWizardry::NAME = "Cunning Wizardry";
const Card::Type Card::CunningWizardry::TYPE = Card::Type::MIXED;
const Card::Rarity Card::CunningWizardry::RARITY = Card::Rarity::MYTHICAL;

const std::string Card::Refreshment::IMG_PATH = ":/images/cards/Refreshment.png";
const std::string Card::Refreshment::NAME = "Refreshment";
const Card::Type Card::Refreshment::TYPE = Card::Type::FRIENDLY_TARGET;
const Card::Rarity Card::Refreshment::RARITY = Card::Rarity::UNCOMMON;

const std::string Card::Armageddon::IMG_PATH = ":/images/cards/Armageddon.png";
const std::string Card::Armageddon::NAME = "Armageddon";
const Card::Type Card::Armageddon::TYPE = Card::Type::RANDOM;
const Card::Rarity Card::Armageddon::RARITY = Card::Rarity::LEGENDARY;

const std::string Card::Thievery::IMG_PATH = ":/images/cards/Thievery.png";
const std::string Card::Thievery::NAME = "Thievery";
const Card::Type Card::Thievery::TYPE = Card::Type::MIXED;
const Card::Rarity Card::Thievery::RARITY = Card::Rarity::MYTHICAL;

const std::string Card::Combustion::IMG_PATH = ":/images/cards/Combustion.png";
const std::string Card::Combustion::NAME = "Combustion";
const Card::Type Card::Combustion::TYPE = Card::Type::FRIENDLY_TARGET;
const Card::Rarity Card::Combustion::RARITY = Card::Rarity::MYTHICAL;

const std::string Card::WrathOfBorgoldin::IMG_PATH = ":/images/cards/Wrath of Borgoldin.png";
const std::string Card::WrathOfBorgoldin::NAME = "Wrath of Borgoldin";
const Card::Type Card::WrathOfBorgoldin::TYPE = Card::Type::BOTH_TARGETS;
const Card::Rarity Card::WrathOfBorgoldin::RARITY = Card::Rarity::LEGENDARY;

const std::string Card::BreakLoose::IMG_PATH = ":/images/cards/Break Loose.png";
const std::string Card::BreakLoose::NAME = "Break Loose";
const Card::Type Card::BreakLoose::TYPE = Card::Type::FRIENDLY_EFFECT;
const Card::Rarity Card::BreakLoose::RARITY = Card::Rarity::RARE;

const std::string Card::ArrowVolley::IMG_PATH = ":/images/cards/Arrow Volley.png";
const std::string Card::ArrowVolley::NAME = "Arrow Volley";
const Card::Type Card::ArrowVolley::TYPE = Card::Type::ENEMY_TARGET;
const Card::Rarity Card::ArrowVolley::RARITY = Card::Rarity::UNCOMMON;

const std::string Card::OneManArmy::IMG_PATH = ":/images/cards/One Man Army.png";
const std::string Card::OneManArmy::NAME = "One Man Army";
const Card::Type Card::OneManArmy::TYPE = Card::Type::FRIENDLY_TARGET;
const Card::Rarity Card::OneManArmy::RARITY = Card::Rarity::LEGENDARY;

const std::string Card::Aristocracy::IMG_PATH = ":/images/cards/Aristocracy.png";
const std::string Card::Aristocracy::NAME = "Aristocracy";
const Card::Type Card::Aristocracy::TYPE = Card::Type::FRIENDLY_EFFECT;
const Card::Rarity Card::Aristocracy::RARITY = Card::Rarity::COMMON;

const std::string Card::CavalryAssembly::IMG_PATH = ":/images/cards/Cavalry Assembly.png";
const std::string Card::CavalryAssembly::NAME = "Cavalry Assembly";
const Card::Type Card::CavalryAssembly::TYPE = Card::Type::FRIENDLY_EFFECT;
const Card::Rarity Card::CavalryAssembly::RARITY = Card::Rarity::UNCOMMON;

const std::string Card::Firesword::IMG_PATH = ":/images/cards/Firesword.png";
const std::string Card::Firesword::NAME = "Firesword";
const Card::Type Card::Firesword::TYPE = Card::Type::FRIENDLY_EFFECT;
const Card::Rarity Card::Firesword::RARITY = Card::Rarity::COMMON;

const std::string Card::BridgeOfDeath::IMG_PATH = ":/images/cards/Bridge of Death.png";
const std::string Card::BridgeOfDeath::NAME = "Bridge of Death";
const Card::Type Card::BridgeOfDeath::TYPE = Card::Type::ENEMY_EFFECT;
const Card::Rarity Card::BridgeOfDeath::RARITY = Card::Rarity::RARE;

const std::string Card::Flood::IMG_PATH = ":/images/cards/Flood.png";
const std::string Card::Flood::NAME = "Flood";
const Card::Type Card::Flood::TYPE = Card::Type::MIXED;
const Card::Rarity Card::Flood::RARITY = Card::Rarity::MYTHICAL;

const std::string Card::Bracers::IMG_PATH = ":/images/cards/Bracers.png";
const std::string Card::Bracers::NAME = "Bracers";
const Card::Type Card::Bracers::TYPE = Card::Type::FRIENDLY_TARGET;
const Card::Rarity Card::Bracers::RARITY = Card::Rarity::COMMON;

const std::string Card::FatalWounds::IMG_PATH = ":/images/cards/Fatal Wounds.png";
const std::string Card::FatalWounds::NAME = "Fatal Wounds";
const Card::Type Card::FatalWounds::TYPE = Card::Type::ENEMY_TARGET;
const Card::Rarity Card::FatalWounds::RARITY = Card::Rarity::UNCOMMON;


const std::vector<std::string> Card::CARD_NAMES = Card::_create_card_names();
const std::map<std::string, std::string> Card::CARD_NAMES_TO_PATH = Card::_create_card_names_to_path();
const std::map<std::string, unsigned> Card::CARD_NAMES_TO_COST = Card::_create_card_names_to_cost();
const std::map<std::string, Card::Rarity> Card::CARD_NAMES_TO_RARITY = Card::_create_card_names_to_rarity();
const std::vector<std::string> Card::COMMON_CARDS = Card::_create_cards_by_rarity(Card::Rarity::COMMON);
const std::vector<std::string> Card::UNCOMMON_CARDS = Card::_create_cards_by_rarity(Card::Rarity::UNCOMMON);
const std::vector<std::string> Card::RARE_CARDS = Card::_create_cards_by_rarity(Card::Rarity::RARE);
const std::vector<std::string> Card::MYTHICAL_CARDS = Card::_create_cards_by_rarity(Card::Rarity::MYTHICAL);
const std::vector<std::string> Card::LEGENDARY_CARDS = Card::_create_cards_by_rarity(Card::Rarity::LEGENDARY);
std::vector<std::pair<double, Card::Rarity>> Card::_prob_rarity_sorted = {
    { 0.03, Card::Rarity::LEGENDARY },  // 3% chance
    { 0.07, Card::Rarity::MYTHICAL },  // 7% chance
    { 0.2, Card::Rarity::RARE },      // 20% chance
    { 0.3, Card::Rarity::UNCOMMON }, // 30% chance
    { 0.4, Card::Rarity::COMMON }   // 40% chance
};

std::vector<std::string> Card::cards_by_rarity(Card::Rarity rarity)
{
    switch (rarity) {
    case Card::Rarity::COMMON:
        return COMMON_CARDS;
    case Card::Rarity::UNCOMMON:
        return UNCOMMON_CARDS;
    case Card::Rarity::RARE:
        return RARE_CARDS;
    case Card::Rarity::MYTHICAL:
        return MYTHICAL_CARDS;
    case Card::Rarity::LEGENDARY:
        return LEGENDARY_CARDS;
    default:
        throw "Unknown rarity type";
    }
}

std::string Card::random_card()
{
    return random_element(CARD_NAMES);
}

std::string Card::random_card_by_rarity()
{
    // Ensure that accumulated sum of probabilites before calling a function is equal to 1.0
    double acc_prob = 0;
    double rnd = random_v();
    for (const auto& [prob, rarity] : _prob_rarity_sorted) {
        acc_prob += prob;

        if (rnd < acc_prob) {
            return random_element(cards_by_rarity(rarity));
        }
    }

    // This code should be unreachable if first condition is met and random_v() works properly
    std::cerr << "Warning: Check accumulated sum of probabilites and random unif(0, 1)" << std::endl;
    return random_element(CARD_NAMES);
}

std::string Card::rarity_to_name(Card::Rarity rarity)
{
    switch (rarity) {
    case Card::Rarity::COMMON:
        return "COMMON";
    case Card::Rarity::UNCOMMON:
        return "UNCOMMON";
    case Card::Rarity::RARE:
        return "RARE";
    case Card::Rarity::MYTHICAL:
        return "MYTHICAL";
    case Card::Rarity::LEGENDARY:
        return "LEGENDARY";
    default:
        throw "Unknown rarity type";
    }
}

// Helper methods for storing card data
std::vector<std::string> Card::_create_card_names()
{
    return std::vector<std::string> {
            Card::HolyLight::NAME,
            Card::WarCry::NAME,
            Card::DeadlyPoison::NAME,
            Card::CrushingBlow::NAME,
            Card::Thunderstorm::NAME,
            Card::CallToArms::NAME,
            Card::Teleport::NAME,
            Card::AdrenalineRush::NAME,
            Card::Silence::NAME,
            Card::Affliction::NAME,
            Card::Rebuild::NAME,
            Card::DragonsBreath::NAME,
            Card::DoubleEdge::NAME,
            Card::Meteor::NAME,
            Card::CunningWizardry::NAME,
            Card::Refreshment::NAME,
            Card::Armageddon::NAME,
            Card::Thievery::NAME,
            Card::Combustion::NAME,
            Card::WrathOfBorgoldin::NAME,
            Card::BreakLoose::NAME,
            Card::ArrowVolley::NAME,
            Card::OneManArmy::NAME,
            Card::Aristocracy::NAME,
            Card::Firesword::NAME,
            Card::BridgeOfDeath::NAME,
            Card::Flood::NAME,
            Card::Bracers::NAME,
            Card::FatalWounds::NAME
        };
}

std::map<std::string, unsigned> Card::_create_card_names_to_cost()
{
    return std::map<std::string, unsigned> {
        { Card::HolyLight::NAME, static_cast<unsigned>(Card::HolyLight::COST) },
        { Card::WarCry::NAME, static_cast<unsigned>(Card::WarCry::COST) },
        { Card::DeadlyPoison::NAME, static_cast<unsigned>(Card::DeadlyPoison::COST) },
        { Card::CrushingBlow::NAME, static_cast<unsigned>(Card::CrushingBlow::COST) },
        { Card::Thunderstorm::NAME, static_cast<unsigned>(Card::Thunderstorm::COST) },
        { Card::CallToArms::NAME, static_cast<unsigned>(Card::CallToArms::COST) },
        { Card::Teleport::NAME, static_cast<unsigned>(Card::Teleport::COST) },
        { Card::AdrenalineRush::NAME, static_cast<unsigned>(Card::AdrenalineRush::COST) },
        { Card::Silence::NAME, static_cast<unsigned>(Card::Silence::COST) },
        { Card::Affliction::NAME, static_cast<unsigned>(Card::Affliction::COST) },
        { Card::Rebuild::NAME, static_cast<unsigned>(Card::Rebuild::COST) },
        { Card::DragonsBreath::NAME, static_cast<unsigned>(Card::DragonsBreath::COST) },
        { Card::DoubleEdge::NAME, static_cast<unsigned>(Card::DoubleEdge::COST) },
        { Card::Meteor::NAME, static_cast<unsigned>(Card::Meteor::COST) },
        { Card::CunningWizardry::NAME, static_cast<unsigned>(Card::CunningWizardry::COST) },
        { Card::Refreshment::NAME, static_cast<unsigned>(Card::Refreshment::COST) },
        { Card::Armageddon::NAME, static_cast<unsigned>(Card::Armageddon::COST) },
        { Card::Thievery::NAME, static_cast<unsigned>(Card::Thievery::COST) },
        { Card::Combustion::NAME, static_cast<unsigned>(Card::Combustion::COST) },
        { Card::WrathOfBorgoldin::NAME, static_cast<unsigned>(Card::WrathOfBorgoldin::COST) },
        { Card::BreakLoose::NAME, static_cast<unsigned>(Card::BreakLoose::COST) },
        { Card::ArrowVolley::NAME, static_cast<unsigned>(Card::ArrowVolley::COST) },
        { Card::OneManArmy::NAME, static_cast<unsigned>(Card::OneManArmy::COST) },
        { Card::Aristocracy::NAME, static_cast<unsigned>(Card::Aristocracy::COST) },
        { Card::CavalryAssembly::NAME, static_cast<unsigned>(Card::CavalryAssembly::COST) },
        { Card::Firesword::NAME, static_cast<unsigned>(Card::Firesword::COST) },
        { Card::BridgeOfDeath::NAME, static_cast<unsigned>(Card::BridgeOfDeath::COST) },
        { Card::Flood::NAME, static_cast<unsigned>(Card::Flood::COST) },
        { Card::Bracers::NAME, static_cast<unsigned>(Card::Bracers::COST) },
        { Card::FatalWounds::NAME, static_cast<unsigned>(Card::FatalWounds::COST) }
    };
}

std::map<std::string, std::string> Card::_create_card_names_to_path()
{
    return std::map<std::string, std::string> {
        { Card::HolyLight::NAME, Card::HolyLight::IMG_PATH },
        { Card::WarCry::NAME, Card::WarCry::IMG_PATH} ,
        { Card::DeadlyPoison::NAME, Card::DeadlyPoison::IMG_PATH },
        { Card::CrushingBlow::NAME, Card::CrushingBlow::IMG_PATH },
        { Card::Thunderstorm::NAME, Card::Thunderstorm::IMG_PATH} ,
        { Card::CallToArms::NAME, Card::CallToArms::IMG_PATH },
        { Card::Teleport::NAME, Card::Teleport::IMG_PATH },
        { Card::AdrenalineRush::NAME, Card::AdrenalineRush::IMG_PATH },
        { Card::Silence::NAME, Card::Silence::IMG_PATH },
        { Card::Affliction::NAME, Card::Affliction::IMG_PATH },
        { Card::Rebuild::NAME, Card::Rebuild::IMG_PATH },
        { Card::DragonsBreath::NAME, Card::DragonsBreath::IMG_PATH },
        { Card::DoubleEdge::NAME, Card::DoubleEdge::IMG_PATH },
        { Card::Meteor::NAME, Card::Meteor::IMG_PATH },
        { Card::CunningWizardry::NAME, Card::CunningWizardry::IMG_PATH },
        { Card::Refreshment::NAME, Card::Refreshment::IMG_PATH },
        { Card::Armageddon::NAME, Card::Armageddon::IMG_PATH },
        { Card::Thievery::NAME, Card::Thievery::IMG_PATH },
        { Card::Combustion::NAME, Card::Combustion::IMG_PATH },
        { Card::WrathOfBorgoldin::NAME, Card::WrathOfBorgoldin::IMG_PATH },
        { Card::BreakLoose::NAME, Card::BreakLoose::IMG_PATH },
        { Card::ArrowVolley::NAME, Card::ArrowVolley::IMG_PATH },
        { Card::OneManArmy::NAME, Card::OneManArmy::IMG_PATH },
        { Card::Aristocracy::NAME, Card::Aristocracy::IMG_PATH },
        { Card::CavalryAssembly::NAME, Card::CavalryAssembly::IMG_PATH },
        { Card::Firesword::NAME, Card::Firesword::IMG_PATH },
        { Card::BridgeOfDeath::NAME, Card::BridgeOfDeath::IMG_PATH },
        { Card::Flood::NAME, Card::Flood::IMG_PATH },
        { Card::Bracers::NAME, Card::Bracers::IMG_PATH },
        { Card::FatalWounds::NAME, Card::FatalWounds::IMG_PATH }
    };
}

std::map<std::string, Card::Rarity> Card::_create_card_names_to_rarity()
{
    // Card name to rarity mapping
    return std::map<std::string, Card::Rarity> {
        { Card::HolyLight::NAME, Card::HolyLight::RARITY },
        { Card::WarCry::NAME, Card::WarCry::RARITY} ,
        { Card::DeadlyPoison::NAME, Card::DeadlyPoison::RARITY },
        { Card::CrushingBlow::NAME, Card::CrushingBlow::RARITY },
        { Card::Thunderstorm::NAME, Card::Thunderstorm::RARITY} ,
        { Card::CallToArms::NAME, Card::CallToArms::RARITY },
        { Card::Teleport::NAME, Card::Teleport::RARITY },
        { Card::AdrenalineRush::NAME, Card::AdrenalineRush::RARITY },
        { Card::Silence::NAME, Card::Silence::RARITY },
        { Card::Affliction::NAME, Card::Affliction::RARITY },
        { Card::Rebuild::NAME, Card::Rebuild::RARITY },
        { Card::DragonsBreath::NAME, Card::DragonsBreath::RARITY },
        { Card::DoubleEdge::NAME, Card::DoubleEdge::RARITY },
        { Card::Meteor::NAME, Card::Meteor::RARITY },
        { Card::CunningWizardry::NAME, Card::CunningWizardry::RARITY },
        { Card::Refreshment::NAME, Card::Refreshment::RARITY },
        { Card::Armageddon::NAME, Card::Armageddon::RARITY },
        { Card::Thievery::NAME, Card::Thievery::RARITY },
        { Card::Combustion::NAME, Card::Combustion::RARITY },
        { Card::WrathOfBorgoldin::NAME, Card::WrathOfBorgoldin::RARITY },
        { Card::BreakLoose::NAME, Card::BreakLoose::RARITY },
        { Card::ArrowVolley::NAME, Card::ArrowVolley::RARITY },
        { Card::OneManArmy::NAME, Card::OneManArmy::RARITY },
        { Card::Aristocracy::NAME, Card::Aristocracy::RARITY },
        { Card::CavalryAssembly::NAME, Card::CavalryAssembly::RARITY },
        { Card::Firesword::NAME, Card::Firesword::RARITY },
        { Card::BridgeOfDeath::NAME, Card::BridgeOfDeath::RARITY },
        { Card::Flood::NAME, Card::Flood::RARITY },
        { Card::Bracers::NAME, Card::Bracers::RARITY },
        { Card::FatalWounds::NAME, Card::FatalWounds::RARITY }
    };
}

std::map<Card::Rarity, double> Card::_create_card_rarity_to_prob()
{
    return std::map<Card::Rarity, double> {
        { Card::Rarity::COMMON, 0.4 },
        { Card::Rarity::UNCOMMON, 0.3 },
        { Card::Rarity::RARE, 0.2 },
        { Card::Rarity::MYTHICAL, 0.07 },
        { Card::Rarity::LEGENDARY, 0.03 }
    };
}

std::vector<std::string> Card::_create_cards_by_rarity(Card::Rarity rarity)
{
    std::vector<std::string> cards;
    for (const auto& [card, _rarity] : CARD_NAMES_TO_RARITY) {
        if (_rarity == rarity) {
            cards.push_back(card);
        }
    }
    return cards;
}

