#ifndef CARDINFO_HPP
#define CARDINFO_HPP

#include <QObject>
#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QButtonGroup>
#include <QPushButton>
#include <QLinkedList>
#include"../engine/player.hpp"
#include<map>

class CardInfo : public QObject, public QGraphicsItem
{
    Q_OBJECT
public:
    static CardInfo* create_cardInfo(const Player* player1, const Player* player2, QGraphicsScene* scene = nullptr);

    void set_player_cards(std::map<std::string,int> cards,unsigned num_of_enemy_cards);
    void set_remaining_steps(unsigned steps);
    void set_player_turn(Player::Index player);
    void set_current_piece(Piece *p);
    void set_enemy_piece(Piece *p);

    void set_colors(QColor& friendly_color, QColor& enemy_color) const;

    // QGraphicsItem interface
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

signals:
    void call_clicked_activate_card(std::string);

private slots:
    void change_cards_right();
    void change_cards_left();
    void activate_clicked();


private:

    void set_card_image();
    void add_buttons();

    CardInfo(const Player* player1, const Player* player2, QGraphicsScene* scene = nullptr);

    //we don't want to have cctor and copy assign operator
    CardInfo (const CardInfo&) = delete;
    CardInfo& operator= (const CardInfo&) = delete;

    QGraphicsScene* m_scene;

    QGraphicsRectItem* rect_item_card;

    QPushButton* btn_left_arrow;
    QPushButton* btn_right_arrow;
    QPushButton* btn_activate;

    unsigned m_remaining_steps;
    Player::Index m_player_turn;

    const Player* m_player1;
    const Player* m_player2;
    Piece*  m_piece;

    std::map<std::string,int> m_cards;
    QLinkedList <std::string> cards_list;

    unsigned m_num_of_enemy_cards;

};

#endif // CARDINFO_HPP
