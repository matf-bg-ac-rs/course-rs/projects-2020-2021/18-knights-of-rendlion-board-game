#ifndef GAMEINFO_HPP
#define GAMEINFO_HPP

#include <QGraphicsItem>
#include <QPainter>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsScene>
#include <QButtonGroup>
#include <QTextEdit>
#include <QApplication>

#include <iostream>
#include <string>
#include <vector>
#include<map>

#include "../pieces/knight.hpp"
#include "../pieces/archer.hpp"
#include "../pieces/catapult.hpp"
#include "../pieces/horseman.hpp"
#include "../pieces/mage.hpp"
#include "../pieces/priest.hpp"
#include "../pieces/shieldman.hpp"
#include "../pieces/spearman.hpp"
#include "../engine/gameController.hpp"


class Knight;
class Archer;
class Catapult;
class Horseman;
class Mage;
class Priest;
class Shieldman;
class Spearman;

class GameInfo :  public QObject , public QGraphicsItem
{
public:

    static GameInfo* create_gameInfo(QGraphicsScene* scene = nullptr, unsigned num_of_pieces_to_choose = 0);

    void mousePressEvent(QGraphicsSceneMouseEvent *event);

    //buttons initializations functions
    void add_buttons_init();
    void add_buttons_play();

    //Setters
    void set_player_turn(Player::Index player);
    void set_current_piece(Piece *p);
    void set_field_info(std::vector<QString> field_info);
    void set_remaining_steps(unsigned steps);
    void set_paint_color (int n, QPainter *painter);
    void set_width(unsigned width);
    void set_colors(QColor& friendly_color, QColor& enemy_color) const;
    void set_enemy_info(QString enemy_info);
    void set_enemy_piece(Piece* p);

    QString piece_init_info(int i) const;
    QString piece_spell_info(int i) const;
    QString piece_spell_info(Piece::Type t) const;
    QString piece_name(int i) const;
    std::map<QString, QString>default_values_map (int i) const;


    int get_spell_range_index() const;
    // QGraphicsItem interface
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

    Q_OBJECT

signals:
    void call_init_select();
    void call_set_clicked_move();
    void call_set_clicked_attack();
    void call_set_clicked_skip_turn();
    void call_update_index(int);
    void call_pressed_attack_range();
    void call_released_attack_range();
    void call_pressed_spell_range();
    void call_released_spell_range();
    void call_spell_cast(std::string, int);



public slots:
    void change_btn_group_index(int i);
    void reset_buttons(int i);
    void enable_btn_move_attack(bool move, bool attack);
    void disable_btn_move_attack();
    void clicked_move();
    void clicked_attack();
    void clicked_skip_turn();
    void pressed_attack_range();
    void released_attack_range();
    void change_spell_index(int i);
    void change_spell_range_index_pressed(int i);
    void change_spell_range_index_released(int i);
    void set_available_spells(std::vector<bool> spells_to_enable);
    void disable_range_btns();
    void disable_spell_btns();

private:

    //Constructor
    GameInfo(QGraphicsScene* scene = nullptr, unsigned num_of_pieces_to_choose = 0);

    //we don't want to have cctor and copy assign operator
    GameInfo (const GameInfo&) = delete;
    GameInfo& operator= (const GameInfo&) = delete;

    QGraphicsScene* m_scene;
    QButtonGroup btn_group;
    QButtonGroup btn_group2;
    QPushButton* button_move;
    QPushButton* button_attack;
    QPushButton* button_attack_range;
    QPushButton* button_skip_turn;
    QTextEdit* spells_info;
    int m_piece_index;
    int m_spell_index;
    int m_spell_range_index;
    Player::Index m_player_turn;
    Piece* m_piece;
    std::vector<QString> m_field_info;
    QString m_enemy_info;
    unsigned m_width;
    std::vector<QPushButton*> m_buttons;
    std::vector<QPushButton*> m_buttons_range;
    std::vector<QGraphicsRectItem*> m_fields;
    std::vector<QGraphicsRectItem*> m_pieces;
    unsigned remaining_steps;
    int m_remaining_pieces;

    const std::map<unsigned , QString> m_index_to_piece {
        {0 , ":/images/pieces/knight.png"},
        {1 , ":/images/pieces/archer.png"},
        {2 , ":/images/pieces/catapult.png"},
        {3 , ":/images/pieces/horseman.png"},
        {4 , ":/images/pieces/mage.png"},
        {5 , ":/images/pieces/priest.png"},
        {6 , ":/images/pieces/shieldman.png"},
        {7 , ":/images/pieces/spearman.png"},
        {8 , ":/images/pieces/castle.png"}
    };

    const std::map <unsigned , QString> m_index_to_field{

        {0 , QString(":images/fields/ground.png")},
        {1 , QString(":images/fields/plain5.png")},
        {2 , QString(":images/fields/forest.png")},
        {3 , QString(":images/fields/bridge.png")},
        {4 , QString(":images/fields/hill1.png")}
    };
};

#endif // GAMEINFO_HPP
