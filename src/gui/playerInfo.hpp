#ifndef PLAYERINFO_HPP
#define PLAYERINFO_HPP

#include <QGraphicsItem>
#include <QPainter>
#include <QString>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsRectItem>

#include <iostream>
#include <vector>
#include <map>

class Player;

class PlayerInfo : public QGraphicsItem
{
public:

    //Method that creates two objects of this class
    static std::pair<PlayerInfo*, PlayerInfo*> create_playersInfo(const Player* player1, const Player* player2);

    void mousePressEvent(QGraphicsSceneMouseEvent *event);

    // QGraphicsItem interface
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

private:

    //Constructor
    PlayerInfo(const Player* player);

    //we don't want to have cctor and copy assign operator
    PlayerInfo (const PlayerInfo&) = delete;
    PlayerInfo& operator= (const PlayerInfo&) = delete;

    //Data
    const Player* m_player;
    std::vector<QGraphicsRectItem*> m_rect_icons;

    const std::map<unsigned , QString> m_index_to_piece {
        {0 , ":/images/pieces/knight.png"},
        {1 , ":/images/pieces/archer.png"},
        {2 , ":/images/pieces/catapult.png"},
        {3 , ":/images/pieces/horseman.png"},
        {4 , ":/images/pieces/mage.png"},
        {5 , ":/images/pieces/priest.png"},
        {6 , ":/images/pieces/shieldman.png"},
        {7 , ":/images/pieces/spearman.png"},
        {8 , ":/images/pieces/castle.png"}
    };
};

#endif // PLAYERINFO_HPP
