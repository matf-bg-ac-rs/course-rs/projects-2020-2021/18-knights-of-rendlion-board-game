#include "field.hpp"

#include <QPen>
#include <QPainter>

#include "../globals.hpp"


std::string Field::type_str(Field::Type field_type)
{
    switch (field_type) {
        case Type::GROUND:
            return "Ground";
        case Type::PLAIN:
            return "Plain";
        case Type::HILL:
            return "Hill";
        case Type::FOREST:
            return "Forest";
        case Type::MOUNTAIN:
            return "Mountain";
        case Type::WATER:
            return "Water";
        case Type::BRIDGE:
            return "Bridge";
        case Type::RBRIDGE:
            return "Reversed bridge";
        case Type::RWATER:
            return "Water";
        case Type::CROSS_WATER:
            return "Water";
        case Type::CROSS_WATER1:
            return "Water";
        case Type::CROSS_WATER2:
            return "Water";
        case Type::CROSS_WATER3:
            return "Water";
        case Type::CROSS_WATER4:
            return "Water";
        default:
            throw "Unknown field type";
    }
}

QString Field::type_Qstr(Field::Type field_type)
{
    return QString(Field::type_str(field_type).data());
}

std::vector<QString> Field::bonus_Qinfo_for_all_pieces(Field::Type field_type)
{
    std::vector<QString> field_info;

    std::pair<int, int> archer_bonuses;
    std::pair<int, int> catapult_bonuses;
    std::pair<int, int> horseman_bonuses;
    std::pair<int, int> knight_bonuses;
    std::pair<int, int> mage_bonuses;
    std::pair<int, int> priest_bonuses;
    std::pair<int, int> shieldman_bonuses;
    std::pair<int, int> spearman_bonuses;

    switch (field_type) {
        case Field::Type::GROUND:
            archer_bonuses = Archer::ground_bonuses();
            catapult_bonuses = Catapult::ground_bonuses();
            horseman_bonuses = Horseman::ground_bonuses();
            knight_bonuses = Knight::ground_bonuses();
            mage_bonuses = Mage::ground_bonuses();
            priest_bonuses = Priest::ground_bonuses();
            shieldman_bonuses = Shieldman::ground_bonuses();
            spearman_bonuses = Spearman::ground_bonuses();
            break;
        case Field::Type::PLAIN:
            archer_bonuses = Archer::plain_bonuses();
            catapult_bonuses = Catapult::plain_bonuses();
            horseman_bonuses = Horseman::plain_bonuses();
            knight_bonuses = Knight::plain_bonuses();
            mage_bonuses = Mage::plain_bonuses();
            priest_bonuses = Priest::plain_bonuses();
            shieldman_bonuses = Shieldman::plain_bonuses();
            spearman_bonuses = Spearman::plain_bonuses();
            break;
        case Field::Type::HILL:
            archer_bonuses = Archer::hill_bonuses();
            catapult_bonuses = Catapult::hill_bonuses();
            horseman_bonuses = Horseman::hill_bonuses();
            knight_bonuses = Knight::hill_bonuses();
            mage_bonuses = Mage::hill_bonuses();
            priest_bonuses = Priest::hill_bonuses();
            shieldman_bonuses = Shieldman::hill_bonuses();
            spearman_bonuses = Spearman::hill_bonuses();
            break;
        case Field::Type::FOREST:
            archer_bonuses = Archer::forest_bonuses();
            catapult_bonuses = Catapult::forest_bonuses();
            horseman_bonuses = Horseman::forest_bonuses();
            knight_bonuses = Knight::forest_bonuses();
            mage_bonuses = Mage::forest_bonuses();
            priest_bonuses = Priest::forest_bonuses();
            shieldman_bonuses = Shieldman::forest_bonuses();
            spearman_bonuses = Spearman::forest_bonuses();
            break;
        case Field::Type::BRIDGE:
        case Field::Type::RBRIDGE:
            archer_bonuses = Archer::bridge_bonuses();
            catapult_bonuses = Catapult::bridge_bonuses();
            horseman_bonuses = Horseman::bridge_bonuses();
            knight_bonuses = Knight::bridge_bonuses();
            mage_bonuses = Mage::bridge_bonuses();
            priest_bonuses = Priest::bridge_bonuses();
            shieldman_bonuses = Shieldman::bridge_bonuses();
            spearman_bonuses = Spearman::bridge_bonuses();
            break;
        default:
            archer_bonuses = std::make_pair(0, 0);
            catapult_bonuses = std::make_pair(0, 0);
            horseman_bonuses = std::make_pair(0, 0);
            knight_bonuses = std::make_pair(0, 0);
            mage_bonuses = std::make_pair(0, 0);
            priest_bonuses = std::make_pair(0, 0);
            shieldman_bonuses = std::make_pair(0, 0);
            spearman_bonuses = std::make_pair(0, 0);
    }



    field_info.push_back(QString(Field::type_str(field_type).c_str()));

    field_info.push_back(QString("\nArcher:\nCatapult:\nHorseman:\nKnight:\nMage:\nPriest:\nShieldman:\nSpearman:\n"));

    field_info.push_back(QString(QString("HEALTH\n") + QString("\t") + number_to_qstring(archer_bonuses.first) + QString("\n\t")+ number_to_qstring(catapult_bonuses.first) + QString("\n\t") +
                                 number_to_qstring(horseman_bonuses.first) + QString("\n\t") + number_to_qstring(knight_bonuses.first) + QString("\n\t") + number_to_qstring(mage_bonuses.first) +
                                 QString("\n\t") + number_to_qstring(priest_bonuses.first) + QString("\n\t") + number_to_qstring(shieldman_bonuses.first) + QString("\n\t") +
                                 number_to_qstring(spearman_bonuses.first) + QString("\n\t")));

    field_info.push_back(QString(QString("DAMAGE\n\t")+number_to_qstring(archer_bonuses.second) + QString("\n\t")+ number_to_qstring(catapult_bonuses.second) + QString("\n\t") +
                                 number_to_qstring(horseman_bonuses.second) + QString("\n\t") + number_to_qstring(knight_bonuses.second) + QString("\n\t") + number_to_qstring(mage_bonuses.second) +
                                 QString("\n\t") + number_to_qstring(priest_bonuses.second) + QString("\n\t") + number_to_qstring(shieldman_bonuses.second) + QString("\n\t") +
                                 number_to_qstring(spearman_bonuses.second) + QString("\n")));


    return field_info;
}



QString Field::number_to_qstring(int number){
    if(number<0){
        return (QString::number(number) + QString("%"));
    }
    else {
        return (QString(" ") + QString::number(number) + QString("%"));
    }
}

QRectF Field::boundingRect() const {
    return QRectF(0,0,50,50);
}

const QRect Field::get_rect() const {
    return QRect(0,0,50,50);
}

void Field::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget){
    Q_UNUSED(option);
    Q_UNUSED(widget);

    //this will be painted always
    painter->setPen(Qt::NoPen);
    painter->drawPixmap(get_rect(),QPixmap(m_paths.at(m_field_type)));

    //this will be painted only if the field is part of the staging area of some player
    if(GameController::Part::INIT == globals::GAME_PART && this->m_marked){

        mark(painter,m_column < 4 ? Qt::red : Qt::blue,2,2.0);
    }
    //this paints field of active piece
    else if(GameController::Part::PLAY == globals::GAME_PART && m_clicked){

        mark(painter,Qt::black,3,2.0);

        //this will be painted if caster can receive own spell
        if(m_marked)
            mark(painter,Qt::white,2,4.0);

    }
    //this paints fields that are available for move/attack/casting a spell (and it's not active piece field)
    else if(GameController::Part::PLAY == globals::GAME_PART && m_marked && !m_clicked){

        //range for attack or spell
        if(globals::ACTION == GameController::Actions::RANGE){
            mark(painter,Qt::magenta,2,2.0);
        }
        else if(globals::ACTION == GameController::Actions::MOVE){
            mark(painter,Qt::yellow,2,2.0);
        }
        else if (globals::ACTION == GameController::Actions::ATTACK){
            mark(painter,Qt::red,2,2.0);
        }
        else if (globals::ACTION == GameController::Actions::SPELL){
            mark(painter,Qt::white,2,2.0);
        }
        else if (globals::ACTION == GameController::Actions::CARD){
            mark(painter,Qt::white,2,2.0);
        }
    }

    //health-bar part
    if(has_piece()){

        // health bar coef
        double line_coef  = m_piece->health()/m_piece->get_default_health();

        QColor color;

        if(line_coef > 1){
            line_coef = 1;
            color = QColor(50,235,100);
        }
        else if(line_coef >= 0.8){
            color = Qt::green;
        }
        else if (line_coef >= 0.6){
            color = Qt::yellow;
        }
        else if (line_coef >= 0.3){
            color = QColor(255,140,0);
        }
        else{
            color =  Qt::red;
        }

        if(get_piece()->has_spell_effect("HOLLY_SHIELD_IMMUNITY"))
            painter->setPen(QPen(Qt::white,5,Qt::SolidLine));
        else
            painter->setPen(QPen(color,5,Qt::SolidLine));

        painter->drawLine(5,6,(5+line_coef*40),6);

    }//end health-bar part

}

//Getters
Field::Type Field::field_type() const {
    return m_field_type;
}


bool Field::is_standable() const{
    return m_standable;
}

void Field::set_row_index(unsigned i){
    m_row = i;
}

void Field::set_column_index(unsigned j){
    m_column = j;
}

void Field::set_piece(Piece* piece){
    m_piece = piece;
    m_piece->setParentItem(this);
    m_piece->setPos(5.0,9.0);
}

void Field::set_type(Field::Type new_type)
{
    m_field_type = new_type;
}

Piece* Field::get_piece() const
{
    return m_piece;
}

const std::pair<unsigned, unsigned> Field::get_board_index() const
{
    return std::make_pair<>(m_row,m_column);
}

bool Field::has_piece() const
{
    if(nullptr == this->m_piece)
        return false;
    else
        return true;
}

bool Field::has_card() const
{
    if (m_card.empty())
        return false;
    else
        return true;
}

unsigned Field::get_row_index() const{
    return m_row;
}

unsigned Field::get_column_index() const{
    return m_column;
}

std::string Field::get_card() const{
    return m_card;
}

void Field::remove_piece()
{
    m_piece = nullptr;
}

void Field::set_marked(bool mark){
    m_marked = mark;
}

bool Field::is_available() const{
    return m_standable && (m_piece == nullptr);
}

bool Field::is_clicked() const{
    return m_clicked;
}

void Field::set_clicked(bool b){
    m_clicked = b;
}

void Field::set_card(std::string card_name){
    m_card = card_name;
}

void Field::mark(QPainter* painter, QColor color, double thickness, double start_point)
{
    QPen pen(color,thickness);
    painter->setPen(pen);
    double end_point = 50 - start_point;
    QVector<QLineF> lines;
    QLineF line(start_point,start_point,end_point,start_point);
    lines.push_back(line);
    line.setLine(end_point,start_point,end_point,end_point);
    lines.push_back(line);
    line.setLine(end_point,end_point,start_point,end_point);
    lines.push_back(line);
    line.setLine(start_point,end_point,start_point,start_point);
    lines.push_back(line);
    painter->drawLines(lines);

}

bool Field::is_marked() const {
    return m_marked;
}

std::pair<int, int> Field::get_field_bonuses()
{
    switch (m_field_type) {
        case Field::Type::GROUND:
            return m_piece->get_ground_bonuses();
        case Field::Type::PLAIN:
            return m_piece->get_plain_bonuses();
        case Field::Type::HILL:
            return m_piece->get_hill_bonuses();
        case Field::Type::FOREST:
            return m_piece->get_forest_bonuses();
        case Field::Type::BRIDGE:
        case Field::Type::RBRIDGE:
            return m_piece->get_bridge_bonuses();
        default:
            return std::make_pair(0, 0);
    }
}

void Field::activate_field_bonuses()
{
    // Extract bonuses in percentage depending on field type and piece type
    auto [health_bonus_percent, damage_bonus_percent] = get_field_bonuses();

    // Calculate exact health bonus
    double health_bonus = static_cast<double>(health_bonus_percent) / 100 * m_piece->health();

    // Memorize (in piece) last bonus in percentage and in exact value
    m_piece->set_last_field_health_bonus_percent(health_bonus_percent);
    m_piece->set_last_field_health_bonus(health_bonus);

    // Activate health bonus for piece standing on this field
    m_piece->set_health(m_piece->health() + health_bonus);

    // Repeat the same for damage bonus
    double damage_bonus = static_cast<double>(damage_bonus_percent) / 100 * m_piece->damage();
    m_piece->set_last_field_damage_bonus_percent(damage_bonus_percent);
    m_piece->set_last_field_damage_bonus(damage_bonus);
    m_piece->set_damage(m_piece->damage() + damage_bonus);
}

void Field::reset_field_bonuses()
{
    // Remove last bonus accordingly (if piece's current health is greater than last bonus,
    // remove bonus in percentage of current health so that piece doesn't die by bonus removal)
    double health_loss = m_piece->health() > m_piece->last_field_health_bonus()
            ? m_piece->last_field_health_bonus()
            : static_cast<double>(m_piece->last_field_health_bonus_percent()) / 100 * m_piece->health();

    m_piece->set_health(m_piece->health() - health_loss);
    m_piece->set_damage(m_piece->damage() - m_piece->last_field_damage_bonus());
}


    const std::map<Field::Type,QString> Field::m_paths {
        {Field::Type::BRIDGE , QString(":/images/fields/bridge.png")},
        {Field::Type::RBRIDGE , QString(":/images/fields/bridge_reverse1.png")},
        {Field::Type::FOREST , QString(":/images/fields/forest.png")},
        {Field::Type::PLAIN , QString(":/images/fields/plain5.png")},
        {Field::Type::HILL , QString(":/images/fields/hill1.png")},
        {Field::Type::MOUNTAIN , QString(":/images/fields/mountain1.png")},
        {Field::Type::WATER , QString(":/images/fields/water_1.png")},
        {Field::Type::CROSS_WATER , QString(":/images/fields/water_3.png")},
        {Field::Type::CROSS_WATER1 , QString(":/images/fields/water_4.png")},
        {Field::Type::CROSS_WATER2 , QString(":/images/fields/water_5.png")},
        {Field::Type::CROSS_WATER3 , QString(":/images/fields/water_6.png")},
        {Field::Type::CROSS_WATER4 , QString(":/images/fields/water_7.png")},
        {Field::Type::RWATER , QString(":/images/fields/water_2.png")},
        {Field::Type::GROUND , QString(":/images/fields/ground.png")}
    };
