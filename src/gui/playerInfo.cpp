#include "playerInfo.hpp"

#include <QPixmap>
#include <QColor>
#include <QBrush>

#include "../engine/player.hpp"

#include <string>
#include <cmath>

PlayerInfo::PlayerInfo(const Player* player)
    : m_player(player)
{

    // piece icons initialization
    for(unsigned i=0 ; i<8 ; i++){
        QGraphicsRectItem* rect_item = new QGraphicsRectItem(0,0,40,40,this);
        rect_item->setPen(Qt::NoPen);
        rect_item->setPos(5,150+i*75);
        QPixmap pix(m_index_to_piece.at(i));
        rect_item->setBrush(QBrush(pix.scaled(QSize(40,40),Qt::KeepAspectRatio)));
        m_rect_icons.push_back(rect_item);
    }
}

QRectF PlayerInfo::boundingRect() const {
    return QRectF(0,0,50,750);
}

void PlayerInfo::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget){
    Q_UNUSED(option);
    Q_UNUSED(widget);

    //paint the whole column
    painter ->fillRect(boundingRect (),QColor(240,234,214));

    //add coloring according to player
    QColor color;
    Player::Index::PLAYER_1 == m_player->get_player_index() ?
                color = Qt::red : color = Qt::blue;
    painter->fillRect(QRectF(0,0,50,110),color);
    painter->fillRect(QRectF(0,110,2,640),color);
    painter->fillRect(QRectF(48,110,2,640),color);
    painter->fillRect(QRectF(2,140,46,2),color);

    //print health
    painter->setPen(Qt::white);
    painter->setFont(QFont("Times",22));
    painter->drawText(5,5,40,20,Qt::AlignHCenter | Qt::AlignVCenter,QString("HP"));

    std::string health = std::to_string(static_cast<unsigned>(std::floor(m_player->get_castles_health())));

    painter->setFont(QFont("Times",18, QFont::DemiBold));
    for(unsigned i=0; i< health.length();i++){
        painter->drawText(5,i*20+35,40,20,Qt::AlignHCenter | Qt::AlignVCenter,QString(health.at(i)));
    }

    //add number of living pieces
    painter->setFont(QFont("Times", 20, QFont::Bold));
    painter->setPen(Qt::green);
    painter->drawText(5,110,40,30,Qt::AlignHCenter | Qt::AlignVCenter,QString::number(m_player->get_num_of_living_pieces()));


    //add numbers bellow icons
    painter->setFont(QFont("Times",13,QFont::DemiBold));
    for(unsigned i=0 ; i< 8 ; i++){

        auto live_dead = m_player->get_live_dead_by_index(i);

        //alive
        painter->setPen(Qt::green);
        painter->drawText(5,200+i*75,20,15,Qt::AlignHCenter | Qt::AlignVCenter,QString::number(live_dead.first));

        //dead
        painter->setPen(Qt::red);
        painter->drawText(25,200+i*75,20,15,Qt::AlignHCenter | Qt::AlignVCenter,QString::number(live_dead.second));

    }
}


std::pair<PlayerInfo* , PlayerInfo* > PlayerInfo::create_playersInfo(const Player* player1, const Player* player2)
{
    static std::pair<PlayerInfo*, PlayerInfo*> players_info =
            std::make_pair<>(new PlayerInfo(player1),new PlayerInfo(player2));

    return players_info;
}

//this registers positions of a click on Column
void PlayerInfo::mousePressEvent(QGraphicsSceneMouseEvent *event){
//    if(event->button() & Qt::LeftButton)
//        std::cout << event->pos().x() << "," << event->pos().y() << std::endl;
}
