#ifndef CARD_HPP
#define CARD_HPP

#include "../engine/player.hpp"
#include "../utility/utility.hpp"
#include <iostream>
#include <vector>

class Card
{
public:
    enum class Type {
        FRIENDLY_TARGET,
        ENEMY_TARGET,
        BOTH_TARGETS,
        FRIENDLY_EFFECT,
        ENEMY_EFFECT,
        MIXED,
        RANDOM
    };

    enum class Rarity {
        COMMON,
        UNCOMMON,
        RARE,
        MYTHICAL,
        LEGENDARY
    };


    static const std::vector<std::string> CARD_NAMES;
    static const std::map<std::string, unsigned> CARD_NAMES_TO_COST;
    static const std::map<std::string, std::string> CARD_NAMES_TO_PATH;
    static const std::map<std::string, Card::Rarity> CARD_NAMES_TO_RARITY;
    static const std::map<Card::Rarity, double> CARD_RARITY_TO_PROB;

    static const std::vector<std::string> COMMON_CARDS;
    static const std::vector<std::string> UNCOMMON_CARDS;
    static const std::vector<std::string> RARE_CARDS;
    static const std::vector<std::string> MYTHICAL_CARDS;
    static const std::vector<std::string> LEGENDARY_CARDS;

    static std::vector<std::string> cards_by_rarity(Card::Rarity rarity);
    static std::string random_card();
    static std::string random_card_by_rarity();
    static std::string rarity_to_name(Card::Rarity rarity);

    // Card classes
    class HolyLight {
    public:
        static const std::string IMG_PATH;
        static const std::string NAME;
        static const Card::Type TYPE;
        static const Card::Rarity RARITY;
        static const unsigned COST =  2;
        static const unsigned HEAL =  70;
        static const bool IS_INSTANT_CAST = false;
    };

    class WarCry {
    public:
        static const std::string IMG_PATH;
        static const std::string NAME;
        static const Card::Type TYPE;
        static const Card::Rarity RARITY;
        static const unsigned COST = 1;
        static const int HEALTH_BONUS_PERCENT = 25;
        static const int DAMAGE_BONUS_PERCENT = 25;
        static const bool IS_INSTANT_CAST = false;
    };

    class DeadlyPoison {
    public:
        static const std::string IMG_PATH;
        static const std::string NAME;
        static const Card::Type TYPE;
        static const Card::Rarity RARITY;
        static const unsigned COST =  3;
        static const unsigned DAMAGE_PER_TURN = 18;
        static const unsigned DURATION = 6;
        static const bool IS_INSTANT_CAST = false;
    };

    class CrushingBlow {
    public:
        static const std::string IMG_PATH;
        static const std::string NAME;
        static const Card::Type TYPE;
        static const Card::Rarity RARITY;
        static const unsigned COST =  2;
        static const unsigned DAMAGE = 65;
        static const bool IS_INSTANT_CAST = false;
    };

    class Thunderstorm {
    public:
        static const std::string IMG_PATH;
        static const std::string NAME;
        static const Card::Type TYPE;
        static const Card::Rarity RARITY;
        static const unsigned COST = 1;
        static const unsigned DIRECT_DAMAGE = 40;
        static const unsigned ADDITIONAL_DAMAGE = 25;
        static const unsigned NUM_OF_ADDITIONAL_FIELDS = 3;
        static const bool IS_INSTANT_CAST = false;
    };

    class CallToArms {
    public:
        static const std::string IMG_PATH;
        static const std::string NAME;
        static const Card::Type TYPE;
        static const Card::Rarity RARITY;
        static const unsigned COST = 2;
        static const bool IS_INSTANT_CAST = true;
    };

    class Teleport {
    public:
        static const std::string IMG_PATH;
        static const std::string NAME;
        static const Card::Type TYPE;
        static const Card::Rarity RARITY;
        static const unsigned COST = 2;
        static const bool IS_INSTANT_CAST = false;
    };

    class AdrenalineRush {
    public:
        static const std::string IMG_PATH;
        static const std::string NAME;
        static const Card::Type TYPE;
        static const Card::Rarity RARITY;
        static const unsigned COST = 0;
        static const unsigned ADDITIONAL_MOVES = 2;
        static const bool IS_INSTANT_CAST = true;
    };

    class Silence {
    public:
        static const std::string IMG_PATH;
        static const std::string NAME;
        static const Card::Type TYPE;
        static const Card::Rarity RARITY;
        static const unsigned COST = 0;
        static const unsigned MOVES_REDUCTION = 2;
        static const bool IS_INSTANT_CAST = true;
    };

    class Affliction {
    public:
        static const std::string IMG_PATH;
        static const std::string NAME;
        static const Card::Type TYPE;
        static const Card::Rarity RARITY;
        static const unsigned COST = 2;
        static const unsigned HEALTH_REDUCTION_PERCENT = 33;
        static const unsigned DAMAGE_REDUCTION_PERCENT = 33;
        static const bool IS_INSTANT_CAST = false;
    };

    class Rebuild {
    public:
        static const std::string IMG_PATH;
        static const std::string NAME;
        static const Card::Type TYPE;
        static const Card::Rarity RARITY;
        static const unsigned COST = 1;
        static const unsigned HEAL = 80;
        static const bool IS_INSTANT_CAST = false;
    };

    class DragonsBreath {
    public:
        static const std::string IMG_PATH;
        static const std::string NAME;
        static const Card::Type TYPE;
        static const Card::Rarity RARITY;
        static const unsigned COST =  3;
        static const unsigned DAMAGE = 40;
        static const unsigned ORTHO_RANGE = 4;
        static const unsigned DIAG_RANGE = 3;
        static const bool USE_G_FIELDS = true;
        static const bool IS_INSTANT_CAST = false;
    };

    class DoubleEdge {
    public:
        static const std::string IMG_PATH;
        static const std::string NAME;
        static const Card::Type TYPE;
        static const Card::Rarity RARITY;
        static const unsigned COST = 1;
        static const bool IS_INSTANT_CAST = false;
    };

    class Meteor {
    public:
        static const std::string IMG_PATH;
        static const std::string NAME;
        static const Card::Type TYPE;
        static const Card::Rarity RARITY;
        static const unsigned COST = 1;
        static const unsigned DAMAGE = 65;
        static const bool IS_INSTANT_CAST = false;
    };

    class CunningWizardry {
    public:
        static const std::string IMG_PATH;
        static const std::string NAME;
        static const Card::Type TYPE;
        static const Card::Rarity RARITY;
        static const unsigned COST =  2;
        static const unsigned DAMAGE = 30;
        static const unsigned HEAL = 30;
        static const unsigned STUN_DURATION = 3;
        static const bool IS_INSTANT_CAST = true;
    };

    class Refreshment {
    public:
        static const std::string IMG_PATH;
        static const std::string NAME;
        static const Card::Type TYPE;
        static const Card::Rarity RARITY;
        static const unsigned COST = 1;
        static const bool IS_INSTANT_CAST = false;
    };

    class Armageddon {
    public:
        static const std::string IMG_PATH;
        static const std::string NAME;
        static const Card::Type TYPE;
        static const Card::Rarity RARITY;
        static const unsigned COST =  3;
        static const unsigned DAMAGE = 70;
        static const unsigned NUM_OF_FIELDS = 100;
        static const bool IS_INSTANT_CAST = true;
    };

    class Thievery {
    public:
        static const std::string IMG_PATH;
        static const std::string NAME;
        static const Card::Type TYPE;
        static const Card::Rarity RARITY;
        static const unsigned COST = 1;
        static const bool IS_INSTANT_CAST = true;
    };

    class Combustion {
    public:
        static const std::string IMG_PATH;
        static const std::string NAME;
        static const Card::Type TYPE;
        static const Card::Rarity RARITY;
        static const unsigned COST = 3;
        static const unsigned DAMAGE = 70;
        static const unsigned ORTHO_RANGE = 1;
        static const unsigned DIAG_RANGE = 1;
        static const bool USE_G_FIELDS = false;
        static const bool IS_INSTANT_CAST = false;
    };

    class WrathOfBorgoldin {
    public:
        static const std::string IMG_PATH;
        static const std::string NAME;
        static const Card::Type TYPE;
        static const Card::Rarity RARITY;
        static const unsigned COST = 3;
        static const unsigned NUM_OF_PIECES = 10;
        static constexpr double ENEMY_SELECTION_CHANCE = 0.51;
        static const bool IS_INSTANT_CAST = true;
    };

    class BreakLoose {
    public:
        static const std::string IMG_PATH;
        static const std::string NAME;
        static const Card::Type TYPE;
        static const Card::Rarity RARITY;
        static const unsigned COST = 1;
        static const bool IS_INSTANT_CAST = false;
    };

    class ArrowVolley {
    public:
        static const std::string IMG_PATH;
        static const std::string NAME;
        static const Card::Type TYPE;
        static const Card::Rarity RARITY;
        static const unsigned COST = 2;
        static const unsigned DAMAGE = 10;
        static const unsigned NUM_OF_PIECES = 8;
        static const bool IS_INSTANT_CAST = true;
    };

    class OneManArmy {
    public:
        static const std::string IMG_PATH;
        static const std::string NAME;
        static const Card::Type TYPE;
        static const Card::Rarity RARITY;
        static const unsigned COST = 3;
        static const unsigned HEALTH_INCREASE = 250;
        static const bool IS_INSTANT_CAST = false;
    };

    class Aristocracy {
    public:
        static const std::string IMG_PATH;
        static const std::string NAME;
        static const Card::Type TYPE;
        static const Card::Rarity RARITY;
        static const unsigned COST = 2;
        static const unsigned HEALTH_INCREASE = 30;
        static const bool IS_INSTANT_CAST = true;
    };

    class CavalryAssembly {
    public:
        static const std::string IMG_PATH;
        static const std::string NAME;
        static const Card::Type TYPE;
        static const Card::Rarity RARITY;
        static const unsigned COST = 2;
        static const bool IS_INSTANT_CAST = true;
    };

    class Firesword {
    public:
        static const std::string IMG_PATH;
        static const std::string NAME;
        static const Card::Type TYPE;
        static const Card::Rarity RARITY;
        static const unsigned COST = 1;
        static const unsigned DAMAGE_INCREASE = 15;
        static const bool IS_INSTANT_CAST = false;
    };

    class BridgeOfDeath {
    public:
        static const std::string IMG_PATH;
        static const std::string NAME;
        static const Card::Type TYPE;
        static const Card::Rarity RARITY;
        static const unsigned COST = 3;
        static const bool IS_INSTANT_CAST = false;
    };

    class Flood {
    public:
        static const std::string IMG_PATH;
        static const std::string NAME;
        static const Card::Type TYPE;
        static const Card::Rarity RARITY;
        static const unsigned COST = 3;
        static const bool IS_INSTANT_CAST = false;
    };

    class Bracers {
    public:
        static const std::string IMG_PATH;
        static const std::string NAME;
        static const Card::Type TYPE;
        static const Card::Rarity RARITY;
        static const unsigned COST = 1;
        static const unsigned HEALTH_INCREASE = 30;
        static const bool IS_INSTANT_CAST = false;
    };

    class FatalWounds {
    public:
        static const std::string IMG_PATH;
        static const std::string NAME;
        static const Card::Type TYPE;
        static const Card::Rarity RARITY;
        static const unsigned COST = 3;
        static const unsigned HEALTH_TRESHOLD = 60;
        static const bool IS_INSTANT_CAST = false;
    };

private:
    static std::vector<std::pair<double, Card::Rarity>> _prob_rarity_sorted;

    static std::vector<std::string> _create_card_names();
    static std::map<std::string, unsigned> _create_card_names_to_cost();
    static std::map<std::string, std::string> _create_card_names_to_path();
    static std::map<std::string, Card::Rarity> _create_card_names_to_rarity();
    static std::map<Card::Rarity, double> _create_card_rarity_to_prob();
    static std::vector<std::string> _create_cards_by_rarity(Card::Rarity rarity);
};


#endif // CARD_HPP
