#include "cardInfo.hpp"

#include <QPainter>
#include <QGraphicsProxyWidget>

#include "../gui/card.hpp"
#include "../pieces/piece.hpp"

#include <iostream>
#include <string>




CardInfo::CardInfo(const Player* player1, const Player* player2, QGraphicsScene* scene)
    : m_player1(player1),
      m_player2(player2),
      m_scene(scene),
      m_num_of_enemy_cards(0),
      rect_item_card(new QGraphicsRectItem(0,0,245,342,this))
{
    rect_item_card->setPen(QPen(Qt::black,10));
    rect_item_card->setBrush(QBrush(Qt::black,Qt::SolidPattern));
    rect_item_card->setPos(30,350);
    add_buttons();
}

CardInfo *CardInfo::create_cardInfo(const Player* player1, const Player* player2, QGraphicsScene* scene)
{
    static CardInfo* card_info = new CardInfo(player1, player2, scene);
    return card_info;
}

void CardInfo::set_colors(QColor &friendly_color, QColor &enemy_color) const
{
    if(Player::Index::PLAYER_1 == m_player_turn){
        friendly_color = Qt::red;
        enemy_color    = Qt::blue;
    }
    else{
        friendly_color = Qt::blue;
        enemy_color    = Qt::red;
    }
}

QRectF CardInfo::boundingRect() const
{
    return QRectF(0,0,300,750);
}

void CardInfo::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
        QColor friendly_color, enemy_color;

        set_colors(friendly_color,enemy_color);

        Q_UNUSED(option);
        Q_UNUSED(widget);


        painter->fillRect(boundingRect(),Qt::gray);
        painter->setFont(QFont("Times",17,QFont::Bold));


        QColor color_player_1=Qt::red;
        QColor color_player_2=Qt::blue;


        painter->fillRect(QRectF(4,0,146,50),color_player_1);

        painter->setFont(QFont("Times",15,QFont::Normal));
        painter->setPen(Qt::white);
        painter->drawText(4,0,146,40,Qt::AlignHCenter | Qt::AlignVCenter,QString::number(std::floor(m_player1->get_castles_health())));


        painter->fillRect(QRectF(150,0,146,50),color_player_2);

        painter->setFont(QFont("Times",15,QFont::Normal));
        painter->setPen(Qt::white);
        painter->drawText(150,0,146,40,Qt::AlignHCenter | Qt::AlignVCenter,QString::number(std::floor(m_player2->get_castles_health())));



        QColor color;
        Player::Index::PLAYER_1 == m_player_turn ?
                   color = Qt::red : color = Qt::blue;

        painter->fillRect(QRectF(4,55,292,50),color);

        painter->setFont(QFont("Times",15,QFont::Normal));
        painter->setPen(Qt::white);
        painter->drawText(4,55,292,40,Qt::AlignHCenter | Qt::AlignVCenter,QString("Remaining steps: ")+QString::number(m_remaining_steps));


        if(m_piece){

            auto map = m_piece->values();
            if(m_piece->owner() == m_player_turn){
                painter->setPen(friendly_color);
            }
            else {
                painter->setPen(enemy_color);
            }
            painter->setFont(QFont("Times",10,QFont::Bold));
            painter->drawText(5,110,190,20, Qt::AlignHCenter | Qt::AlignVCenter ,map["Type"]);

            painter->setPen(Qt::black);

            painter->setFont(QFont("Times",8,QFont::Normal));
            if(map["Last field health bonus"].toDouble() > 0.0){
                 painter->drawText(5,130,190,15, Qt::AlignVCenter ,QString("Health: ") + map["Health"]  + QString(" (+") + map["Last field health bonus"] + QString(" FB)"));
            }
            else if(map["Last field health bonus"].toDouble() < 0.0) {
                painter->drawText(5,130,190,15, Qt::AlignVCenter ,QString("Health: ") + map["Health"] + QString(" (") + map["Last field health bonus"] + QString(" FB)"));
            }
            else if(map["Last field health bonus"].toDouble() == 0.0){
                painter->drawText(5,130,190,15, Qt::AlignVCenter ,QString("Health: ") + map["Health"]);
            }
            if(map["Last field damage bonus"].toDouble() > 0.0){
                 painter->drawText(5,145,190,15, Qt::AlignVCenter ,QString("Damage: ") + map["Damage"]  + QString(" (+") + map["Last field damage bonus"] + QString(" FB)"));
            }
            else if(map["Last field damage bonus"].toDouble() < 0.0) {
                painter->drawText(5,145,190,15, Qt::AlignVCenter ,QString("Damage: ") + map["Damage"] + QString(" (") + map["Last field damage bonus"] + QString(" FB)"));
            }
            else if(map["Last field damage bonus"].toDouble() == 0.0){
                painter->drawText(5,145,190,15, Qt::AlignVCenter ,QString("Damage: ") + map["Damage"]);
            }
            painter->drawText(5,160,190,15, Qt::AlignVCenter ,QString("Attacks remaining: ")+ map["Attacks remaining"]);
            painter->setFont(QFont("Times",8,QFont::DemiBold));
            painter->drawText(5,175,190,15, Qt::AlignVCenter ,QString("Moves remaining: "));
            painter->setFont(QFont("Times",8,QFont::Normal));
            painter->drawText(5,190,190,15, Qt::AlignVCenter ,QString("Ortho moves: ") + map["Ortho moves remaining"]);
            painter->drawText(5,205,190,15, Qt::AlignVCenter ,QString("Diag moves: ") + map["Diag moves remaining"]);
            painter->drawText(5,220,190,15, Qt::AlignVCenter ,QString("Stunned: ") + map["Stunned"]);
            painter->drawText(5,235,190,15, Qt::AlignVCenter ,QString("Immovable: ") + map["Immovable"]);
            update();
    }



        painter->setPen(Qt::black);
        painter->setFont(QFont("Times",17,QFont::Bold));


        if(!cards_list.isEmpty()){
            std::string card_name = cards_list.first();
            if(m_cards[card_name] > 1){
                painter->drawText(150,300,160,20,Qt::AlignVCenter, "x" + QString::number(static_cast<unsigned>(m_cards[card_name])));
            }
        }
}
void CardInfo::add_buttons(){

    QGraphicsProxyWidget* proxy;

    btn_left_arrow = new QPushButton();
    btn_left_arrow->setCheckable(false);
    btn_left_arrow->setAttribute(Qt::WA_TranslucentBackground);
    btn_left_arrow->setStyleSheet("background-color: gray;");
    btn_left_arrow->setIcon(QIcon(QString(":/images/buttons/left_arrow.png")));
    btn_left_arrow->setGeometry(0,0,20,20);
    btn_left_arrow->setIconSize(QSize(15,15));
    btn_left_arrow->setDisabled(false);
    btn_left_arrow->show();
    proxy = m_scene->addWidget(btn_left_arrow);
    proxy->setParentItem(this);
    proxy->setPos(5,520);


    btn_right_arrow = new QPushButton();
    btn_right_arrow->setCheckable(false);
    btn_right_arrow->setAttribute(Qt::WA_TranslucentBackground);
    btn_right_arrow->setStyleSheet("background-color: gray;");
    btn_right_arrow->setIcon(QIcon(QString(":/images/buttons/right_arrow.png")));
    btn_right_arrow->setGeometry(0,0,20,20);
    btn_right_arrow->setIconSize(QSize(15,15));
    btn_right_arrow->setDisabled(false);
    btn_right_arrow->show();
    proxy = m_scene->addWidget(btn_right_arrow);
    proxy->setParentItem(this);
    proxy->setPos(280,520);

    connect(btn_left_arrow, SIGNAL(clicked()),this,SLOT(change_cards_left()));
    connect(btn_right_arrow, SIGNAL(clicked()),this,SLOT(change_cards_right()));


    btn_activate = new QPushButton("Activate");
    btn_activate->setCheckable(false);
    btn_activate->setAttribute(Qt::WA_TranslucentBackground);
    btn_activate->setStyleSheet("background-color: gray;");
    btn_activate->setGeometry(0,0,70,30);
    btn_activate->setDisabled(false);
    btn_activate->show();
    proxy = m_scene->addWidget(btn_activate);
    proxy->setParentItem(this);
    proxy->setPos(118,700);

    connect(btn_activate,SIGNAL(clicked(bool)),this,SLOT(activate_clicked()));

}

void CardInfo::change_cards_right()
{
    if(!cards_list.isEmpty()){
        std::string current_card_path = cards_list.back();
        cards_list.pop_back();
        cards_list.push_front(current_card_path);
        set_card_image();
    }
}


void CardInfo::change_cards_left()
{

    if(!cards_list.isEmpty()){
        std::string current_card_path=cards_list.front();
        cards_list.pop_front();
        cards_list.push_back(current_card_path);
        set_card_image();
    }

}

void CardInfo::activate_clicked()
{
    emit call_clicked_activate_card(cards_list.first());
}

void CardInfo::set_card_image(){


    if(!cards_list.isEmpty()){

        auto card_name = cards_list.first();
        QString card_path = QString(Card::Card::CARD_NAMES_TO_PATH.at(card_name).c_str());

        if(m_remaining_steps >= Card::CARD_NAMES_TO_COST.at(card_name)){

            if(Card::Thievery::NAME != card_name){
                btn_activate->setEnabled(true);
            }
            else{
                m_num_of_enemy_cards > 0 ?
                            btn_activate->setEnabled(true) : btn_activate->setDisabled(true);
            }
        }
        else{
            btn_activate->setDisabled(true);
        }
        QPixmap pix_card(card_path);
        rect_item_card->setPen(Qt::NoPen);
        rect_item_card->setBrush(QBrush(pix_card.scaled(QSize(245,342),Qt::KeepAspectRatio,Qt::SmoothTransformation)));
        update();

    }
}

void CardInfo::set_player_cards(std::map<std::string, int> cards, unsigned num_of_enemy_cards)
{
    m_cards.clear();
    cards_list.clear();
    m_cards = cards;
    m_num_of_enemy_cards = num_of_enemy_cards;

    if(m_cards.size() > 0){
        btn_left_arrow->setEnabled(true);
        btn_right_arrow->setEnabled(true);

        for(auto item : cards){
           cards_list.push_back(item.first);
        }

        set_card_image();
    }
    else{
        btn_left_arrow->setDisabled(true);
        btn_right_arrow->setDisabled(true);
        btn_activate->setDisabled(true);
        rect_item_card->setBrush(QBrush(Qt::gray,Qt::SolidPattern));
    }

}

void CardInfo::set_remaining_steps(unsigned steps)
{
    m_remaining_steps = steps;
    set_card_image();
    update();
}

void CardInfo::set_player_turn(Player::Index player)
{
    m_player_turn = player;
}

void CardInfo::set_current_piece(Piece *p)
{
    m_piece = p;
    update();
}

void CardInfo::set_enemy_piece(Piece *p)
{
    m_piece = p;
    update();
}

