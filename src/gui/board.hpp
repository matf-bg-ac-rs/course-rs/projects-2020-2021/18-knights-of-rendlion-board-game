#ifndef BOARD_HPP
#define BOARD_HPP

#include <QGraphicsItem>
#include <QPainter>
#include <QGraphicsSceneMouseEvent>

#include <vector>
#include <string>
#include <iostream>
#include <filesystem>
#include <cstdlib>
#include <ctime>
#include <fstream>

#include "field.hpp"
#include "../pieces/piece.hpp"
#include "../utility/utility.hpp"

class Field;

class Board : public QObject,public QGraphicsItem
{
public:


    static Board* create_board();

    //Board size
    enum Size
    {
        ROWS = 15,
        COLS = 21,
        STAGING_AREA = 4
    };

    enum class RangeType {
        MOVE,
        TARGET
    };

    enum class PieceCheck {
        NO_CHECK,
        ENEMY,
        FRIENDLY,
        BOTH
    };    

    // QGraphicsItem interface
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

    //this will initialize board with fields
    void initialize();

    // Board methods for game logic
    void mark_player1_fields();
    void mark_player2_fields();
    void unmark_player1_fields();
    void unmark_player2_fields();
    void remove_from_standable_list(Field* f);

    // board size getters
    unsigned rows() const;
    unsigned cols() const;
    std::vector<std::vector<Field*>> fields() const;

    unsigned num_of_standable_fields() const;

    std::list<Field*> get_left_standable_list() const;
    std::list<Field*> get_center_standable_list() const;
    std::list<Field*> get_right_standable_list() const;


    // this will emit signal when click is registered on the board
    void mousePressEvent(QGraphicsSceneMouseEvent *event);

    //Field getter operator
    Field* operator()(unsigned row, unsigned column);
    const Field* operator()(unsigned row, unsigned column) const;
    Field* at(unsigned row, unsigned col) const;
    const Field* at_c(unsigned row, unsigned col) const;
    Field* at(const std::pair<unsigned, unsigned>& board_index) const;
    const Field* at_c(const std::pair<unsigned, unsigned>& board_index) const;

    // Actual method for activating target bonuses - DIRECTLY MODIFES PIECE OBJECT
    void activate_target_damage_bonus(Piece::Type target_type);

    // Resets previous target damage bonus - DIRECTLY MODIFES PIECE OBJECT
    void reset_target_damage_bonus();

    // Moves piece from one field to another
    void move(Piece* p, Field* new_field, bool update_remaining_moves=true);

    // Same as previous methods, but fields are returned instead
    std::vector<Field*> fields_in_target_range(const Piece* piece) const;

    // Returns list of pairs representing board indices where piece can step
    std::vector<Field*> fields_in_move_range(const Piece* piece) const;

    // High level method for range calculation depending on variety of parameters
    std::list<Field*> fields_in_range(
        Field* current_field,
        unsigned max_ortho_range,
        unsigned max_diag_range,
        bool use_g_fields=false,
        bool check_field_availability=false
    ) const;

    // High level method for range calculation depending on variety of parameters
    std::list<Field*> fields_in_range(
        const std::pair<unsigned, unsigned>& board_index,
        unsigned max_ortho_range,
        unsigned max_diag_range,
        bool use_g_fields=false,
        bool check_field_availability=false
    ) const;

    // Remove indices out of range and check availability if neccessarry
    std::set<std::pair<unsigned, unsigned>> trimmed_indices(
        std::set<std::pair<int, int>>& indices,
        bool check_availability
    ) const;

    // Remove fields out of range and check availability if neccessarry
    std::set<Field*> trimmed_fields(
        std::set<std::pair<int, int>>& indices,
        bool check_availability
    ) const;

    template <typename TK, typename Pred>
    void remove_fields_by(TK& fields, Pred remove_if_predicate);

    // Returns fields around position with a given distance
    std::vector<Field*> nearby_fields(const Field* position, unsigned distance = 1) const;

    bool check_row(int row) const;
    bool check_col(int col) const;
    bool check_indices(int row, int col) const;

    //board signal for mousePressEvent
    Q_OBJECT
signals:
    void call_handle_mouse_event(unsigned,unsigned);
    void call_handle_right_click(unsigned,unsigned);
private:
    //Constructor
    Board();

    //we don't want to have cctor and copy assign operator
    Board (const Board&) = delete;
    Board& operator= (const Board&) = delete;

    std::vector< std::vector < Field* >> m_fields;
    unsigned m_rows;
    unsigned m_cols;
    unsigned m_standable_fields;

    std::list<Field*> m_left_standable;
    std::list<Field*> m_right_standable;
    std::list<Field*> m_center_standable;

    void add_to_standable_list(Field* f);



};

#endif // BOARD_HPP

