#include "gameInfo.hpp"

#include <QGraphicsProxyWidget>
#include <QPushButton>
#include <QButtonGroup>
#include <QObject>
#include <QScrollBar>
#include <QGridLayout>

#include "../globals.hpp"

#include<string>



GameInfo::GameInfo(QGraphicsScene *scene, unsigned num_of_pieces_to_choose)
    : m_scene(scene),
      m_piece_index(-1),
      m_piece(nullptr),
      m_width(300),
      m_enemy_info(QString()),
      spells_info(new QTextEdit()),
      m_remaining_pieces(num_of_pieces_to_choose + 1)
{
    //disabling opening of extra menus when right clicked
    //those clicks can distort scene geometry
    spells_info->setContextMenuPolicy(Qt::NoContextMenu);
    QScrollBar* bar= spells_info->verticalScrollBar();
    bar->setContextMenuPolicy(Qt::NoContextMenu);

    for(unsigned i=0 ; i<5 ; i++){
        QGraphicsRectItem* rect_item = new QGraphicsRectItem(0,0,30,30,this);
        rect_item->setPen(Qt::NoPen);
        rect_item->setPos(15+60*i,330);
        QPixmap pix(m_index_to_field.at(i));
        rect_item->setBrush(QBrush(pix.scaled(QSize(30,30),Qt::KeepAspectRatio)));
        rect_item->hide();
        m_fields.push_back(rect_item);
    }
    for(unsigned i=0 ; i<9 ; i++){
        QGraphicsRectItem* rect_item_pieces = new QGraphicsRectItem(0,0,30,30,this);
        rect_item_pieces->setPen(Qt::NoPen);
        rect_item_pieces->setPos(55+80*(i%3),435+(i/3)*75);
        QPixmap pix(m_index_to_piece.at(i));
        rect_item_pieces->setBrush(QBrush(pix.scaled(QSize(30,30),Qt::KeepAspectRatio)));
        rect_item_pieces->hide();
        m_pieces.push_back(rect_item_pieces);
    }
}

QRectF GameInfo::boundingRect() const{

    return QRectF(0,0,m_width,750);
}
void GameInfo::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget){
    Q_UNUSED(option);
    Q_UNUSED(widget);
    painter ->fillRect(boundingRect (), Qt::gray);

    //paint for INIT game part
    if(GameController::Part::INIT == globals::GAME_PART ){

        painter->setFont(QFont("Times",15,QFont::Bold));

        QColor color;
        Player::Index::PLAYER_1 == m_player_turn ?
                    color = Qt::red : color = Qt::blue;

        painter->fillRect(QRectF(10,10,280,140), color);
        painter->setPen(Qt::black);

        if(m_piece_index > -1){
            painter->setFont(QFont("Times",15,QFont::Bold));
            painter->drawText(10,150,280,30,Qt::AlignHCenter | Qt::AlignVCenter,QString(piece_name(m_piece_index)));

            std::map<QString,QString> map_init = default_values_map(m_piece_index);
            painter->setFont(QFont("Times",10,QFont::Normal));

            painter->drawText(10,180,90,15, Qt::AlignHCenter | Qt::AlignVCenter,QString("Health"));
            painter->drawText(105,180,90,15, Qt::AlignHCenter | Qt::AlignVCenter ,QString("Damage"));
            painter->drawText(200,180,90,15, Qt::AlignHCenter | Qt::AlignVCenter ,QString("Attacks"));

            painter->drawText(10,195,90,15, Qt::AlignHCenter | Qt::AlignVCenter, map_init["HEALTH"]);
            painter->drawText(105,195,90,15, Qt::AlignHCenter | Qt::AlignVCenter ,map_init["DAMAGE"]);
            painter->drawText(200,195,90,15, Qt::AlignHCenter | Qt::AlignVCenter ,map_init["MAX_ATTACKS"]);

            painter->setFont(QFont("Times",10,QFont::Bold));
            painter->drawText(10,210,280,15, Qt::AlignHCenter | Qt::AlignVCenter ,QString("Moves"));

            painter->setFont(QFont("Times",10,QFont::Normal));
            painter->drawText(10,210,90,15, Qt::AlignHCenter | Qt::AlignVCenter, QString("Ortho"));
            painter->drawText(200,210,90,15, Qt::AlignHCenter | Qt::AlignVCenter , QString("Diag"));
            painter->drawText(10,225,90,15, Qt::AlignHCenter | Qt::AlignVCenter ,QString(map_init["MAX_ORTHO_MOVES"]));
            painter->drawText(200,225,90,15, Qt::AlignHCenter | Qt::AlignVCenter ,QString(map_init["MAX_DIAG_MOVES"]));

            //Target range
            painter->setFont(QFont("Times",10,QFont::Bold));
            painter->drawText(10,240,280,15, Qt::AlignHCenter | Qt::AlignVCenter ,QString("Target range"));

            painter->setFont(QFont("Times",10,QFont::Normal));
            painter->drawText(10,240,90,15, Qt::AlignHCenter | Qt::AlignVCenter, QString("Ortho"));
            painter->drawText(200,240,90,15, Qt::AlignHCenter | Qt::AlignVCenter , QString("Diag"));
            painter->drawText(10,255,90,15, Qt::AlignHCenter | Qt::AlignVCenter ,QString(map_init["MAX_ORTHO_TARGET_RANGE"]));
            painter->drawText(200,255,90,15, Qt::AlignHCenter | Qt::AlignVCenter ,QString(map_init["MAX_DIAG_TARGET_RANGE"]));

            painter->setFont(QFont("Times",10,QFont::Bold));
            painter->drawText(10,290,280,15, Qt::AlignHCenter| Qt::AlignVCenter ,QString("Field bonus"));
            painter->setPen(Qt::black);
            painter->setFont(QFont("Times",7,QFont::Bold));

            set_paint_color(map_init["GROUND_HEALTH"].toInt(), painter);
            painter->drawText(10,310,160,15, Qt::AlignVCenter, ((map_init["GROUND_HEALTH"].toInt())<0 ? "" :"+") + map_init["GROUND_HEALTH"] +"% HP ");
            set_paint_color(map_init["PLAIN_HEALTH"].toInt(), painter);
            painter->drawText(70,310,160,15, Qt::AlignVCenter, ((map_init["PLAIN_HEALTH"].toInt())<0 ? "" :"+") + map_init["PLAIN_HEALTH"] +"% HP ");
            set_paint_color(map_init["FOREST_HEALTH"].toInt(), painter);
            painter->drawText(130,310,160,15, Qt::AlignVCenter, ((map_init["FOREST_HEALTH"].toInt())<0 ? "" :"+") + map_init["FOREST_HEALTH"] +"% HP ");
            set_paint_color(map_init["BRIDGE_HEALTH"].toInt(), painter);
            painter->drawText(190,310,160,15, Qt::AlignVCenter, ((map_init["BRIDGE_HEALTH"].toInt())<0 ? "" :"+") + map_init["BRIDGE_HEALTH"] +"% HP ");
            set_paint_color(map_init["HILL_HEALTH"].toInt(), painter);
            painter->drawText(250,310,160,15, Qt::AlignVCenter, ((map_init["HILL_HEALTH"].toInt())<0 ? "" :"+") + map_init["HILL_HEALTH"] +"% HP ");
            set_paint_color(map_init["GROUND_DAMAGE"].toInt(), painter);
            painter->drawText(10,365,160,15, Qt::AlignVCenter,  ((map_init["GROUND_DAMAGE"].toInt())<0 ? "" :"+") + map_init["GROUND_DAMAGE"] + "% DMG");
            set_paint_color(map_init["PLAIN_DAMAGE"].toInt(), painter);
            painter->drawText(70,365,160,15, Qt::AlignVCenter, ((map_init["PLAIN_DAMAGE"].toInt())<0 ? "" :"+") + map_init["PLAIN_DAMAGE"] + "% DMG");
            set_paint_color(map_init["FOREST_DAMAGE"].toInt(), painter);
            painter->drawText(130,365,160,15, Qt::AlignVCenter, ((map_init["FOREST_DAMAGE"].toInt())<0 ? "" :"+") + map_init["FOREST_DAMAGE"] + "% DMG");
            set_paint_color(map_init["BRIDGE_DAMAGE"].toInt(), painter);
            painter->drawText(190,365,160,15, Qt::AlignVCenter, ((map_init["BRIDGE_DAMAGE"].toInt())<0 ? "" :"+") + map_init["BRIDGE_DAMAGE"] + "% DMG");
            set_paint_color(map_init["HILL_DAMAGE"].toInt(), painter);
            painter->drawText(250,365,160,15, Qt::AlignVCenter,  ((map_init["HILL_DAMAGE"].toInt())<0 ? "" :"+") + map_init["HILL_DAMAGE"] + "% DMG");

            painter->setPen(Qt::black);
            painter->setFont(QFont("Times",10,QFont::Bold));

            QVector<QLineF> lines={QLineF(45,425,45,637),QLineF(45,425,257,425),QLineF(257,425,257,637),QLineF(257,637,45,637)};
            painter->drawLines(lines);

            painter->drawText(10,400,280,15, Qt::AlignHCenter | Qt::AlignVCenter ,QString("Target bonus"));
            painter->setFont(QFont("Times",7,QFont::Normal));
            set_paint_color(map_init["KNIGHT_DAMAGE"].toInt(), painter);
            painter->drawText(50,470,160,15, Qt::AlignVCenter , ((map_init["KNIGHT_DAMAGE"].toInt())>=0 ? "+" :"")  + map_init["KNIGHT_DAMAGE"] +"% DMG");
            set_paint_color(map_init["ARCHER_DAMAGE"].toInt(), painter);
            painter->drawText(125,470,160,15, Qt::AlignVCenter ,((map_init["ARCHER_DAMAGE"].toInt())>=0 ? "+" :"")  + map_init["ARCHER_DAMAGE"] + "% DMG");
            set_paint_color(map_init["CATAPULT_DAMAGE"].toInt(), painter);
            painter->drawText(205,470,160,15, Qt::AlignVCenter ,((map_init["CATAPULT_DAMAGE"].toInt())>=0 ? "+" :"")  + map_init["CATAPULT_DAMAGE"] + "% DMG");
            set_paint_color(map_init["HORSEMAN_DAMAGE"].toInt(), painter);
            painter->drawText(50,545,160,15, Qt::AlignVCenter , ((map_init["HORSEMAN_DAMAGE"].toInt())>=0 ? "+" :"")  + map_init["HORSEMAN_DAMAGE"] +"% DMG");
            set_paint_color(map_init["MAGE_DAMAGE"].toInt(), painter);
            painter->drawText(125,545,160,15, Qt::AlignVCenter ,((map_init["MAGE_DAMAGE"].toInt())>=0 ? "+" :"")  + map_init["MAGE_DAMAGE"] + "% DMG");
            set_paint_color(map_init["PRIEST_DAMAGE"].toInt(), painter);
            painter->drawText(205,545,160,15, Qt::AlignVCenter ,((map_init["PRIEST_DAMAGE"].toInt())>=0 ? "+" :"")  + map_init["PRIEST_DAMAGE"] + "% DMG");
            set_paint_color(map_init["SHIELDMAN_DAMAGE"].toInt(), painter);
            painter->drawText(50,620,160,15, Qt::AlignVCenter , ((map_init["SHIELDMAN_DAMAGE"].toInt())>=0 ? "+" :"")  + map_init["SHIELDMAN_DAMAGE"] +"% DMG");
            set_paint_color(map_init["SPEARMAN_DAMAGE"].toInt(), painter);
            painter->drawText(125,620,160,15, Qt::AlignVCenter ,((map_init["SPEARMAN_DAMAGE"].toInt())>=0 ? "+" :"")  + map_init["SPEARMAN_DAMAGE"] + "% DMG");
            set_paint_color(map_init["CASTLE_DAMAGE"].toInt(), painter);
            painter->drawText(205,620,160,15, Qt::AlignVCenter ,((map_init["CASTLE_DAMAGE"].toInt())>=0 ? "+" :"")  + map_init["CASTLE_DAMAGE"] + "% DMG");


            }
        else{
            QColor color;
            Player::Index::PLAYER_1 == m_player_turn ?
                        color = Qt::red : color = Qt::blue;
            painter->setPen(color);
            painter->drawText(10, 160, 280, 25, Qt::AlignHCenter | Qt::AlignVCenter, QString("Remaining pieces: ") + QString::number(m_remaining_pieces));
        }

        }
    else if(GameController::Part::PLAY == globals::GAME_PART){
        QColor friendly_color, enemy_color;

        set_colors(friendly_color,enemy_color);
        painter->fillRect(QRectF(5,5,190,50),friendly_color);

        painter->setFont(QFont("Times",15,QFont::Normal));
        painter->setPen(Qt::white);
        painter->drawText(5,10,190,30,Qt::AlignHCenter | Qt::AlignVCenter,QString("Remaining steps: ")+QString::number(remaining_steps));

        //current piece info
        if(m_piece){

            painter->setFont(QFont("Times",8,QFont::Normal));
            auto map = m_piece->values();
            if(m_piece->owner() == m_player_turn){
                painter->setPen(friendly_color);
            }
            else {
                painter->setPen(enemy_color);
            }
            painter->setFont(QFont("Times",10,QFont::Bold));
            painter->drawText(5,65,190,15, Qt::AlignHCenter | Qt::AlignVCenter ,map["Type"]);

            painter->setPen(Qt::black);

            painter->setFont(QFont("Times",8,QFont::Normal));

            if(map["Last field health bonus"].toDouble() > 0.0){
                 painter->drawText(5,85,190,15, Qt::AlignVCenter ,QString("Health: ") + map["Health"]  + QString(" (+") + map["Last field health bonus"] + QString(" FB)"));
            }
            else if(map["Last field health bonus"].toDouble() < 0.0) {
                painter->drawText(5,85,190,15, Qt::AlignVCenter ,QString("Health: ") + map["Health"] + QString(" (") + map["Last field health bonus"] + QString(" FB)"));
            }
            else if(map["Last field health bonus"].toDouble() == 0.0){
                painter->drawText(5,85,190,15, Qt::AlignVCenter ,QString("Health: ") + map["Health"]);
            }
            if(map["Last field damage bonus"].toDouble() > 0.0){
                 painter->drawText(5,100,190,15, Qt::AlignVCenter ,QString("Damage: ") + map["Damage"]  + QString(" (+") + map["Last field damage bonus"] + QString(" FB)"));
            }
            else if(map["Last field damage bonus"].toDouble() < 0.0) {
                painter->drawText(5,100,190,15, Qt::AlignVCenter ,QString("Damage: ") + map["Damage"] + QString(" (") + map["Last field damage bonus"] + QString(" FB)"));
            }
            else if(map["Last field damage bonus"].toDouble() == 0.0){
                painter->drawText(5,100,190,15, Qt::AlignVCenter ,QString("Damage: ") + map["Damage"]);
            }
            painter->drawText(5,115,190,15, Qt::AlignVCenter ,QString("Attacks remaining: ")+ map["Attacks remaining"]);
            painter->setFont(QFont("Times",8,QFont::DemiBold));
            painter->drawText(5,130,190,15, Qt::AlignVCenter ,QString("Moves remaining: "));
            painter->setFont(QFont("Times",8,QFont::Normal));
            painter->drawText(5,145,190,15, Qt::AlignVCenter ,QString("Ortho moves: ") + map["Ortho moves remaining"]);
            painter->drawText(5,160,190,15, Qt::AlignVCenter ,QString("Diag moves: ") + map["Diag moves remaining"]);
            painter->drawText(5,175,190,15, Qt::AlignVCenter ,QString("Stunned: ") + map["Stunned"]);
            painter->drawText(5,190,190,15, Qt::AlignVCenter ,QString("Immovable: ") + map["Immovable"]);


            std::vector<QString> spells = m_piece->spell_names();
            painter->setFont(QFont("Times",10,QFont::Bold));
            painter->drawText(5,650,190,15, Qt::AlignHCenter | Qt::AlignVCenter, QString("Cooldowns"));

            painter->setFont(QFont("Times",8,QFont::Normal));

            if (m_piece->has_cooldowns()){
                int i = 0;
                for (auto spell:spells){
                    if(m_piece->has_cooldown(spell.toStdString())){
                        int duration = m_piece->cooldowns()[spell.toStdString()];
                        QString turn_s;
                        duration == 1 ? turn_s = QString(" turn") : turn_s = QString(" turns");
                        painter->drawText(5,665+i*15,190,15, Qt::AlignHCenter | Qt::AlignVCenter, spell + QString(": ") + QString::number(duration) + turn_s);
                    i++;
                    }
                }
            }
            else
                painter->drawText(5,665,190,15, Qt::AlignHCenter | Qt::AlignVCenter, QString("No cooldowns"));


            if(m_enemy_info.length() > 0){
                painter->setFont(QFont("Times",12,QFont::Normal));
                painter->setPen(enemy_color);
                painter->fillRect(QRectF(5,215,190,60),enemy_color);
                painter->setPen(Qt::white);
                painter->drawText(5,215,190,60,Qt::AlignHCenter | Qt::AlignVCenter, m_enemy_info);
            }

        }
        else {
            painter->setPen(Qt::black);
            painter->setFont(QFont("Times",8,QFont::Normal));
            if(m_field_info.size()>0){
            painter->setFont(QFont("Times",10,QFont::Bold));
            painter->drawText(5,60,190,20, Qt::AlignHCenter | Qt::AlignVCenter,m_field_info[0]);
            painter->setFont(QFont("Times",8,QFont::Normal));
            painter->drawText(5,70,140,180, Qt::AlignVCenter,m_field_info[1]);
            painter->drawText(70,70,140,180, Qt::AlignVCenter,m_field_info[2]);
            painter->drawText(135,70,140,180, Qt::AlignVCenter,m_field_info[3]);


            }

        }


    }

}

GameInfo *GameInfo::create_gameInfo(QGraphicsScene* scene, unsigned num_of_pieces_to_choose)
{
    static GameInfo* game_info = new GameInfo(scene, num_of_pieces_to_choose);
    return game_info;
}

void GameInfo::mousePressEvent(QGraphicsSceneMouseEvent *event){
    //    if(event->button() & Qt::LeftButton)
    //        std::cout << event->pos().x() << "," << event->pos().y() << std::endl;
}

//button initialization in INIT part
void GameInfo::add_buttons_init(){

    //proxy that will add buttons to scene and set parent to this
    QGraphicsProxyWidget* proxy;

    //group that will have all the piece buttons
    btn_group.setExclusive(true);



    for(auto i = 0; i < 8 ; i++){
        QPushButton* btn = new QPushButton();
        btn->setGeometry(0,0,50,50);
        btn->setStyleSheet("background-color: white;");
        btn->setCheckable(true);
        //btn->setAttribute(Qt::WA_TranslucentBackground);
        btn->setIcon(QIcon(m_index_to_piece.at(i)));
        btn->setIconSize(QSize(45,45));
        btn->setLayout(new QGridLayout());
        btn->show();
        proxy = m_scene->addWidget(btn);
        proxy->setParentItem(this);

        i < 4 ? proxy->setPos(20+i*70,20) : proxy->setPos(20+(i-4)*70,90);
        btn_group.addButton(btn,i);
        m_buttons.push_back(btn);
    }

    //connecting group..
    connect(&btn_group, SIGNAL(buttonClicked(int)),this,SLOT(change_btn_group_index(int)));
    // buttonClicked should be isClicked in newer version of Qt

   // spells_info = new QTextEdit();
    spells_info->setStyleSheet("background-color: gray;");
    spells_info->setGeometry(0,0,250,80);
    spells_info->setReadOnly(true);
    proxy = m_scene->addWidget(spells_info);
    proxy->setParentItem(this);
    proxy->setPos(30,650);
    spells_info->hide();
}

//button initialization for play part
void GameInfo::add_buttons_play(){


    //since we use the same button group  we need to disconnect the old signals
    disconnect(&btn_group, SIGNAL(buttonClicked(int)),this,SLOT(change_btn_group_index(int)));


    //deleting old buttons
    QAbstractButton* btn;
    for(unsigned i =0 ;i<8;i++){
        btn = btn_group.button(i);
        delete btn;
    }

    //spells_info->deleteLater();
    m_buttons.erase(m_buttons.begin(),m_buttons.end());


    QGraphicsProxyWidget* proxy;
    QString style = "QPushButton {\
            background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1,   stop:0 gray, stop:1 rgba(199, 199, 199, 255));\
        border-radius: 5px;\
    }\
    QPushButton:pressed {\
        background-color: gray;\
        border-radius: 5px;\
    }\
    QPushButton:hover {\
        background-color: rgb(119, 119, 119);}";

    QString style2 = "QPushButton {\
            background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1,   stop:0 gray, stop:1 rgba(199, 199, 199, 255));\
    border-radius: 5px;\
    font-size: 8px;\
    }\
    QPushButton:pressed {\
        background-color: gray;\
        border-radius: 5px;\
    }\
    QPushButton:hover {\
        background-color: rgb(119, 119, 119);}";


    button_move = new QPushButton();
    button_move->setCheckable(false);
    button_move->setAttribute(Qt::WA_TranslucentBackground);
    button_move->setStyleSheet(style);
    button_move->setText("Move");
    button_move->setGeometry(0,0,90,30);
    button_move->setDisabled(true);
    button_move->setShortcut(Qt::Key_Q);
    button_move->show();
    proxy = m_scene->addWidget(button_move);
    proxy->setParentItem(this);
    proxy->setPos(5,285);

    button_attack = new QPushButton();
    button_attack->setCheckable(false);
    //button_attack->setStyleSheet("background-color: gray; border:1px solid black;");
    button_move->setAttribute(Qt::WA_TranslucentBackground);
    button_attack->setStyleSheet(style);
    button_attack->setText("Attack");
    button_attack->setGeometry(0,0,90,30);
    button_attack->setDisabled(true);
    button_attack->setShortcut(Qt::Key_W);
    button_attack->show();
    proxy = m_scene->addWidget(button_attack);
    proxy->setParentItem(this);
    proxy->setPos(105,285);

    button_attack_range = new QPushButton();
    button_attack_range->setCheckable(false);
    //button_attack_range->setStyleSheet("background-color: gray; border:1px solid black");
    button_move->setAttribute(Qt::WA_TranslucentBackground);
    button_attack_range->setStyleSheet(style);
    button_attack_range->setText("Attack range");
    button_attack_range->setGeometry(0,0,90,30);
    button_attack_range->setDisabled(true);
    button_attack_range->show();
    proxy = m_scene->addWidget(button_attack_range);
    proxy->setParentItem(this);
    proxy->setPos(105,325);

    button_skip_turn = new QPushButton();
    button_skip_turn->setCheckable(false);
    button_move->setAttribute(Qt::WA_TranslucentBackground);
    //button_skip_turn->setStyleSheet("background-color: gray; border:1px solid black");
    button_skip_turn->setStyleSheet(style);
    button_skip_turn->setText("Skip turn");
    button_skip_turn->setGeometry(0,0,90,30);
    button_skip_turn->show();
    proxy = m_scene->addWidget(button_skip_turn);
    proxy->setParentItem(this);
    proxy->setPos(5,325);

//NOVO

for(unsigned i=1; i < 5; i++){
    QPushButton* btn = new QPushButton();
    //btn->setStyleSheet("background-color: gray;font-size:8px;font:bold");
    //btn->setStyleSheet("background-color: gray; border:1px solid black;font-size:8px;font:bold");
    btn->setAttribute(Qt::WA_TranslucentBackground);
    btn->setStyleSheet(style2);
    btn->setText("Spell "+ QString::number(i));
    btn->setGeometry(0,0,70,25);
    btn->setMinimumWidth(50);
    btn->setMinimumHeight(25);
    m_buttons.push_back(btn);
    btn->hide();
    proxy =  m_scene->addWidget(btn);
    proxy->setParentItem(this);
    btn_group.addButton(btn,i);
    proxy->setPos(25,475+(i-1)*40);

}

connect(&btn_group, SIGNAL(buttonClicked(int)),this,SLOT(change_spell_index(int)));

for(unsigned i=1; i < 5; i++){
    QPushButton* btn = new QPushButton();
    //btn->setStyleSheet("background-color: gray;font-size:8px;font:bold");
    //btn->setStyleSheet("background-color: gray; border:1px solid black;font-size:8px;font:bold");
    btn->setAttribute(Qt::WA_TranslucentBackground);
    btn->setStyleSheet(style2);
    btn->setText("Range");
    btn->setGeometry(0,0,70,25);
    btn->setMinimumWidth(50);
    btn->setMinimumHeight(25);
    m_buttons_range.push_back(btn);
    btn->hide();
    proxy =  m_scene->addWidget(btn);
    proxy->setParentItem(this);
    btn_group2.addButton(btn,i);
    proxy->setPos(105,475+(i-1)*40);
}


connect(&btn_group2, SIGNAL(buttonPressed(int)),this,SLOT(change_spell_range_index_pressed(int)));
connect(&btn_group2, SIGNAL(buttonReleased(int)),this,SLOT(change_spell_range_index_released(int)));


spells_info->setGeometry(5,375,190,80);

connect(button_move, SIGNAL(clicked()),this,SLOT(clicked_move()));
connect(button_attack, SIGNAL(clicked()),this,SLOT(clicked_attack()));
connect(button_skip_turn, SIGNAL(clicked()),this,SLOT(clicked_skip_turn()));

connect(button_attack_range, SIGNAL(pressed()),this,SLOT(pressed_attack_range()));
connect(button_attack_range, SIGNAL(released()),this,SLOT(released_attack_range()));

}


void GameInfo::set_player_turn(Player::Index player){
    m_player_turn = player;
    if (Player::Index::PLAYER_1 == m_player_turn)
        m_remaining_pieces--;
    update();
}

void GameInfo::set_current_piece(Piece *p)
{
    m_piece = p;
    if(nullptr != m_piece && Piece::Type::CASTLE != p->piece_type()){
        std::vector<QString>spells=m_piece->spell_names();
        int i=0;
        for(auto spell:spells){
            m_buttons[i]->setText(spell);
            m_buttons[i]->show();
            m_buttons_range[i]->setEnabled(true);
            m_buttons_range[i]->show();
            i++;
        }
        spells_info->setText(piece_spell_info(m_piece->piece_type()));
        spells_info->show();

    }
    else {
        for(unsigned i=0;i<4;i++){
            m_buttons[i]->hide();
            m_buttons_range[i]->hide();
        }
        spells_info->setText(QString(""));
        spells_info->hide();
        m_enemy_info.clear();
    }
    update();
}

void GameInfo::set_field_info(std::vector<QString> field_info)
{
    m_field_info = field_info;
}

void GameInfo::set_remaining_steps(unsigned steps)
{
    remaining_steps = steps;
    m_enemy_info.clear();
    update();
}

void GameInfo::set_paint_color(int n, QPainter *painter)
{
    if(n<0)
        painter->setPen(Qt::red);
    else if(n>0)
        painter->setPen(QColor(0,100,0));
    else
        painter->setPen(Qt::black);
}

int GameInfo::get_spell_range_index() const
{
    return m_spell_range_index;
}

void GameInfo::set_colors(QColor &friendly_color, QColor &enemy_color) const
{
    if(Player::Index::PLAYER_1 == m_player_turn){
        friendly_color = Qt::red;
        enemy_color    = Qt::blue;
    }
    else{
        friendly_color = Qt::blue;
        enemy_color    = Qt::red;
    }
}


void GameInfo::clicked_move(){
    button_move->setAttribute(Qt::WA_UnderMouse, false);
    emit call_set_clicked_move();

}

void GameInfo::clicked_attack(){
    button_attack->setAttribute(Qt::WA_UnderMouse, false);
    emit call_set_clicked_attack();

}
void GameInfo::pressed_attack_range(){
    emit call_pressed_attack_range();
}

void GameInfo::released_attack_range(){
    emit call_released_attack_range();
}

void GameInfo::change_spell_index(int i)
{
    m_spell_index = i; 
    std::string spell= m_buttons[i-1]->text().toStdString();
    emit call_spell_cast(spell,i-1);
}

void GameInfo::change_spell_range_index_pressed(int i)
{
    m_spell_range_index = i;
    emit call_pressed_spell_range();
}

void GameInfo::change_spell_range_index_released(int i)
{
    m_spell_range_index = i;
    emit call_released_spell_range();
}

void GameInfo::set_available_spells(std::vector<bool> spells_to_enable)
{
    for(unsigned i = 0; i< spells_to_enable.size(); i++){
        m_buttons[i]->setEnabled(spells_to_enable[i]);
    }
}

void GameInfo::disable_range_btns()
{
    for(auto btn : m_buttons_range)
        btn->setDisabled(true);
}

void GameInfo::disable_spell_btns()
{
    for(auto btn : m_buttons)
        btn->setDisabled(true);
    disable_range_btns();
}


void GameInfo::clicked_skip_turn(){
    emit call_set_clicked_skip_turn();
}

void GameInfo::change_btn_group_index(int i){
    m_piece_index = i;
    spells_info->setText(piece_spell_info(m_piece_index));
    spells_info->show();
    for (auto field: m_fields)
        field->show();

   for (auto piece: m_pieces)
        piece->show();

    emit call_init_select();
    emit call_update_index(i);
    this->update();

}

void GameInfo::reset_buttons(int i){
    m_piece_index = -1;
    spells_info->setText("");
    spells_info->hide();
    for (auto field: m_fields)
        field->hide();
    for (auto piece: m_pieces)
        piece->hide();
    btn_group.setExclusive(false);
    btn_group.button(i)->setChecked(false);
    btn_group.setExclusive(true);
}

void GameInfo::enable_btn_move_attack(bool move, bool attack){
    button_move->setEnabled(move);
    button_attack->setEnabled(attack);
    button_attack_range->setEnabled(true);

}

void GameInfo::disable_btn_move_attack(){
    button_move->setDisabled(true);
    button_attack->setDisabled(true);
    button_attack_range->setDisabled(true);

}

QString GameInfo::piece_init_info(int i) const{

    switch(i){
    case 0:
        return Knight::default_Qinfo() + Knight::field_bonus_Qinfo() + Knight::target_bonus_Qinfo();
    case 1:
        return Archer::default_Qinfo() + Archer::field_bonus_Qinfo() + Archer::target_bonus_Qinfo();
    case 2:
        return Catapult::default_Qinfo() + Catapult::field_bonus_Qinfo() + Catapult::target_bonus_Qinfo();
    case 3:
        return Horseman::default_Qinfo() + Horseman::field_bonus_Qinfo() + Horseman::target_bonus_Qinfo();
    case 4:
        return Mage::default_Qinfo() + Mage::field_bonus_Qinfo() + Mage::target_bonus_Qinfo();
    case 5:
        return Priest::default_Qinfo() + Priest::field_bonus_Qinfo() + Priest::target_bonus_Qinfo();
    case 6:
        return Shieldman::default_Qinfo() + Shieldman::field_bonus_Qinfo() + Shieldman::target_bonus_Qinfo();
    case 7:
        return Spearman::default_Qinfo() + Spearman::field_bonus_Qinfo() + Spearman::target_bonus_Qinfo();
    default:
        throw "Unknows PIECE_INDEX";
    }
}

std::map<QString,QString> GameInfo::default_values_map(int i) const{

    switch(i){
    case 0:
        return Knight::default_values();
    case 1:
        return Archer::default_values();
    case 2:
        return Catapult::default_values();
    case 3:
        return Horseman::default_values();
    case 4:
        return Mage::default_values();
    case 5:
        return Priest::default_values();
    case 6:
        return Shieldman::default_values();
    case 7:
        return Spearman::default_values();
    default:
        throw "Unknows PIECE_INDEX";
    }
}





QString GameInfo::piece_spell_info(int i) const{

    std::vector<std::map<QString, QString>>spell_map_vec;


    switch(i){
    case 0:
        spell_map_vec = Knight::spells_info();
        break;
    case 1:
         spell_map_vec = Archer::spells_info();
         break;
    case 2:
        spell_map_vec = Catapult::spells_info();
        break;
    case 3:
        spell_map_vec = Horseman::spells_info();
        break;
    case 4:
        spell_map_vec = Mage::spells_info();
        break;
    case 5:
        spell_map_vec = Priest::spells_info();
        break;
    case 6:
        spell_map_vec = Shieldman::spells_info();
        break;
    case 7:
        spell_map_vec = Spearman::spells_info();
        break;
    default:
        throw "Unknown PIECE INDEX";
    }

    QString text=QString("");
    for(i=0;i<spell_map_vec.size();i++){
        auto map=spell_map_vec[i];
        QString spell_name=map.begin()->first;
        QString name=QString("%1").arg(spell_name,30);
        text+=name;
        text+="\n";
        text+=map.begin()->second;
        }



    return text;
}


QString GameInfo::piece_spell_info(Piece::Type type) const
{
    std::vector<std::map<QString, QString>>spell_map_vec;

    switch(type){
    case Piece::Type::KNIGHT:
         spell_map_vec = Knight::spells_info();
         break;
    case Piece::Type::ARCHER:
         spell_map_vec = Archer::spells_info();
         break;
    case Piece::Type::CATAPULT:
         spell_map_vec = Catapult::spells_info();
         break;
    case Piece::Type::HORSEMAN:
         spell_map_vec = Horseman::spells_info();
         break;
    case Piece::Type::MAGE:
         spell_map_vec = Mage::spells_info();
         break;
    case Piece::Type::PRIEST:
         spell_map_vec = Priest::spells_info();
         break;
    case Piece::Type::SHIELDMAN:
         spell_map_vec = Shieldman::spells_info();
         break;
    case Piece::Type::SPEARMAN:
         spell_map_vec = Spearman::spells_info();
         break;
     default:
        throw "Unknows PIECE_TYPE";
    }

     QString text=QString("");
         for(int i=0;i<spell_map_vec.size();i++){
             auto map=spell_map_vec[i];
             QString spell_name=map.begin()->first;
             QString name=QString("%1").arg(spell_name,18);
             text+=name;
             text+="\n";
             text+=map.begin()->second;
             text+="\n";
          }



         return text;

}

QString GameInfo::piece_name(int i) const{

    switch(i){
    case 0:
        return QString("Knight");
    case 1:
        return QString("Archer");
    case 2:
        return QString("Catapult");
    case 3:
        return QString("Horseman");
    case 4:
        return QString("Mage");
    case 5:
        return QString("Priest");
    case 6:
        return QString("Shieldman");
    case 7:
        return QString("Spearman");
    default:
        throw "Unknows PIECE_INDEX";
    }
}

void GameInfo::set_enemy_info(QString enemy_info)
{
    m_enemy_info = enemy_info;
    update();
}

void GameInfo::set_enemy_piece(Piece *p)
{
    m_piece = p;
    update();
}

void GameInfo::set_width(unsigned width)
{
    m_width = width;
}
