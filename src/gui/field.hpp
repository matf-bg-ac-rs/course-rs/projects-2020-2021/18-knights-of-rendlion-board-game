#ifndef FIELD_HPP
#define FIELD_HPP

#include "../pieces/piece.hpp"
#include "../pieces/archer.hpp"
#include "../pieces/catapult.hpp"
#include "../pieces/horseman.hpp"
#include "../pieces/knight.hpp"
#include "../pieces/mage.hpp"
#include "../pieces/priest.hpp"
#include "../pieces/shieldman.hpp"
#include "../pieces/spearman.hpp"

#include <QGraphicsItem>
#include <QPainter>
#include <QString>



class Field : public QGraphicsItem {
public:
    enum class Type {
        _NO_TYPE,
        GROUND,
        PLAIN,
        HILL,
        FOREST,
        MOUNTAIN,
        WATER,
        BRIDGE,
        RBRIDGE,
        RWATER,
        CROSS_WATER,
        CROSS_WATER1,
        CROSS_WATER2,
        CROSS_WATER3,
        CROSS_WATER4,
        NONE
    };

    static std::string type_str(Field::Type field_type);
    static QString type_Qstr(Field::Type field_type);
    //static std::pair<std::string,std::string> bonus_info_for_all_pieces(Field::Type field_type);
    static std::vector<QString> bonus_Qinfo_for_all_pieces(Field::Type field_type);
    static QString number_to_qstring(int number);

    const QRect get_rect() const;

    // QGraphicsItem interface
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

    //Constructors
    Field() {}
    Field(Type field_type)
        : m_field_type(field_type), m_card("")
    {}
    Field(Type field_type, bool standable, Piece* piece=nullptr, bool clicked=false)
        : m_field_type(field_type), m_standable(standable), m_piece(piece), m_clicked(clicked), m_card("")
    {}

    //Getters
    Piece* get_piece() const;
    const std::pair<unsigned,unsigned> get_board_index() const;
    unsigned get_row_index() const;
    unsigned get_column_index() const;
    std::string get_card() const;
    bool has_piece() const;
    bool has_card() const;
    Type field_type() const;
    bool is_standable() const;
    bool is_available() const;
    bool is_marked() const;
    bool is_clicked() const;

    //Setters
    void set_row_index(unsigned i);
    void set_column_index(unsigned j);
    void set_piece(Piece* piece);
    void set_type(Field::Type new_type);
    void remove_piece();
    void set_marked(bool mark);
    void set_clicked(bool b);
    void set_card(std::string card_name);

    //this will do the marking of the field according to sent parameters
    void mark(QPainter* painter, QColor color, double thickness, double start_point);

    /**
     * Bonus-handling methods
     **/

    // Helper method for extracting x% field bonuses for piece standing on this field
    std::pair<int, int> get_field_bonuses();

    // Applies bonuses to a piece standing on this field
    void activate_field_bonuses();

    // Removes previous bonuses from a piece standing on this field
    void reset_field_bonuses();


private:
    Type m_field_type;
    bool m_standable;
    Piece* m_piece;
    bool m_clicked;
    unsigned m_row;
    unsigned m_column;
    bool m_marked;
    std::string m_card;

    static const std::map<Type,QString> m_paths;

};



#endif // FIELD_HPP
