#ifndef GAMECONTROLLER_HPP
#define GAMECONTROLLER_HPP

#include <QGraphicsView>
#include <QGraphicsScene>
#include <QWidget>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsItem>
#include <QPushButton>
#include <QGraphicsPixmapItem>
#include <QTextEdit>

#include "../gui/playerInfo.hpp"
#include "../gui/gameInfo.hpp"
#include "../gui/board.hpp"
#include "../gui/field.hpp"
#include "../pieces/castle.hpp"
#include "./player.hpp"
#include "../utility/utility.hpp"
#include "updater.hpp"
#include "actionmanager.hpp"
#include "../gui/cardInfo.hpp"
#include "../engine/cardhandler.hpp"

#include <iostream>
#include <string>
#include <map>
#include <fstream>
#include <iomanip>
#include <list>
#include <set>

#define __LOG__ 1

class GameInfo;
class CardInfo;

class GameController : public QGraphicsView
{
Q_OBJECT
public:
    //Game part indicators
    enum class Part{
        NONE,
        INIT,
        PLAY
    };

    //Actions indicators
    enum class Actions{
        MOVE,
        ATTACK,
        SPELL,
        RANGE,
        CARD,
        NONE
    };

    //method that creates GC as a singleton class
    static GameController* create_gameController();

    void mousePressEvent(QMouseEvent *event);

    //signals
signals:
    void call_card_selection();
    void call_play_part();
    void call_game_over(Player::Index);
    void call_reset_buttons(int);
    void call_enable_btn_move_attack(bool,bool);
    void call_disable_btn_move_attack();
    void call_set_available_spells(std::vector<bool>);
    void call_disable_range_btns();
    void call_disable_spell_btns();
    void call_enable_btn_pick();

    // QWidget interface
protected:
    void resizeEvent(QResizeEvent *event);

    //slot methods for according signals
private slots:
    //fullscreen controls
    void enterFullScreen();
    void exitFullScreen();

    //instruction intialization
    void instructions();
    void init_instructions();
    void cards_instructions();
    void play_instructions();

    //game part initialization
    void piece_selection();
    void card_selection();
    void play_part();

    //mouse event handlers
    void handle_mouse_event(unsigned row, unsigned column);
    void handle_right_click(unsigned row, unsigned column);

    //method that selects/marks fields in piece selection
    void init_select();

    //this shows end game screen
    void game_over(Player::Index winner);

    //updates index that will later be used for piece creation
    void update_piece_index(int index);

    //Indicator setters for game logic
    void set_clicked_move();
    void set_clicked_attack();
    void set_clicked_skip_turn();
    void clicked_cast_spell(std::string,int i);

    //Range buttons methods for markin/unmarking fields
    void pressed_attack_range();
    void released_attack_range();
    void pressed_spell_range();
    void released_spell_range();

    //Swaps game and card info
    void swap_info();

    //Card picking methods in init part
    void card_group_index(int);
    void enable_btn_pick();
    void pick_a_card();

    void clicked_activate_card(std::string);



private:
    static inline const char* ATTACK_LOG_FNAME = "log_attack.txt";
    static std::ofstream attack_log;

    //Constructor
    GameController();

    //we don't want to have cctor and copy assign operator
    GameController (const GameController&) = delete;
    GameController& operator= (const GameController&) = delete;


    //select/deselect methods according to game part
    void init_deselect();
    void play_select();
    void play_deselect(unsigned row, unsigned column);
    //helper method for select/deselect
    void set_marked(std::list<Field*>& collection,bool value, bool& indicator);

    //this will recalculate move/attack/spell parameters
    //if player have steps in current turn
    void recalculate(unsigned row, unsigned column);

    //puts castles on the board for both players
    void add_castles();

    //turn change methods
    void change_turn();
    void check_turn_change();
    void change_remaining_steps(int cost);

    bool check_turn_and_staging_area(unsigned col);
    bool check_if_friendly_piece(unsigned row, unsigned column) const;
    bool check_if_enemy_piece(unsigned row, unsigned column) const;

    //creates piece according to index(piece_type) that was set previously
    void create_and_place_piece(int piece_type,unsigned row, unsigned column);

    //Game logic methods
    void move_if_coords_match(unsigned row, unsigned column);
    void attack_if_coords_match(unsigned row, unsigned column);
    void cast_spell_if_coords_match(unsigned row, unsigned column);
    void activate_card_if_coords_match(unsigned row,unsigned column);
    bool check_if_game_over();
    //Helper methods
    bool check_move_attack(Piece* piece);
    void check_spellable(Piece* piece);

    //Player getters
    bool is_enemy(Player::Index index) const;
    Player* get_player_on_turn_ptr();
    Player* get_enemy();
    Player* get_player_ptr_by_index(Player::Index index);

    //Active piece getters/setters
    void set_active_piece(Piece* piece);
    void reset_active_piece();
    void change_active_piece(unsigned row, unsigned column);
    bool has_active_piece() const;

    //Card methods
    std::vector<std::string> get_10_random_cards() const;
    void set_n_random_cards_to_board(unsigned n);
    void pop_up_card_found(std::string card_name) const;
    void check_field_for_card(Field* field);
    void check_fields_with_cards_cache();

    //Piece methods : updaters, trackers, etc..
    void update_pieces();
    void create_updater(Piece* piece, UpdateType update_type, int start_after);
    void add_updater(Updater* updater);
    void delete_updater(Updater* updater);
    void delete_updaters_for_piece(Piece* piece);
    void add_stats_tracker(Piece* piece);
    void remove_stats_tracker(Piece* piece);
    bool is_stats_tracked(Piece* piece);
    void delete_piece(Piece* piece);
    void remove_all_piece_data(Piece* piece);



    /***
      *   DATA
    ***/

    //scene elements
    QGraphicsScene* m_scene;
    QPushButton* m_btn_play;
    QPushButton* m_btn_exit;
    QPushButton* m_btn_swap;

    QPushButton* btn_instructions = nullptr;
    QPushButton* btn_init_part = nullptr;
    QPushButton* btn_play_part = nullptr;
    QPushButton* btn_cards_part = nullptr;
    QGraphicsPixmapItem* pix_item;
    QTextEdit* instructions_text = nullptr;

    //custom classes present on scene
    PlayerInfo* m_player1_info;
    PlayerInfo* m_player2_info;
    GameInfo* m_game_info;
    Board* m_board;
    CardInfo* m_card_info;
    QGraphicsItem* m_active_info;

    //AM for handling spells and cards
    ActionManager* m_action_manager;

    //Players
    Player* m_player1;
    Player* m_player2;

    //game logic fields
    Part m_game_part;
    Player::Index m_player_turn;
    int piece_index;

    QButtonGroup card_group;
    QPushButton* btn_pick;

    Piece* m_active_piece;

    //Game logic indicators
    bool m_clicked_move;
    bool m_clicked_attack;
    bool m_clicked_attack_range;
    bool m_clicked_spell;
    bool m_clicked_card;
    int m_spell_range_index;
    int m_spell_index;
    int m_card_index;
    const unsigned m_num_of_pieces_selection = 15;
    const double m_num_of_cards_for_board_percent = 0.2;

    //Turn step counter
    unsigned m_remaining_steps;
    unsigned m_next_steps;

    //Structures for storing already calculated data
    //for active piece
    std::list<Field *> m_possible_moves;
    std::list<Field *> m_possible_attacks;
    std::vector< std::list<Field*> > m_possible_spells;
    std::list<Field*> m_possible_card_fields;

    std::list<Updater*> m_piece_updaters;
    std::set<Piece*> m_piece_stats_tracked;
    std::set<Piece*> m_dead_pieces_cache;
    std::set<Field*> m_fields_with_cards_cache;

    std::string m_spell_string;
    std::vector<unsigned> m_spell_costs;

    std::vector<std::string> m_cards;
    CardHandler* m_card_handler;



};

#endif // GAMECONTROLLER_HPP
