#ifndef CARDHANDLER_HPP
#define CARDHANDLER_HPP

#include "../gui/board.hpp"
#include "../utility/utility.hpp"
#include "../gui/card.hpp"
#include "handletypes.hpp"

#include <string>
#include <vector>
#include <tuple>
#include <list>
#include <set>


class Updater;

class CardHandler
{
public:
    virtual ~CardHandler() = default;

    CardHandler(Board* board, Player* player_1, Player* player_2, Player::Index card_owner);

    virtual void handle(Field* field=nullptr) = 0;
    virtual std::list<Field*> get_potential_fields() const  = 0;

    Player* get_owner() const;
    Player* get_enemy() const;
    Field* new_field_after_move() const;
    bool has_new_field_after_move() const;
    std::string card_name() const;
    unsigned card_cost() const;
    Card::Type card_type() const;
    Player::Index card_owner() const;
    bool is_instant_cast() const;
    int additional_moves() const;
    std::vector<Piece*>& killed_pieces();
    bool has_killed_pieces() const;
    std::vector<Updater*> get_updaters() const;


protected:
    Board* m_board;
    Player* m_player_1;
    Player* m_player_2;
    Field* m_new_field_after_move = nullptr;
    std::string m_card_name;
    unsigned m_card_cost;
    Card::Type m_card_type;
    Player::Index m_card_owner;
    bool m_is_instant_cast;
    int m_additional_moves = 0;
    std::vector<Piece*> m_killed_pieces;
    std::vector<std::tuple<Piece*, UpdateType, int>> m_updater_parameters;
};

class HolyLight : public CardHandler
{
public:
    HolyLight(Board* board, Player* player_1, Player* player_2, Player::Index card_owner)
        : CardHandler(board, player_1, player_2, card_owner)
    {
        m_card_cost = Card::HolyLight::COST;
        m_card_type = Card::HolyLight::TYPE;
        m_card_name = Card::HolyLight::NAME;
        m_is_instant_cast = Card::HolyLight::IS_INSTANT_CAST;
    }

    void handle(Field* selected_field) override;
    std::list<Field*> get_potential_fields() const  override;
};

class WarCry : public CardHandler
{
public:
    WarCry(Board* board, Player* player_1, Player* player_2, Player::Index card_owner)
        : CardHandler(board, player_1, player_2, card_owner)
    {
        m_card_cost = Card::WarCry::COST;
        m_card_type = Card::WarCry::TYPE;
        m_card_name = Card::WarCry::NAME;
        m_is_instant_cast = Card::WarCry::IS_INSTANT_CAST;
    }

    void handle(Field* selected_field) override;
    std::list<Field*> get_potential_fields() const  override;
};

class DeadlyPoison : public CardHandler
{
public:
    DeadlyPoison(Board* board, Player* player_1, Player* player_2, Player::Index card_owner)
        : CardHandler(board, player_1, player_2, card_owner)
    {
        m_card_cost = Card::DeadlyPoison::COST;
        m_card_type = Card::DeadlyPoison::TYPE;
        m_card_name = Card::DeadlyPoison::NAME;
        m_is_instant_cast = Card::DeadlyPoison::IS_INSTANT_CAST;
    }

    void handle(Field* selected_field) override;
    std::list<Field*> get_potential_fields() const  override;
};

class CrushingBlow : public CardHandler
{
public:
    CrushingBlow(Board* board, Player* player_1, Player* player_2, Player::Index card_owner)
        : CardHandler(board, player_1, player_2, card_owner)
    {
        m_card_cost = Card::CrushingBlow::COST;
        m_card_type = Card::CrushingBlow::TYPE;
        m_card_name = Card::CrushingBlow::NAME;
        m_is_instant_cast = Card::CrushingBlow::IS_INSTANT_CAST;
    }

    void handle(Field* selected_field) override;
    std::list<Field*> get_potential_fields() const  override;
};

class Thunderstorm : public CardHandler
{
public:
    Thunderstorm(Board* board, Player* player_1, Player* player_2, Player::Index card_owner)
        : CardHandler(board, player_1, player_2, card_owner)
    {
        m_card_cost = Card::Thunderstorm::COST;
        m_card_type = Card::Thunderstorm::TYPE;
        m_card_name = Card::Thunderstorm::NAME;
        m_is_instant_cast = Card::Thunderstorm::IS_INSTANT_CAST;
    }

    void handle(Field* selected_field) override;
    std::list<Field*> get_potential_fields() const  override;
};

class CallToArms : public CardHandler
{
public:
    CallToArms(Board* board, Player* player_1, Player* player_2, Player::Index card_owner)
        : CardHandler(board, player_1, player_2, card_owner)
    {
        m_card_cost = Card::CallToArms::COST;
        m_card_type = Card::CallToArms::TYPE;
        m_card_name = Card::CallToArms::NAME;
        m_is_instant_cast = Card::CallToArms::IS_INSTANT_CAST;
    }

    void handle(Field* selected_field) override;
    std::list<Field*> get_potential_fields() const  override;
};

class Teleport : public CardHandler
{
public:
    Teleport(Board* board, Player* player_1, Player* player_2, Player::Index card_owner)
        : CardHandler(board, player_1, player_2, card_owner)
    {
        m_card_cost = Card::Teleport::COST;
        m_card_type = Card::Teleport::TYPE;
        m_card_name = Card::Teleport::NAME;
        m_is_instant_cast = Card::Teleport::IS_INSTANT_CAST;
    }

    void handle(Field* selected_field) override;
    std::list<Field*> get_potential_fields() const override;
};

class AdrenalineRush : public CardHandler
{
public:
    AdrenalineRush(Board* board, Player* player_1, Player* player_2, Player::Index card_owner)
        : CardHandler(board, player_1, player_2, card_owner)
    {
        m_card_cost = Card::AdrenalineRush::COST;
        m_card_type = Card::AdrenalineRush::TYPE;
        m_card_name = Card::AdrenalineRush::NAME;
        m_is_instant_cast = Card::AdrenalineRush::IS_INSTANT_CAST;
    }

    void handle(Field* selected_field) override;
    std::list<Field*> get_potential_fields() const  override;
};

class Silence : public CardHandler
{
public:
    Silence(Board* board, Player* player_1, Player* player_2, Player::Index card_owner)
        : CardHandler(board, player_1, player_2, card_owner)
    {
        m_card_cost = Card::Silence::COST;
        m_card_type = Card::Silence::TYPE;
        m_card_name = Card::Silence::NAME;
        m_is_instant_cast = Card::Silence::IS_INSTANT_CAST;
    }

    void handle(Field* selected_field) override;
    std::list<Field*> get_potential_fields() const  override;
};

class Affliction : public CardHandler
{
public:
    Affliction(Board* board, Player* player_1, Player* player_2, Player::Index card_owner)
        : CardHandler(board, player_1, player_2, card_owner)
    {
        m_card_cost = Card::Affliction::COST;
        m_card_type = Card::Affliction::TYPE;
        m_card_name = Card::Affliction::NAME;
        m_is_instant_cast = Card::Affliction::IS_INSTANT_CAST;
    }

    void handle(Field* selected_field) override;
    std::list<Field*> get_potential_fields() const  override;
};

class Rebuild : public CardHandler
{
public:
    Rebuild(Board* board, Player* player_1, Player* player_2, Player::Index card_owner)
        : CardHandler(board, player_1, player_2, card_owner)
    {
        m_card_cost = Card::Rebuild::COST;
        m_card_type = Card::Rebuild::TYPE;
        m_card_name = Card::Rebuild::NAME;
        m_is_instant_cast = Card::Rebuild::IS_INSTANT_CAST;
    }

    void handle(Field* selected_field) override;
    std::list<Field*> get_potential_fields() const  override;
};

class DragonsBreath : public CardHandler
{
public:
    DragonsBreath(Board* board, Player* player_1, Player* player_2, Player::Index card_owner)
        : CardHandler(board, player_1, player_2, card_owner)
    {
        m_card_cost = Card::DragonsBreath::COST;
        m_card_type = Card::DragonsBreath::TYPE;
        m_card_name = Card::DragonsBreath::NAME;
        m_is_instant_cast = Card::DragonsBreath::IS_INSTANT_CAST;
    }

    void handle(Field* selected_field) override;
    std::list<Field*> get_potential_fields() const override;
};

class DoubleEdge : public CardHandler
{
public:
    DoubleEdge(Board* board, Player* player_1, Player* player_2, Player::Index card_owner)
        : CardHandler(board, player_1, player_2, card_owner)
    {
        m_card_cost = Card::DoubleEdge::COST;
        m_card_type = Card::DoubleEdge::TYPE;
        m_card_name = Card::DoubleEdge::NAME;
        m_is_instant_cast = Card::DoubleEdge::IS_INSTANT_CAST;
    }

    void handle(Field* selected_field) override;
    std::list<Field*> get_potential_fields() const  override;
};

class Meteor : public CardHandler
{
public:
    Meteor(Board* board, Player* player_1, Player* player_2, Player::Index card_owner)
        : CardHandler(board, player_1, player_2, card_owner)
    {
        m_card_cost = Card::Meteor::COST;
        m_card_type = Card::Meteor::TYPE;
        m_card_name = Card::Meteor::NAME;
        m_is_instant_cast = Card::Meteor::IS_INSTANT_CAST;
    }

    void handle(Field* selected_field) override;
    std::list<Field*> get_potential_fields() const  override;
};

class CunningWizardry : public CardHandler
{
public:
    CunningWizardry(Board* board, Player* player_1, Player* player_2, Player::Index card_owner)
        : CardHandler(board, player_1, player_2, card_owner)
    {
        m_card_cost = Card::CunningWizardry::COST;
        m_card_type = Card::CunningWizardry::TYPE;
        m_card_name = Card::CunningWizardry::NAME;
        m_is_instant_cast = Card::CunningWizardry::IS_INSTANT_CAST;
    }

    void handle(Field* selected_field) override;
    std::list<Field*> get_potential_fields() const  override;
};

class Refreshment : public CardHandler
{
public:
    Refreshment(Board* board, Player* player_1, Player* player_2, Player::Index card_owner)
        : CardHandler(board, player_1, player_2, card_owner)
    {
        m_card_cost = Card::Refreshment::COST;
        m_card_type = Card::Refreshment::TYPE;
        m_card_name = Card::Refreshment::NAME;
        m_is_instant_cast = Card::Refreshment::IS_INSTANT_CAST;
    }

    void handle(Field* selected_field) override;
    std::list<Field*> get_potential_fields() const  override;
};

class Armageddon : public CardHandler
{
public:
    Armageddon(Board* board, Player* player_1, Player* player_2, Player::Index card_owner)
        : CardHandler(board, player_1, player_2, card_owner)
    {
        m_card_cost = Card::Armageddon::COST;
        m_card_type = Card::Armageddon::TYPE;
        m_card_name = Card::Armageddon::NAME;
        m_is_instant_cast = Card::Armageddon::IS_INSTANT_CAST;
    }

    void handle(Field* selected_field) override;
    std::list<Field*> get_potential_fields() const  override;
};

class Thievery : public CardHandler
{
public:
    Thievery(Board* board, Player* player_1, Player* player_2, Player::Index card_owner)
        : CardHandler(board, player_1, player_2, card_owner)
    {
        m_card_cost = Card::Thievery::COST;
        m_card_type = Card::Thievery::TYPE;
        m_card_name = Card::Thievery::NAME;
        m_is_instant_cast = Card::Thievery::IS_INSTANT_CAST;
    }

    void handle(Field* selected_field) override;
    std::list<Field*> get_potential_fields() const  override;
};

class Combustion: public CardHandler
{
public:
    Combustion(Board* board, Player* player_1, Player* player_2, Player::Index card_owner)
        : CardHandler(board, player_1, player_2, card_owner)
    {
        m_card_cost = Card::Combustion::COST;
        m_card_type = Card::Combustion::TYPE;
        m_card_name = Card::Combustion::NAME;
        m_is_instant_cast = Card::Combustion::IS_INSTANT_CAST;
    }

    void handle(Field* selected_field) override;
    std::list<Field*> get_potential_fields() const override;
};

class WrathOfBorgoldin: public CardHandler
{
public:
    WrathOfBorgoldin(Board* board, Player* player_1, Player* player_2, Player::Index card_owner)
        : CardHandler(board, player_1, player_2, card_owner)
    {
        m_card_cost = Card::WrathOfBorgoldin::COST;
        m_card_type = Card::WrathOfBorgoldin::TYPE;
        m_card_name = Card::WrathOfBorgoldin::NAME;
        m_is_instant_cast = Card::WrathOfBorgoldin::IS_INSTANT_CAST;
    }

    void handle(Field* selected_field) override;
    std::list<Field*> get_potential_fields() const override;
};

class BreakLoose : public CardHandler
{
public:
    BreakLoose(Board* board, Player* player_1, Player* player_2, Player::Index card_owner)
        : CardHandler(board, player_1, player_2, card_owner)
    {
        m_card_cost = Card::BreakLoose::COST;
        m_card_type = Card::BreakLoose::TYPE;
        m_card_name = Card::BreakLoose::NAME;
        m_is_instant_cast = Card::BreakLoose::IS_INSTANT_CAST;
    }

    void handle(Field* selected_field) override;
    std::list<Field*> get_potential_fields() const override;
};

class ArrowVolley : public CardHandler
{
public:
    ArrowVolley(Board* board, Player* player_1, Player* player_2, Player::Index card_owner)
        : CardHandler(board, player_1, player_2, card_owner)
    {
        m_card_cost = Card::ArrowVolley::COST;
        m_card_type = Card::ArrowVolley::TYPE;
        m_card_name = Card::ArrowVolley::NAME;
        m_is_instant_cast = Card::ArrowVolley::IS_INSTANT_CAST;
    }

    void handle(Field* selected_field) override;
    std::list<Field*> get_potential_fields() const override;
};

class OneManArmy : public CardHandler
{
public:
    OneManArmy(Board* board, Player* player_1, Player* player_2, Player::Index card_owner)
        : CardHandler(board, player_1, player_2, card_owner)
    {
        m_card_cost = Card::OneManArmy::COST;
        m_card_type = Card::OneManArmy::TYPE;
        m_card_name = Card::OneManArmy::NAME;
        m_is_instant_cast = Card::OneManArmy::IS_INSTANT_CAST;
    }

    void handle(Field* selected_field) override;
    std::list<Field*> get_potential_fields() const override;
};

class Aristocracy : public CardHandler
{
public:
    Aristocracy(Board* board, Player* player_1, Player* player_2, Player::Index card_owner)
        : CardHandler(board, player_1, player_2, card_owner)
    {
        m_card_cost = Card::Aristocracy::COST;
        m_card_type = Card::Aristocracy::TYPE;
        m_card_name = Card::Aristocracy::NAME;
        m_is_instant_cast = Card::Aristocracy::IS_INSTANT_CAST;
    }

    void handle(Field* selected_field) override;
    std::list<Field*> get_potential_fields() const override;
};

class CavalryAssembly : public CardHandler
{
public:
    CavalryAssembly(Board* board, Player* player_1, Player* player_2, Player::Index card_owner)
        : CardHandler(board, player_1, player_2, card_owner)
    {
        m_card_cost = Card::CavalryAssembly::COST;
        m_card_type = Card::CavalryAssembly::TYPE;
        m_card_name = Card::CavalryAssembly::NAME;
        m_is_instant_cast = Card::CavalryAssembly::IS_INSTANT_CAST;
    }

    void handle(Field* selected_field) override;
    std::list<Field*> get_potential_fields() const override;
};

class Firesword : public CardHandler
{
public:
    Firesword(Board* board, Player* player_1, Player* player_2, Player::Index card_owner)
        : CardHandler(board, player_1, player_2, card_owner)
    {
        m_card_cost = Card::Firesword::COST;
        m_card_type = Card::Firesword::TYPE;
        m_card_name = Card::Firesword::NAME;
        m_is_instant_cast = Card::Firesword::IS_INSTANT_CAST;
    }

    void handle(Field* selected_field) override;
    std::list<Field*> get_potential_fields() const override;
};


class BridgeOfDeath : public CardHandler
{
public:
    BridgeOfDeath(Board* board, Player* player_1, Player* player_2, Player::Index card_owner)
        : CardHandler(board, player_1, player_2, card_owner)
    {
        m_card_cost = Card::BridgeOfDeath::COST;
        m_card_type = Card::BridgeOfDeath::TYPE;
        m_card_name = Card::BridgeOfDeath::NAME;
        m_is_instant_cast = Card::BridgeOfDeath::IS_INSTANT_CAST;
    }

    void handle(Field* selected_field) override;
    std::list<Field*> get_potential_fields() const override;
};

class Flood : public CardHandler
{
public:
    Flood(Board* board, Player* player_1, Player* player_2, Player::Index card_owner)
        : CardHandler(board, player_1, player_2, card_owner)
    {
        m_card_cost = Card::Flood::COST;
        m_card_type = Card::Flood::TYPE;
        m_card_name = Card::Flood::NAME;
        m_is_instant_cast = Card::Flood::IS_INSTANT_CAST;
    }

    void handle(Field* selected_field) override;
    std::list<Field*> get_potential_fields() const override;
};

class Bracers : public CardHandler
{
public:
    Bracers(Board* board, Player* player_1, Player* player_2, Player::Index card_owner)
        : CardHandler(board, player_1, player_2, card_owner)
    {
        m_card_cost = Card::Bracers::COST;
        m_card_type = Card::Bracers::TYPE;
        m_card_name = Card::Bracers::NAME;
        m_is_instant_cast = Card::Bracers::IS_INSTANT_CAST;
    }

    void handle(Field* selected_field) override;
    std::list<Field*> get_potential_fields() const override;
};

class FatalWounds : public CardHandler
{
public:
    FatalWounds(Board* board, Player* player_1, Player* player_2, Player::Index card_owner)
        : CardHandler(board, player_1, player_2, card_owner)
    {
        m_card_cost = Card::FatalWounds::COST;
        m_card_type = Card::FatalWounds::TYPE;
        m_card_name = Card::FatalWounds::NAME;
        m_is_instant_cast = Card::FatalWounds::IS_INSTANT_CAST;
    }

    void handle(Field* selected_field) override;
    std::list<Field*> get_potential_fields() const override;
};


#endif // CARDHANDLER_HPP
