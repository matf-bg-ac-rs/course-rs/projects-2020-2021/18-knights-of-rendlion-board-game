#include "actionmanager.hpp"
#include <algorithm>
#include <cctype>


ActionManager *ActionManager::create_manager(Board *board, Player* player_1, Player* player_2)
{
    static auto manager = new ActionManager(board, player_1, player_2);
    return manager;
}

ActionManager::ActionManager(Board *board, Player* player_1, Player* player_2)
    : m_board(board)
    , m_player_1(player_1)
    , m_player_2(player_2)
{}

SpellHandler* ActionManager::spell_handler(Archer::Spell archer_spell)
{
    switch (archer_spell) {
        case Archer::Spell::STEADY_AIM:
            return new SteadyAim(m_board);
        default:
            throw Piece::SPELL_ERROR;
    }
}

SpellHandler* ActionManager::spell_handler(Catapult::Spell catapult_spell)
{
    switch (catapult_spell) {
        case Catapult::Spell::BARRAGE:
            return new Barrage(m_board);
        default:
            throw Piece::SPELL_ERROR;
    }
}

SpellHandler* ActionManager::spell_handler(Horseman::Spell horseman_spell)
{
    switch (horseman_spell) {
        case Horseman::Spell::GALLOP:
            return new Gallop(m_board);
        default:
            throw Piece::SPELL_ERROR;
    }
}

SpellHandler* ActionManager::spell_handler(Knight::Spell knight_spell)
{
    switch (knight_spell) {
        case Knight::Spell::MORTAL_STRIKE:
            return new MortalStrike(m_board);
        default:
            throw Piece::SPELL_ERROR;
    }
}

SpellHandler* ActionManager::spell_handler(Mage::Spell mage_spell)
{
    switch (mage_spell) {
        case Mage::Spell::FIREBOLT:
            return new Firebolt(m_board);
        case Mage::Spell::FROSTBOLT:
            return new Frostbolt(m_board);
        case Mage::Spell::ARCANEBOLT:
            return new Arcanebolt(m_board);
        case Mage::Spell::SHOCKWAVE:
            return new Shockwave(m_board);
        default:
            throw Piece::SPELL_ERROR;
    }
}

SpellHandler* ActionManager::spell_handler(Priest::Spell priest_spell)
{
    switch (priest_spell) {
        case Priest::Spell::HEAL:
            return new Heal(m_board);
        case Priest::Spell::EMPOWER:
            return new Empower(m_board);
        case Priest::Spell::HOLY_SHIELD:
            return new HolyShield(m_board);
        case Priest::Spell::STUN:
            return new Stun(m_board);
        default:
            throw Piece::SPELL_ERROR;
    }
}

SpellHandler* ActionManager::spell_handler(Shieldman::Spell shieldman_spell)
{
    switch (shieldman_spell) {
        case Shieldman::Spell::TURTLE:
            return new Turtle(m_board);
        default:
            throw Piece::SPELL_ERROR;
    }
}

SpellHandler* ActionManager::spell_handler(Spearman::Spell spearman_spell)
{
    switch (spearman_spell) {
        case Spearman::Spell::SPEARTHROW:
            return new Spearthrow(m_board);
        default:
            throw Piece::SPELL_ERROR;
    }
}

SpellHandler* ActionManager::spell_handler(std::string spell_str)
{
    std::string spell_lower = spell_str.c_str();
    std::transform(
        spell_lower.begin(), 
        spell_lower.end(), 
        spell_lower.begin(),
        [](unsigned char c) {
            return c == '_' ? ' ' : std::tolower(c);
        }
    );

    switch (str2int(spell_lower.c_str())) {
        case str2int("steady aim"):
            return new SteadyAim(m_board);
        case str2int("barrage"):
            return new Barrage(m_board);
        case str2int("gallop"):
            return new Gallop(m_board);
        case str2int("mortal strike"):
            return new MortalStrike(m_board);
        case str2int("firebolt"):
            return new Firebolt(m_board);
        case str2int("frostbolt"):
            return new Frostbolt(m_board);
        case str2int("arcanebolt"):
            return new Arcanebolt(m_board);
        case str2int("shockwave"):
            return new Shockwave(m_board);
        case str2int("heal"):
            return new Heal(m_board);
        case str2int("empower"):
            return new Empower(m_board);
        case str2int("holy shield"):
            return new HolyShield(m_board);
        case str2int("stun"):
            return new Stun(m_board);
        case str2int("turtle"):
            return new Turtle(m_board);
        case str2int("spearthrow"):
            return new Spearthrow(m_board);
        default:
            std::string err = "";
            throw err + "Invalid spell_str after conversion: " + spell_lower;
    }
}

CardHandler *ActionManager::card_handler(std::string card_name, Player::Index card_owner)
{
    std::string card_lower = card_name.c_str();
    std::transform(
        card_lower.begin(),
        card_lower.end(),
        card_lower.begin(),
        [](unsigned char c) {
            return c == '_' ? ' ' : std::tolower(c);
        }
    );

    switch (str2int(card_lower.c_str())) {
    case str2int("holy light"):
        return new HolyLight(m_board, m_player_1, m_player_2, card_owner);
    case str2int("war cry"):
        return new WarCry(m_board, m_player_1, m_player_2, card_owner);
    case str2int("deadly poison"):
        return new DeadlyPoison(m_board, m_player_1, m_player_2, card_owner);
    case str2int("crushing blow"):
        return new CrushingBlow(m_board, m_player_1, m_player_2, card_owner);
    case str2int("thunderstorm"):
        return new Thunderstorm(m_board, m_player_1, m_player_2, card_owner);
    case str2int("call to arms"):
        return new CallToArms(m_board, m_player_1, m_player_2, card_owner);
    case str2int("teleport"):
        return new Teleport(m_board, m_player_1, m_player_2, card_owner);
    case str2int("adrenaline rush"):
        return new AdrenalineRush(m_board, m_player_1, m_player_2, card_owner);
    case str2int("silence"):
        return new Silence(m_board, m_player_1, m_player_2, card_owner);
    case str2int("affliction"):
        return new Affliction(m_board, m_player_1, m_player_2, card_owner);
    case str2int("rebuild"):
        return new Rebuild(m_board, m_player_1, m_player_2, card_owner);
    case str2int("dragon's breath"):
        return new DragonsBreath(m_board, m_player_1, m_player_2, card_owner);
    case str2int("double edge"):
        return new DoubleEdge(m_board, m_player_1, m_player_2, card_owner);
    case str2int("meteor"):
        return new Meteor(m_board, m_player_1, m_player_2, card_owner);
    case str2int("cunning wizardry"):
        return new CunningWizardry(m_board, m_player_1, m_player_2, card_owner);
    case str2int("refreshment"):
        return new Refreshment(m_board, m_player_1, m_player_2, card_owner);
    case str2int("armageddon"):
        return new Armageddon(m_board, m_player_1, m_player_2, card_owner);
    case str2int("thievery"):
        return new Thievery(m_board, m_player_1, m_player_2, card_owner);
    case str2int("combustion"):
        return new Combustion(m_board, m_player_1, m_player_2, card_owner);
    case str2int("wrath of borgoldin"):
        return new WrathOfBorgoldin(m_board, m_player_1, m_player_2, card_owner);
    case str2int("break loose"):
        return new BreakLoose(m_board, m_player_1, m_player_2, card_owner);
    case str2int("arrow volley"):
        return new ArrowVolley(m_board, m_player_1, m_player_2, card_owner);
    case str2int("one man army"):
        return new OneManArmy(m_board, m_player_1, m_player_2, card_owner);
    case str2int("aristocracy"):
        return new Aristocracy(m_board, m_player_1, m_player_2, card_owner);
    case str2int("cavalry assembly"):
        return new CavalryAssembly(m_board, m_player_1, m_player_2, card_owner);
    case str2int("firesword"):
        return new Firesword(m_board, m_player_1, m_player_2, card_owner);
    case str2int("bridge of death"):
        return new BridgeOfDeath(m_board, m_player_1, m_player_2, card_owner);
    case str2int("flood"):
        return new Flood(m_board, m_player_1, m_player_2, card_owner);
    case str2int("bracers"):
        return new Bracers(m_board, m_player_1, m_player_2, card_owner);
    case str2int("fatal wounds"):
        return new FatalWounds(m_board, m_player_1, m_player_2, card_owner);
    default:
        std::string err = "";
        throw err + "Invalid card name after conversion: " + card_lower;
    }
}

