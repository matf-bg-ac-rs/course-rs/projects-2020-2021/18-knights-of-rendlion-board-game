#include "spellhandler.hpp"
#include "updater.hpp"


std::string SpellHandler::archer_spell_to_str(Archer::Spell spell) 
{
    switch (spell) {
        case Archer::Spell::STEADY_AIM:
            return "Steady Aim";
        default:
            throw Piece::SPELL_ERROR;
    }
}

std::string SpellHandler::catapult_spell_to_str(Catapult::Spell spell)
{
    switch (spell) {
        case Catapult::Spell::BARRAGE:
            return "Barrage";
        default:
            throw Piece::SPELL_ERROR;
    }
}

std::string SpellHandler::horseman_spell_to_str(Horseman::Spell spell)
{
    switch (spell) {
        case Horseman::Spell::GALLOP:
            return "Gallop";
        default:
            throw Piece::SPELL_ERROR;
    }
}

std::string SpellHandler::knight_spell_to_str(Knight::Spell spell)
{
    switch (spell) {
        case Knight::Spell::MORTAL_STRIKE:
            return "Mortal Strike";
        default:
            throw Piece::SPELL_ERROR;
    }
}

std::string SpellHandler::mage_spell_to_str(Mage::Spell spell)
{
    switch (spell) {
        case Mage::Spell::FIREBOLT:
            return "Firebolt";
        case Mage::Spell::FROSTBOLT:
            return "Frostbolt";
        case Mage::Spell::ARCANEBOLT:
            return "Arcanebolt";
        case Mage::Spell::SHOCKWAVE:
            return "Shockwave";
        default:
            throw Piece::SPELL_ERROR;
    }
}

std::string SpellHandler::priest_spell_to_str(Priest::Spell spell)
{
    switch (spell) {
        case Priest::Spell::HEAL:
            return "Heal";
        case Priest::Spell::EMPOWER:
            return "Empower";
        case Priest::Spell::HOLY_SHIELD:
            return "Holy Shield";
        case Priest::Spell::STUN:
            return "Stun";
        default:
            throw Piece::SPELL_ERROR;
    }
}

std::string SpellHandler::shieldman_spell_to_str(Shieldman::Spell spell)
{
    switch (spell) {
        case Shieldman::Spell::TURTLE:
            return "Turtle";
        default:
            throw Piece::SPELL_ERROR;
    }
}

std::string SpellHandler::spearman_spell_to_str(Spearman::Spell spell)
{
    switch (spell) {
        case Spearman::Spell::SPEARTHROW:
            return "Spearthrow";
        default:
            throw Piece::SPELL_ERROR;
    }
}

std::string SpellHandler::spell_var_to_str(SpellVar spell_var)
{
    unsigned spell_index = spell_var.index();
    std::string res;

    switch (spell_index) {
        case 0: {
            auto archer_spell = std::get<Archer::Spell>(spell_var);
            res = archer_spell_to_str(archer_spell);
            break;
        }
        case 1: {
            auto catapult_spell = std::get<Catapult::Spell>(spell_var);
            res = catapult_spell_to_str(catapult_spell);
            break;
        }
        case 2: {
            auto horseman_spell = std::get<Horseman::Spell>(spell_var);
            res = horseman_spell_to_str(horseman_spell);
            break;
        }
        case 3: {
            auto knight_spell = std::get<Knight::Spell>(spell_var);
            res = knight_spell_to_str(knight_spell);
            break;
        }
        case 4: {
            auto mage_spell = std::get<Mage::Spell>(spell_var);
            res = mage_spell_to_str(mage_spell);
            break;
        }
        case 5: {
            auto priest_spell = std::get<Priest::Spell>(spell_var);
            res = priest_spell_to_str(priest_spell);
            break;
        }
        case 6: {
            auto shieldman_spell = std::get<Shieldman::Spell>(spell_var);
            res = shieldman_spell_to_str(shieldman_spell);
            break;
        }
        case 7: {
            auto spearman_spell = std::get<Spearman::Spell>(spell_var);
            res = spearman_spell_to_str(spearman_spell);
            break;
        }
        default:
            throw "Unknown spell index";
    }

    return res;
}

std::string SpellHandler::spell_effect_to_str(Piece::SpellEffect spell_effect)
{
    switch (spell_effect) {
        case Piece::SpellEffect::FIREBOLT_DOT:
            return "FIREBOLT_DOT";
        case Piece::SpellEffect::EMPOWER_BONUS:
            return "EMPOWER_BONUS";
        case Piece::SpellEffect::HOLY_SHIELD_IMMUNITY:
            return "HOLLY_SHIELD_IMMUNITY";
        case Piece::SpellEffect::TURTLE_BONUS:
            return "TURTLE_BONUS";
        case Piece::SpellEffect::DEADLY_POISON:
            return "DEADLY_POISON";
        default:
            throw "Unknown spell effect";
    }
}

// Constructors
SpellHandler::SpellHandler(Board* board, Archer::Spell archer_spell)
    : m_board(board), m_spell(archer_spell), m_spell_str(archer_spell_to_str(archer_spell))
{}

SpellHandler::SpellHandler(Board* board, Catapult::Spell catapult_spell)
    : m_board(board), m_spell(catapult_spell), m_spell_str(catapult_spell_to_str(catapult_spell))
{}

SpellHandler::SpellHandler(Board* board, Horseman::Spell horseman_spell)
    : m_board(board), m_spell(horseman_spell), m_spell_str(horseman_spell_to_str(horseman_spell))
{}

SpellHandler::SpellHandler(Board* board, Knight::Spell knight_spell)
    : m_board(board), m_spell(knight_spell), m_spell_str(knight_spell_to_str(knight_spell))
{}

SpellHandler::SpellHandler(Board* board, Mage::Spell mage_spell)
    : m_board(board), m_spell(mage_spell), m_spell_str(mage_spell_to_str(mage_spell))
{}

SpellHandler::SpellHandler(Board* board, Priest::Spell priest_spell)
    : m_board(board), m_spell(priest_spell), m_spell_str(priest_spell_to_str(priest_spell))
{}

SpellHandler::SpellHandler(Board* board, Shieldman::Spell shieldman_spell)
    : m_board(board), m_spell(shieldman_spell), m_spell_str(shieldman_spell_to_str(shieldman_spell))
{}

SpellHandler::SpellHandler(Board* board, Spearman::Spell spearman_spell)
    : m_board(board), m_spell(spearman_spell), m_spell_str(spearman_spell_to_str(spearman_spell))
{}

Field *SpellHandler::new_field_after_move() const
{
    return m_new_field_after_move;
}

bool SpellHandler::has_new_field_after_move() const
{
    return m_new_field_after_move != nullptr;
}

std::vector<Piece*>& SpellHandler::killed_pieces() 
{
    return m_killed_pieces;
}

bool SpellHandler::has_killed_pieces() const
{
    return !m_killed_pieces.empty();
}

SpellVar SpellHandler::spell() const
{
    return m_spell;
}

std::string SpellHandler::spell_str() const
{
    return m_spell_str;
}

unsigned SpellHandler::spell_cost() const
{
    return m_spell_cost;
}

unsigned SpellHandler::spell_cooldown() const
{
    return m_spell_cooldown;
}

std::vector<Updater*> SpellHandler::get_updaters() const
{
    std::vector<Updater*> updaters;

    for (const auto& parameters : m_updater_parameters) {
        auto [piece, update_type, start_after] = parameters;
        updaters.push_back(new Updater(piece, update_type, start_after));
    }

    return updaters;
}

// Handling different spells seperately by infered classes
void SteadyAim::handle(Piece* caster, Field* selected_field) {
    auto target = selected_field->get_piece();
    double steady_aim_dmg_bonus = static_cast<double>(Archer::SpellData::STEADY_AIM_DMG_BONUS);
    double steady_aim_dmg = caster->damage() + steady_aim_dmg_bonus / 100 * caster->damage();
    unsigned self_stun_duration = static_cast<unsigned>(Archer::SpellData::STEADY_AIM_SELF_STUN_DURATION);

    target->set_health(target->health() - steady_aim_dmg);
    if (target->is_dead()) {
        m_killed_pieces.push_back(target);
    }

    caster->set_stunned(true, self_stun_duration);
    if (!caster->has_new_stun()) {
        // Create updater only if caster was not previously stunned or was stunned for shorter duration
        m_updater_parameters.emplace_back(caster, Piece::DataIndicator::STUN_STATUS, 4);
    }

    m_updater_parameters.emplace_back(caster, m_spell, 4);
}

bool SteadyAim::is_instant_cast() const
{
    return false;
}

bool SteadyAim::stuns_caster() const
{
    return true;
}

bool SteadyAim::immobilizes_caster() const
{
    return false;
}

std::list<Field*> SteadyAim::get_potential_fields(Piece* caster) const
{
    unsigned ortho_range = static_cast<unsigned>(Archer::SpellRange::STEADY_AIM_ORTHO);
    unsigned diag_range = static_cast<unsigned>(Archer::SpellRange::STEADY_AIM_DIAG);
    bool use_g_fields = static_cast<bool>(Archer::SpellRange::STEADY_AIM_USE_G_FIELDS);

    auto fields = m_board->fields_in_range(caster->board_index(), ortho_range, diag_range, use_g_fields, false);

//    m_board->remove_fields_by(fields, [caster](Field* field) {
//        // Keep fields with enemy pieces on them
//        if (field->has_piece() && field->get_piece()->owner() != c->owner() &&
//            !field->get_piece()->is_immune()) {
//            return false;
//        } else {
//            return true;
//        }
//    });

    fields.erase(
        std::remove_if(
            std::begin(fields),
            std::end(fields),
            [caster](Field* field) {
                // Keep fields with enemy pieces on them
                if (field->has_piece() && field->get_piece()->owner() != caster->owner() &&
                    !field->get_piece()->is_immune()) {
                    return false;
                } else {
                    return true;
                }
            }
        ),
        std::end(fields)
    );

    return fields;
}

void Barrage::handle(Piece* caster, Field* selected_field) {
    auto target = selected_field->get_piece();
    // Deal damage to targeted piece first
    target->set_health(target->health() - caster->damage());
    if (target->is_dead()) {
        m_killed_pieces.push_back(target);
    }

    // Select 2 random fields around the target and deal damage to pieces standing on them, if any
    auto nearby_fields = m_board->nearby_fields(m_board->at_c(target->board_index()));
    auto rnd_field_1 = random_element(nearby_fields);
    auto rnd_field_2 = random_element(nearby_fields);
    while (rnd_field_1 == rnd_field_2) {
        rnd_field_2 = random_element(nearby_fields);
    }

    // Check if there are targets at random fields and deal damage if yes
    if (rnd_field_1->has_piece()) {
        auto piece = rnd_field_1->get_piece();
        if (!piece->is_immune()) {
            piece->set_health(piece->health() - caster->damage());
            if (piece->is_dead()) {
                m_killed_pieces.push_back(piece);
            }
        }
    }

    if (rnd_field_2->has_piece()) {
        auto piece = rnd_field_2->get_piece();
        if (!piece->is_immune()) {
            piece->set_health(piece->health() - caster->damage());
            if (piece->is_dead()) {
                m_killed_pieces.push_back(piece);
            }
        }
    }

    // Catapult is stunned after throwing barrage
    int stun_duration = static_cast<int>(Catapult::SpellData::BARRAGE_SELF_STUN_DURATION);
    caster->set_stunned(true, stun_duration);

    // If barrage eventually killed its own caster, don't add any updaters afterwards
    if (!caster->is_dead()) {
        if (!caster->has_new_stun()) {
            // Create updater only if caster was not previously stunned or was stunned for shorter duration
            m_updater_parameters.emplace_back(caster, Piece::DataIndicator::STUN_STATUS, 4);
        }

        m_updater_parameters.emplace_back(caster, m_spell, 4);
    }
}

bool Barrage::is_instant_cast() const
{
    return false;
}

bool Barrage::stuns_caster() const
{
    return true;
}

bool Barrage::immobilizes_caster() const
{
    return false;
}

std::list<Field*> Barrage::get_potential_fields(Piece* caster) const
{
    unsigned ortho_range = static_cast<unsigned>(Catapult::SpellRange::BARRAGE_ORTHO);
    unsigned diag_range = static_cast<unsigned>(Catapult::SpellRange::BARRAGE_DIAG);
    bool use_g_fields = static_cast<bool>(Catapult::SpellRange::BARRAGE_USE_G_FIELDS);

    auto fields = m_board->fields_in_range(caster->board_index(), ortho_range, diag_range, use_g_fields, false);
    
    fields.erase(
        std::remove_if(
            std::begin(fields),
            std::end(fields),
            [caster](Field* field) {
                // Keep fields with ANY pieces on them, since barrage has friendly-fire
                if (field->has_piece() && !field->get_piece()->is_immune()) {
                    return false;
                } else {
                    return true;
                }
            }
        ),
        std::end(fields)
    );

    return fields;
}

void Gallop::handle(Piece* caster, Field* selected_field) {
    m_board->move(caster, selected_field, false);
    m_new_field_after_move = selected_field;

    // Horseman is immovable after galloping
    int immovable_duration = static_cast<int>(Horseman::SpellData::GALLOP_SELF_IMMOVABLE_DURATION);
    caster->set_immovable(true, immovable_duration);
    if (!caster->has_new_immovable()) {
        // Create updater only if caster was not previously immovable or was immovable for shorter duration
        m_updater_parameters.emplace_back(caster, Piece::DataIndicator::MOBILITY, 4);
    }

    m_updater_parameters.emplace_back(caster, m_spell, 4);
}

bool Gallop::is_instant_cast() const
{
    return false;
}

bool Gallop::stuns_caster() const
{
    return false;
}

bool Gallop::immobilizes_caster() const
{
    return true;
}

std::list<Field*> Gallop::get_potential_fields(Piece* caster) const
{
    // Potential fields are any standable fields in Gallop range
    unsigned ortho_range = static_cast<unsigned>(Horseman::SpellRange::GALLOP_ORTHO);
    unsigned diag_range = static_cast<unsigned>(Horseman::SpellRange::GALLOP_DIAG);
//    bool use_g_fields = static_cast<bool>(Horseman::SpellRange::GALLOP_USE_G_FIELDS);

    std::list<Field*> fields;
    auto curr_field = m_board->at(caster->board_index());
    int curr_row = curr_field->get_row_index();
    int curr_col = curr_field->get_column_index();
    bool blocked_left = false;
    bool blocked_right = false;
    bool blocked_top = false;
    bool blocked_bottom = false;
    bool blocked_upper_left = false;
    bool blocked_lower_left = false;
    bool blocked_upper_right = false;
    bool blocked_lower_right = false;

    // Handle ortho movements
    for (int i = 1; i <= ortho_range; i++) {
        int left_col = curr_col - i;
        int right_col = curr_col + i;
        int upper_row = curr_row - i;
        int lower_row = curr_row + i;

        // If there is mountain on the left path, block movement left-wards
        if (left_col < 0 || m_board->at(curr_row, left_col)->field_type() == Field::Type::MOUNTAIN) {
            blocked_left = true;
        }

        if (!blocked_left && m_board->at(curr_row, left_col)->is_available()) {
            fields.push_back(m_board->at(curr_row, left_col));
        }

        // If there is mountain on the right path, block movement right-wards
        if (right_col >= m_board->cols() ||
            m_board->at(curr_row, right_col)->field_type() == Field::Type::MOUNTAIN) {
            blocked_right = true;       
        }

        if (!blocked_right && m_board->at(curr_row, right_col)->is_available()) {
            fields.push_back(m_board->at(curr_row, right_col));
        }

        // If there is mountain on the upper path, block movement upwards
        if (upper_row < 0 || m_board->at(upper_row, curr_col)->field_type() == Field::Type::MOUNTAIN) {
            blocked_top = true;
        }

        if (!blocked_top && m_board->at(upper_row, curr_col)->is_available()) {
            fields.push_back(m_board->at(upper_row, curr_col));
        }

        // If there is mountain on the lower path, block movement bottom-wards
        if (lower_row >= m_board->rows() || 
            m_board->at(lower_row, curr_col)->field_type() == Field::Type::MOUNTAIN) {
            blocked_bottom = true;
        }

        if (!blocked_bottom && m_board->at(lower_row, curr_col)->is_available()) {
            fields.push_back(m_board->at(lower_row, curr_col));
        }
    }

    // Gallop doesn't allow G-field movement
    for (int i = 1; i <= diag_range; i++) {
        int upper_left_row = curr_row - i;
        int upper_left_col = curr_col - i;
        int lower_left_row = curr_row + i;
        int lower_left_col = curr_col - i;
        int upper_right_row = curr_row - i;
        int upper_right_col = curr_col + i;
        int lower_right_row = curr_row + i;
        int lower_right_col = curr_col + i;

        // If there is mountain on the upper-left path, block movement in that direction
        if (!m_board->check_indices(upper_left_row, upper_left_col) ||
            m_board->at(upper_left_row, upper_left_col)->field_type() == Field::Type::MOUNTAIN) {
            blocked_upper_left = true;
        }

        if (!blocked_upper_left && m_board->at(upper_left_row, upper_left_col)->is_available()) {
            fields.push_back(m_board->at(upper_left_row, upper_left_col));
        }

        // If there is mountain on the lower-left path, block movement in that direction
        if (!m_board->check_indices(lower_left_row, lower_left_col) ||
            m_board->at(lower_left_row, lower_left_col)->field_type() == Field::Type::MOUNTAIN) {
            blocked_lower_left = true;
        }

        if (!blocked_lower_left && m_board->at(lower_left_row, lower_left_col)->is_available()) {
            fields.push_back(m_board->at(lower_left_row, lower_left_col));
        }

        // If there is mountain on the upper-right path, block movement in that direction
        if (!m_board->check_indices(upper_right_row, upper_right_col) ||
            m_board->at(upper_right_row, upper_right_col)->field_type() == Field::Type::MOUNTAIN) {
            blocked_upper_right = true;
        }

        if (!blocked_upper_right && m_board->at(upper_right_row, upper_right_col)->is_available()) {
            fields.push_back(m_board->at(upper_right_row, upper_right_col));
        }

        // If there is mountain on the lower_right path, block movement in that direction
        if (!m_board->check_indices(lower_right_row, lower_right_col) ||
            m_board->at(lower_right_row, lower_right_col)->field_type() == Field::Type::MOUNTAIN) {
            blocked_lower_right = true;
        }

        if (!blocked_lower_right && m_board->at(lower_right_row, lower_right_col)->is_available()) {
            fields.push_back(m_board->at(lower_right_row, lower_right_col));
        }
    }

    return fields;
}

void MortalStrike::handle(Piece* caster, Field* selected_field)  {
    auto target = selected_field->get_piece();
    target->set_health(0.0);
    m_killed_pieces.push_back(target);

    double self_dmg = static_cast<double>(Knight::SpellData::MORTAL_STRIKE_SELF_DAMAGE);
    caster->set_health(caster->health() - self_dmg);
    if (caster->is_dead()) {
        m_killed_pieces.push_back(caster);
    }

    // Don't save updaters info if mortal strike killed its own caster
    if (!caster->is_dead()) {
        m_updater_parameters.emplace_back(caster, m_spell, 4);
    }
}

bool MortalStrike::is_instant_cast() const 
{
    return false;
}

bool MortalStrike::stuns_caster() const
{
    return false;
}

bool MortalStrike::immobilizes_caster() const
{
    return false;
}

std::list<Field*> MortalStrike::get_potential_fields(Piece* caster) const
{
    unsigned ortho_range = static_cast<unsigned>(Knight::SpellRange::MORTAL_STRIKE_ORTHO);
    unsigned diag_range = static_cast<unsigned>(Knight::SpellRange::MORTAL_STRIKE_DIAG);
    double kill_treshold = static_cast<double>(Knight::SpellData::MORTAL_STRIKE_KILL_TRESHOLD);
    bool use_g_fields = static_cast<bool>(Knight::SpellRange::MORTAL_STRIKE_USE_G_FIELDS);

    auto fields = m_board->fields_in_range(caster->board_index(), ortho_range, diag_range, use_g_fields, false);
    
    fields.erase(
        std::remove_if(
            std::begin(fields),
            std::end(fields),
            [caster, kill_treshold](Field* field) {
                // Keep fields with enemy pieces on them with HP <= kill_treshold
                // Mortal strike pierces immunity effect !
                if (field->has_piece() && field->get_piece()->owner() != caster->owner() &&
                    field->get_piece()->health() <= kill_treshold) {
                    return false;
                } else {
                    return true;
                }
            }
        ),
        std::end(fields)
    );

    return fields;
}

void Firebolt::handle(Piece* caster, Field* selected_field)  {
    auto target = selected_field->get_piece();
    double firebolt_damage = static_cast<double>(Mage::SpellData::FIREBOLT_DMG);
    target->set_health(target->health() - firebolt_damage);

    std::string fb_dot_str = SpellHandler::spell_effect_to_str(Piece::SpellEffect::FIREBOLT_DOT);
    int fb_dot_duration = static_cast<int>(Mage::SpellData::FIREBOLT_DURATION);
    // Apply firebolt effect only if target doesn't already have firebolt dot (firebolt doesn't stack)
    if (!target->has_spell_effect(fb_dot_str)) {
        target->add_spell_effect(fb_dot_str, fb_dot_duration);
    }

    if (target->is_dead()) {
        m_killed_pieces.push_back(target);
    } else {
        m_updater_parameters.emplace_back(target, Piece::SpellEffect::FIREBOLT_DOT, 2);
    }

    m_updater_parameters.emplace_back(caster, m_spell, 4);
}

bool Firebolt::is_instant_cast() const
{
    return false;
}

bool Firebolt::stuns_caster() const
{
    return false;
}

bool Firebolt::immobilizes_caster() const
{
    return false;
}

std::list<Field*> Firebolt::get_potential_fields(Piece* caster) const
{
    unsigned ortho_range = static_cast<unsigned>(Mage::SpellRange::FIREBOLT_ORTHO);
    unsigned diag_range = static_cast<unsigned>(Mage::SpellRange::FIREBOLT_DIAG);
    bool use_g_fields = static_cast<bool>(Mage::SpellRange::FIREBOLT_USE_G_FIELDS);

    auto fields = m_board->fields_in_range(caster->board_index(), ortho_range, diag_range, use_g_fields, false);
    
    fields.erase(
        std::remove_if(
            std::begin(fields),
            std::end(fields),
            [caster](Field* field) {
                // Keep fields with enemy non-castle non-immune pieces on them
                if (field->has_piece() && field->get_piece()->owner() != caster->owner() &&
                    !field->get_piece()->is_immune() && field->get_piece()->piece_type() != Piece::Type::CASTLE) {
                    return false;
                } else {
                    return true;
                }
            }
        ),
        std::end(fields)
    );

    return fields;
}

void Frostbolt::handle(Piece* caster, Field* selected_field)  {
    auto target = selected_field->get_piece();
    double frostbolt_damage = static_cast<double>(Mage::SpellData::FROSTBOLT_DMG);
    int frostbolt_freeze_duration = static_cast<int>(Mage::SpellData::FROSTBOLT_FREEZE_DURATION);

    target->set_health(target->health() - frostbolt_damage);
    target->set_immovable(true, frostbolt_freeze_duration);

    if (target->is_dead()) {
        m_killed_pieces.push_back(target);
    } else if (!target->has_new_immovable()) {
        // Create updater only if target was not previously immovable or was immovable for shorter duration
        m_updater_parameters.emplace_back(target, Piece::DataIndicator::MOBILITY, 4);
    }

    m_updater_parameters.emplace_back(caster, m_spell, 4);
}

bool Frostbolt::is_instant_cast() const
{
    return false;
}

bool Frostbolt::stuns_caster() const
{
    return false;
}
bool Frostbolt::immobilizes_caster() const
{
    return false;
}

std::list<Field*> Frostbolt::get_potential_fields(Piece* caster) const
{
    unsigned ortho_range = static_cast<unsigned>(Mage::SpellRange::FROSTBOLT_ORTHO);
    unsigned diag_range = static_cast<unsigned>(Mage::SpellRange::FROSTBOLT_DIAG);
    bool use_g_fields = static_cast<bool>(Mage::SpellRange::FROSTBOLT_USE_G_FIELDS);

    auto fields = m_board->fields_in_range(caster->board_index(), ortho_range, diag_range, use_g_fields, false);
    
    fields.erase(
        std::remove_if(
            std::begin(fields),
            std::end(fields),
            [caster](Field* field) {
                // Keep fields with enemy non-castle non-immune pieces on them
                if (field->has_piece() && field->get_piece()->owner() != caster->owner() &&
                    !field->get_piece()->is_immune() && field->get_piece()->piece_type() != Piece::Type::CASTLE) {
                    return false;
                } else {
                    return true;
                }
            }
        ),
        std::end(fields)
    );

    return fields;
}

void Arcanebolt::handle(Piece* caster, Field* selected_field)  {
    auto target = selected_field->get_piece();
    double arcanebolt_damage = static_cast<double>(Mage::SpellData::ARCANEBOLT_DMG);
    target->set_health(target->health() - arcanebolt_damage);
    if (target->is_dead()) {
        // If target died, memorize it and return since there is no need to calculate push field
        m_killed_pieces.push_back(target);
        return;
    }

    // Calculate direction to push target
    int target_row = static_cast<int>(target->board_index().first);
    int target_col = static_cast<int>(target->board_index().second);
    int caster_row = static_cast<int>(caster->board_index().first);
    int caster_col = static_cast<int>(caster->board_index().second);
    int board_rows = static_cast<int>(m_board->rows());
    int board_cols = static_cast<int>(m_board->cols());
    int new_row = 0;
    int new_col = 0;

    if (target_row == caster_row) {
        int push_range = static_cast<int>(Mage::SpellData::ARCANEBOLT_PUSH_ORTHO_RANGE);
        new_row = target_row;  // row stays the same

        // Target is right -> push him right
        if (target_col > caster_col) {
            new_col = std::min(target_col + push_range, board_cols);
        } // Target is left -> push him left
        else {
            new_col = std::max(target_col - push_range, 0);
        }
    } else if (target_col == caster_col) {
        int push_range = static_cast<int>(Mage::SpellData::ARCANEBOLT_PUSH_ORTHO_RANGE);
        new_col = target_col;  // col stays the same

        // Target is up -> push him up
        if (target_row < caster_row) {
            new_row = std::max(target_row - push_range, 0);
        } // Target is down -> push him down
        else {
            new_row = std::min(target_row + push_range, board_rows);
        }
    } else {
        // Here target is in diag position
        int push_range = static_cast<int>(Mage::SpellData::ARCANEBOLT_PUSH_DIAG_RANGE);

        if (target_row < caster_row && target_col < caster_col) {
            // Target is upper-left -> push him upper-left
            new_row = std::max(target_row - push_range, 0);
            new_col = std::max(target_col - push_range, 0);
        } else if (target_row < caster_row && target_col > caster_col) {
            // Target is upper-right -> push him upper-right
            new_row = std::max(target_row - push_range, 0);
            new_col = std::min(target_col + push_range, board_cols);
        } else if (target_row > caster_row && target_col < caster_col) {
            // Target is lower-left -> push him lower-left
            new_row = std::min(target_row + push_range, board_rows);
            new_col = std::max(target_col - push_range, 0);
        } else {
            // Target is lower-right -> push him lower-right
            new_row = std::min(target_row + push_range, board_rows);
            new_col = std::min(target_col + push_range, board_cols);
        }
    }

    // Move target piece to calculated index
    auto new_field = m_board->at(new_row, new_col);

    if (!new_field->is_available()) {
        m_killed_pieces.push_back(target);
    } else {
        m_board->move(target, new_field, false);
        m_new_field_after_move = new_field;
    }

    m_updater_parameters.emplace_back(caster, m_spell, 4);
}

bool Arcanebolt::is_instant_cast() const
{
    return false;
}

bool Arcanebolt::stuns_caster() const 
{
    return false;
}

bool Arcanebolt::immobilizes_caster() const 
{
    return false;
}

std::list<Field*> Arcanebolt::get_potential_fields(Piece* caster) const
{
    unsigned ortho_range = static_cast<unsigned>(Mage::SpellRange::ARCANEBOLT_ORTHO);
    unsigned diag_range = static_cast<unsigned>(Mage::SpellRange::ARCANEBOLT_DIAG);
    bool use_g_fields = static_cast<bool>(Mage::SpellRange::ARCANEBOLT_USE_G_FIELDS);

    // Arcanebolt doesnt use G-fields
    auto fields = m_board->fields_in_range(caster->board_index(), ortho_range, diag_range, use_g_fields, false);
    
    fields.erase(
        std::remove_if(
            std::begin(fields),
            std::end(fields),
            [caster](Field* field) {
                // Keep fields with enemy non-castle non-immune pieces on them
                if (field->has_piece() && field->get_piece()->owner() != caster->owner() &&
                    !field->get_piece()->is_immune() && field->get_piece()->piece_type() != Piece::Type::CASTLE) {
                    return false;
                } else {
                    return true;
                }
            }
        ),
        std::end(fields)
    );

    return fields;
}

// Shockwave can always be used and has no need to check for potential fields
void Shockwave::handle(Piece* caster, Field* selected_field)  {
    Q_UNUSED(selected_field);

    bool use_g_fields = static_cast<bool>(Mage::SpellRange::SHOCKWAVE_USE_G_FIELDS);
    auto shock_fields = m_board->fields_in_range(
        m_board->at(caster->board_index()),
        static_cast<unsigned>(Mage::SpellRange::SHOCKWAVE_ORTHO),
        static_cast<unsigned>(Mage::SpellRange::SHOCKWAVE_DIAG),
        use_g_fields,
        false
    );

    double shockwave_dmg = static_cast<double>(Mage::SpellData::SHOCKWAVE_DMG);

    for (auto field : shock_fields) {
        if (field->has_piece()) {
            auto piece = field->get_piece();
            if (!piece->is_immune()) {
                piece->set_health(piece->health() - shockwave_dmg);
            }

            if (piece->is_dead()) {
                m_killed_pieces.push_back(piece);
            }
        }
    }

    m_updater_parameters.emplace_back(caster, m_spell, 4);
}

bool Shockwave::is_instant_cast() const
{
    return true;
}

bool Shockwave::stuns_caster() const 
{
    return false;
}

bool Shockwave::immobilizes_caster() const 
{
    return false;
}

std::list<Field*> Shockwave::get_potential_fields(Piece* caster) const
{
    // Return its own field
    return std::list<Field*> { m_board->at(caster->board_index()) };
}

void Heal::handle(Piece* caster, Field* selected_field)  {
    auto target = selected_field->get_piece();
    // Check if target is at full health or has more than default health from GC
    double default_hp = static_cast<double>(target->get_default_health());
    double heal_hp = static_cast<double>(Priest::SpellData::HEAL_HP);
    if (target->health() < default_hp) {
        target->set_health(std::min(target->health() + heal_hp, default_hp));
    }

    m_updater_parameters.emplace_back(caster, m_spell, 4);
}

bool Heal::is_instant_cast() const
{
    return false;
}

bool Heal::stuns_caster() const 
{
    return false;
}

bool Heal::immobilizes_caster() const 
{
    return false;
}

std::list<Field*> Heal::get_potential_fields(Piece* caster) const
{
    unsigned ortho_range = static_cast<unsigned>(Priest::SpellRange::HEAL_ORTHO);
    unsigned diag_range = static_cast<unsigned>(Priest::SpellRange::HEAL_DIAG);
    bool use_g_fields = static_cast<bool>(Priest::SpellRange::HEAL_USE_G_FIELDS);

    auto fields = m_board->fields_in_range(caster->board_index(), ortho_range, diag_range, use_g_fields, false);
    
    fields.erase(
        std::remove_if(
            std::begin(fields),
            std::end(fields),
            [caster](Field* field) {
                // Keep fields with friendly non-castle pieces on them
                if (field->has_piece() && field->get_piece()->owner() == caster->owner()
                    && field->get_piece()->health() < field->get_piece()->get_default_health()
                    && field->get_piece()->piece_type() != Piece::Type::CASTLE) {
                    return false;
                } else {
                    return true;
                }
            }
        ),
        std::end(fields)
    );

    if (caster->health() < caster->get_default_health()) {
        fields.push_back(m_board->at(caster->board_index()));
    }

    return fields;
}

void Empower::handle(Piece* caster, Field* selected_field)  {
    auto target = selected_field->get_piece();
    int empower_bonus_percent = static_cast<int>(Priest::SpellData::EMPOWER_BONUS_PERCENT);
    double health_bonus = static_cast<double>(empower_bonus_percent) / 100 * target->health();
    target->set_health(target->health() + health_bonus);
    target->set_last_spell_health_bonus_percent(empower_bonus_percent);
    target->set_last_spell_health_bonus(health_bonus);

    double damage_bonus = static_cast<double>(empower_bonus_percent) / 100 * target->damage();
    target->set_damage(target->damage() + damage_bonus);
    target->set_last_spell_damage_bonus_percent(empower_bonus_percent);
    target->set_last_spell_damage_bonus(damage_bonus);

    std::string empower_str = SpellHandler::spell_effect_to_str(Piece::SpellEffect::EMPOWER_BONUS);
    int empower_duration = static_cast<int>(Priest::SpellData::EMPOWER_DURATION);
    target->add_spell_effect(empower_str, empower_duration);

    m_updater_parameters.emplace_back(target, Piece::SpellEffect::EMPOWER_BONUS, 4);
    m_updater_parameters.emplace_back(caster, m_spell, 4);
}

bool Empower::is_instant_cast() const
{
    return false;
}

bool Empower::stuns_caster() const 
{
    return false;
}
bool Empower::immobilizes_caster() const 
{
    return false;
}

std::list<Field*> Empower::get_potential_fields(Piece* caster) const
{
    unsigned ortho_range = static_cast<unsigned>(Priest::SpellRange::EMPOWER_ORTHO);
    unsigned diag_range = static_cast<unsigned>(Priest::SpellRange::EMPOWER_DIAG);
    bool use_g_fields = static_cast<bool>(Priest::SpellRange::EMPOWER_USE_G_FIELDS);

    auto fields = m_board->fields_in_range(caster->board_index(), ortho_range, diag_range, use_g_fields, false);
    
    fields.erase(
        std::remove_if(
            std::begin(fields),
            std::end(fields),
            [caster](Field* field) {
                // Keep fields with friendly non-castle pieces on them
                if (field->has_piece() && field->get_piece()->owner() == caster->owner()
                    && field->get_piece()->piece_type() != Piece::Type::CASTLE) {
                    return false;
                } else {
                    return true;
                }
            }
        ),
        std::end(fields)
    );

    fields.push_back(m_board->at(caster->board_index()));

    return fields;
}

void HolyShield::handle(Piece* caster, Field* selected_field)  {
    auto target = selected_field->get_piece();
    std::string holy_shield_str = SpellHandler::spell_effect_to_str(Piece::SpellEffect::HOLY_SHIELD_IMMUNITY);
    int holy_shield_duration = static_cast<int>(Priest::SpellData::HOLY_SHIELD_DURATION);
    target->add_spell_effect(holy_shield_str, holy_shield_duration);

    m_updater_parameters.emplace_back(target, Piece::SpellEffect::HOLY_SHIELD_IMMUNITY, 4);
    m_updater_parameters.emplace_back(caster, m_spell, 4);
}

bool HolyShield::is_instant_cast() const
{
    return false;
}

bool HolyShield::stuns_caster() const
{
    return false;
}

bool HolyShield::immobilizes_caster() const
{
    return false;
}

std::list<Field*> HolyShield::get_potential_fields(Piece* caster) const
{
    unsigned ortho_range = static_cast<unsigned>(Priest::SpellRange::HOLY_SHIELD_ORTHO);
    unsigned diag_range = static_cast<unsigned>(Priest::SpellRange::HOLY_SHIELD_DIAG);
    bool use_g_fields = static_cast<bool>(Priest::SpellRange::HOLY_SHIELD_USE_G_FIELDS);

    auto fields = m_board->fields_in_range(caster->board_index(), ortho_range, diag_range, use_g_fields, false);
    
    fields.erase(
        std::remove_if(
            std::begin(fields),
            std::end(fields),
            [caster](Field* field) {
                // Keep fields with friendly non-castle pieces on them
                if (field->has_piece() && field->get_piece()->owner() == caster->owner() &&
                    field->get_piece()->piece_type() != Piece::Type::CASTLE) {
                    return false;
                } else {
                    return true;
                }
            }
        ),
        std::end(fields)
    );

    fields.push_back(m_board->at(caster->board_index()));

    return fields;
}

void Stun::handle(Piece* caster, Field* selected_field)  {
    auto target = selected_field->get_piece();
    int stun_duration = static_cast<int>(Priest::SpellData::STUN_DURATION);
    target->set_stunned(true, stun_duration);
    if (!target->has_new_stun()) {
        // Create updater only if target was not previously stunned or was stunned for shorter duration
        m_updater_parameters.emplace_back(target, Piece::DataIndicator::STUN_STATUS, 4);
    }

    m_updater_parameters.emplace_back(caster, m_spell, 4);
}

bool Stun::is_instant_cast() const
{
    return false;
}

bool Stun::stuns_caster() const 
{
    return false;
}

bool Stun::immobilizes_caster() const 
{
    return false;
}

std::list<Field*> Stun::get_potential_fields(Piece* caster) const
{
    unsigned ortho_range = static_cast<unsigned>(Priest::SpellRange::STUN_ORTHO);
    unsigned diag_range = static_cast<unsigned>(Priest::SpellRange::STUN_DIAG);
    bool use_g_fields = static_cast<bool>(Priest::SpellRange::STUN_USE_G_FIELDS);

    auto fields = m_board->fields_in_range(caster->board_index(), ortho_range, diag_range, use_g_fields, false);
    
    fields.erase(
        std::remove_if(
            std::begin(fields),
            std::end(fields),
            [caster](Field* field) {
                // Keep fields with enemy non-castle pieces on them
                if (field->has_piece() && field->get_piece()->owner() != caster->owner() &&
                    field->get_piece()->piece_type() != Piece::Type::CASTLE) {
                    return false;
                } else {
                    return true;
                }
            }
        ),
        std::end(fields)
    );

    return fields;
}

void Turtle::handle(Piece* caster, Field* selected_field)  {
    Q_UNUSED(selected_field);

    Shieldman* shieldman = dynamic_cast<Shieldman*>(caster);
    double turtle_health_bonus_percent = static_cast<double>(Shieldman::SpellData::TURTLE_BONUS_PERCENT);
    double health_bonus = turtle_health_bonus_percent / 100 * shieldman->health();
    shieldman->set_last_turtle_health_bonus(health_bonus);
    shieldman->set_health(shieldman->health() + health_bonus);

    double turtle_damage_bonus_percent 
        = static_cast<double>(Shieldman::SpellData::TURTLE_DMG_REDUCTION_PERCENT);
    double damage_bonus = turtle_damage_bonus_percent / 100 * shieldman->damage();
    shieldman->set_last_turtle_damage_bonus(damage_bonus);
    shieldman->set_damage(shieldman->damage() - damage_bonus);

    std::string turtle_str = SpellHandler::spell_effect_to_str(Piece::SpellEffect::TURTLE_BONUS);
    int turtle_duration = static_cast<int>(Shieldman::SpellData::TURTLE_DURATION);
    shieldman->add_spell_effect(turtle_str, turtle_duration);

    m_updater_parameters.emplace_back(shieldman, Piece::SpellEffect::TURTLE_BONUS, 4);
    m_updater_parameters.emplace_back(shieldman, m_spell, 4);
}

bool Turtle::is_instant_cast() const
{
    return true;
}

bool Turtle::stuns_caster() const
{
    return false;
}

bool Turtle::immobilizes_caster() const
{
    return false;
}

std::list<Field*> Turtle::get_potential_fields(Piece* caster) const
{
    // Return its own field
    return std::list<Field*> { m_board->at(caster->board_index()) };
}

void Spearthrow::handle(Piece* caster, Field* selected_field)  {
    auto target = selected_field->get_piece();
    double spearthrow_dmg = static_cast<double>(Spearman::SpellData::SPEARTHROW_DMG);
    target->set_health(target->health() - spearthrow_dmg);
    if (target->is_dead()) {
        m_killed_pieces.push_back(target);
    }

    int stun_duration = static_cast<int>(Spearman::SpellData::SPEARTHROW_SELF_STUN_DURATION);
    caster->set_stunned(true, stun_duration);
    if (!caster->has_new_stun()) {
        // Create updater only if caster was not previously stunned or was stunned for shorter duration
        m_updater_parameters.emplace_back(caster, Piece::DataIndicator::STUN_STATUS, 4);
    }

    m_updater_parameters.emplace_back(caster, m_spell, 4);
}

bool Spearthrow::is_instant_cast() const
{
    return false;
}

bool Spearthrow::stuns_caster() const
{
    return true;
}

bool Spearthrow::immobilizes_caster() const
{
    return false;
}

std::list<Field*> Spearthrow::get_potential_fields(Piece* caster) const
{
    unsigned ortho_range = static_cast<unsigned>(Spearman::SpellRange::SPEARTHROW_ORTHO);
    unsigned diag_range = static_cast<unsigned>(Spearman::SpellRange::SPEARTHROW_DIAG);
    bool use_g_fields = static_cast<bool>(Spearman::SpellRange::SPEARTHROW_USE_G_FIELDS);

    auto fields = m_board->fields_in_range(caster->board_index(), ortho_range, diag_range, use_g_fields, false);
    
    fields.erase(
        std::remove_if(
            std::begin(fields),
            std::end(fields),
            [caster](Field* field) {
                // Keep fields with enemy pieces on them
                if (field->has_piece() && field->get_piece()->owner() != caster->owner() &&
                    !field->get_piece()->is_immune()) {
                    return false;
                } else {
                    return true;
                }
            }
        ),
        std::end(fields)
    );

    return fields;
}
