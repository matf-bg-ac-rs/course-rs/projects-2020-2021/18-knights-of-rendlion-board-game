#ifndef ACTIONMANAGER_HPP
#define ACTIONMANAGER_HPP

#include "spellhandler.hpp"
#include "cardhandler.hpp"
#include "../gui/card.hpp"
#include "../utility/utility.hpp"
#include "player.hpp"


// Fabric class for creating action handlers
class ActionManager
{
public:
    static ActionManager *create_manager(Board *board, Player* player_1, Player* player_2);

    SpellHandler* spell_handler(Archer::Spell archer_spell);
    SpellHandler* spell_handler(Catapult::Spell catapult_spell);
    SpellHandler* spell_handler(Horseman::Spell horseman_spell);
    SpellHandler* spell_handler(Knight::Spell knight_spell);
    SpellHandler* spell_handler(Mage::Spell mage_spell);
    SpellHandler* spell_handler(Priest::Spell priest_spell);
    SpellHandler* spell_handler(Shieldman::Spell shieldman_spell);
    SpellHandler* spell_handler(Spearman::Spell spearman_spell);
    SpellHandler* spell_handler(std::string spell_str);
    CardHandler* card_handler(std::string card_name, Player::Index card_owner);


private:
    Board* m_board;
    Player* m_player_1;
    Player* m_player_2;

    ActionManager(Board* board, Player* player_1, Player* player_2);

    // Forbid copy/assignment of singleton
    ActionManager(const ActionManager& other) = delete;
    ActionManager& operator=(const ActionManager& other) = delete;
};

#endif // ACTIONMANAGER_HPP
