#include "updater.hpp"
#include "spellhandler.hpp"


Updater::Updater(Piece *piece, UpdateType update_type, unsigned start_after)
    : m_piece(piece)
    , m_update_type(update_type)
    , m_start_after(start_after)
    , m_cycle(0)
    , m_is_finished(false)
{}

Updater::Updater(Piece* piece, Piece::DataIndicator indicator, unsigned start_after)
    : m_piece(piece)
    , m_update_type(indicator)
    , m_start_after(start_after)
    , m_cycle(0)
    , m_is_finished(false)
{}

Updater::Updater(Piece* piece, SpellVar spell, unsigned start_after)
    : m_piece(piece)
    , m_update_type(spell)
    , m_start_after(start_after)
    , m_cycle(0)
    , m_is_finished(false)
{}

Updater::Updater(Piece* piece, Piece::SpellEffect spell_effect, unsigned start_after)
    : m_piece(piece)
    , m_update_type(spell_effect)
    , m_start_after(start_after)
    , m_cycle(0)
    , m_is_finished(false)
{}

Piece *Updater::get_piece() const
{
    return m_piece;
}

UpdateType Updater::update_type() const
{
    return m_update_type;
}

bool Updater::is_finished() const
{
    return m_is_finished;
}

void Updater::next_cycle()
{
    // Cycle is either 0 or 1. Pieces are updated when cycle is 0
    m_cycle = (m_cycle + 1) & 1;
}

bool Updater::is_updater_of(UpdateType update_type) const
{
    return m_update_type == update_type;
}

void Updater::update()
{
    // If this is a stun/mobility updater, but it's outdated, it has to reset
    if (is_updater_of(Piece::DataIndicator::STUN_STATUS) && m_piece->has_new_stun()) {
        m_piece->set_has_new_stun(false);
        reset(2);
        return;
    }

    if (is_updater_of(Piece::DataIndicator::MOBILITY) && m_piece->has_new_immovable()) {
        m_piece->set_has_new_immovable(false);
        reset(2);
        return;
    }

    // Countdown until piece actually starts being updated
    if (m_start_after > 0) {
        m_start_after--;
    }

    // If counter has reached 0 and cycle is correct, it's time to update piece data
    if (m_start_after == 0 && m_cycle == 0) {
        // At this point, piece is actually updated (cycle determines the correct turn for piece to be updated on)

        // Check what has to be updated
        auto update_index = m_update_type.index();

        // 0 - stored value is Update::Indicator
        if (update_index == 0) {
            Piece::DataIndicator indicator = std::get<0>(m_update_type);

            // Update piece data based on its indicator
            switch (indicator) {
            case Piece::DataIndicator::STATS: {
                m_piece->update_actions();
                m_is_finished = true;
                break;
            }
            case Piece::DataIndicator::MOBILITY: {
                if (m_piece->has_new_immovable()) {
                    // Updater is notified about new immovable duration - reset values
                    m_piece->set_has_new_immovable(false);
                    reset(0);
                } else {
                    m_piece->update_mobility();
                    m_is_finished = !m_piece->is_immovable();
                }
                break;
            }
            case Piece::DataIndicator::STUN_STATUS: {
                if (m_piece->has_new_stun()) {
                    // Updater is notified about new stun duration - reset values
                    m_piece->set_has_new_stun(false);
                    reset(0);
                } else {
                    m_piece->update_stun_status();
                    m_is_finished = !m_piece->is_stunned();
                }
                break;
            }
            default:
                    throw "Unknown Update indicator";
            }
        } // 1 - stored value is SpellVar and is used to update specific spell cooldown
        else if (update_index == 1) {
            SpellVar spell_var = std::get<1>(m_update_type);
            std::string spell_str = SpellHandler::spell_var_to_str(spell_var);
            m_piece->update_cooldown(spell_str);
            m_is_finished = !m_piece->has_cooldown(spell_str);
        } // 2 - stored value is Piece::SpellEffect and is used to update spell effects on piece
        else if (update_index == 2) {
            Piece::SpellEffect spell_effect = std::get<2>(m_update_type);
            std::string spell_effect_str = SpellHandler::spell_effect_to_str(spell_effect);
            m_piece->update_spell_effect(spell_effect_str);
            m_is_finished = !m_piece->has_spell_effect(spell_effect_str);
        } else {
            // Unreachable code ?
            throw "Invalid update_index";
        }
    }

    // Iterate over cycles only if updating has already started
    if (m_start_after == 0) {
        next_cycle();
    }
}

void Updater::reset(unsigned start_after)
{
    m_start_after = start_after;
    m_cycle = 0;
}
