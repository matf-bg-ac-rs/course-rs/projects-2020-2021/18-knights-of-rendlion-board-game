#ifndef PLAYER_HPP
#define PLAYER_HPP

#include <map>
#include <set>

class Piece;

class Player
{

public:
    enum Index {
        NO_PLAYER,
        PLAYER_1,
        PLAYER_2,
    };

    //Method that creates two objects of this class
    static std::pair<Player* , Player* > create_players();

    //Getters
    std::set<Piece*>& living_pieces();
    unsigned get_num_of_living_pieces() const;
    std::pair<unsigned,unsigned> get_live_dead_by_index(unsigned index) const;

    std::set<Piece*> get_castles() const;
    double get_castles_health() const;

    Index get_player_index() const;

    std::map<std::string, int>& get_cards();
    unsigned get_num_of_cards() const;

    // Card methods
    void add_card(std::string card_name);
    void remove_card(std::string card_name);
    bool has_card(const std::string& card_name) const;

    // Methods for adding/removing piece
    void add_piece(Piece* piece);
    void remove_piece(Piece* piece);


private:

    //Constructor
    Player(Player::Index index);

    //we don't want to have cctor and copy assign operator
    Player (const Player&) = delete;
    Player& operator= (const Player&) = delete;

    //Data
    Index m_index;

    std::set<Piece*> m_living_pieces;
    std::set<Piece*> m_castles;
    std::map< unsigned , std::pair<unsigned , unsigned> > m_live_dead_map;

    unsigned m_num_of_cards;
    std::map < std::string , int > m_cards;
};



#endif // PLAYER_HPP
