#include "player.hpp"

#include "../pieces/piece.hpp"

#include <numeric>

const std::map<Piece::Type,unsigned> piece_to_index{
    {Piece::Type::KNIGHT    , 0},
    {Piece::Type::ARCHER    , 1},
    {Piece::Type::CATAPULT  , 2},
    {Piece::Type::HORSEMAN  , 3},
    {Piece::Type::MAGE      , 4},
    {Piece::Type::PRIEST    , 5},
    {Piece::Type::SHIELDMAN , 6},
    {Piece::Type::SPEARMAN  , 7}
};


Player::Player(Player::Index index)
    : m_index(index),
      m_num_of_cards(0)
{
    for(unsigned i = 0; i<8; i++)
        m_live_dead_map[i];

}

void Player::add_card(std::string card_name)
{
    auto it = m_cards.find(card_name);

    if(it != m_cards.end())
        it->second++;
    else
        m_cards[card_name] = 1;

    m_num_of_cards++;
}

void Player::remove_card(std::string card_name)
{
    auto it = m_cards.find(card_name);

    if(it != m_cards.end()){
        it->second--;
        if(it->second <= 0)
            m_cards.erase(it);
    }
    else
        throw "NO SUCH CARD";

    m_num_of_cards--;
}

bool Player::has_card(const std::string& card_name) const
{
    return m_cards.find(card_name) != m_cards.end();
}

double Player::get_castles_health() const{

    return std::accumulate(m_castles.begin(),
                           m_castles.end(),
                           0.0,
                           [](double acc, Piece* castle){return acc + castle->health();}
    );

}

std::set<Piece *> Player::get_castles() const
{
    return m_castles;
}

std::pair<unsigned, unsigned> Player::get_live_dead_by_index(unsigned index) const
{
    return std::make_pair<>
            (m_live_dead_map.at(index).first , m_live_dead_map.at(index).second);
}

unsigned Player::get_num_of_living_pieces() const{
    return m_living_pieces.size();
}

Player::Index Player::get_player_index() const
{
    return m_index;
}

std::map<std::string, int>& Player::get_cards()
{
    return m_cards;
}

unsigned Player::get_num_of_cards() const
{
    return m_num_of_cards;
}

std::set<Piece*>& Player::living_pieces()
{
    return m_living_pieces;
}

void Player::add_piece(Piece *piece)
{
    if(Piece::Type::CASTLE == piece->piece_type()){
        m_castles.insert(piece);
    }
    else{
        m_living_pieces.insert(piece);
        m_live_dead_map[piece_to_index.at(piece->piece_type())].first++;
    }

}

void Player::remove_piece(Piece *piece)
{
   std::set<Piece*>* collection;

   if(Piece::Type::CASTLE == piece->piece_type()){
       collection = &m_castles;
   }
   else{
       collection = &m_living_pieces;
       m_live_dead_map[piece_to_index.at(piece->piece_type())].first--;
       m_live_dead_map[piece_to_index.at(piece->piece_type())].second++;
   }

    auto it = collection->find(piece);
    if(it != collection->end()){
        collection->erase(it);
    }
}

std::pair<Player*,Player*> Player::create_players()
{
    static std::pair<Player*, Player*> players = std::make_pair<>(new Player(Index::PLAYER_1),new Player(Index::PLAYER_2));
    return players;
}
