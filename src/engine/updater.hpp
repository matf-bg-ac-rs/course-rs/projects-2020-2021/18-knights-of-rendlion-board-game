#ifndef UPDATER_HPP
#define UPDATER_HPP

#include "../pieces/piece.hpp"
#include "handletypes.hpp"

class Updater
{
public:

    Updater(Piece* piece, UpdateType update_type, unsigned start_after);
    Updater(Piece* piece, Piece::DataIndicator indicator, unsigned start_after);
    Updater(Piece* piece, SpellVar spell, unsigned start_after);
    Updater(Piece* piece, Piece::SpellEffect spell_effect, unsigned start_after);

    ~Updater() {}
    Updater(const Updater& other) = delete;
    Updater& operator=(const Updater& other) = delete;

    Piece* get_piece() const;
    UpdateType update_type() const;
    bool is_finished() const;
    void next_cycle();
    std::string update_cache_str() const;
    bool is_updater_of(UpdateType update_type) const;
    void update();
    void reset(unsigned start_after=2);

private:
    Piece* m_piece;
    UpdateType m_update_type;
    unsigned m_start_after;
    unsigned m_cycle;
    bool m_is_finished;
    std::string m_update_cache_str;
};

#endif // UPDATER_HPP
