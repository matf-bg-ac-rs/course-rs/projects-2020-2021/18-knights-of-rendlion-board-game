#include "cardhandler.hpp"
#include "spellhandler.hpp"
#include "updater.hpp"


CardHandler::CardHandler(Board* board, Player* player_1, Player* player_2, Player::Index card_owner)
    : m_board(board)
    , m_player_1(player_1)
    , m_player_2(player_2)
    , m_card_owner(card_owner)
{}

Player *CardHandler::get_owner() const
{
    return m_card_owner == m_player_1->get_player_index()
            ? m_player_1
            : m_player_2
            ;
}

Player *CardHandler::get_enemy() const
{
    return m_card_owner == m_player_1->get_player_index()
            ? m_player_2
            : m_player_1
              ;
}

Field *CardHandler::new_field_after_move() const
{
    return m_new_field_after_move;
}

bool CardHandler::has_new_field_after_move() const
{
    return m_new_field_after_move != nullptr;
}

bool CardHandler::has_killed_pieces() const
{
    return !m_killed_pieces.empty();
}

std::string CardHandler::card_name() const
{
    return m_card_name;
}

unsigned CardHandler::card_cost() const
{
    return m_card_cost;
}

Card::Type CardHandler::card_type() const
{
    return m_card_type;
}

Player::Index CardHandler::card_owner() const
{
    return m_card_owner;
}

bool CardHandler::is_instant_cast() const
{
    return m_is_instant_cast;
}

int CardHandler::additional_moves() const
{
    return m_additional_moves;
}

std::vector<Piece *> &CardHandler::killed_pieces()
{
    return m_killed_pieces;
}

std::vector<Updater *> CardHandler::get_updaters() const
{
    std::vector<Updater*> updaters;

    for (const auto& parameters: m_updater_parameters) {
        auto [piece, update_type, start_after] = parameters;
        updaters.push_back(new Updater(piece, update_type, start_after));
    }

    return updaters;
}

void HolyLight::handle(Field* selected_field)
{
    auto piece = selected_field->get_piece();
    double heal_amount = static_cast<double>(Card::HolyLight::HEAL);
    double default_hp = static_cast<double>(piece->get_default_health());
    piece->set_health(std::min(piece->health() + heal_amount, default_hp));
    m_board->at(piece->board_index())->update();
}

std::list<Field*> HolyLight::get_potential_fields() const
{
    std::list<Field*> fields;
    auto friendly_pieces = get_owner()->living_pieces();

    std::transform(
        friendly_pieces.begin(),
        friendly_pieces.end(),
        std::back_inserter(fields),
        [this](Piece* piece) {
            return m_board->at(piece->board_index());
        }
    );

    return fields;
}

void WarCry::handle(Field* selected_field)
{
    auto piece = selected_field->get_piece();
    double health_bonus_percent = static_cast<double>(Card::WarCry::HEALTH_BONUS_PERCENT);
    double health_bonus = health_bonus_percent / 100 * piece->health();
    piece->set_health(piece->health() + health_bonus);

    double damage_bonus_percent = static_cast<double>(Card::WarCry::DAMAGE_BONUS_PERCENT);
    double damage_bonus = damage_bonus_percent / 100 * piece->damage();
    piece->set_damage(piece->damage() + damage_bonus);
    m_board->at(piece->board_index())->update();
}

std::list<Field*> WarCry::get_potential_fields() const
{
    std::list<Field*> fields;
    auto friendly_pieces = get_owner()->living_pieces();

    std::transform(
        friendly_pieces.begin(),
        friendly_pieces.end(),
        std::back_inserter(fields),
        [this](Piece* piece) {
            return m_board->at(piece->board_index());
        }
    );

    return fields;
}

void DeadlyPoison::handle(Field* selected_field)
{
    auto piece = selected_field->get_piece();
    std::string deadly_poison_str = Piece::spell_effect_to_str(Piece::SpellEffect::DEADLY_POISON);
    int duration = static_cast<int>(Card::DeadlyPoison::DURATION);
    piece->add_spell_effect(deadly_poison_str, duration);
    m_updater_parameters.emplace_back(piece, Piece::SpellEffect::DEADLY_POISON, 2);
}

std::list<Field*> DeadlyPoison::get_potential_fields() const
{
    std::list<Field*> fields;
    auto enemy_pieces = get_enemy()->living_pieces();

    std::transform(
        enemy_pieces.begin(),
        enemy_pieces.end(),
        std::back_inserter(fields),
        [this](Piece* piece) {
            return m_board->at(piece->board_index());
        }
    );

    return fields;
}

void CrushingBlow::handle(Field* selected_field)
{
    auto piece = selected_field->get_piece();
    double cb_damage = static_cast<double>(Card::CrushingBlow::DAMAGE);
    piece->set_health(piece->health() - cb_damage);
    if (piece->is_dead()) {
        m_killed_pieces.push_back(piece);
    }
    m_board->at(piece->board_index())->update();
}

std::list<Field*> CrushingBlow::get_potential_fields() const
{
    std::list<Field*> fields;
    auto enemy_pieces = get_enemy()->living_pieces();

    for (auto& piece : enemy_pieces) {
        if (!piece->is_immune()) {
            fields.push_back(m_board->at(piece->board_index()));
        }
    }

    return fields;
}

void Thunderstorm::handle(Field* selected_field)
{
    auto piece = selected_field->get_piece();
    double direct_dmg = static_cast<double>(Card::Thunderstorm::DIRECT_DAMAGE);
    double add_dmg = static_cast<double>(Card::Thunderstorm::ADDITIONAL_DAMAGE);
    unsigned num_of_fields = Card::Thunderstorm::NUM_OF_ADDITIONAL_FIELDS;

    if (!piece->is_immune()) {
        piece->set_health(piece->health() - direct_dmg);
        m_board->at(piece->board_index())->update();
    }

    // Select num_of_additional_fields random nearby fields
    auto fields = m_board->nearby_fields(m_board->at(piece->board_index()));
    for (int i = 0; i < num_of_fields; i++) {
        auto it = random_element_it(fields);
        if (it != fields.end()) {
            auto field = *it;

            if (field->has_piece() && !field->get_piece()->is_immune()) {
                auto piece = field->get_piece();
                piece->set_health(piece->health() - add_dmg);

                if (piece->is_dead()) {
                    m_killed_pieces.push_back(piece);
                }

                m_board->at(piece->board_index())->update();

                // Don't select the same field again
                fields.erase(it);
            }
        }
    }
}

std::list<Field*> Thunderstorm::get_potential_fields() const
{
    std::list<Field*> fields;
    auto enemy_pieces = get_enemy()->living_pieces();

    for (auto& piece : enemy_pieces) {
        if (!piece->is_immune()) {
            fields.push_back(m_board->at(piece->board_index()));
        }
    }

    return fields;
}

void CallToArms::handle(Field* selected_field)
{
    Q_UNUSED(selected_field);
    // Determine staging area and cols range
    unsigned board_rows = static_cast<unsigned>(Board::Size::ROWS);
    unsigned board_cols = static_cast<unsigned>(Board::Size::COLS);
    unsigned staging_area_size = static_cast<unsigned>(Board::Size::STAGING_AREA);
    std::pair<unsigned, unsigned> col_range;

    if (m_card_owner == m_player_1->get_player_index()) {
        col_range = {0, staging_area_size};
    } else {
        col_range = {board_cols - staging_area_size, board_cols};
    }

    // Select potential fields from the staging area by saving available fields
    std::list<Field*> potential_fields;
    for (unsigned i = 0; i < board_rows; i++) {
        for (unsigned j = col_range.first; j < col_range.second; j++) {
            auto field = m_board->at(i, j);
            if (field->is_available()) {
                potential_fields.push_back(field);
            }
        }
    }

    // Select random field from potential fields
    auto it = random_element_it(potential_fields);
    if (it != potential_fields.end()) {
        auto selected_field = *it;

        // Select random piece
        std::vector<Piece::Type> piece_types = {
            Piece::Type::ARCHER,
            Piece::Type::CATAPULT,
            Piece::Type::HORSEMAN,
            Piece::Type::KNIGHT,
            Piece::Type::MAGE,
            Piece::Type::PRIEST,
            Piece::Type::SHIELDMAN,
            Piece::Type::SPEARMAN
        };

        // Random element will always work here because piece_types is never empty
        auto selected_piece = random_element(piece_types);
        Piece* piece = nullptr;

        // Check selected piece type and place it on the seleceted field
        switch (selected_piece) {
        case Piece::Type::ARCHER: {
            piece = new Archer(m_card_owner, selected_field->get_board_index());
            break;
        }
        case Piece::Type::CATAPULT: {
            piece = new Catapult(m_card_owner, selected_field->get_board_index());
            break;
        }
        case Piece::Type::HORSEMAN: {
            piece = new Horseman(m_card_owner, selected_field->get_board_index());
            break;
        }
        case Piece::Type::KNIGHT: {
            piece = new Knight(m_card_owner, selected_field->get_board_index());
            break;
        }
        case Piece::Type::MAGE: {
            piece = new Mage(m_card_owner, selected_field->get_board_index());
            break;
        }
        case Piece::Type::PRIEST: {
            piece = new Priest(m_card_owner, selected_field->get_board_index());
            break;
        }
        case Piece::Type::SHIELDMAN: {
            piece = new Shieldman(m_card_owner, selected_field->get_board_index());
            break;
        }
        case Piece::Type::SPEARMAN: {
            piece = new Spearman(m_card_owner, selected_field->get_board_index());
            break;
        }
        default:
            throw "Unknown piece type";
        }

        selected_field->set_piece(piece);
        selected_field->activate_field_bonuses();
        m_new_field_after_move = selected_field;
        get_owner()->add_piece(piece);
        selected_field->update();
    } else {
        std::cerr << "Cannot summon a piece because there are no free fields left in a staging area!" << std::endl;
        return;
    }

}

std::list<Field*> CallToArms::get_potential_fields() const
{
    // No need for potential fields
    return {};
}

void Teleport::handle(Field *selected_field)
{
    // Select random friendly piece
    auto friendly_pieces = get_owner()->living_pieces();
    auto selected_piece = random_element(friendly_pieces);  // safe call

    // Move friendly piece to a selected field
    m_board->move(selected_piece, selected_field, false);
    m_new_field_after_move = selected_field;
}

std::list<Field*> Teleport::get_potential_fields() const
{
    std::list<Field*> fields;
    for (const auto& row : m_board->fields()) {
        for (const auto& field : row) {
            if (field->is_available()) {
                fields.push_back(field);
            }
        }
    }

    return fields;
}

void AdrenalineRush::handle(Field* selected_field)
{
    Q_UNUSED(selected_field);
    m_additional_moves = Card::AdrenalineRush::ADDITIONAL_MOVES;
}

std::list<Field*> AdrenalineRush::get_potential_fields() const
{
    // No need for potential fields
    return {};
}

void Silence::handle(Field* selected_field)
{
    Q_UNUSED(selected_field);
    m_additional_moves = -Card::AdrenalineRush::ADDITIONAL_MOVES;
}

std::list<Field*> Silence::get_potential_fields() const
{
    // No need for potential fields
    return {};
}

void Affliction::handle(Field* selected_field)
{
    auto piece = selected_field->get_piece();
    double hp_loss_percent = static_cast<double>(Card::Affliction::HEALTH_REDUCTION_PERCENT);
    double hp_loss = hp_loss_percent / 100 * piece->health();
    piece->set_health(piece->health() - hp_loss);

    double dmg_loss_percent = static_cast<double>(Card::Affliction::DAMAGE_REDUCTION_PERCENT);
    double dmg_loss = dmg_loss_percent / 100 * piece->damage();
    piece->set_damage(piece->damage() - dmg_loss);
    m_board->at(piece->board_index())->update();
}

std::list<Field*> Affliction::get_potential_fields() const
{
    std::list<Field*> fields;
    auto enemy_pieces = get_enemy()->living_pieces();

    for (auto& piece : enemy_pieces) {
        if (!piece->is_immune()) {
            fields.push_back(m_board->at(piece->board_index()));
        }
    }

    return fields;
}

void Rebuild::handle(Field* selected_field)
{
    auto piece = selected_field->get_piece();
    double heal_amount = static_cast<double>(Card::Rebuild::HEAL);
    double default_hp = static_cast<double>(piece->get_default_health());
    piece->set_health(std::min(piece->health() + heal_amount, default_hp));
    m_board->at(piece->board_index())->update();
}

std::list<Field*> Rebuild::get_potential_fields() const
{
    auto friendly_castles = get_owner()->get_castles();
    std::list<Field*> fields;

    std::transform(
        friendly_castles.begin(),
        friendly_castles.end(),
        std::back_inserter(fields),
        [this](Piece* piece) {
            return m_board->at(piece->board_index());
        }
    );

    return fields;
}

void DragonsBreath::handle(Field *selected_field)
{
    double damage = static_cast<double>(Card::DragonsBreath::DAMAGE);

    auto target_fields = m_board->fields_in_range(
        selected_field->get_board_index(),
        Card::DragonsBreath::ORTHO_RANGE,
        Card::DragonsBreath::DIAG_RANGE,
        Card::DragonsBreath::USE_G_FIELDS
    );
    target_fields.push_back(selected_field);  // include selected field

    // Check selected fields and fields in range whether they should be burned
    for (auto field : target_fields) {
        if (field->field_type() == Field::Type::FOREST || field->field_type() == Field::Type::PLAIN) {
            field->set_type(Field::Type::GROUND);
            if (field->has_piece()) {
                field->reset_field_bonuses();
            }
            field->update();
        }

        // Damage non-immune pieces in range
        if (field->has_piece() && !field->get_piece()->is_immune() &&
                field->get_piece()->piece_type() != Piece::Type::CASTLE) {
            auto piece = field->get_piece();
            piece->set_health(piece->health() - damage);
            if (piece->is_dead()) {
                m_killed_pieces.push_back(piece);
            }

            m_board->at(piece->board_index())->update();
        }
    }
}

std::list<Field*> DragonsBreath::get_potential_fields() const
{
    std::list<Field*> fields;
    for (const auto& row : m_board->fields()) {
        for (const auto& field : row) {
            if (field->has_piece() && field->get_piece()->piece_type() == Piece::Type::CASTLE) {
                // Ignore castle fields
            } else {
                fields.push_back(field);
            }
        }
    }

    return fields;
}

void DoubleEdge::handle(Field* selected_field)
{
    auto friendly_piece = selected_field->get_piece();
    // Kill selected friendly piece
    friendly_piece->set_health(0.0);

    auto enemy_pieces = get_enemy()->living_pieces();

    // Select random enemy piece
    auto enemy_piece = random_element(enemy_pieces);  // safe call
    enemy_piece->set_health(0.0);

    m_killed_pieces.push_back(friendly_piece);
    m_killed_pieces.push_back(enemy_piece);
}

std::list<Field*> DoubleEdge::get_potential_fields() const
{
    std::list<Field*> fields;
    auto friendly_pieces = get_owner()->living_pieces();

    std::transform(
        friendly_pieces.begin(),
        friendly_pieces.end(),
        std::back_inserter(fields),
        [this](Piece* piece) {
            return m_board->at(piece->board_index());
        }
    );

    return fields;
}

void Meteor::handle(Field* selected_field)
{
    auto piece = selected_field->get_piece();
    double damage = static_cast<double>(Card::Meteor::DAMAGE);
    piece->set_health(piece->health() - damage);
    if (piece->is_dead()) {
        m_killed_pieces.push_back(piece);
    }

    m_board->at(piece->board_index())->update();
}

std::list<Field*> Meteor::get_potential_fields() const
{
    std::list<Field*> fields;
    auto enemy_castles = get_enemy()->get_castles();

    std::transform(
        enemy_castles.begin(),
        enemy_castles.end(),
        std::back_inserter(fields),
        [this](Piece* piece) {
            return m_board->at(piece->board_index());
        }
    );

    return fields;
}

void CunningWizardry::handle(Field* selected_field)
{
    Q_UNUSED(selected_field);

    double heal = static_cast<double>(Card::CunningWizardry::HEAL);
    double damage = static_cast<double>(Card::CunningWizardry::DAMAGE);
    int stun_duration = static_cast<int>(Card::CunningWizardry::STUN_DURATION);

    // Select random friendly piece to heal
    auto friendly_pieces = get_owner()->living_pieces();
    auto heal_piece = random_element(friendly_pieces);  // safe call
    double default_piece_hp = static_cast<double>(heal_piece->get_default_health());
    heal_piece->set_health(std::min(heal_piece->health() + heal, default_piece_hp));
    m_board->at(heal_piece->board_index())->update();

    // Select random enemy piece to deal damage
    auto enemy_pieces = get_enemy()->living_pieces();
    auto damage_piece = random_element(enemy_pieces);  // safe call
    if (!damage_piece->is_immune()) {
        damage_piece->set_health(damage_piece->health() - damage);
        if (damage_piece->is_dead()) {
            m_killed_pieces.push_back(damage_piece);
        }

        m_board->at(damage_piece->board_index())->update();
    }

    // Select random enemy piece to stun
    auto stun_piece = random_element(enemy_pieces);  // safe call
    stun_piece->set_stunned(true, stun_duration);

    if (!stun_piece->has_new_stun()) {
        m_updater_parameters.emplace_back(stun_piece, Piece::DataIndicator::STUN_STATUS, 2);
    }
}

std::list<Field*> CunningWizardry::get_potential_fields() const
{
    // No fields needed
    return {};
}

void Refreshment::handle(Field* selected_field)
{
    auto piece = selected_field->get_piece();
    for (auto& [spell_name, cooldown] : piece->cooldowns()) {
        piece->remove_cooldown(spell_name);
    }
}

std::list<Field*> Refreshment::get_potential_fields() const
{
    std::list<Field*> fields;
    auto friendly_pieces = get_owner()->living_pieces();

    std::transform(
        friendly_pieces.begin(),
        friendly_pieces.end(),
        std::back_inserter(fields),
        [this](Piece* piece) {
            return m_board->at(piece->board_index());
        }
    );

    return fields;
}

void Armageddon::handle(Field* selected_field)
{
    Q_UNUSED(selected_field);

    unsigned num_of_fields = Card::Armageddon::NUM_OF_FIELDS;
    double arma_dmg = static_cast<double>(Card::Armageddon::DAMAGE);

    std::list<Field*> fields;
    // Copy all fields to this list
    for (const auto& row : m_board->fields()) {
        for (const auto& field : row) {
            fields.push_back(field);
        }
    }

    // Select num_of_fields number of random fields
    for (int i = 0; i < num_of_fields; i++) {
        auto it = fields.begin();
        int rand_ind = rand() % fields.size();
        std::advance(it, rand_ind);

        auto field = *it;
        if (field->has_piece() && !field->get_piece()->is_immune()) {
            auto piece = field->get_piece();
            piece->set_health(piece->health() - arma_dmg);
            if (piece->is_dead()) {
                m_killed_pieces.push_back(piece);
            }

            m_board->at(piece->board_index())->update();
        }

        // Don't select the same field again
        fields.erase(it);
    }
}

std::list<Field*> Armageddon::get_potential_fields() const
{
    // No fields needed
    return {};
}

void Thievery::handle(Field* selected_field)
{
    Q_UNUSED(selected_field);

    auto enemy_cards = get_enemy()->get_cards();

    // Select random enemy card and remove it from his deck
    auto it = random_element_it(enemy_cards);
    if (it != enemy_cards.end()) {
        auto selected_card = *it;
        auto card_name = selected_card.first;
        get_enemy()->remove_card(card_name);

        // Add this card to your deck
        get_owner()->add_card(card_name);
    } else {
        std::cerr << "No cards to steal!" << std::endl;
    }
}

std::list<Field*> Thievery::get_potential_fields() const
{
    // No fields needed
    return {};
}

void Combustion::handle(Field* selected_field)
{
    auto piece = selected_field->get_piece();
    piece->set_health(0.0);
    m_killed_pieces.push_back(piece);

    auto burn_fields = m_board->fields_in_range(
        piece->board_index(),
        Card::Combustion::ORTHO_RANGE,
        Card::Combustion::DIAG_RANGE,
        Card::Combustion::USE_G_FIELDS
    );

    double damage = static_cast<double>(Card::Combustion::DAMAGE);
    for (auto field : burn_fields) {
        if (field->has_piece() && !field->get_piece()->is_immune()) {
            auto piece = field->get_piece();
            piece->set_health(piece->health() - damage);
            if (piece->is_dead()) {
                m_killed_pieces.push_back(piece);
            }

            m_board->at(piece->board_index())->update();
        }
    }
}

std::list<Field *> Combustion::get_potential_fields() const
{
    std::list<Field*> fields;
    auto friendly_pieces = get_owner()->living_pieces();

    std::transform(
        friendly_pieces.begin(),
        friendly_pieces.end(),
        std::back_inserter(fields),
        [this](Piece* piece) {
            return m_board->at(piece->board_index());
        }
    );

    return fields;
}

void WrathOfBorgoldin::handle(Field* selected_field)
{
    Q_UNUSED(selected_field);

    std::list<Piece*> enemy_pieces;
    std::copy(
        get_enemy()->living_pieces().begin(),
        get_enemy()->living_pieces().end(),
        std::back_inserter(enemy_pieces)
    );

    std::list<Piece*> friendly_pieces;
    std::copy(
        get_owner()->living_pieces().begin(),
        get_owner()->living_pieces().end(),
        std::back_inserter(friendly_pieces)
    );

    for (int i = 0; i < Card::WrathOfBorgoldin::NUM_OF_PIECES; i++) {
        // Select randomly a player
        double rnd = random_v();
        std::cout << "WoB rnd: " << std::to_string(rnd) << " => ";
        if (rnd < Card::WrathOfBorgoldin::ENEMY_SELECTION_CHANCE) {
            std::cout << "Selecting enemy piece" << std::endl;
            // Select random enemy piece and destroy it
            auto it = random_element_it(enemy_pieces);
            if (it != enemy_pieces.end()) {
                m_killed_pieces.push_back(*it);
                enemy_pieces.erase(it);
            }
        } // Select random friendly piece and destroy it
        else {
            std::cout << "Selecting friendly piece" << std::endl;
            auto it = random_element_it(friendly_pieces);
            if (it != friendly_pieces.end()) {
                m_killed_pieces.push_back(*it);
                friendly_pieces.erase(it);
            }
        }
    }
}

std::list<Field *> WrathOfBorgoldin::get_potential_fields() const
{
    // No need for potential fields
    return {};
}

void BreakLoose::handle(Field* selected_field)
{
    auto piece = selected_field->get_piece();
    if (!piece->has_spell_effect(Piece::spell_effect_to_str(Piece::SpellEffect::DEADLY_POISON))) {
        piece->remove_spell_effect(Piece::spell_effect_to_str(Piece::SpellEffect::DEADLY_POISON));
    }

    if (!piece->has_spell_effect(Piece::spell_effect_to_str(Piece::SpellEffect::FIREBOLT_DOT))) {
        piece->remove_spell_effect(Piece::spell_effect_to_str(Piece::SpellEffect::FIREBOLT_DOT));
    }

    piece->set_stunned(false, 0);
    piece->set_immovable(false, 0);
}

std::list<Field *> BreakLoose::get_potential_fields() const
{
    std::list<Field*> fields;
    auto friendly_pieces = get_owner()->living_pieces();

    std::transform(
        friendly_pieces.begin(),
        friendly_pieces.end(),
        std::back_inserter(fields),
        [this](Piece* piece) {
            return m_board->at(piece->board_index());
        }
    );

    return fields;
}

void ArrowVolley::handle(Field* selected_field)
{
    Q_UNUSED(selected_field);

    double arrow_dmg = static_cast<double>(Card::ArrowVolley::DAMAGE);
    std::list<Piece*> enemy_pieces;
    std::copy(
        get_enemy()->living_pieces().begin(),
        get_enemy()->living_pieces().end(),
        std::back_inserter(enemy_pieces)
    );

    for (int i = 0; i < Card::ArrowVolley::NUM_OF_PIECES; i++) {
        // Select random enemy piece
        auto it = random_element_it(enemy_pieces);
        if (it != enemy_pieces.end()) {
            auto piece = *it;
            if (!piece->is_immune()) {
                piece->set_health(piece->health() - arrow_dmg);
                if (piece->is_dead()) {
                    m_killed_pieces.push_back(piece);
                }

                m_board->at(piece->board_index())->update();
            }

            // Remove selected piece so it's not selected again
            enemy_pieces.erase(it);
        }
    }
}

std::list<Field *> ArrowVolley::get_potential_fields() const
{
    // No need for potential fields
    return {};
}

void OneManArmy::handle(Field *selected_field)
{
    auto piece = selected_field->get_piece();
    double health_increase = static_cast<double>(Card::OneManArmy::HEALTH_INCREASE);
    piece->set_health(piece->health() + health_increase);
    m_board->at(piece->board_index())->update();
}

std::list<Field *> OneManArmy::get_potential_fields() const
{
    std::list<Field*> fields;
    auto friendly_pieces = get_owner()->living_pieces();

    std::transform(
        friendly_pieces.begin(),
        friendly_pieces.end(),
        std::back_inserter(fields),
        [this](Piece* piece) {
            return m_board->at(piece->board_index());
        }
    );

    return fields;
}

void Aristocracy::handle(Field *selected_field)
{
    Q_UNUSED(selected_field);

    double health_increase = static_cast<double>(Card::Aristocracy::HEALTH_INCREASE);
    auto friendly_castles = get_owner()->get_castles();
    for (auto& castle : friendly_castles) {
        castle->set_health(castle->health() + health_increase);
        m_board->at(castle->board_index())->update();
    }
}

std::list<Field *> Aristocracy::get_potential_fields() const
{
    // No need for fields
    return {};
}

void CavalryAssembly::handle(Field *selected_field)
{
    Q_UNUSED(selected_field);

    auto friendly_pieces = get_owner()->living_pieces();
    for (auto piece : friendly_pieces) {
        if (piece->piece_type() == Piece::Type::HORSEMAN) {
            piece->remove_cooldown(SpellHandler::horseman_spell_to_str(Horseman::Spell::GALLOP));
        }
    }
}

std::list<Field *> CavalryAssembly::get_potential_fields() const
{
    // No need for fields
    return {};
}

void Firesword::handle(Field *selected_field)
{
    auto piece = selected_field->get_piece();
    double damage_increase = static_cast<double>(Card::Firesword::DAMAGE_INCREASE);
    piece->set_damage(piece->damage() + damage_increase);
}

std::list<Field *> Firesword::get_potential_fields() const
{
    std::list<Field*> fields;
    auto friendly_pieces = get_owner()->living_pieces();

    std::transform(
        friendly_pieces.begin(),
        friendly_pieces.end(),
        std::back_inserter(fields),
        [this](Piece* piece) {
            return m_board->at(piece->board_index());
        }
    );

    return fields;
}

void BridgeOfDeath::handle(Field *selected_field)
{
    auto piece = selected_field->get_piece();
    piece->set_health(0.0);
    m_killed_pieces.push_back(piece);
}

std::list<Field *> BridgeOfDeath::get_potential_fields() const
{
    std::list<Field*> fields;
    auto enemy_pieces = get_enemy()->living_pieces();
    for (const auto& piece : enemy_pieces) {
        auto field = m_board->at(piece->board_index());
        if (field->field_type() == Field::Type::BRIDGE || field->field_type() == Field::Type::RBRIDGE) {
            fields.push_back(field);
        }
    }
    return fields;
}

void Flood::handle(Field *selected_field)
{
    auto nearby_fields = m_board->nearby_fields(selected_field);
    for (auto& field : nearby_fields) {
        if (field->has_piece()) {
            auto piece = field->get_piece();
            piece->set_health(0.0);
            m_killed_pieces.push_back(piece);
        }
    }
}

std::list<Field *> Flood::get_potential_fields() const
{
    std::list<Field*> fields;
    for (const auto& row : m_board->fields()) {
        for (const auto& field : row) {
            if (field->field_type() == Field::Type::WATER ||
                field->field_type() == Field::Type::RWATER ||
                field->field_type() == Field::Type::CROSS_WATER ||
                field->field_type() == Field::Type::CROSS_WATER1 ||
                field->field_type() == Field::Type::CROSS_WATER2 ||
                field->field_type() == Field::Type::CROSS_WATER3 ||
                field->field_type() == Field::Type::CROSS_WATER4
                    ) {
                fields.push_back(field);
            }
        }
    }

    return fields;
}

void Bracers::handle(Field *selected_field)
{
    auto piece = selected_field->get_piece();
    double health_increase = static_cast<double>(Card::Bracers::HEALTH_INCREASE);
    piece->set_health(piece->health() + health_increase);
    m_board->at(piece->board_index())->update();
}

std::list<Field *> Bracers::get_potential_fields() const
{
    std::list<Field*> fields;
    auto friendly_pieces = get_owner()->living_pieces();

    std::transform(
        friendly_pieces.begin(),
        friendly_pieces.end(),
        std::back_inserter(fields),
        [this](Piece* piece) {
            return m_board->at(piece->board_index());
        }
    );

    return fields;
}

void FatalWounds::handle(Field *selected_field)
{
    auto piece = selected_field->get_piece();
    piece->set_health(1);
    m_board->at(piece->board_index())->update();
}

std::list<Field *> FatalWounds::get_potential_fields() const
{
    double hp_treshold = static_cast<double>(Card::FatalWounds::HEALTH_TRESHOLD);
    std::list<Field*> fields;

    auto enemy_pieces = get_enemy()->living_pieces();
    for (const auto& piece : enemy_pieces) {
        if (piece->health() < hp_treshold) {
            fields.push_back(m_board->at(piece->board_index()));
        }
    }
    return fields;
}
