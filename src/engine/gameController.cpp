#include "gameController.hpp"

#include "../gui/field.hpp"
#include "../pieces/archer.hpp"
#include "../pieces/catapult.hpp"
#include "../pieces/horseman.hpp"
#include "../pieces/knight.hpp"
#include "../pieces/priest.hpp"
#include "../pieces/mage.hpp"
#include "../pieces/shieldman.hpp"
#include "../pieces/spearman.hpp"
#include "../globals.hpp"
#include "../utility/targetdamagebonus.hpp"
#include "../gui/card.hpp"
#include "../utility/utility.hpp"

#include <iostream>
#include <algorithm>

#include <QGraphicsView>
#include <QMouseEvent>
#include <QShortcut>
#include <QGraphicsProxyWidget>
#include <QGraphicsTextItem>
#include <QMessageBox>
#include<QString>
#include <QList>
#include <QTextStream>


//#define __TEST_ATTACK_INF__ 1f
//#define __TEST_MOVE_INF__ 1
//#define __TEST_PLAYER_TURN_INF__ 1


namespace globals{
GameController::Part GAME_PART = GameController::Part::NONE;
GameController::Actions ACTION = GameController::Actions::NONE;
}

GameController::GameController()
    : m_game_part(Part::NONE),
      m_player_turn(Player::Index::NO_PLAYER),
      piece_index(-1),
      m_clicked_move(false),
      m_clicked_attack(false),
      m_clicked_attack_range(false),
      m_clicked_spell(false),
      m_spell_range_index(0),
      m_clicked_card(false),
      m_card_index(-1),
      m_next_steps(3),
      m_card_handler(nullptr)
{


    //global variable initializatio
    globals::GAME_PART = Part::NONE;
    globals::ACTION = Actions::NONE;

    //creating scene
    m_scene = new QGraphicsScene();

    //setting windows Title and putting the View frame to none
    setWindowTitle("Knights of Rendelion");
    setFrameStyle(0);


    //QGraphicsView scroll bars
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    // view related : setting the scene, size and putting the view to the center of the scene
    m_scene->setSceneRect(0,0,1350,750);
    setScene(m_scene);
    setMinimumSize(1350,750);
    centerOn(this->m_scene->sceneRect().center());
    fitInView(m_scene->sceneRect(),Qt::KeepAspectRatio);

    //adding background for the "first window" where you can choose to play or leave
    QPixmap pix(":/images/background.jpg");
    pix_item = m_scene->addPixmap(pix.scaled(width(),height(),Qt::KeepAspectRatio));
    pix_item->setX((m_scene->width()-pix_item->pixmap().width())/2);
    m_scene->setBackgroundBrush(Qt::white);

    // creating shortcuts and linking them to keys
    QShortcut* enterFullScreen = new QShortcut(this);
    QShortcut* exitFullScreen  = new QShortcut(this);

    enterFullScreen->setKey(Qt::Key_F11);
    exitFullScreen->setKey(Qt::Key_Escape);

    connect(enterFullScreen,SIGNAL(activated()),this,SLOT(enterFullScreen()));
    connect(exitFullScreen,SIGNAL(activated()),this,SLOT(exitFullScreen()));

    //view rendering option
    //setRenderHint(QPainter::Antialiasing);
    //(^ this in combination with resizeEvent gives some strange white outter line on fields when is clicked)


    //creating buttons for starting or exiting and linking them with actions
    m_btn_play = new QPushButton();
    m_btn_exit = new QPushButton();
    btn_instructions = new QPushButton();


    //this will be use to set scene as parent for buttons
    QGraphicsProxyWidget* proxy;

    m_btn_play->setText("Play");
    m_btn_play->setGeometry(0,0,150,50);
    proxy = m_scene->addWidget(m_btn_play);
    proxy->setPos(150,150);

    m_btn_exit->setText("Exit");
    m_btn_exit->setGeometry(0,0,150,50);
    proxy = m_scene->addWidget(m_btn_exit);
    proxy->setPos(150,350);

    btn_instructions->setText("Instructions");
    btn_instructions->setGeometry(0,0,150,50);
    proxy = m_scene->addWidget(btn_instructions);
    proxy->setPos(150,250);

    connect(m_btn_play, SIGNAL(clicked()),this,SLOT(piece_selection()));
    connect(m_btn_exit, SIGNAL(clicked()),this,SLOT(close()));
    connect(btn_instructions, SIGNAL(clicked()),this,SLOT(instructions()));
}


GameController *GameController::create_gameController()
{
    static GameController* game_contoller= new GameController();
    return game_contoller;

}

//this recognizes clicks on scene and on its parts
void GameController::mousePressEvent(QMouseEvent *event){

    if(event->button() & Qt::LeftButton) {
        //std::cout << "Custom view clicked." << std::endl;
        QGraphicsView::mousePressEvent(event);
    }
    else if (event->button() & Qt::RightButton){
        QGraphicsView::mousePressEvent(event);
    }
    else{
        event->ignore();
    }
}

//this will initialize every part of the scene and handle
//piece initialization logic
void GameController::piece_selection(){

    std::cout << "Piece selection started" << std::endl;

    m_scene->setBackgroundBrush(Qt::white);

    m_active_piece = nullptr;
    m_game_part = Part::INIT;
    globals::GAME_PART = Part::INIT;

    //clearing the scene from background and buttons
    setWindowTitle("Knights of Rendelion");
    m_scene->removeItem(pix_item);
    m_btn_play->deleteLater();
    btn_instructions->deleteLater();
    m_btn_exit->deleteLater();

    //clearing instructions part (if it was clicked)
    if(instructions_text != nullptr){

        instructions_text->deleteLater();
        btn_init_part->deleteLater();
        btn_play_part->deleteLater();
        btn_cards_part->deleteLater();
    }
    //creating players
    auto players = Player::create_players();
    m_player1 = players.first;
    m_player2 = players.second;

    //creating player info for each player (this will be viisble in other part of the game)
    auto players_info = PlayerInfo::create_playersInfo(m_player1,m_player2);
    m_player1_info = players_info.first;
    m_player2_info = players_info.second;


    //Creating board and adding it to scene
    m_board = Board::create_board();
    m_scene->addItem(m_board);
    m_board->setPos(0,0);
    //this will add fields according to map
    m_board->initialize();
    //this will add castles to the board and link them to their players
    add_castles();

    // Create action manager (singleton)
    m_action_manager = ActionManager::create_manager(m_board, m_player1, m_player2);

    //Creating game info and adding it to scene
    m_game_info = GameInfo::create_gameInfo(m_scene,m_num_of_pieces_selection);
    m_scene->addItem(m_game_info);
    m_game_info->setPos(1050,0);
    m_game_info->add_buttons_init();

    //Player 1 will first pick the piece
    m_player_turn = Player::PLAYER_1;
    m_game_info->set_player_turn(m_player_turn);


    //connecting signals with slots for init_part logic
    connect(m_game_info,SIGNAL(call_update_index(int)),this,SLOT(update_piece_index(int)));
    connect(m_board,SIGNAL(call_handle_mouse_event(uint,uint)),this,SLOT(handle_mouse_event(uint,uint)));
    connect(this, SIGNAL(call_card_selection()),this,SLOT(card_selection()));
    connect(this, SIGNAL(call_reset_buttons(int)),m_game_info,SLOT(reset_buttons(int)));
    connect(m_game_info,SIGNAL(call_init_select()),this,SLOT(init_select()));
}

void GameController::card_selection()
{


    std::cout << "Card selection started" << std::endl;

    unsigned empty_fields = m_board->num_of_standable_fields();
    unsigned n_random_cards = static_cast<unsigned>(m_num_of_cards_for_board_percent * empty_fields);

    std::cout<<"Num of standable_fields : " << empty_fields << std::endl;
    std::cout<<"Placing " << n_random_cards << " on the board" << std::endl;

    set_n_random_cards_to_board(n_random_cards);

    m_player_turn = Player::Index::PLAYER_2;

    m_scene->removeItem(m_board);
    m_scene->removeItem(m_game_info);

    //treba da se oboji nesto zbog raspoznaje

    QPixmap pix(":/images/card_selection_background.jpg");
    pix_item= m_scene->addPixmap(pix.scaled(m_scene->width(),m_scene->height()));
    pix_item->setOpacity(0.8);


   // scene->update();

    btn_pick = new QPushButton();
    btn_pick->setText("P\ni\nc\nk");
    btn_pick->setShortcut(Qt::Key_Space);
    btn_pick->setMinimumWidth(50);
    btn_pick->setMinimumHeight(100);
    btn_pick->setGeometry(0,0,50,100);
    btn_pick->setDisabled(true);
    QGraphicsProxyWidget *proxy = m_scene->addWidget(btn_pick);
    proxy->setPos(1275,325);

    connect(btn_pick,SIGNAL(clicked(bool)),this,SLOT(pick_a_card()));



    card_group.setExclusive(true);

    for(unsigned i=0; i < 2; i++){
        for(unsigned j=0; j < 5; j++){
            QPushButton* btn = new QPushButton();
            btn->setMinimumWidth(215);
            btn->setMinimumHeight(300);
            btn->setGeometry(0,0,215,300);
            btn->setCheckable(true);
            card_group.addButton(btn,i*5+j);
            proxy = m_scene->addWidget(btn);
            proxy->setPos(40+j*245,50 + i*350);
        }
    }

    connect(&card_group, SIGNAL(buttonClicked(int)),this,SLOT(card_group_index(int)));
    connect(this,SIGNAL(call_enable_btn_pick()),this,SLOT(enable_btn_pick()));

    connect(this,SIGNAL(call_play_part()),this,SLOT(play_part()));

    m_cards.clear();
    m_cards = get_10_random_cards();

    Player::Index::PLAYER_1 == m_player_turn ?
        btn_pick->setStyleSheet("background-color : red;") : btn_pick->setStyleSheet("background-color : blue;") ;


}


void GameController::enterFullScreen(){

    if(this->isFullScreen() != true)
        showFullScreen();

}

void GameController::exitFullScreen(){

    if(this->isFullScreen())
        showNormal();

}

void GameController::play_part(){

    m_game_part = Part::PLAY;
    globals::GAME_PART = Part::PLAY;

    std::cout<< "Play part started" << std::endl;

    m_scene->removeItem(pix_item);

    btn_pick->deleteLater();

    //now we add players info to the scene
    m_scene->addItem(m_player1_info);
    m_player1_info->setPos(0,0);
    m_scene->addItem(m_player2_info);
    m_player2_info->setPos(1100,0);

    //adjusting board position and setting new width and position for game info
    m_scene->addItem(m_board);
    m_board->setPos(50,0);
    m_scene->addItem(m_game_info);
    m_active_info = m_game_info;
    m_game_info->set_width(200);
    m_game_info->setPos(1150,0);
    m_game_info->add_buttons_play();

    //creating card info
    m_card_info = CardInfo::create_cardInfo(m_player1,m_player2,m_scene);
    m_card_info->setPos(1050,0);

    //creating swap button and adding it to scene
    //text should be changed for picture
    m_btn_swap = new QPushButton();
    QGraphicsProxyWidget *proxy = m_scene->addWidget(m_btn_swap);
    m_btn_swap->setMinimumWidth(50);
    m_btn_swap->setAttribute(Qt::WA_TranslucentBackground);
    m_btn_swap->setStyleSheet("background-color: gray;");
    m_btn_swap->setIcon(QIcon(QString(":/images/buttons/swap.png")));
    m_btn_swap->setIconSize(QSize(40,40));
    m_btn_swap->setMinimumHeight(50);
    m_btn_swap->setGeometry(0,0,50,50);
    proxy->setPos(1300,700);
    proxy->setZValue(1);

    connect(m_btn_swap,SIGNAL(clicked(bool)),this,SLOT(swap_info()));
    //resets active piece
    reset_active_piece();

    //connecting singals and slots for play_part logic
    connect(this,SIGNAL(call_enable_btn_move_attack(bool,bool)),m_game_info,SLOT(enable_btn_move_attack(bool,bool)));
    connect(this,SIGNAL(call_disable_btn_move_attack()),m_game_info,SLOT(disable_btn_move_attack()));
    connect(m_game_info,SIGNAL(call_set_clicked_move()),this,SLOT(set_clicked_move()));
    connect(m_game_info,SIGNAL(call_set_clicked_attack()),this,SLOT(set_clicked_attack()));

    connect(m_game_info,SIGNAL(call_pressed_attack_range()),this,SLOT(pressed_attack_range()));
    connect(m_game_info,SIGNAL(call_released_attack_range()),this,SLOT(released_attack_range()));
    connect(m_game_info,SIGNAL(call_set_clicked_skip_turn()),this,SLOT(set_clicked_skip_turn()));
    connect(m_game_info,SIGNAL(call_pressed_spell_range()),this,SLOT(pressed_spell_range()));
    connect(m_game_info,SIGNAL(call_released_spell_range()),this,SLOT(released_spell_range()));
    connect(m_game_info,SIGNAL(call_spell_cast(std::string,int)),this,SLOT(clicked_cast_spell(std::string,int)));


    connect(m_board,SIGNAL(call_handle_right_click(uint,uint)),this,SLOT(handle_right_click(uint,uint)));

    connect(this,SIGNAL(call_set_available_spells(std::vector<bool>)),m_game_info,SLOT(set_available_spells(std::vector<bool>)));

    connect(this,SIGNAL(call_game_over(Player::Index)),this,SLOT(game_over(Player::Index)));
    connect(this,SIGNAL(call_disable_range_btns()),m_game_info,SLOT(disable_range_btns()));
    connect(this,SIGNAL(call_disable_spell_btns()),m_game_info,SLOT(disable_spell_btns()));

    //treba se proslediti string
    connect(m_card_info,SIGNAL(call_clicked_activate_card(std::string)),this,SLOT(clicked_activate_card(std::string)));

    //determine who plays first by random (fix)
    change_turn();
}

void GameController::game_over(Player::Index winner)
{
    //this is always on scene in play part
    m_scene->removeItem(m_board);

    QList<QGraphicsItem*> scene_items;
    scene_items = m_scene->items();
    if(scene_items.contains(m_player1_info))
        m_scene->removeItem(m_player1_info);
    if(scene_items.contains(m_player2_info))
        m_scene->removeItem(m_player2_info);
    if(scene_items.contains(m_game_info))
        m_scene->removeItem(m_game_info);
    if(scene_items.contains(m_card_info))
        m_scene->removeItem(m_card_info);

    m_btn_swap->deleteLater();

    QColor b;
    QString message;
    if(Player::Index::PLAYER_1 == winner){
        b = Qt::red;
        message = "Player 1 WON";
    }
    else if (Player::Index::PLAYER_2 == winner){
        b = Qt::blue;
        message = "Player 2 WON";
    }
    else{
        b = Qt::white;
        message = "Nobody won :(";
    }

    QPixmap pix(":/images/background_end_game.jpg");
    pix_item = m_scene->addPixmap(pix.scaled(this->width(),this->height()));

    m_btn_exit = new QPushButton();

    QGraphicsProxyWidget* proxy;
    QGraphicsTextItem* text;

    m_btn_exit->setText("Exit");
    m_btn_exit->setGeometry(0,0,150,50);
    proxy = m_scene->addWidget(m_btn_exit);


    text = m_scene->addText(message,QFont("times",30));
    text->setPos(600,200);
    proxy->setPos(1200,600);
    connect(m_btn_exit,SIGNAL(clicked()),this,SLOT(close()));

    //maybe add play again button ?
}


void GameController::init_deselect(){

    Player::Index::PLAYER_1 == m_player_turn ?
                m_board->unmark_player1_fields() : m_board->unmark_player2_fields();
}

void GameController::recalculate(unsigned row, unsigned column)
{
    if(check_move_attack(m_active_piece)){
        m_board->at(row,column)->set_clicked(true);
    }
    else{
        m_board->at(row,column)->set_clicked(false);
    }
    check_spellable(m_active_piece);
    m_board->update();

}

void GameController::init_select(){

    Player::Index::PLAYER_1 == m_player_turn ?
                m_board->mark_player1_fields() : m_board->mark_player2_fields();
}


void GameController::add_castles(){
    Field* f1;
    Field* f2;
    for(unsigned i=6;i<9;i++){
        f1 = m_board->at(i,0);
        f2 = m_board->at(i,20);
        Castle* c1 = new Castle(Player::PLAYER_1,std::make_pair<unsigned,unsigned>(std::move(i),0));
        Castle* c2 = new Castle(Player::PLAYER_2,std::make_pair<unsigned,unsigned>(std::move(i),20));
        m_player1->add_piece(c1);
        m_player2->add_piece(c2);
        f1->set_piece(c1);
        f2->set_piece(c2);
        m_board->remove_from_standable_list(f1);
        m_board->remove_from_standable_list(f2);
    }

}


void GameController::change_turn()
{
    Player::Index::PLAYER_1 == m_player_turn
            ? m_player_turn = Player::Index::PLAYER_2
            : m_player_turn = Player::Index::PLAYER_1;


    m_game_info->set_player_turn(m_player_turn);

    if (m_game_part == GameController::Part::PLAY) {
        // Check if some cards were collected in previous turn by enemy spell or card effect
        check_fields_with_cards_cache();

        // Update all pieces that have to be updated
        update_pieces();

        //we set remaining steps as next_steps (maybe Silence was thrown)
        m_remaining_steps = m_next_steps;
        m_next_steps = 3;
        m_game_info->set_remaining_steps(m_remaining_steps);


        //we send players cards to card info
        m_card_info->set_player_cards(get_player_on_turn_ptr()->get_cards(),get_enemy()->get_num_of_cards());
        m_card_info->set_player_turn(m_player_turn);
        m_card_info->set_remaining_steps(m_remaining_steps);

        if(nullptr != m_active_piece){
            auto coords = m_active_piece->board_index();
            play_deselect(coords.first,coords.second);
            reset_active_piece();
            emit call_disable_spell_btns();
        }

        if(m_active_info == m_card_info)
            swap_info();

        m_board->update();

    }
}

void GameController::update_pieces()
{

    auto it = m_piece_updaters.begin();
    while (it != m_piece_updaters.end()) {
        auto updater = *it;
        updater->update();

        // Cache dead piece for later removal if it died by firebolt dot
        if ((updater->is_updater_of(Piece::SpellEffect::FIREBOLT_DOT) || 
            updater->is_updater_of(Piece::SpellEffect::DEADLY_POISON)) && updater->get_piece()->is_dead()) {
            m_dead_pieces_cache.insert(updater->get_piece());
        }

        if (updater->is_finished()) {
            if (updater->is_updater_of(Piece::DataIndicator::STATS)) {
                remove_stats_tracker(updater->get_piece());
            }

            delete updater;
            it = m_piece_updaters.erase(it);
        } else {
            it++;
        }
    }

    // Check cache and update updaters
    if (!m_dead_pieces_cache.empty()) {
        auto it = m_dead_pieces_cache.begin();
        while (it != m_dead_pieces_cache.end()) {
            auto dead_piece = *it;
            remove_all_piece_data(dead_piece);
            it = m_dead_pieces_cache.erase(it);
        }
    }
}

void GameController::create_updater(Piece *piece, UpdateType update_type, int start_after)
{
    m_piece_updaters.push_back(new Updater(piece, update_type, start_after));
}

void GameController::add_updater(Updater *updater)
{
    m_piece_updaters.push_back(updater);
}

void GameController::delete_updaters_for_piece(Piece *piece)
{
    auto it = m_piece_updaters.begin();
    while (it != m_piece_updaters.end()) {
        auto updater = *it;
        if (updater->get_piece() == piece) {
            delete updater;
            it = m_piece_updaters.erase(it);
        } else {
            it++;
        }
    }

}

void GameController::add_stats_tracker(Piece *piece)
{
    m_piece_stats_tracked.insert(piece);
}

void GameController::remove_stats_tracker(Piece *piece)
{
    m_piece_stats_tracked.erase(piece);
}

bool GameController::is_stats_tracked(Piece *piece)
{
    return m_piece_stats_tracked.find(piece) != m_piece_stats_tracked.end();
}


void GameController::update_piece_index(int index){
    piece_index = index;
}

//Checking if the field is part of players staging area
bool GameController::check_turn_and_staging_area(unsigned col)
{

    if ((Player::Index::PLAYER_1 == m_player_turn && col > m_board->Size::STAGING_AREA - 1) ||
            (Player::Index::PLAYER_2 == m_player_turn && col < m_board->Size::COLS - m_board->Size::STAGING_AREA)) {
        return false;
    }

    return true;
}

bool GameController::check_if_friendly_piece(unsigned row, unsigned column) const{

    Field* field = m_board->at(row,column);

    if(!field->has_piece())
        return false;

    Piece* piece = field->get_piece();

    if(piece->owner() != m_player_turn)
        return false;

    return true;
}

bool GameController::check_if_enemy_piece(unsigned row, unsigned column) const
{
    Field* field = m_board->at(row,column);

    Piece* piece = field->get_piece();

    if(piece != nullptr && is_enemy(piece->owner())){
        m_game_info->set_enemy_piece(piece);
        m_card_info->set_enemy_piece(piece);
        return true;
    }
    return false;
}

void GameController::create_and_place_piece(int piece_type, unsigned row, unsigned column){

    Piece* p;
    Field* f = (*m_board)(row,column);
    switch(piece_type){
    case 0:
        p = new Knight(m_player_turn,std::make_pair<>(row,column));
        break;
    case 1:
        p = new Archer(m_player_turn,std::make_pair<>(row,column));
        break;
    case 2:
        p = new Catapult(m_player_turn,std::make_pair<>(row,column));
        break;
    case 3:
        p = new Horseman(m_player_turn,std::make_pair<>(row,column));
        break;
    case 4:
        p = new Mage(m_player_turn,std::make_pair<>(row,column));
        break;
    case 5:
        p = new Priest(m_player_turn,std::make_pair<>(row,column));
        break;
    case 6:
        p = new Shieldman(m_player_turn,std::make_pair<>(row,column));
        break;
    case 7:
        p = new Spearman(m_player_turn,std::make_pair<>(row,column));
        break;
    default:
        throw "Unknows PIECE_INDEX";
    }

    f->set_piece(p);
    f->activate_field_bonuses();
    f->set_marked(false);

    m_board->remove_from_standable_list(f);

    get_player_on_turn_ptr()->add_piece(p);

    init_deselect();

    change_turn();

    emit call_reset_buttons(piece_type);
    piece_index = -1;

    //this should be 20
    if(m_num_of_pieces_selection == m_player2->get_num_of_living_pieces())
        emit call_card_selection();
}

void GameController::handle_mouse_event(unsigned row ,unsigned column){

    Field* f = (*m_board)(row,column);

    if(Part::INIT == m_game_part){

        if(check_turn_and_staging_area(column) && f->is_available() && piece_index > -1)
            create_and_place_piece(piece_index,row,column);


    }//end game_part == init
    else if (Part::PLAY == m_game_part){

        if(m_clicked_card){
            activate_card_if_coords_match(row,column);
        }
        else if(!has_active_piece()){//we don't have active piece

            if(check_if_friendly_piece(row,column)){
                change_active_piece(row,column);
            }
            else if (check_if_enemy_piece(row,column)){
            }
            else{
                m_game_info->set_field_info(Field::bonus_Qinfo_for_all_pieces(f->field_type()));
                m_game_info->update();
            }
        }//end of NO active piece
        else if (has_active_piece()){   // we have active piece

            auto coords = m_active_piece->board_index();

            if(m_clicked_move){

                if(coords.first == row && coords.second == column)
                    play_deselect(row,column);
                else if(check_if_friendly_piece(row,column)){
                    change_active_piece(row,column);
                }
                else
                    move_if_coords_match(row,column);

            }//end move clicked
            else if (m_clicked_attack){

                if (coords.first == row && coords.second == column)
                    play_deselect(row,column);
                else if(check_if_friendly_piece(row,column)){
                    change_active_piece(row,column);
                }
                else
                    attack_if_coords_match(row, column);

            }
            else if (m_clicked_spell){

                cast_spell_if_coords_match(row, column);

            }
            else if (check_if_friendly_piece(row,column)){

                if (coords.first == row && coords.second == column)
                    play_deselect(row,column);
                else{
                    change_active_piece(row,column);
                }
            }
            else if  (check_if_enemy_piece(row,column)){
            }


        }//end of we have active piece

    }//end game_part == play
    m_board->update();
}

void GameController::handle_right_click(unsigned row, unsigned column)
{
    Field* f = m_board->at(row,column);

    auto it = std::find(m_possible_attacks.begin(),m_possible_attacks.end(),f);

    if (has_active_piece() && m_clicked_attack && f->has_piece() && it != m_possible_attacks.end()){
        QString enemy = QString(f->get_piece()->type_str(f->get_piece()->piece_type()).c_str());
        QString health = QString("Health : " + QString::number(f->get_piece()->health(),'f',2));
        double damage_bonus = TargetDamageBonus(m_active_piece, f->get_piece());
        double potential_health = f->get_piece()->health() - (m_active_piece->damage() + damage_bonus/100*m_active_piece->damage());

        QString health_after = QString::number(potential_health,'f',2);
        m_game_info->set_enemy_info(QString("Enemy : " + enemy + "\n"
                                          + health + "\n Health after attack : " + health_after));
    }
}

void GameController::set_clicked_move(){
    globals::ACTION = Actions::MOVE;
    m_clicked_move = true;
    emit call_disable_spell_btns();
    play_select();
    m_board->update();

}

void GameController::set_clicked_attack(){
    globals::ACTION = Actions::ATTACK;
    m_clicked_attack = true;
    emit call_disable_spell_btns();
    play_select();
    m_board->update();
}

void GameController::pressed_attack_range()
{
    if(m_active_piece != nullptr){
        globals::ACTION = Actions::RANGE;
        auto fields_in_target_range = m_board->fields_in_target_range(m_active_piece);
        for (auto field : fields_in_target_range)
            field->set_marked(true);
        m_board->update();
    }
}

void GameController::released_attack_range()
{
    if(m_active_piece != nullptr){
        globals::ACTION = Actions::NONE;
        auto fields_in_target_range = m_board->fields_in_target_range(m_active_piece);
        for (auto field : fields_in_target_range)
            field->set_marked(false);
        m_board->update();
    }
}


void GameController::set_clicked_skip_turn()
{
    change_turn();
    m_game_info->update();
}

void GameController::pressed_spell_range()
{
    m_spell_range_index=m_game_info->get_spell_range_index();
    unsigned ortho_range=m_active_piece->get_max_ortho_spells_range(m_spell_range_index);
    unsigned diag_range=m_active_piece->get_max_diag_spells_range(m_spell_range_index);
    bool use_g_fields=m_active_piece->get_use_g_fields(m_spell_range_index);
    Field* field = m_board->at(m_active_piece->board_index().first, m_active_piece->board_index().second);
    auto fields_in_range=m_board->fields_in_range(field,ortho_range,diag_range,use_g_fields);
    for(auto f:fields_in_range){
        f->set_marked(true);
    }
    globals::ACTION=GameController::Actions::RANGE;
    m_board->update();
}

void GameController::released_spell_range()
{
    m_spell_range_index=m_game_info->get_spell_range_index();
    unsigned ortho_range=m_active_piece->get_max_ortho_spells_range(m_spell_range_index);
    unsigned diag_range=m_active_piece->get_max_diag_spells_range(m_spell_range_index);
    Field* field = m_board->at(m_active_piece->board_index().first, m_active_piece->board_index().second);
    auto fields_in_range=m_board->fields_in_range(field,ortho_range,diag_range,true);
    for(auto f:fields_in_range){
        f->set_marked(false);
    }
    globals::ACTION=GameController::Actions::NONE;
    m_board->update();
}

void GameController::clicked_cast_spell(std::string spell, int i)
{
    //if another spell was selected we want to deselect old range fields
    if(m_clicked_spell){
        auto coords = m_active_piece->board_index();
        play_deselect(coords.first,coords.second);
        set_active_piece(m_board->at(coords.first,coords.second)->get_piece());
        m_board->at(coords)->set_clicked(true);
    }
    m_spell_index = i;
    m_spell_string = spell;
    globals::ACTION = GameController::Actions::SPELL;
    set_marked(m_possible_spells[i],true,m_clicked_spell);
    m_board->update();
    emit call_disable_range_btns();
    emit call_disable_btn_move_attack();
}

void GameController::swap_info()
{
    //we want to deselect any marked fields and deselect active piece
    if(nullptr != m_active_piece){
        auto coords = m_active_piece->board_index();
        play_deselect(coords.first,coords.second);
    }

    m_scene->removeItem(m_active_info);

    if(m_active_info == m_game_info){
        m_active_info = m_card_info;

        m_board->setPos(0,0);
        m_scene->removeItem(m_player1_info);
        m_scene->removeItem(m_player2_info);
    }
    else{
        play_deselect(0,0);

        m_active_info = m_game_info;

        m_scene->addItem(m_player1_info);
        m_scene->addItem(m_player2_info);
        m_board->setPos(50,0);
    }
    m_scene->addItem(m_active_info);

}

void GameController::card_group_index(int index)
{
    QAbstractButton* btn;
    if(m_card_index != -1){
        btn = card_group.button(m_card_index);
        btn->setIconSize(QSize(215,300));
    }
    m_card_index = index;
    emit call_enable_btn_pick();
    btn = card_group.button(m_card_index);
    btn->setIconSize(QSize(btn->size().width()*1.1,btn->size().height()*1.1));
}

void GameController::enable_btn_pick()
{
    btn_pick->setEnabled(true);
}

void GameController::pick_a_card()
{
    //Get a card and add it to player
    int index = card_group.checkedId();
    get_player_on_turn_ptr()->add_card(m_cards[index]);

    //remove card/button and reset group
    card_group.button(m_card_index)->setChecked(false);
    delete card_group.button(m_card_index);
    m_card_index = -1;
    card_group.setExclusive(false);
    card_group.setExclusive(true);

    //we don't have any cards so we can go to play part
    if(0 == card_group.buttons().size()){
        emit call_play_part();
    }
    //disable pick button
    btn_pick->setDisabled(true);


    //change turn
    change_turn();

    Player::Index::PLAYER_1 == m_player_turn ?
        btn_pick->setStyleSheet("background-color : red;") : btn_pick->setStyleSheet("background-color : blue;") ;

}

void GameController::clicked_activate_card(std::string card_name)
{
    std::cout << "Activated card : " << card_name << std::endl;

    //if some card was already activated and fields were marked
    //we want to unmark them
    if(m_clicked_card){
        play_deselect(0,0);
        delete m_card_handler;
        m_card_handler = nullptr;
    }

    globals::ACTION = GameController::Actions::CARD;

    auto card = m_action_manager->card_handler(card_name,m_player_turn);

    if(card->is_instant_cast()){

        card->handle();

        if(card_name == Card::AdrenalineRush::NAME || card_name == Card::Silence::NAME){
            auto additonal_moves = card->additional_moves();
            if(additonal_moves > 0)
                change_remaining_steps(additonal_moves);

            else if(additonal_moves < 0 && 3 == m_next_steps)
                m_next_steps += additonal_moves;
        }


        if(card->has_killed_pieces()){
            for(auto piece : card->killed_pieces()){
                remove_all_piece_data(piece);
            }
        }

        auto updaters = card->get_updaters();
        for(auto updater : updaters)
            add_updater(updater);

        if(check_if_game_over())
            return;

        get_player_on_turn_ptr()->remove_card(card_name);
        m_card_info->set_player_cards(get_player_on_turn_ptr()->get_cards(),get_enemy()->get_num_of_cards());
        change_remaining_steps(-card->card_cost());

        delete card;

    }
    else{

        m_clicked_card = true;
        m_card_handler = card;
        m_possible_card_fields = m_card_handler->get_potential_fields();
        for(auto field : m_possible_card_fields){
            field->set_marked(true);
        }

        m_board->update();
    }
}

void GameController::instructions(){
     m_game_part = Part::NONE;
     globals::GAME_PART = Part::NONE;
     globals::ACTION = Actions::NONE;
     m_player_turn = Player::Index::NO_PLAYER;

     //reposition play and exit, and remove instructions button
     m_btn_play->move(1050,650);
     m_btn_exit->move(150,650);
     btn_instructions->hide();

     setWindowTitle("Instructions");
     QPixmap pix(":/images/background_instructions.jpg");
     pix_item->setPixmap(pix.scaled(m_scene->width(),m_scene->height(),Qt::KeepAspectRatio,Qt::SmoothTransformation));
     pix_item->setX((m_scene->width()-pix_item->pixmap().width())/2);

     m_scene->setBackgroundBrush(Qt::black);

     btn_init_part = new QPushButton();
     btn_play_part = new QPushButton();
     btn_cards_part = new QPushButton();

     QGraphicsProxyWidget* proxy;

     instructions_text = new QTextEdit();
     instructions_text->setGeometry(0,0,900,80);
     instructions_text->setReadOnly(true);
     instructions_text->setContextMenuPolicy(Qt::NoContextMenu);
     instructions_text->setStyleSheet("background-image: url(:images/background_instructions.jpg); color:white; font:20px");
     instructions_text->setText("The aim of is Knights Of Rendlion is to defeat the oponents army. Destroy oponents castles or kill his army.");

     proxy = m_scene->addWidget(instructions_text);
     proxy->setPos(225,20);

     btn_init_part->setText("Deploying an army");
     btn_init_part->setGeometry(0,0,150,50);
     proxy = m_scene->addWidget(btn_init_part);
     proxy->setPos(400,350);

     btn_play_part->setText("Playing the game");
     btn_play_part->setGeometry(0,0,150,50);
     proxy = m_scene->addWidget(btn_play_part);
     proxy->setPos(800,350);

     btn_cards_part->setText("Cards");
     btn_cards_part->setGeometry(0,0,150,50);
     proxy = m_scene->addWidget(btn_cards_part);
     proxy->setPos(600,350);


     connect(btn_init_part, SIGNAL(clicked()),this,SLOT(init_instructions()));
     connect(btn_play_part, SIGNAL(clicked()),this,SLOT(play_instructions()));
     connect(btn_cards_part,SIGNAL(clicked()),this, SLOT(cards_instructions()));

}

void GameController::init_instructions()
{
    QFile file(":info/1.txt");
    if(!file.open(QFile::ReadOnly | QFile::Text)){

    }
    QTextStream in(&file);
    QString text = in.readAll();

    instructions_text->setText(text);
    instructions_text->setGeometry(225,100,900,280);

    file.flush();
    file.close();
    instructions_text->show();
}

void GameController::play_instructions()
{
    QFile file(":info/3.txt");
    if(!file.open(QFile::ReadOnly | QFile::Text)){

    }
    QTextStream in(&file);
    QString text = in.readAll();

    instructions_text->setText(text);
    instructions_text->setGeometry(225,100,900,280);

    file.flush();
    file.close();
    instructions_text->show();
}

void GameController::cards_instructions()
{
    QFile file(":info/2.txt");
    if(!file.open(QFile::ReadOnly | QFile::Text)){
    }
    QTextStream in(&file);
    QString text = in.readAll();

    instructions_text->setText(text);
    instructions_text->setGeometry(225,100,900,280);

    file.flush();
    file.close();
    instructions_text->show();
}

void GameController::play_select(){

    m_board->at(m_active_piece->board_index())->set_clicked(true);

    if (m_clicked_move){
        set_marked(m_possible_moves,true,m_clicked_move);
    }
    else if (m_clicked_attack){
        set_marked(m_possible_attacks,true,m_clicked_attack);
    }

    m_board->update();

    emit call_disable_btn_move_attack();
}

void GameController::play_deselect(unsigned row, unsigned column){


    //deselect old field where the piece was (if we moved) or spell
    //moved the piece
    m_board->at(row,column)->set_clicked(false);

    //deselect active piece (if we attacked)
    if(m_active_piece != nullptr)
        m_board->at(m_active_piece->board_index())->set_clicked(false);


    if(m_clicked_move){
        set_marked(m_possible_moves,false,m_clicked_move);
    }
    else if(m_clicked_attack){
        set_marked(m_possible_attacks,false,m_clicked_attack);
    }
    else if(m_clicked_spell){
        set_marked(m_possible_spells[m_spell_index],false,m_clicked_spell);
    }
    else if (m_clicked_card){
        set_marked(m_possible_card_fields,false,m_clicked_card);
    }

    reset_active_piece();

    m_board->update();
    emit call_disable_btn_move_attack();

}

void GameController::move_if_coords_match(unsigned row, unsigned column){

    //check if coords of a click are valid move
    auto it = std::find(m_possible_moves.begin(),m_possible_moves.end(),(*m_board)(row,column));

    //if true do the move
    if(it != m_possible_moves.end()){

        //save old coords to call play_deselect
        auto old_board_index = m_active_piece->board_index();

        //do the move
        Piece* p = m_active_piece;
        m_board->move(m_active_piece,*it);

        if (!is_stats_tracked(p)) {
            create_updater(p, Piece::DataIndicator::STATS, 1);
            add_stats_tracker(p);
        }

        auto current_field = m_board->at(m_active_piece->board_index());


        //deselect
        play_deselect(old_board_index.first,old_board_index.second);

        //check if field has card
        if (current_field->has_card())
        {
            //show pop-up message
            pop_up_card_found(current_field->get_card());

            //add found card to player and update cards in card_info
            get_player_on_turn_ptr()->add_card(current_field->get_card());
            m_card_info->set_player_cards(get_player_on_turn_ptr()->get_cards(),get_enemy()->get_num_of_cards());

            //remove collected card from board
            current_field->set_card("");
        }


        //if player have steps and piece can move/attack we want to keep the focus on it
        auto old_remaining_steps = m_remaining_steps - 1;

        change_remaining_steps(-1);

        if(0 < old_remaining_steps){
            set_active_piece(p);
            recalculate(row,column);
        }

    }

    if(check_if_enemy_piece(row,column));
}

void GameController::delete_piece(Piece *p)
{
    auto field = m_board->at(p->board_index());
    field->remove_piece();
    get_player_ptr_by_index(p->owner())->remove_piece(p);

    if(p->piece_type() != Piece::Type::CASTLE) {
        m_player1_info->update();
        m_player2_info->update();
    }

    delete p;
    p = nullptr;

    //check_if_game_over();
}

void GameController::remove_all_piece_data(Piece *piece)
{
    // Handle all needed obligation for piece removal and data related to it
    delete_piece(piece);
    delete_updaters_for_piece(piece);
    remove_stats_tracker(piece);
}

bool GameController::check_if_game_over()
{

    if(m_player1->living_pieces().empty() && m_player2->living_pieces().empty()){
        emit call_game_over(Player::Index::NO_PLAYER);
        std::cout << "Nereseno je " << std::endl;
        return true;
    }
    else if(m_player1->get_castles_health() <= 0 || m_player1->living_pieces().empty()){
        emit call_game_over(Player::Index::PLAYER_2);
        std::cout<<"player 2 je pobedio"<<std::endl;
        return true;
    }

    else if(m_player2->get_castles_health() <= 0 || m_player2->living_pieces().empty()){
        emit call_game_over(Player::Index::PLAYER_1);
        std::cout<<"player 1 je pobedio"<<std::endl;
        return true;
    }

    return false;
}

void GameController::attack_if_coords_match(unsigned row, unsigned column){
    const auto field = (*m_board)(row,column);

    auto it = std::find(m_possible_attacks.begin(),m_possible_attacks.end(),field);

    if (field->has_piece() && is_enemy(field->get_piece()->owner()) && it != m_possible_attacks.end()){
        auto target = field->get_piece();
        m_active_piece->attack(target);
        Piece* p = m_active_piece;

        // If there is no updater tracking current piece's stats, create one and set piece tracker
        if (!is_stats_tracked(p)) {
            create_updater(p, Piece::DataIndicator::STATS, 1);
            add_stats_tracker(p);
        }

        if (target->is_dead()) {
            remove_all_piece_data(target);
        }

        //ovo treba popraviti da se update-uje samo za playera koji gubi hp
        m_player1_info->update();
        m_player2_info->update();
        play_deselect(row,column);

        if(check_if_game_over())
            return;

        auto old_remaining_steps = m_remaining_steps - 1;
        change_remaining_steps(-1);

        if(0 < old_remaining_steps){
            set_active_piece(p);
            auto coords = m_active_piece->board_index();
            recalculate(coords.first,coords.second);
        }


    }

    if(check_if_enemy_piece(row,column));

}

void GameController::cast_spell_if_coords_match(unsigned row, unsigned column)
{
    Field* f = m_board->at(row,column);
    auto it = std::find(m_possible_spells[m_spell_index].begin(),m_possible_spells[m_spell_index].end(),f);

    if(it != m_possible_spells[m_spell_index].end()){

        //we need old coords of m_active_piece if spell moves the piece
        auto [old_row, old_column] = m_active_piece->board_index();

        auto spell = m_action_manager->spell_handler(m_spell_string);
        if(spell->is_instant_cast()){
            spell->handle(m_active_piece);
        }
        else {
            spell->handle(m_active_piece, f);
        }

        m_active_piece->add_cooldown(spell->spell_str(), spell->spell_cooldown());

        // Manage dead pieces after spell has been cast
        for(auto& piece : spell->killed_pieces()){
            remove_all_piece_data(piece);
        }

        // Check if it's game over after some pieces died
        if(check_if_game_over()) {
            return;
        }

        // Check if piece was moved by a spell and collected a card in the process
        if (spell->has_new_field_after_move()) {
            auto field = spell->new_field_after_move();
            if (field->has_piece() && is_enemy(field->get_piece()->owner())) {
                m_fields_with_cards_cache.insert(field);
            } else {
                check_field_for_card(field);
            }
        }

        // Save updaters for living pieces after spell has been cast
        auto updaters = spell->get_updaters();
        for (const auto& updater : updaters) {
            add_updater(updater);
        }

        // Reduce remaining steps with respect to spell cost
        // this will also check for turn change
        change_remaining_steps(-spell->spell_cost());

        play_deselect(old_row,old_column);

        // delete spell: OSTAVITI ZA VALGRIND
        delete spell;
    }
    else{
            if(check_if_friendly_piece(row,column))
                change_active_piece(row,column);
            else
                play_deselect(row,column);
            if(check_if_enemy_piece(row,column));
        }


}

void GameController::activate_card_if_coords_match(unsigned row, unsigned column)
{

        auto field = m_board->at(row,column);
        auto it = std::find(m_possible_card_fields.begin(),m_possible_card_fields.end(),field);

        if(it != m_possible_card_fields.end()){
            m_card_handler->handle(field);

            if(m_card_handler->has_killed_pieces()){
                for(auto piece : m_card_handler->killed_pieces()){
                    remove_all_piece_data(piece);
                }
            }

            // After some pieces died, check if it's game over
            if (check_if_game_over()) {
                return;
            }

            auto updaters = m_card_handler->get_updaters();
            for(auto updater : updaters)
                add_updater(updater);


            get_player_on_turn_ptr()->remove_card(m_card_handler->card_name());
            m_card_info->set_player_cards(get_player_on_turn_ptr()->get_cards(),get_enemy()->get_num_of_cards());
            change_remaining_steps(-m_card_handler->card_cost());

            play_deselect(row,column);

        }
        else{
            play_deselect(0,0);

        }

        if (m_card_handler->has_new_field_after_move()) {
            auto field = m_card_handler->new_field_after_move();
            if (field->has_piece() && is_enemy(field->get_piece()->owner())) {
                m_fields_with_cards_cache.insert(field);
            } else {
                check_field_for_card(field);
            }
        }

        //we delete card handler in both cases
        delete m_card_handler;
        m_card_handler = nullptr;

}

bool GameController::is_enemy(Player::Index index) const
{
    return m_player_turn != index;
}

Player* GameController::get_enemy()
{
    if (Player::Index::PLAYER_1 == m_player_turn)
        return m_player2;
    else
        return m_player1;
}

Player* GameController::get_player_ptr_by_index(Player::Index index)
{
    if (Player::Index::PLAYER_1 == index)
        return m_player1;
    else
        return m_player2;
}

Player* GameController::get_player_on_turn_ptr()
{
    if (Player::Index::PLAYER_1 == m_player_turn){
        return m_player1;
    }
    else{
        return m_player2;
    }
}

void GameController::reset_active_piece(){
    set_active_piece(nullptr);
}

bool GameController::has_active_piece() const{
    if (m_active_piece != nullptr)
        return true;
    else
        return false;
}

void GameController::set_active_piece(Piece *piece)
{
    m_active_piece = piece;
    globals::ACTION = GameController::Actions::NONE;
    m_game_info->set_current_piece(piece);
    m_card_info->set_current_piece(piece);
}

void GameController::change_active_piece(unsigned row, unsigned column)
{
    Field* f = m_board->at(row,column);

    if(nullptr != m_active_piece){
        m_board->at(m_active_piece->board_index())->set_clicked(false);
        play_deselect(row,column);
    }
    Piece *p=f->get_piece();

    if(Piece::Type::CASTLE == p->piece_type()){
        m_game_info->set_current_piece(p);
    }
    else{
        f->set_clicked(true);
        set_active_piece(p);
        check_move_attack(p);
    }
}

std::vector<std::string> GameController::get_10_random_cards() const
{
    std::vector<std::string> cards;

    for(unsigned i = 0; i < 10; i++){
//        auto card_name = random_element(Card::CARD_NAMES);
        auto card_name = Card::random_card_by_rarity();
        auto path = Card::CARD_NAMES_TO_PATH.at(card_name);
        cards.push_back(card_name);
        QAbstractButton* btn = card_group.button(i);
        btn->setStyleSheet("background-color: black;");
        btn->setIcon(QIcon(QString(path.c_str())));
        btn->setIconSize(btn->size());
    }


    return cards;
}

void GameController::set_n_random_cards_to_board(unsigned n)
{
    auto left = m_board->get_left_standable_list();
    auto middle = m_board->get_center_standable_list();
    auto right = m_board->get_right_standable_list();

    for(unsigned i=0; i < n; i++){
        auto card = Card::random_card_by_rarity();
        double rnd = random_v();

        if (rnd < 0.3) {
            auto it = random_element_it(left);
            auto field = *it;
            left.erase(it);
            field->set_card(card);
        } else if (0.3 <= rnd && rnd < 0.6) {
            auto it = random_element_it(right);
            auto field = *it;
            right.erase(it);
            field->set_card(card);
        } else {
            auto it = random_element_it(middle);
            auto field = *it;
            middle.erase(it);
            field->set_card(card);
        }
        //std::cout <<"Random card no. "<<i<<" : "<< card << std::endl;
    }
}


void GameController::check_turn_change()
{
    if(0 == m_remaining_steps)
        change_turn();
}

bool GameController::check_move_attack(Piece * piece)
{
    if (piece->is_stunned()){
        emit call_enable_btn_move_attack(false,false);
        return false;
    }

    m_possible_moves.erase(m_possible_moves.begin(),m_possible_moves.end());
    m_possible_attacks.erase(m_possible_attacks.begin(),m_possible_attacks.end());


    if(!piece->is_immovable() ){
    auto move_fields = m_board->fields_in_move_range(piece);
    for(auto mf : move_fields)
        if(mf->is_available())
            m_possible_moves.push_back(mf);
    }


    auto target_fields = m_board->fields_in_target_range(piece);
    for(auto tf : target_fields)
        if(tf->has_piece() && is_enemy(tf->get_piece()->owner()) && !tf->get_piece()->has_spell_effect("HOLLY_SHIELD_IMMUNITY"))
            m_possible_attacks.push_back(tf);


    check_spellable(piece);


    emit call_enable_btn_move_attack(!m_possible_moves.empty(), !m_possible_attacks.empty());

    return !m_possible_moves.empty() || !m_possible_attacks.empty();
}

void GameController::check_spellable(Piece *piece)
{
    //vector that will be emitted to game_info
    std::vector<bool> spells_to_enable{};

    //getting names and costs
    auto spell_names = m_active_piece->spell_names();
    m_spell_costs = m_active_piece->spell_costs();

    m_possible_spells.clear();

    for(unsigned i= 0 ;  i< m_spell_costs.size();  i++){
        if(m_remaining_steps >= m_spell_costs[i] && !m_active_piece->has_cooldown(spell_names[i].toStdString())){
                auto spell = m_action_manager->spell_handler(spell_names[i].toStdString());
                bool skip_stunned = spell->stuns_caster() && m_active_piece->is_stunned();
                bool skip_immobilized = spell->immobilizes_caster() && m_active_piece->is_immovable();
                if (!(skip_stunned || skip_immobilized )){
                    auto potential_fields = spell->get_potential_fields(m_active_piece);
                    m_possible_spells.push_back(potential_fields);
                    spells_to_enable.push_back(!m_possible_spells[i].empty());
                    delete spell;
                }
                else{
                    m_possible_spells.push_back(std::list<Field*>());
                    spells_to_enable.push_back(false);
                }
        }
        else{
            m_possible_spells.push_back(std::list<Field*>());
            spells_to_enable.push_back(false);
        }
    }

    emit call_set_available_spells(spells_to_enable);

}

void GameController::set_marked(std::list<Field *> &collection, bool value, bool &indicator)
{
    for(auto field : collection)
        field->set_marked(value);
    indicator = value;

}


void GameController::change_remaining_steps(int cost)
{
    m_remaining_steps += cost;
    m_game_info->set_remaining_steps(m_remaining_steps);
    m_card_info->set_remaining_steps(m_remaining_steps);
    check_turn_change();
}

void GameController::pop_up_card_found(std::string card_name) const
{
    //Create message box that shows notification
    QMessageBox card_msg;
    auto card_path = Card::CARD_NAMES_TO_PATH.at(card_name); //std::string
    card_msg.setIconPixmap(QPixmap(QString(card_path.c_str())));
    card_msg.setWindowTitle("You have collected a card!");
    card_msg.exec();
}

void GameController::check_field_for_card(Field *field)
{
    if (field->has_card()) {
        pop_up_card_found(field->get_card());
        auto player = get_player_on_turn_ptr();
        player->add_card(field->get_card());
        m_card_info->set_player_cards(player->get_cards(), get_enemy()->get_num_of_cards());
        field->set_card("");
    }
}

void GameController::check_fields_with_cards_cache()
{
    auto it = m_fields_with_cards_cache.begin();
    while (it != m_fields_with_cards_cache.end()) {
        auto field = *it;
        check_field_for_card(field);
        it = m_fields_with_cards_cache.erase(it);
    }
}

void GameController::resizeEvent(QResizeEvent *event)
{
    fitInView(m_scene->sceneRect(),Qt::KeepAspectRatio);

    QGraphicsView::resizeEvent(event);
}
