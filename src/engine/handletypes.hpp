#ifndef HANDLETYPES_HPP
#define HANDLETYPES_HPP

#include "../pieces/piece.hpp"
#include "../pieces/archer.hpp"
#include "../pieces/catapult.hpp"
#include "../pieces/horseman.hpp"
#include "../pieces/knight.hpp"
#include "../pieces/mage.hpp"
#include "../pieces/priest.hpp"
#include "../pieces/shieldman.hpp"
#include "../pieces/spearman.hpp"

#include <variant>

typedef std::variant<
    Archer::Spell,
    Catapult::Spell,
    Horseman::Spell,
    Knight::Spell,
    Mage::Spell,
    Priest::Spell,
    Shieldman::Spell,
    Spearman::Spell
> SpellVar;

typedef std::variant<Piece::DataIndicator, SpellVar, Piece::SpellEffect> UpdateType;

#endif // HANDLETyPES_HPP
