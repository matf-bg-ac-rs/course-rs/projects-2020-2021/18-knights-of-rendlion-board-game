#ifndef SPELLHANDLER_HPP
#define SPELLHANDLER_HPP

#include "../gui/board.hpp"
#include "../utility/utility.hpp"
#include "handletypes.hpp"

#include <list>
#include <string>
#include <algorithm>
#include <tuple>

class Updater;

class SpellHandler
{
public:
    virtual ~SpellHandler() {}

    static std::string archer_spell_to_str(Archer::Spell spell);
    static std::string catapult_spell_to_str(Catapult::Spell spell);
    static std::string horseman_spell_to_str(Horseman::Spell spell);
    static std::string knight_spell_to_str(Knight::Spell spell);
    static std::string mage_spell_to_str(Mage::Spell spell);
    static std::string priest_spell_to_str(Priest::Spell spell);
    static std::string shieldman_spell_to_str(Shieldman::Spell spell);
    static std::string spearman_spell_to_str(Spearman::Spell spell);
    static std::string spell_var_to_str(SpellVar spell_var);
    static std::string spell_effect_to_str(Piece::SpellEffect spell_effect);

    SpellHandler(Board* board, Archer::Spell archer_spell);
    SpellHandler(Board* board, Catapult::Spell catapult_spell);
    SpellHandler(Board* board, Horseman::Spell horseman_spell);
    SpellHandler(Board* board, Knight::Spell knight_spell);
    SpellHandler(Board* board, Mage::Spell mage_spell);
    SpellHandler(Board* board, Priest::Spell priest_spell);
    SpellHandler(Board* board, Shieldman::Spell shieldman_spell);
    SpellHandler(Board* board, Spearman::Spell spearman_spell);

    virtual void handle(Piece* caster, Field* selected_field=nullptr) = 0;
    virtual bool is_instant_cast() const = 0;
    virtual bool stuns_caster() const = 0;
    virtual bool immobilizes_caster() const = 0;
    virtual std::list<Field*> get_potential_fields(Piece* caster) const = 0;

    Field* new_field_after_move() const;
    bool has_new_field_after_move() const;
    std::vector<Piece*>& killed_pieces();
    bool has_killed_pieces() const;
    SpellVar spell() const;
    std::string spell_str() const;
    unsigned spell_cost() const;
    unsigned spell_cooldown() const;
    std::vector<Updater*> get_updaters() const;

protected:
    Board* m_board;
    Field* m_new_field_after_move = nullptr;
    std::vector<Piece*> m_killed_pieces;
    SpellVar m_spell;
    std::string m_spell_str;
    unsigned m_spell_cost = 0;
    unsigned m_spell_cooldown = 0;
    std::vector<std::tuple<Piece*, UpdateType, int>> m_updater_parameters;
};

// Classes that implement spell-handling
class SteadyAim : public SpellHandler
{
public:
    SteadyAim(Board* board)
        : SpellHandler(board, Archer::Spell::STEADY_AIM)
    {
        m_spell_cost = static_cast<unsigned>(Archer::SpellCost::STEADY_AIM);
        m_spell_cooldown = static_cast<unsigned>(Archer::SpellCooldown::STEADY_AIM);
    }

    void handle(Piece* caster, Field* selected_field) override;
    bool is_instant_cast() const override;
    bool stuns_caster() const override;
    bool immobilizes_caster() const override;
    std::list<Field*> get_potential_fields(Piece* caster) const override;
};

class Barrage : public SpellHandler
{
public:
    Barrage(Board* board)
        : SpellHandler(board, Catapult::Spell::BARRAGE)
    {
        m_spell_cost = static_cast<unsigned>(Catapult::SpellCost::BARRAGE);
        m_spell_cooldown = static_cast<unsigned>(Catapult::SpellCooldown::BARRAGE);
    }

    void handle(Piece* caster, Field* selected_field) override;
    bool is_instant_cast() const override;
    bool stuns_caster() const override;
    bool immobilizes_caster() const override;
    std::list<Field*> get_potential_fields(Piece* caster) const override;
};

class Gallop : public SpellHandler
{
public:
    Gallop(Board* board)
        : SpellHandler(board, Horseman::Spell::GALLOP)
    {
        m_spell_cost = static_cast<unsigned>(Horseman::SpellCost::GALLOP);
        m_spell_cooldown = static_cast<unsigned>(Horseman::SpellCooldown::GALLOP);
    }

    void handle(Piece* caster, Field* selected_field) override;
    bool is_instant_cast() const override;
    bool stuns_caster() const override;
    bool immobilizes_caster() const override;
    std::list<Field*> get_potential_fields(Piece* caster) const override;
};

class MortalStrike : public SpellHandler
{
public:
    MortalStrike(Board* board)
        : SpellHandler(board, Knight::Spell::MORTAL_STRIKE)
    {
        m_spell_cost = static_cast<unsigned>(Knight::SpellCost::MORTAL_STRIKE);
        m_spell_cooldown = static_cast<unsigned>(Knight::SpellCooldown::MORTAL_STRIKE);
    }

    void handle(Piece* caster, Field* selected_field) override;
    bool is_instant_cast() const override;
    bool stuns_caster() const override;
    bool immobilizes_caster() const override;
    std::list<Field*> get_potential_fields(Piece* caster) const override;
};

class Firebolt : public SpellHandler
{
public:
    Firebolt(Board* board)
        : SpellHandler(board, Mage::Spell::FIREBOLT)
    {
        m_spell_cost = static_cast<unsigned>(Mage::SpellCost::FIREBOLT);
        m_spell_cooldown = static_cast<unsigned>(Mage::SpellCooldown::FIREBOLT);
    }

    void handle(Piece* caster, Field* selected_field) override;
    bool is_instant_cast() const override;
    bool stuns_caster() const override;
    bool immobilizes_caster() const override;
    std::list<Field*> get_potential_fields(Piece* caster) const override;
};

class Frostbolt : public SpellHandler
{
public:
    Frostbolt(Board* board)
        : SpellHandler(board, Mage::Spell::FROSTBOLT)
    {
        m_spell_cost = static_cast<unsigned>(Mage::SpellCost::FROSTBOLT);
        m_spell_cooldown = static_cast<unsigned>(Mage::SpellCooldown::FROSTBOLT);
    }

    void handle(Piece* caster, Field* selected_field) override;
    bool is_instant_cast() const override;
    bool stuns_caster() const override;
    bool immobilizes_caster() const override;
    std::list<Field*> get_potential_fields(Piece* caster) const override;
};

class Arcanebolt : public SpellHandler
{
public:
    Arcanebolt(Board* board)
        : SpellHandler(board, Mage::Spell::ARCANEBOLT)
    {
        m_spell_cost = static_cast<unsigned>(Mage::SpellCost::ARCANEBOLT);
        m_spell_cooldown = static_cast<unsigned>(Mage::SpellCooldown::ARCANEBOLT);
    }

    void handle(Piece* caster, Field* selected_field) override;
    bool is_instant_cast() const override;
    bool stuns_caster() const override;
    bool immobilizes_caster() const override;
    std::list<Field*> get_potential_fields(Piece* caster) const override;
};

class Shockwave : public SpellHandler
{
public:
    Shockwave(Board* board)
        : SpellHandler(board, Mage::Spell::SHOCKWAVE)
    {
        m_spell_cost = static_cast<unsigned>(Mage::SpellCost::SHOCKWAVE);
        m_spell_cooldown = static_cast<unsigned>(Mage::SpellCooldown::SHOCKWAVE);
    }

    void handle(Piece* caster, Field* selected_field) override;
    bool is_instant_cast() const override;
    bool stuns_caster() const override;
    bool immobilizes_caster() const override;
    std::list<Field*> get_potential_fields(Piece* caster) const override;
};

class Heal : public SpellHandler
{
public:
    Heal(Board* board)
        : SpellHandler(board, Priest::Spell::HEAL)
    {
        m_spell_cost = static_cast<unsigned>(Priest::SpellCost::HEAL);
        m_spell_cooldown = static_cast<unsigned>(Priest::SpellCooldown::HEAL);
    }

    void handle(Piece* caster, Field* selected_field) override;
    bool is_instant_cast() const override;
    bool stuns_caster() const override;
    bool immobilizes_caster() const override;
    std::list<Field*> get_potential_fields(Piece* caster) const override;
};

class Empower : public SpellHandler
{
public:
    Empower(Board* board)
        : SpellHandler(board, Priest::Spell::EMPOWER)
    {
        m_spell_cost = static_cast<unsigned>(Priest::SpellCost::EMPOWER);
        m_spell_cooldown = static_cast<unsigned>(Priest::SpellCooldown::EMPOWER);
    }

    void handle(Piece* caster, Field* selected_field) override;
    bool is_instant_cast() const override;
    bool stuns_caster() const override;
    bool immobilizes_caster() const override;
    std::list<Field*> get_potential_fields(Piece* caster) const override;
};

class HolyShield : public SpellHandler
{
public:
    HolyShield(Board* board)
        : SpellHandler(board, Priest::Spell::HOLY_SHIELD)
    {
        m_spell_cost = static_cast<unsigned>(Priest::SpellCost::HOLY_SHIELD);
        m_spell_cooldown = static_cast<unsigned>(Priest::SpellCooldown::HOLY_SHIELD);
    }

    void handle(Piece* caster, Field* selected_field) override;
    bool is_instant_cast() const override;
    bool stuns_caster() const override;
    bool immobilizes_caster() const override;
    std::list<Field*> get_potential_fields(Piece* caster) const override;
};

class Stun : public SpellHandler
{
public:
    Stun(Board* board)
        : SpellHandler(board, Priest::Spell::STUN)
    {
        m_spell_cost = static_cast<unsigned>(Priest::SpellCost::STUN);
        m_spell_cooldown = static_cast<unsigned>(Priest::SpellCooldown::STUN);
    }

    void handle(Piece* caster, Field* selected_field) override;
    bool is_instant_cast() const override;
    bool stuns_caster() const override;
    bool immobilizes_caster() const override;
    std::list<Field*> get_potential_fields(Piece* caster) const override;
};

class Turtle : public SpellHandler
{
public:
    Turtle(Board* board)
        : SpellHandler(board, Shieldman::Spell::TURTLE)
    {
        m_spell_cost = static_cast<unsigned>(Shieldman::SpellCost::TURTLE);
        m_spell_cooldown = static_cast<unsigned>(Shieldman::SpellCooldown::TURTLE);
    }

    void handle(Piece* caster, Field* selected_field) override;
    bool is_instant_cast() const override;
    bool stuns_caster() const override;
    bool immobilizes_caster() const override;
    std::list<Field*> get_potential_fields(Piece* caster) const override;
};

class Spearthrow : public SpellHandler
{
public:
    Spearthrow(Board* board)
        : SpellHandler(board, Spearman::Spell::SPEARTHROW)
    {
        m_spell_cost = static_cast<unsigned>(Spearman::SpellCost::SPEARTHROW);
    }

    void handle(Piece* caster, Field* selected_field) override;
    bool is_instant_cast() const override;
    bool stuns_caster() const override;
    bool immobilizes_caster() const override;
    std::list<Field*> get_potential_fields(Piece* caster) const override;
};

#endif // SPELLHANDLER_HPP
